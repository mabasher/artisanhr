﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmForms.aspx.cs" Inherits="Saas.Hr.WebForm.Security.FrmForms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>Entry Forms Setup</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-lg-12">
            <div class="col-md-6">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hidFormInfoId" Value="0" runat="server" />
                <div class="form-horizontal">
                    
                    <div class="form-group">
                        <asp:Label ID="lblModule" runat="server" Text="Module:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddModule" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblFormName" runat="server" Text="Form Name:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtFormName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblFormPath" runat="server" Text="Form Path:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtFormPath" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblFormDisplayName" runat="server" Text="Display Name:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtFormDisplayName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPriority" runat="server" Text="Priority:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtPriority" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblIcon" runat="server" Text="Icon:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtIcon" runat="server" CssClass="form-control input-sm" Text="arrow"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblFormDescription" runat="server" Text="Description:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtFormDescription" runat="server" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-offset-4 col-sm-10">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnSave_Click" Width="80px" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="80px" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvForm" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvForm_RowDataBound" OnSelectedIndexChanged="gdvForm_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Form Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridCategoryName" runat="server" Text='<%# Bind("FormName") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridFormId" Value='<%# Bind("ScrFormInfoId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Path">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridFormPath" runat="server" Text='<%# Bind("FormPath") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Display Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridDisplay" runat="server" Text='<%# Bind("FormDisplayName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Priority">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Module">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridModule" runat="server" Text='<%# Bind("ScrModuleInfo.ModuleName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>