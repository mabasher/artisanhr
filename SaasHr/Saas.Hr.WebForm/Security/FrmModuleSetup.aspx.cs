﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData.Menu;
using Saas.Hr.WebForm.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Security
{
    public partial class FrmModuleSetup : Page
    {
        private MenuEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindModuleFormForRole();
                //BindModule();
                BindRole();
                //BindForm();
                BindFormInGrid();
            }
        }

        private void BindModuleFormForRole(int roleId = 0)
        {
            using (_context = new MenuEntities())
            {
                var r = _context
                    .ScrModuleFormForRoles
                    .Include(m => m.ScrRoleInfo)
                    .Include(m => m.ScrModuleInfo)
                    .Include(m => m.ScrFormInfo)
                    .AsQueryable();
                if (roleId != 0)
                {
                    r = r.Where(rl => rl.ScrRoleInfoId == roleId);
                }
                gdvModuleFormForRole.DataSource = r.OrderBy(m => m.ScrModuleInfoId).ToList();
                gdvModuleFormForRole.DataBind();
            }
        }

        private void BindRole()
        {
            using (_context = new MenuEntities())
            {
                ddRole.DataSource = _context.ScrRoleInfoes.ToList();
                ddRole.DataTextField = "RoleName";
                ddRole.DataValueField = "ScrRoleInfoId";
                ddRole.DataBind();
                ddRole.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
        }

        protected void ddRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new MenuEntities();
            var rId = int.Parse(ddRole.SelectedValue);
            var module = _context.ScrModuleFormForRoles.Where(r => r.ScrRoleInfoId == rId);
            foreach (GridViewRow rw in gdvFormList.Rows)
            {
                var hdnFrmId = (HiddenField)rw.FindControl("hidAddGridFormId");
                var frmId = int.Parse(hdnFrmId.Value);
                var frmChkId = (CheckBox)rw.FindControl("chkAddGridForm");
                frmChkId.Checked = module.Any(m => m.ScrFormInfoId.Value == frmId);
            }
        }

        private void BindModule()
        {
            using (_context = new MenuEntities())
            {
                ddModule.DataSource = _context.ScrModuleInfoes.ToList();
                ddModule.DataTextField = "ModuleName";
                ddModule.DataValueField = "ScrModuleInfoId";
                ddModule.DataBind();
                ddModule.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
        }

        //        private void BindForm()
        //        {
        //            using (_context = new MenuEntities())
        //            {
        //                ddForm.DataSource = _context.ScrFormInfoes.ToList();
        //                ddForm.DataTextField = "FormName";
        //                ddForm.DataValueField = "ScrFormInfoId";
        //                ddForm.DataBind();
        //                ddForm.Items.Insert(0, new ListItem("--- Select ---", "0"));
        //            }
        //        }

        private void BindFormInGrid()
        {
            using (_context = new MenuEntities())
            {
                gdvFormList.DataSource = _context.ScrFormInfoes.OrderBy(r => r.ModuleId).ThenBy(r => r.Priority).ToList(); //.Where(r => roleId == 0 || r.ScrModuleFormForRoles.Contains(rl => rl.ScrRoleInfoId == roleId)).ToList();
                gdvFormList.DataBind();
            }
        }

        private void ClearAll()
        {
            hidFormModuleForRoleId.Value = "0";
            ddRole.SelectedIndex = 0;
            //ddModule.SelectedIndex = 0;
            //ddForm.SelectedIndex = 0;
            btnSave.Text = "Save";
            lblMsg.Text = string.Empty;
            BindModuleFormForRole();
        }

        private bool IsModuleValid()
        {
            if (ddRole.SelectedValue == "0")
            {
                MessageBox.ShowMessageInLabel("No Role Selected!!!", lblMsg, MessageType.ErrorMessage);
                ddRole.Focus();
                return false;
            }

            //if (ddModule.SelectedIndex >= 1)
            //MessageBox.ShowMessageInLabel("Module should be selected!!!", lblMsg, MessageType.ErrorMessage);
            //ddModule.Focus();
            //return false;

            return true;
        }

        protected void gdvModule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvModuleFormForRole, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new MenuEntities();
            var id = int.Parse(((HiddenField)gdvModuleFormForRole.Rows[gdvModuleFormForRole.SelectedIndex].FindControl("hidFormModuleForRoleId")).Value);
            var module = _context.ScrModuleFormForRoles.First(r => r.ScrModuleFormForRoleId == id);
            hidFormModuleForRoleId.Value = module.ScrModuleFormForRoleId.ToString();

            ddRole.SelectedValue = module.ScrRoleInfoId.ToString();
            ddModule.SelectedValue = module.ScrModuleInfoId.ToString();

            var formIds = new List<int>();
            foreach (GridViewRow rw in gdvFormList.Rows)
            {
                var hdnFrmId = (HiddenField)rw.FindControl("hidAddGridFormId");
                var frmId = module.ScrFormInfoId.ToString();
                var frmChkId = (CheckBox)rw.FindControl("chkAddGridForm");
                if (hdnFrmId.Value == frmId)
                {
                    frmChkId.Checked = true;
                }
            }

            btnSave.Text = "Update";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsModuleValid()) return;
            lblMsg.Text = string.Empty;
            _context = new MenuEntities();

            if (hidFormModuleForRoleId.Value.Trim() == "0")
            {
                var formIds = new List<FormModule>();
                foreach (GridViewRow rw in gdvFormList.Rows)
                {
                    var frmChkId = (CheckBox)rw.FindControl("chkAddGridForm");
                    if (frmChkId.Checked)
                    {
                        var hdnFrmId = (HiddenField)rw.FindControl("hidAddGridFormId");
                        var hdnModuleId = (HiddenField)rw.FindControl("hidAddGridModuleId");
                        var frm = new FormModule
                        {
                            FormId = int.Parse(hdnFrmId.Value),
                            ModuleId = int.Parse(hdnModuleId.Value)
                        };
                        formIds.Add(frm);
                    }
                }

                var roleId = int.Parse(ddRole.SelectedValue.Trim());
                var formModules = _context.ScrModuleFormForRoles.Where(mf => mf.ScrRoleInfoId == roleId);
                if (formModules.Any())
                {
                    _context.ScrModuleFormForRoles.RemoveRange(formModules);
                }
                foreach (var fm in formIds)
                {
                    var module = new ScrModuleFormForRole
                    {
                        ScrRoleInfoId = roleId,
                        ScrModuleInfoId = fm.ModuleId,
                        ScrFormInfoId = fm.FormId,
                        CreatedBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                        CreateDate = DateTime.Now
                    };
                    _context.ScrModuleFormForRoles.Add(module);
                }

                _context.SaveChanges();
                ClearAll();
                MessageBox.ShowMessageInLabel("Successfully Done!!!", lblMsg, MessageType.SuccessMessage);
            }
            else
            {
                var id = int.Parse(hidFormModuleForRoleId.Value);
                var module = _context.ScrModuleFormForRoles.FirstOrDefault(r => r.ScrModuleInfoId == id);
                if (module == null)
                {
                    MessageBox.ShowMessageInLabel("Module and form for role not found to update!!!", lblMsg, MessageType.ErrorMessage);
                }
                else
                {
                    module.ScrRoleInfoId = int.Parse(ddRole.SelectedValue.Trim());
                    module.ScrModuleInfoId = int.Parse(ddModule.SelectedValue.Trim());
                    module.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                    module.UpdateDate = DateTime.Now;
                    _context.SaveChanges();
                    ClearAll();
                    MessageBox.ShowMessageInLabel("Updated successfully!!!", lblMsg, MessageType.SuccessMessage);
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            _context = new MenuEntities();
            var id = int.Parse(hidFormModuleForRoleId.Value);
            var module = _context.ScrModuleFormForRoles.FirstOrDefault(r => r.ScrModuleFormForRoleId == id);
            if (module == null)
            {
                MessageBox.ShowMessageInLabel("Module and form for role not found to Delete!!!", lblMsg, MessageType.ErrorMessage);
            }
            else
            {
                _context.ScrModuleFormForRoles.Remove(module);
                _context.SaveChanges();
                ClearAll();
                MessageBox.ShowMessageInLabel("Module and form for role Deleted!!!", lblMsg, MessageType.SuccessMessage);
            }

            //ClearAll();
        }
    }
}