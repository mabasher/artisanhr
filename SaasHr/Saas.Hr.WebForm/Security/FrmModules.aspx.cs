﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData.Menu;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Security
{
    public partial class FrmModules : Page
    {
        private MenuEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindModule();
                BindParentModule();
            }
        }

        private void BindModule()
        {
            using (_context = new MenuEntities())
            {
                gdvModule.DataSource = _context.ScrModuleInfoes.Include(m => m.ScrModuleInfo2).OrderBy(r=>r.ScrModuleInfo2.Priority+""+r.Priority).ThenBy(r => r.Priority).ToList();
                gdvModule.DataBind();
            }
        }

        private void BindParentModule()
        {
            using (_context = new MenuEntities())
            {
                ddParentModule.DataSource = _context.ScrModuleInfoes.ToList();
                ddParentModule.DataTextField = "ModuleName";
                ddParentModule.DataValueField = "ScrModuleInfoId";
                ddParentModule.DataBind();
                ddParentModule.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
        }

        private void ClearAll()
        {
            hidModuleInfoId.Value = "0";
            txtModuleName.Text = string.Empty;
            txtModuleDescription.Text = string.Empty;
            txtIcon.Text = string.Empty;
            txtPriority.Text = string.Empty;
            btnSave.Text = "Save";
            lblMsg.Text = string.Empty;
            BindParentModule();
            BindModule();
        }

        private bool IsModuleValid()
        {
            if (txtModuleName.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Module name should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtModuleName.Focus();
                return false;
            }

            if (txtPriority.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Priority should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtPriority.Focus();
                return false;
            }

            if (!int.TryParse(txtPriority.Text.Trim(), out _))
            {
                MessageBox.ShowMessageInLabel("Priority must be numeric!!!", lblMsg, MessageType.ErrorMessage);
                txtPriority.Focus();
                return false;
            }

            if (txtIcon.Text.Trim() != string.Empty) return true;
            MessageBox.ShowMessageInLabel("Icon should not be blank!!!", lblMsg, MessageType.ErrorMessage);
            txtIcon.Focus();
            return false;
        }

        protected void gdvModule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvModule, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new MenuEntities();
            var id = int.Parse(((HiddenField)gdvModule.Rows[gdvModule.SelectedIndex].FindControl("hidGridModuleId")).Value);
            var module = _context.ScrModuleInfoes.First(r => r.ScrModuleInfoId == id);
            hidModuleInfoId.Value = module.ScrModuleInfoId.ToString();
            txtModuleName.Text = module.ModuleName;
            txtIcon.Text = module.ModuleIcon;
            txtPriority.Text = module.Priority.ToString();
            if (module.ParentModuleInfoId != null)
            {
                ddParentModule.SelectedValue = module.ParentModuleInfoId.ToString();
            }
            txtModuleName.Text = module.ModuleName;
            txtModuleDescription.Text = module.ModuleDescription;
            btnSave.Text = "Update";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsModuleValid()) return;
            lblMsg.Text = string.Empty;
            _context = new MenuEntities();
            if (hidModuleInfoId.Value.Trim() == "0")
            {
                var module = new ScrModuleInfo
                {
                    ModuleName = txtModuleName.Text.Trim(),
                    Priority = int.Parse(txtPriority.Text.Trim()),
                    ModuleIcon = txtIcon.Text.Trim(),
                    ModuleDescription = txtModuleDescription.Text.Trim(),
                    CreatedBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                    CreateDate = DateTime.Now
                };
                if (ddParentModule.SelectedIndex > 0)
                {
                    module.ParentModuleInfoId = int.Parse(ddParentModule.SelectedValue);
                }
                _context.ScrModuleInfoes.Add(module);
                _context.SaveChanges();
                ClearAll();
                MessageBox.ShowMessageInLabel("Module added successfully!!!", lblMsg, MessageType.SuccessMessage);
            }
            else
            {
                var id = int.Parse(hidModuleInfoId.Value);
                var module = _context.ScrModuleInfoes.FirstOrDefault(r => r.ScrModuleInfoId == id);
                if (module == null)
                {
                    MessageBox.ShowMessageInLabel("Module not found to update!!!", lblMsg, MessageType.ErrorMessage);
                }
                else
                {
                    module.ModuleName = txtModuleName.Text;
                    module.Priority = int.Parse(txtPriority.Text.Trim());
                    module.ModuleIcon = txtIcon.Text.Trim();
                    if (ddParentModule.SelectedIndex > 0)
                    {
                        module.ParentModuleInfoId = int.Parse(ddParentModule.SelectedValue);
                    }
                    else
                    {
                        module.ParentModuleInfoId = null;
                    }
                    module.ModuleDescription = txtModuleDescription.Text;
                    module.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                    module.UpdateDate = DateTime.Now;
                    _context.SaveChanges();
                    ClearAll();
                    MessageBox.ShowMessageInLabel("Module updated successfully!!!", lblMsg, MessageType.SuccessMessage);
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
    }
}