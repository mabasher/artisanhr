﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Security
{
    public partial class Frm_Manageuser : Page
    {
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //commonfunctions.g_b_FillDropDownList(dd_user,
                //    "T_SignIn",
                //    "UserName",
                //    "autoId", string.Empty);

                v_loadGridView_Company();
            }
        }

        private void v_loadGridView_Company()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string SessionCompanyId = string.Empty;
            string qry = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            qry = "proc_loadEmployeeforManageUser '" + txtSearch.Value + "'";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvCompanyList.DataSource = ds;
            gdvCompanyList.DataBind();
            sqlconnection.Close();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_Company();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            string s_select = string.Empty;
            s_select = "";
            string s_Select_returnValue = string.Empty;
            string s_trautoId = string.Empty;
            var employees = new List<EmpSigninModel>();
            var mailSendingOptions = new List<MailSendingOption>();
            foreach (GridViewRow gdvRow in gdvCompanyList.Rows)
            {
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked)
                {
                    s_trautoId = ((HiddenField)gdvCompanyList.Rows[gdvRow.RowIndex].FindControl("hid_GridItemAutoId")).Value;
                    var emp = new EmpSigninModel
                    {
                        EmployeeId = int.Parse(s_trautoId),
                        CompanyId = int.Parse(SessionCompanyId),
                        EmpName = ((Label)gdvCompanyList.Rows[gdvRow.RowIndex].FindControl("lblGridEmpName")).Text,
                        EmpEmail = ((Label)gdvCompanyList.Rows[gdvRow.RowIndex].FindControl("lblGridEmail")).Text,
                        Password = Guid.NewGuid()
                    };

                    s_select = s_select + "execute proc_InsertEmployeeforManageUser '" + emp.CompanyId + "','" + emp.EmployeeId + "','" + emp.Password + "'";
                    employees.Add(emp);
                    if (!string.IsNullOrEmpty(emp.EmpEmail))
                    {
                        mailSendingOptions.Add(GetMailSendingOption(emp.EmpName, emp.EmpEmail, emp.Password.ToString()));
                    }
                }
            }
            s_save_ = s_save_ + " " + s_select + " Select ''";

            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    lblMsg.Text = GlobalVariables.g_s_insertSendSuccessfull;
                    v_loadGridView_Company();
                    InsertMode();
                    MailSender.SendMultipleMail(mailSendingOptions);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            string s_op = "N";

            foreach (GridViewRow gdvRow in gdvCompanyList.Rows)
            {
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true)
                {
                    s_op = "Y";
                }
            }

            if (s_op == "N")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "No Employee Selected!!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Send User Information";
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //CommonFunctions commonFunctions = new CommonFunctions();
            //SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            //sqlconnection.Open();

            //SqlCommand cmd = new SqlCommand("select sc.AutoId as AutoId, CategoryName as CategoryName,SubCategory as SubCategoryName, sc.code as Code, sc.Company_Id as Company_Id,c.AutoId as CategoryId from T_Inv_SubCategory sc left outer join T_InvCategory c ON sc.CategoryId=c.AutoId ", sqlconnection);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //da.Fill(ds);
            //gdv_costingHead.PageIndex = e.NewPageIndex;
            //gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            //gdv_costingHead.PageSize = 8;
            //gdv_costingHead.DataBind();
            //sqlconnection.Close();
        }

        protected void dd_user_SelectedIndexChanged(object sender, EventArgs e)
        {
            ////For Color
            Connection connection = new Connection();
            string s_returnValue = string.Empty;
            string s_Select = string.Empty;
            string s_SelectedId = string.Empty;

            s_Select = "SELECT isnull(Company_Id,0) as Company_Id FROM T_UserWiseCompany Where UserId='2';";
            s_returnValue = connection.connection_DB(s_Select, 0, false, false, false);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        foreach (GridViewRow gdvRow in gdvCompanyList.Rows)
                        {
                            CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                            s_SelectedId = ((HiddenField)gdvCompanyList.Rows[gdvRow.RowIndex].FindControl("hid_GridItemAutoId")).Value;
                            if (s_SelectedId == drow["Company_Id"].ToString())
                            {
                                checkbox.Checked = true;
                            }
                        }
                    }
                }
            }
            ///=======================
        }

        private MailSendingOption GetMailSendingOption(string employeeName, string employeeEmail,
            string employeePassword)
        {
            List<string> receivers = new List<string>
            {
                employeeEmail
            };
            return new MailSendingOption(receivers, null, "noreply", "HR Software User access information", GetEmailMessage(employeeName, employeeEmail, employeePassword), null);
        }

        private string GetEmailMessage(string employeeName, string employeeEmail,
            string employeePassword)
        {
            var baseUrl = string.Empty;
            if (Request.ApplicationPath != null)
            {
                baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                          Request.ApplicationPath.TrimEnd('/') + "/";
            }
            return $@"
                <p>Dear <strong>{employeeName}</strong>,</p>
                <p>Your user information in <strong>SAAS ERP System</strong> has been created/modified. Now you can access to the software through below link:</P>
                <p><a href=""{baseUrl}"" target=""_blank"">Click here</a> to open software.</p>
                <p><strong>Your user id:</strong>{employeeEmail}</P>
                <p><strong>Your password:</strong>{employeePassword}</P>
                <br />
                <br />
                <p style=""Color: red;"">*** NB: This is confidential information. Don't share with others.</p>
                <br />
                <p style=""Color: green;""><strong>SAAS ERP SYSTEM</strong> Design, developed and maintained by <a href=""http://stmsoftwareltd.com/"" target=""_blank"">STM Software Limited</a>.</p>

                ";
        }



    }
}