﻿using Saas.Hr.WebForm.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Security
{
    public partial class Frm_UserRegistration : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(dd_userlevel,
                    "ScrRoleInfo",
                    "RoleName",
                    "ScrRoleInfoId", string.Empty);

                //"T_UserLevel",
                //    "UserLevel",
                //    "autoId", string.Empty);

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select AutoId as AutoId, UserName as UserName,logInId as logInId,Contact as Contact,Email as Email,UserAddress as UserAddress,isnull(userlevel,0) as userlevel,pwd as pwd,ISNULL(URL,'') as URL  from T_SignIn ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            //            gdv_costingHead.AllowPaging = true;
            //            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = txtlogInId.Text + "_" + filename;
                URL = "~/UserPic/" + filename;

                s_Update = "[proc_frm_UserCreation_UPDATE_T_UserCreation]"
                        + "'" + hidAutoIdForUpdate.Value + "','0','"
                        + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAddress.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtemail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtlogInId.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassword.Text.Trim())
                        + "','Y','"
                        + dd_userlevel.SelectedValue
                        + "','"
                        + filename
                        + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = txtlogInId.Text + "_" + filename;
                URL = "~/UserPic/" + filename;

                s_save_ = "[proc_frm_UserCreation_INSERT_T_UserCreation]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAddress.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtemail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtlogInId.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassword.Text.Trim())
                        + "','Y','"
                        + dd_userlevel.SelectedValue
                        + "','"
                        + filename
                        + "'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txt_GroupName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                txtAddress.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                txtemail.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridEmail")).Value;
                txtCode.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridContact")).Value;
                txtPassword.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidPwd")).Value;
                txtConfirmPassword.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidPwd")).Value;
                hidURL.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridURL")).Value;
                txtlogInId.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCode")).Text;
                dd_userlevel.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (dd_userlevel.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "User Level Can Not Blank!";
                return false;
            }

            if (txtCode.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "LogIn Id Can Not Blank!";
                return false;
            }

            if (txtPassword.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Password Can Not Blank!";
                return false;
            }

            if (txtConfirmPassword.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Confirm Password Can Not Blank!";
                return false;
            }
            if (txtConfirmPassword.Text != txtPassword.Text)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Password Not Match!";
                return false;
            }

            if (txt_GroupName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "User Name Can Not Blank!";
                return false;
            }

            if (imgUpload.HasFile)
            {
                int imgSize = imgUpload.PostedFile.ContentLength;
                string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
                if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
                {
                    lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
                    lblMsg.Visible = true;
                    return false;
                }
                else if (imgSize > 800000)//1048576)//1048576 bit=1MB
                {
                    lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
                    lblMsg.Visible = true;
                    return false;
                }
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "User Photo Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txt_GroupName.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtemail.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtlogInId.Text = string.Empty;
            txtAddress.Text = string.Empty;
            dd_userlevel.SelectedValue = "0";
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //CommonFunctions commonFunctions = new CommonFunctions();
            //SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            //sqlconnection.Open();

            //SqlCommand cmd = new SqlCommand("select sc.AutoId as AutoId, CategoryName as CategoryName,SubCategory as SubCategoryName, sc.code as Code, sc.Company_Id as Company_Id,c.AutoId as CategoryId from T_Inv_SubCategory sc left outer join T_InvCategory c ON sc.CategoryId=c.AutoId ", sqlconnection);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //da.Fill(ds);
            //gdv_costingHead.PageIndex = e.NewPageIndex;
            //gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            //gdv_costingHead.PageSize = 8;
            //gdv_costingHead.DataBind();
            //sqlconnection.Close();
        }
    }
}