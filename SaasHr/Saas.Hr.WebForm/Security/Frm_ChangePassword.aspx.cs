﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Security
{
    public partial class Frm_ChangePassword : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            string SessionUserMode = Convert.ToString(Session[GlobalVariables.g_s_userMode]);

            if (SessionUserMode == "Gn")
            {
                s_select = "SELECT * FROM T_SignInGn Where empId='" + SessionUserId + "'";
            }
            if (SessionUserMode == "A")
            {
                s_select = "SELECT * FROM T_SignIn Where AutoId='" + SessionUserId + "'";
            }

            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtloginId.Text = drow["logInId"].ToString();
                        txtcurrentpassword.Text = "";
                        txtnewPassword.Text = "";
                        txtConfirmPassword.Text = "";
                    }
                }
            }

        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            string SessionUserMode = Convert.ToString(Session[GlobalVariables.g_s_userMode]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            s_Update = "[proc_UpdateEmployeeforManageUser]"
                + "'"
                + HttpUtility.HtmlDecode(txtloginId.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtcurrentpassword.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtnewPassword.Text.Trim())
                + "','"
                + SessionUserMode
                + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = "Done!";
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            


        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            if (txtloginId.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Log In id Can Not Blank!";
                return false;
            }
            if (txtcurrentpassword.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Current password Can Not Blank!";
                return false;
            }

            if (txtnewPassword.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "New Password Can Not Blank!";
                return false;
            }
            if (txtConfirmPassword.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Confirm Password Can Not Blank!";
                return false;
            }

            if (txtnewPassword.Text != txtConfirmPassword.Text)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Password not matched!";
                return false;
            }


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtloginId.Text = string.Empty;
            txtcurrentpassword.Text = string.Empty;
            txtnewPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
        }




    }
}