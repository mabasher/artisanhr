﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmModuleSetup.aspx.cs" Inherits="Saas.Hr.WebForm.Security.FrmModuleSetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>User Role Access Permission</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-lg-12">
            <div class="col-md-4">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hidFormModuleForRoleId" Value="0" runat="server" />
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lblRole" runat="server" Text="Role Name:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddRole" AutoPostBack="true" OnSelectedIndexChanged="ddRole_SelectedIndexChanged" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <asp:Label ID="lblModule" runat="server" Text="Module:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddModule" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group"  style="height: 400px; overflow: scroll;">
                        <asp:GridView ID="gdvFormList" runat="server" Style="width: 100%; margin-left: 0;"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                            CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="">
                                     <HeaderTemplate>
                                         <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                     </HeaderTemplate>
                                    <ItemTemplate>
                                        
                                        <asp:CheckBox ID="chkAddGridForm" runat="server" AutoPostBack="false" />
                                        <asp:HiddenField ID="hidAddGridFormId" Value='<%# Bind("ScrFormInfoId") %>' runat="server" />
                                        <asp:HiddenField ID="hidAddGridModuleId" Value='<%# Bind("ModuleId") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Form Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddGridFormName" runat="server" Text='<%# Eval("FormDisplayName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Module">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddGridModuleName" runat="server" Text='<%# Eval("ScrModuleInfo.ModuleName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-sa" />
                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                        </asp:GridView>
                    </div>

                    <div class="form-group center">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnSave_Click" Width="80px" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="80px" />
                            <asp:Button ID="btnDelete" runat="server" Visible="false" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                    </div>
                </div>
            </div>
            <div class="col-md-8 hidden">
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvModuleFormForRole" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvModule_RowDataBound" OnSelectedIndexChanged="gdvModule_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Role Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridRoleName" runat="server" Text='<%# Bind("ScrRoleInfo.RoleName") %>'></asp:Label>
                                    <asp:HiddenField ID="hidFormModuleForRoleId" Value='<%# Bind("ScrModuleFormForRoleId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Module Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridModuleName" runat="server" Text='<%# Bind("ScrModuleInfo.ModuleName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Form Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridFormName" runat="server" Text='<%# Eval("ScrFormInfo.FormDisplayName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
            
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
    
<script type="text/javascript">
function checkAll(objRef)
{
    var GridView = objRef.parentNode.parentNode.parentNode;
    var inputList = GridView.getElementsByTagName("input");
    for (var i=0;i<inputList.length;i++)
    {
        //Get the Cell To find out ColumnIndex
        var row = inputList[i].parentNode.parentNode;
        if(inputList[i].type == "checkbox"  && objRef != inputList[i])
        {
            if (objRef.checked)
            {
                //row.style.backgroundColor = "aqua";
                inputList[i].checked=true;
            }
            else
            {
                //if(row.rowIndex % 2 == 0)
                //{
                //   row.style.backgroundColor = "#C2D69B";
                //}
                //else
                //{
                //   row.style.backgroundColor = "white";
                //}
                inputList[i].checked=false;
            }
        }
    }
}
 </script>
</asp:Content>