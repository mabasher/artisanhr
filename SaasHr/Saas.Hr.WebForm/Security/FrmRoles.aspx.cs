﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData.Menu;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Security
{
    public partial class FrmRoles : Page
    {
        private MenuEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRole();
            }
        }

        private void BindRole()
        {
            using (_context = new MenuEntities())
            {
                gdvRole.DataSource = _context.ScrRoleInfoes.ToList();
                gdvRole.DataBind();
            }
        }

        private void ClearAll()
        {
            hidRoleInfoId.Value = "0";
            txtRoleName.Text = string.Empty;
            txtRoleDescription.Text = string.Empty;
            btnSave.Text = "Save";
            lblMsg.Text = string.Empty;
            BindRole();
        }

        private bool IsRoleValid()
        {
            if (txtRoleName.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Role name should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtRoleName.Focus();
                return false;
            }

            return true;
        }

        protected void gdvRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvRole, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new MenuEntities();
            var id = int.Parse(((HiddenField)gdvRole.Rows[gdvRole.SelectedIndex].FindControl("hidGridRoleId")).Value);
            var role = _context.ScrRoleInfoes.First(r => r.ScrRoleInfoId == id);
            hidRoleInfoId.Value = role.ScrRoleInfoId.ToString();
            txtRoleName.Text = role.RoleName;
            txtRoleDescription.Text = role.RoleDescription;
            btnSave.Text = "Update";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsRoleValid()) return;
            lblMsg.Text = string.Empty;
            _context = new MenuEntities();
            if (hidRoleInfoId.Value.Trim() == "0")
            {
                var role = new ScrRoleInfo
                {
                    RoleName = txtRoleName.Text.Trim(),
                    RoleDescription = txtRoleDescription.Text.Trim(),
                    CreatedBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                    CreateDate = DateTime.Now
                };
                _context.ScrRoleInfoes.Add(role);
                _context.SaveChanges();
                ClearAll();
                MessageBox.ShowMessageInLabel("Role added successfully!!!", lblMsg, MessageType.SuccessMessage);
            }
            else
            {
                var id = int.Parse(hidRoleInfoId.Value);
                var role = _context.ScrRoleInfoes.FirstOrDefault(r => r.ScrRoleInfoId == id);
                if (role == null)
                {
                    MessageBox.ShowMessageInLabel("Role not found to update!!!", lblMsg, MessageType.ErrorMessage);
                }
                else
                {
                    role.RoleName = txtRoleName.Text;
                    role.RoleDescription = txtRoleDescription.Text;
                    role.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                    role.UpdateDate = DateTime.Now;
                    _context.SaveChanges();
                    ClearAll();
                    MessageBox.ShowMessageInLabel("Role updated successfully!!!", lblMsg, MessageType.SuccessMessage);
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
    }
}