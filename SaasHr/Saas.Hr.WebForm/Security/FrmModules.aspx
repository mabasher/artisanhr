﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmModules.aspx.cs" Inherits="Saas.Hr.WebForm.Security.FrmModules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>Modules</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-lg-12">
            <div class="col-md-4">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hidModuleInfoId" Value="0" runat="server" />
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lblModuleName" runat="server" Text="Module Name:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtModuleName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPriority" runat="server" Text="Priority:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtPriority" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblIcon" runat="server" Text="Icon:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtIcon" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblParentModule" runat="server" Text="Parent Module:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddParentModule" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblModuleDescription" runat="server" Text="Description:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtModuleDescription" runat="server" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-offset-4 col-sm-10">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnSave_Click" Width="80px" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="80px" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvModule" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvModule_RowDataBound" OnSelectedIndexChanged="gdvModule_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Module Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridModuleName" runat="server" Text='<%# Bind("ModuleName") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridModuleId" Value='<%# Bind("ScrModuleInfoId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Priority">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parent Module">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridParentModuleName" runat="server" Text='<%# Eval("ScrModuleInfo2.ModuleName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>