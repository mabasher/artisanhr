﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Security
{
    public partial class Frm_CompanyProfile : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            // [autoId] ,[groupName] ,[compName],[address],[city],[zip],[country],[phoneNo],[fax],[email],Code],[HOAddress2],[FactoryAddress1],[FactoryAddress2]
            SqlCommand cmd = new SqlCommand("select autoId as AutoId, groupName as groupName,compName as compName, code as Code, [address] as HOAddress,phoneNo as Contact,email as Email,'' as Factory  from T_CompanyInfo ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 5;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[proc_frm_CompanyInfo_Update_T_CompanyInfo_LCM]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtCompanyName.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtAddress.Text.Trim())
                + "','','','','"
                + HttpUtility.HtmlDecode(txtContact.Text.Trim())
                + "','','"
                + HttpUtility.HtmlDecode(txtEmail.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                + "','','"
                + HttpUtility.HtmlDecode(txtFactory.Text.Trim())
                + "',''";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[proc_frm_CompanyInfo_Inser_T_CompanyInfo_LCM]"
                        + "'"
                        + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyName.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAddress.Text.Trim())
                        + "','','','','"
                        + HttpUtility.HtmlDecode(txtContact.Text.Trim())
                        + "','','"
                        + HttpUtility.HtmlDecode(txtEmail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFactory.Text.Trim())
                        + "',''";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtCompanyName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCompanyName")).Text;
                txt_GroupName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridGroupName")).Value;
                txtCode.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCode")).Text;
                txtAddress.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridHOAddressName")).Text;
                txtContact.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridContact")).Text;
                txtEmail.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEmail")).Text;
                txtFactory.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridFactory")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        //protected void btn_remove_Click(object sender, EventArgs e)
        //{
        //    Connection connection = new Connection();

        //    string S_Delete_T_LeaveAssignByHR = string.Empty;
        //    S_Delete_T_LeaveAssignByHR = "proc_frm_CompanyInfo_Delete_T_CompanyInfo_LCM"
        //         + "'"
        //         + gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].Cells[1].Text.Trim()
        //         + "'";

        //    string S_DeleteReturn_T_LeaveAssignByHR = string.Empty;
        //    S_DeleteReturn_T_LeaveAssignByHR = connection.connection_DB(S_Delete_T_LeaveAssignByHR, 1, false, true, true);

        //    if (S_DeleteReturn_T_LeaveAssignByHR != GlobalVariables.g_s_connectionErrorReturnValue)
        //    {
        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationSuccessfull;
        //        v_loadGridView_CostingHead();
        //        InsertMode();

        //    }
        //    else
        //    {
        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationFailed;
        //    }
        //}
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            if (txtCode.Text.Length != 4)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Code will be 4 digit!!";
                return false;
            }
            if (txt_GroupName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Group Name Can Not Blank!";
                return false;
            }
            if (txtCompanyName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Company Name Can Not Blank!";
                return false;
            }

            if (txtAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Address Can Not Blank!";
                return false;
            }
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txt_GroupName.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtFactory.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtContact.Text = string.Empty;
            txtCompanyName.Text = string.Empty;
            txtAddress.Text = string.Empty;
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select autoId as AutoId, groupName as groupName,compName as compName, code as Code, [address] as HOAddress,phoneNo as Contact,email as Email,FactoryAddress1 as Factory  from T_CompanyInfo ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.PageIndex = e.NewPageIndex;
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 5;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }
    }
}