﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData.Menu;
using System;
using System.Linq;
using System.Data.Entity;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Security
{
    public partial class FrmForms : Page
    {
        private MenuEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindForm();
                BindModule();
            }
        }

        private void BindModule()
        {
            using (_context = new MenuEntities())
            {
                ddModule.DataSource = _context.ScrModuleInfoes.ToList();
                ddModule.DataTextField = "ModuleName";
                ddModule.DataValueField = "ScrModuleInfoId";
                ddModule.DataBind();
                ddModule.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
        }
        private void BindForm()
        {
            using (_context = new MenuEntities())
            {
                gdvForm.DataSource = _context.ScrFormInfoes.Include(c => c.ScrModuleInfo).OrderBy(r=>r.ModuleId).ThenBy(m=>m.Priority).ToList();
                gdvForm.DataBind();
            }
        }

        private void ClearAll()
        {
            hidFormInfoId.Value = "0";
            txtFormName.Text = string.Empty;
            txtFormPath.Text = string.Empty;
            txtFormDisplayName.Text = string.Empty;
            txtFormDescription.Text = string.Empty;
            txtIcon.Text = string.Empty;
            txtPriority.Text = string.Empty;
            btnSave.Text = "Save";
            lblMsg.Text = string.Empty;
            BindForm();
        }

        private bool IsFormValid()
        {
            if (txtFormName.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Form name should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtFormName.Focus();
                return false;
            }

            if (ddModule.SelectedValue == "0")
            {
                MessageBox.ShowMessageInLabel("Module should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                ddModule.Focus();
                return false;
            }

            if (txtFormPath.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Path should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtFormPath.Focus();
                return false;
            }

            if (txtFormDisplayName.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Display name should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtFormDisplayName.Focus();
                return false;
            }

            if (txtPriority.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Priority should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtPriority.Focus();
                return false;
            }

            if (!int.TryParse(txtPriority.Text.Trim(), out _))
            {
                MessageBox.ShowMessageInLabel("Priority must be numeric!!!", lblMsg, MessageType.ErrorMessage);
                txtPriority.Focus();
                return false;
            }

            if (txtIcon.Text.Trim() != string.Empty) return true;
            MessageBox.ShowMessageInLabel("Icon should not be blank!!!", lblMsg, MessageType.ErrorMessage);
            txtIcon.Focus();
            return false;
        }

        protected void gdvForm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvForm, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new MenuEntities();
            var id = int.Parse(((HiddenField)gdvForm.Rows[gdvForm.SelectedIndex].FindControl("hidGridFormId")).Value);
            var form = _context.ScrFormInfoes.First(r => r.ScrFormInfoId == id);
            hidFormInfoId.Value = form.ScrFormInfoId.ToString();
            txtFormName.Text = form.FormName;
            txtFormPath.Text = form.FormPath;
            txtFormDisplayName.Text = form.FormDisplayName;
            txtPriority.Text = form.Priority.ToString();
            txtIcon.Text = form.FormIcon;
            txtFormDescription.Text = form.FormDescription;
            if (form.ModuleId != null)
                ddModule.SelectedValue = form.ModuleId.ToString();
            btnSave.Text = "Update";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsFormValid()) return;
            lblMsg.Text = string.Empty;
            _context = new MenuEntities();
            if (hidFormInfoId.Value.Trim() == "0")
            {
                var form = new ScrFormInfo
                {
                    FormName = txtFormName.Text.Trim(),
                    FormPath = txtFormPath.Text.Trim(),
                    FormDisplayName = txtFormDisplayName.Text.Trim(),
                    Priority = int.Parse(txtPriority.Text.Trim()),
                    FormIcon = txtIcon.Text.Trim(),
                    FormDescription = txtFormDescription.Text.Trim(),
                    ModuleId = int.Parse(ddModule.SelectedValue),
                    CreatedBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                    CreateDate = DateTime.Now
                };
                _context.ScrFormInfoes.Add(form);
                _context.SaveChanges();
                ClearAll();
                MessageBox.ShowMessageInLabel("Successfull!!!", lblMsg, MessageType.SuccessMessage);
            }
            else
            {
                var id = int.Parse(hidFormInfoId.Value);
                var form = _context.ScrFormInfoes.FirstOrDefault(r => r.ScrFormInfoId == id);
                if (form == null)
                {
                    MessageBox.ShowMessageInLabel("Data Not found!!!", lblMsg, MessageType.ErrorMessage);
                }
                else
                {
                    form.FormName = txtFormName.Text;
                    form.FormPath = txtFormPath.Text.Trim();
                    form.FormDisplayName = txtFormDisplayName.Text.Trim();
                    form.Priority = int.Parse(txtPriority.Text.Trim());
                    form.FormIcon = txtIcon.Text.Trim();
                    form.FormDescription = txtFormDescription.Text;
                    form.ModuleId = int.Parse(ddModule.SelectedValue);
                    form.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                    form.UpdateDate = DateTime.Now;
                    _context.SaveChanges();
                    ClearAll();
                    MessageBox.ShowMessageInLabel("Successfull!!!", lblMsg, MessageType.SuccessMessage);
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
    }
}