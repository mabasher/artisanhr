﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmRoles.aspx.cs" Inherits="Saas.Hr.WebForm.Security.FrmRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>User Role Setup</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-lg-12">

            <div class="col-md-4">
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvRole" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvRole_RowDataBound" OnSelectedIndexChanged="gdvRole_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="Role Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridRoleName" runat="server" Text='<%# Bind("RoleName") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridRoleId" Value='<%# Bind("ScrRoleInfoId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-6">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hidRoleInfoId" Value="0" runat="server" />
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lblRoleName" runat="server" Text="Role Name:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtRoleName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblRoleDescription" runat="server" Text="Description:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtRoleDescription" runat="server" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-offset-4 col-sm-10">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnSave_Click" Width="80px" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="80px" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>