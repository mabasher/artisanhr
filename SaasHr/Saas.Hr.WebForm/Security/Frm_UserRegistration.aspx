﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="Frm_UserRegistration.aspx.cs" Inherits="Saas.Hr.WebForm.Security.Frm_UserRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        User Registration
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-4">
                        <div style="height: 400px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" OnPageIndexChanging="gdv_costingHead_PageIndexChanging" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCategoryName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LogIn">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCode" runat="server" Text='<%# Bind("logInId") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridEmail" Value='<%# Bind("Email") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridContact" Value='<%# Bind("Contact") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridAddress" Value='<%# Bind("UserAddress") %>' runat="server" />
                                            <asp:HiddenField ID="hidGriduserlevel" Value='<%# Bind("userlevel") %>' runat="server" />
                                            <asp:HiddenField ID="hidPwd" Value='<%# Bind("pwd") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridURL" Value='<%# Bind("URL") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="User Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txt_GroupName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Contact No :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Email Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtemail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="User Level :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:DropDownList ID="dd_userlevel" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="User LogIn Id :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtlogInId" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Password :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Confirm Password :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="User Picture :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">

                                    <asp:FileUpload ID="imgUpload" runat="server" />
                                    <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Image size must be lower than 100KB)"></asp:Label>
                                </div>
                            </div>

                            <div class="row row-custom">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="text-danger">
                                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 10px;">
                                <div class="col-sm-offset-4 col-sm-10">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>