﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmSignIn.aspx.cs" Inherits="Saas.Hr.WebForm.Security.FrmSignIn" %>

<%@ Import Namespace="Saas.Hr.WebForm.Common" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title><%: Page.Title %></title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:BundleReference runat="server" Path="~/Content/acestyle" />
    <!--[if lte IE 9]>
        <webopt:bundlereference runat="server" path="~/Content/aceparttwo" />
    <![endif]-->
    <webopt:BundleReference runat="server" Path="~/Content/aceskinandrtl" />
    <!--[if lte IE 9]>
        <webopt:bundlereference runat="server" path="~/Content/aceie" />
    <![endif]-->
    <webopt:BundleReference runat="server" Path="~/Content/custom" />

    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/acesextracript") %>
        <!--[if lte IE 8]>
            <%: Scripts.Render("~/bundles/shivandrespond") %>
        <![endif]-->
    </asp:PlaceHolder>
</head>
<body class="login-layout light-login">

    <form id="frmUserSignIn" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <div class="main-container light-login">
            <div class="main-content light-login">
                <div class="row row-custom">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <img style="background-color: white;" src="<%= Page.ResolveUrl("~/") %>Images/logo.png" alt="<%=GlobalVariables.BaseCompanyName %>" class="img img-responsive" />
                                <p class="software-name">SAAS ERP SYSTEM</p>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Please Enter Your Information
                                            </h4>

                                            <div class="space-6"></div>

                                            <div class="text-danger">
                                                <asp:Label ID="lblErrorMessage" runat="server" Text=""></asp:Label>
                                            </div>
                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <asp:DropDownList ID="ddlCompany" CssClass="form-control input-sm" runat="server"></asp:DropDownList>
                                                    <i class="ace-icon fa fa-building"></i>
                                                </span>
                                            </label>

                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <asp:TextBox ID="txtUserName" CssClass="form-control input-sm" placeholder="Username" runat="server"></asp:TextBox>
                                                    <i class="ace-icon fa fa-user"></i>
                                                </span>
                                            </label>

                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <asp:TextBox ID="txtPassword" CssClass="form-control input-sm" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
                                                    <i class="ace-icon fa fa-lock"></i>
                                                </span>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <asp:CheckBox ID="chkRememberMe" runat="server" />
                                                    <span class="lbl">Remember Me</span>
                                                </label>

                                                <%--<asp:LinkButton ID="lnkBtnSignIn" runat="server" CssClass="width-35 pull-right btn btn-xs btn-primary" OnClick="lnkBtnSignIn_Click"><i class="ace-icon fa fa-key"></i>
                                                        <span class="bigger-110">Sign In</span></asp:LinkButton>--%>
                                                <asp:Button ID="btnSignIn" runat="server" Text="Sign In" CssClass="width-35 pull-right btn btn-sm btn-primary" OnClick="btnSignIn_Click" />
                                            </div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <asp:CheckBox ID="chkForgot" AutoPostBack="true" OnCheckedChanged="chkForgot_CheckedChanged" runat="server" />
                                                    <span class="lbl">Forgot your password ?</span>
                                                </label>
                                            </div>

                                            <div class="clearfix" id="iForgot" runat="server">
                                                
                                                <div class="clearfix">
                                                    <label class="inline">
                                                        <asp:CheckBox ID="chkotp" AutoPostBack="true" OnCheckedChanged="chkotp_CheckedChanged" runat="server" />
                                                        <span class="lbl">Have OTP ?</span>
                                                    </label>
                                                </div>

                                                <div class="clearfix" id="iOtp" runat="server">                                                    
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <asp:TextBox ID="txtOtp" AutoComplete="off" CssClass="form-control input-sm" placeholder="OTP" runat="server"></asp:TextBox>
                                                        <i class="ace-icon fa fa-anchor"></i>
                                                    </span>  
                                                </label>                                             
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <asp:TextBox ID="txtnewPassword" AutoComplete="off" CssClass="form-control input-sm" placeholder="New Password" runat="server" TextMode="Password"></asp:TextBox>
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                                </label>
                                                </div>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <asp:TextBox ID="txtEmail" AutoComplete="off" CssClass="form-control input-sm" placeholder="Email Address" runat="server"></asp:TextBox>
                                                        <i class="ace-icon fa fa-address-card"></i>
                                                    </span>
                                                </label>

                                                <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="width-35 pull-right btn btn-sm btn-danger" OnClick="btnSend_Click" />
                                            </div>


                                            <div class="space-4"></div>
                                        </div>
                                        <!-- /.widget-main -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.login-box -->
                            </div>
                            <!-- /.position-relative -->
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-12">
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.main-content -->
            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content" style="left: 0;">
                        <div class="row row-custom">
                            <div class="col-sm-6">
                                <span class="bigger-120">&copy;<%: DateTime.Now.Year %> <a href="<%=GlobalVariables.BaseCompanyWebAddress %>" target="_blank"><%=GlobalVariables.BaseCompanyName %></a>
                                </span>
                            </div>
                            <div class="col-sm-6">
                                Design &amp; Developed By : <a href="http://www.stmsoftwareltd.com/" target="_blank">STM Software Ltd.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.main-container -->
    </form>

    <%: Scripts.Render("~/bundles/acescript") %>
</body>
</html>
