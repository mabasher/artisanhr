﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web.UI;

namespace Saas.Hr.WebForm.Security
{
    public partial class FrmSignIn : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    if (Request.QueryString["cId"] != null)
                    {
                        ddlCompany.SelectedValue = Request.QueryString["cId"].ToString().Trim();
                    }
                    if (Request.QueryString["userId"] != null)
                    {
                        txtUserName.Text = Request.QueryString["userId"].ToString().Trim();
                    }
                }

                commonFunctions.g_b_FillDropDownList(ddlCompany,
                    "T_CompanyInfo",
                    "compName",
                    "autoId", string.Empty);

                commonFunctions.g_b_FillDropDownListLoaded(ddlCompany,
                    "T_CompanyInfo",
                    "compName",
                    "autoId", string.Empty);
                iForgot.Visible = false;
                iOtp.Visible = false;
            }
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == string.Empty)
            {
                lblErrorMessage.Text = "UserID cann't be blank";
            }
            else
            {
                if (txtPassword.Text == string.Empty)
                {
                    lblErrorMessage.Text = "Password cann't be blank";
                }
                else
                {
                    Connection connection = new Connection();
                    CommonFunctions.password_Handler password_Handler = new CommonFunctions.password_Handler();
                    string s_cryptUserName = string.Empty, s_cryptPwd = string.Empty,
                                            issertUserLog = string.Empty,
                                            s_sql_findUser = string.Empty,
                                            return_autoId = string.Empty,
                                            redirectPageURL = string.Empty;
                    decimal i_row = 0;
                    if (Request.QueryString.Count > 0)
                    {
                        if (Request.QueryString["pageURL"] != null)
                        {
                            redirectPageURL = Request.QueryString["pageURL"].ToString().Replace("@", "&");
                            //Response.Redirect(redirectPageURL);
                        }

                        if (Request.QueryString["s_pageURL_feedBack"] != null)
                        {
                            redirectPageURL = Request.QueryString["s_pageURL_feedback"].ToString().Replace("@", "&");
                            //Response.Redirect(redirectPageURL);
                        }
                    }

                    s_cryptUserName = txtUserName.Text;
                    s_cryptPwd = txtPassword.Text;

                    //s_sql_findUser = Procedures.HumanResource.Proc_Web_frm_login_Select_validUser
                    //                    + "'"
                    //                    + s_cryptUserName
                    //                    + "','"
                    //                    + s_cryptPwd
                    //                    + "','"
                    //                    + dd_companyName.SelectedValue
                    //                    + "'";

                    s_sql_findUser = "proc_frm_SignIn_For_validUser"
                                        + "'"
                                        + txtUserName.Text
                                        + "','"
                                        + txtPassword.Text
                                        + "','"
                                        + ddlCompany.SelectedValue
                                        + "'";

                    Session[GlobalVariables.g_s_procedureReturnType] = connection.connection_DB(s_sql_findUser, 0, true, false, false);
                    if (Session[GlobalVariables.g_s_procedureReturnType].ToString() != GlobalVariables.g_s_connectionErrorReturnValue)
                    {
                        if (connection.ResultsDataSet != null)
                        {
                            if (connection.ResultsDataSet.Tables != null)
                            {
                                if (connection.ResultsDataSet.Tables[0].Rows.Count > 0)
                                {
                                    i_row = Convert.ToDecimal(connection.ResultsDataSet.Tables[0].Rows.Count.ToString());
                                    if (connection.ResultsDataSet.Tables[0].Rows[0]["password"].ToString() == txtPassword.Text)
                                    {
                                    }
                                    else
                                    {
                                        i_row = 0;
                                    }
                                }
                            }
                        }
                    }
                    if (i_row > 0)
                    {
                        //insert start into Table T_UserLogBook
                        string user_autoId = string.Empty;
                        string sMode = string.Empty;
                        sMode = "";
                        string s_sql_selectCompanyInfo = string.Empty;
                        DataTable dt_companyInformation = new DataTable();
                        CommonFunctions.Date_Validation date_validation = new CommonFunctions.Date_Validation();
                        int userRoleId = 0;

                        foreach (DataRow datarow in connection.ResultsDataSet.Tables[0].Rows)
                        {
                            userRoleId = int.Parse(datarow["userlevel"].ToString());
                            //Session[GlobalVariables.g_s_userName] = txt_userName.Text;
                            Session[GlobalVariables.g_s_WinMode] = datarow["WinMode"].ToString();
                            Session[GlobalVariables.g_s_DummyMode] = datarow["trMode"].ToString();
                            Session[GlobalVariables.g_s_userName] = datarow["UserName"].ToString();
                            //Session[GlobalVariables.g_s_userAutoId] = datarow["empAutoId"].ToString();
                            Session[GlobalVariables.g_s_userAutoId] = datarow["autoId"].ToString();
                            Session[GlobalVariables.g_s_userSupervisorAutoId] = datarow["Supplier_Id"].ToString();
                            Session[GlobalVariables.g_s_userMode] = datarow["userMode"].ToString();
                            //string emp = Session[GlobalVariables.g_s_userAutoId].ToString();
                            Session[GlobalVariables.g_s_CompanyAutoId] = ddlCompany.SelectedValue;
                            string CompanyAutoID = Session[GlobalVariables.g_s_CompanyAutoId].ToString();
                            //user_autoId = datarow["empAutoId"].ToString();
                            Session[GlobalVariables.g_s_userStatus] = datarow["userStatus"].ToString();
                            Session[GlobalVariables.g_s_userLevel] = userRoleId;
                            Session[GlobalVariables.g_s_userPIC] = datarow["URL"].ToString();
                            Session[GlobalVariables.g_s_userIP] = ClientInformation.GetIpAddress();

                            //Session[GlobalVariables.g_s_userStatus] = "Sa";
                            //                            Session[GlobalVariables.g_s_userLevel] = "";
                        }
                        MenuGenerator mg = new MenuGenerator(userRoleId);
                        Session[GlobalVariables.g_s_userMenu] = mg.GetMenu(Page.ResolveUrl("~/"));
                        // track down company Information

                        s_sql_selectCompanyInfo = Procedures.HumanResource.Proc_Web_frm_login_Select_CompanyInformation
                                                    + " '"
                                                    + Session[GlobalVariables.g_s_CompanyAutoId].ToString()
                                                    + "'";
                        Session[GlobalVariables.g_s_procedureReturnType] = connection.connection_DB(s_sql_selectCompanyInfo, 0, true, false, false);
                        dt_companyInformation = connection.ResultsDataSet.Tables[0];

                        if (Session[GlobalVariables.g_s_procedureReturnType].ToString() != GlobalVariables.g_s_connectionErrorReturnValue)
                        {
                            if (dt_companyInformation != null)
                            {
                                foreach (DataRow datarow_companyInformation in dt_companyInformation.Rows)
                                {
                                    //Session[GlobalVariables.g_s_CompanyAutoId] = Convert.ToString(datarow_companyInformation["autoId"]);
                                    Session[GlobalVariables.g_s_GroupName] = Convert.ToString(datarow_companyInformation["groupName"].ToString());
                                    Session[GlobalVariables.g_s_companyName] = Convert.ToString(datarow_companyInformation["compName"].ToString());
                                    Session[GlobalVariables.g_s_Address] = Convert.ToString(datarow_companyInformation["address"].ToString());
                                    Session[GlobalVariables.g_s_City] = Convert.ToString(datarow_companyInformation["city"].ToString());
                                    Session[GlobalVariables.g_s_Zip] = Convert.ToString(datarow_companyInformation["zip"].ToString());
                                    Session[GlobalVariables.g_s_Country] = Convert.ToString(datarow_companyInformation["country"].ToString());
                                    Session[GlobalVariables.g_s_Phone] = Convert.ToString(datarow_companyInformation["phoneNo"].ToString());
                                    Session[GlobalVariables.g_s_Fax] = Convert.ToString(datarow_companyInformation["fax"].ToString());
                                    Session[GlobalVariables.g_s_Email] = Convert.ToString(datarow_companyInformation["email"].ToString());
                                    Session[GlobalVariables.g_s_Code] = Convert.ToString(datarow_companyInformation["Code"].ToString());
                                    Session[GlobalVariables.g_s_autoId] = "0";
                                }
                            }
                        }

                        // end track down company Information

                        //Procedures.Accounts.proc_frm_accountHeadOpeningBalance_Select_allAccountHeadOpeningBalance +
                        //                      " '" + Session[GlobalVariables.g_s_CompanyAutoId].ToString() +
                        //"'";
                        issertUserLog = Procedures.HumanResource.Proc_Web_Insert_UserLogBook
                                        + "'" + Session[GlobalVariables.g_s_userAutoId].ToString() +
                                        "','" + DateTime.Now.ToString("MM/dd/yyyy") +
                                        "','" + DateTime.Now.ToString(new CultureInfo("en-US")) +
                                        "','" + DateTime.Now.ToString(new CultureInfo("en-US")) +
                                        "'";

                        return_autoId = connection.connection_DB(issertUserLog, 1, true, true, true);
                        Session["company_AutoId"] = ddlCompany.SelectedValue;
                        if (return_autoId != string.Empty)
                        {
                            Session[GlobalVariables.g_s_userLogBookAutoId] = return_autoId;
                            txtUserName.EnableViewState = true;
                        }
                        Session["Isvalid"] = "true";
                        Session["autoId"] = return_autoId.ToString();
                        if (Request.QueryString.Count > 0)
                        {
                            Response.Redirect(redirectPageURL);
                        }
                        else
                        {
                            Response.Redirect(GlobalVariables.g_s_URL_homePage);
                        }

                        //End insert start into Table T_UserLogBook
                    }
                    else
                    {
                        Session["Isvalid"] = "false";
                        //                        lblErrorMessage.Text = string.Empty;
                        lblErrorMessage.Text = "Invalid user name or password";
                    }
                }
            }
        }

        protected void chkForgot_CheckedChanged(object sender, EventArgs e)
        {
            iForgot.Visible = false;
            if (chkForgot.Checked)
            {
                iForgot.Visible = true;
            }
        }
        protected void chkotp_CheckedChanged(object sender, EventArgs e)
        {
            iOtp.Visible = false;
            if (chkotp.Checked)
            {
                iOtp.Visible = true;
            }
        }
        
        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (chkotp.Checked == false)
            {
                if (txtEmail.Text == string.Empty)
                {
                    lblErrorMessage.Text = "Email cann't be blank";
                    return;
                }

                Connection connection = new Connection();

                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                string s_returnValue = string.Empty;
                string s_save_ = string.Empty;
                string s_Update_returnValue = string.Empty;
                string s_Update = string.Empty;
                string s_select = string.Empty;
                s_select = "";
                string s_Select_returnValue = string.Empty;
                string s_trautoId = string.Empty;
                //var employees = new List<EmpSigninModel>();
                var EmpEmail = txtEmail.Text.ToString();
                var otp = Guid.NewGuid();
                var receivers = new List<string>
                        {
                           EmpEmail
                        };
                s_select = s_select + "execute proc_InsertOTP '" + EmpEmail + "','" + otp + "','','Y'";
                var mailOptions = new MailSendingOption(receivers, null, EmpEmail, "Forgot Password ?", " OTP : " + otp + "<br/> OTP Live for 10 Minutes", null);
                s_save_ = s_save_ + " " + s_select + " Select ''";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblErrorMessage.Text = GlobalVariables.g_s_insertSendSuccessfull;
                        //MailSender.SendMultipleMail(mailSendingOptions);//Manage User
                        MailSender.SendMail(mailOptions);
                    }
                    else
                    {
                        lblErrorMessage.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblErrorMessage.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }


            if (chkotp.Checked == true)
            {
                if (txtOtp.Text == string.Empty)
                {
                    lblErrorMessage.Text = "OTP cann't be blank";
                    return;
                }
                if (txtnewPassword.Text == string.Empty)
                {
                    lblErrorMessage.Text = "Password cann't be blank";
                    return;
                }
                if (txtEmail.Text == string.Empty)
                {
                    lblErrorMessage.Text = "Email cann't be blank";
                    return;
                }

                Connection connection = new Connection();

                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                string s_returnValue = string.Empty;
                string s_save_ = string.Empty;
                string s_Update_returnValue = string.Empty;
                string s_Update = string.Empty;
                string s_select = string.Empty;
                s_select = "";
                string s_Select_returnValue = string.Empty;
                string s_trautoId = string.Empty;
                var EmpEmail = txtEmail.Text.ToString();
                var otp = txtOtp.Text.ToString();
                var pwd = txtnewPassword.Text.ToString();

                s_select = s_select + "execute proc_InsertOTP '" + EmpEmail + "','" + otp + "','" + pwd + "','N'";
                s_save_ = s_save_ + " " + s_select + " Select ''";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblErrorMessage.Text = GlobalVariables.g_s_insertSendSuccessfull;
                    }
                    else
                    {
                        lblErrorMessage.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblErrorMessage.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }

        }



    }
}