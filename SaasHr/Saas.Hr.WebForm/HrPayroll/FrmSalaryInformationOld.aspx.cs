﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrPayroll
{

    public partial class FrmSalaryInformationOld : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txteffectiveDate.Text = DateTime.Now.ToString();
                txtIncrDate.Text = DateTime.Now.ToString();

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
                CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();
            
                SqlCommand cmd = new SqlCommand("SELECT SalaryInfoId,sGross,sBasic,sHR,sConveyance,sMedical,sPF,sFood,sTransportation,sOtherAllowance,sEffectiveDate,sIncrDate,sAccountNumber,sPaymentMode,sIsTaxxable,sIsOTPayable,sRoutingNumber,sIsFixedTransport From SalaryInfo Where EmpAutoId='" + hidEmpAutoId.Value + "' Order by sEffectiveDate desc", sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.DataBind();
                sqlconnection.Close();            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {                
                s_Update = "[ProcSalaryInfoINSERT]"
                        + "'"
                        + hidtrAutoId.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','0','0','"
                        + HttpUtility.HtmlDecode(txtGross.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBasic.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtHousing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtConveyance.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMedical.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPF.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtFood.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtTransportation.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOtherAllowance.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txteffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtIncrDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtBankAccount.Text.Trim())
                        + "','"
                        + hidPaymentMode.Value
                        + "','"
                        + hidTaxxable.Value
                        + "','"
                        + hidOTPayable.Value
                        + "','"
                        + hidFixedTransport.Value
                        + "','Y','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U','"
                        + HttpUtility.HtmlDecode(txtRoutingNo.Text.Trim()) + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                       
                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                }
            }
            else
            {

                s_save_ = "[ProcSalaryInfoINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','0','0','"
                        + HttpUtility.HtmlDecode(txtGross.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBasic.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtHousing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtConveyance.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMedical.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPF.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtFood.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtTransportation.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOtherAllowance.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txteffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtIncrDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtBankAccount.Text.Trim())
                        + "','"
                        + hidPaymentMode.Value
                        + "','"
                        + hidTaxxable.Value
                        + "','"
                        + hidOTPayable.Value
                        + "','"
                        + hidFixedTransport.Value
                        + "','Y','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I','"
                        + HttpUtility.HtmlDecode(txtRoutingNo.Text.Trim())
                        + "'";
                               
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        
                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidtrAutoId.Value = "0";
                hidtrAutoId.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridSalaryInfoId")).Value;
                txtGross.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridGross")).Text;
                txtBasic.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridBasic")).Text;
                txtHousing.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridHR")).Text;
                txtConveyance.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridConveyance")).Text;
                txtMedical.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridMedical")).Text;
                txtPF.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridPF")).Text;
                txtFood.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridFood")).Text;
                txtTransportation.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridTransportation")).Text;
                txtOtherAllowance.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridOtherAllowance")).Text;

                txteffectiveDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEffectiveDate")).Text;
                txtIncrDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridIncrDate")).Text;
                txtBankAccount.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAccountNumber")).Text;
                txtRoutingNo.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridsRoutingNumber")).Text;
                if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridPaymentMode")).Value=="B")
                {
                    chkBank.Checked = true;
                    chkCash.Checked = false;
                }
                if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridPaymentMode")).Value == "C")
                {
                    chkCash.Checked = true;
                    chkBank.Checked = false;
                }
                chktaxxable.Checked = false;
                if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridTaxxable")).Value == "Y")
                {
                    chktaxxable.Checked = true;
                }
                chkOT.Checked = false;
                if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridOTPayable")).Value == "Y")
                {
                    chkOT.Checked = true;
                }

                chkFixedTransport.Checked = false;
                if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridFixedTransport")).Value == "Y")
                {
                    chkFixedTransport.Checked = true;
                }

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            
            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }
            if (txtGross.Text == ""){ txtGross.Text = "0"; }
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }

            if (Convert.ToDouble(txtGross.Text)<1)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Gross Can Not Blank!";
                return false;
            }

            hidFixedTransport.Value = "N";
            if (chkFixedTransport.Checked) { hidFixedTransport.Value = "Y"; }
            hidTaxxable.Value = "N";
            if (chktaxxable.Checked) { hidTaxxable.Value = "Y"; }
            hidOTPayable.Value = "N";
            if (chkOT.Checked) { hidOTPayable.Value = "Y"; }
            hidPaymentMode.Value = "C";
            if (chkBank.Checked) {
                hidPaymentMode.Value = "B";
                if (txtBankAccount.Text == "")
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Bank Account Can Not Blank!";
                    return false;
                }
                if (txtRoutingNo.Text == "")
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Routing No Can Not Blank!";
                    return false;
                }
            }


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtGross.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtBankAccount.Text = string.Empty;
            txtBasic.Text = string.Empty;
            txtConveyance.Text = string.Empty;
            txtPF.Text = string.Empty;
            txtFood.Text = string.Empty;
            txtTransportation.Text = string.Empty;
            txtMedical.Text = string.Empty;
            txtHousing.Text = string.Empty;
            txtRoutingNo.Text = string.Empty;
            chkBank.Checked = false;
            chkCash.Checked = false;
            chkOT.Checked = false;
            chktaxxable.Checked = false;
            chkFixedTransport.Checked = false;
        }

        protected void txtGross_TextChanged(object sender, EventArgs e)
        {
            if (txtGross.Text == "") { txtGross.Text = "0"; }
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }
            if (Convert.ToDouble(txtGross.Text) > 0)
            {
                getSalaryBreakDown();
            }
            txtOtherAllowance.Focus();
        }

        protected void txtOtherAllowance_TextChanged(object sender, EventArgs e)
        {
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }
            if (txtGross.Text == "") { txtGross.Text = "0"; }
            if (Convert.ToDouble(txtGross.Text) > 0)
            {
                getSalaryBreakDown();
            }
            txteffectiveDate.Focus();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            //s_select = "SELECT * FROM EmployeePersonalInfo e left outer join  Where EmployeePersonalInfoId='" + sEmpId + "'";
            //s_select = "SELECT 'PIN :' + PIN + char(13) + 'Name :' + EmployeeName + char(13) + 'Department :' + d.Department + char(13) + 'Emp Type :' + et.TypeName as empName FROM EmployeeOfficialInfo O left outer join EmployeePersonalInfo e ON O.EmpAutoId = e.EmployeePersonalInfoId left outer join DepartmentInfo d on O.DepartmentAutoId = d.DepartmentInfoId  left outer join EmployeeType et on O.EmployeeTypeAutoId = et.EmployeeTypeId   Where EmployeePersonalInfoId ='" + sEmpId + "'";
            s_select = "SELECT  'Department :' + d.Department + char(13) + 'Emp Type :' + et.TypeName as empName,d.Department as empDept,et.TypeName as empType FROM EmployeeOfficialInfo O left outer join EmployeePersonalInfo e ON O.EmpAutoId = e.EmployeePersonalInfoId left outer join DepartmentInfo d on O.DepartmentAutoId = d.DepartmentInfoId  left outer join EmployeeType et on O.EmployeeTypeAutoId = et.EmployeeTypeId   Where EmployeePersonalInfoId ='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        //txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["empName"].ToString();
                        //txtEmpName.Text = txtPIN.Text + " " + txtEmpName.Text;
                        hidEmpType.Value = drow["empType"].ToString();
                        hidEmpDepartment.Value = drow["empDept"].ToString();  
                        //join Date for Next Incr Date Calculation
                    }
                }
            }

            v_loadGridView_CostingHead();


        }


        private void getSalaryBreakDown()
        {
            string sOA = string.Empty;
            string dOthers = string.Empty;
            dOthers = "0";
            sOA = "0";
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }
            if (hidEmpType.Value == "Worker")
            {                
                dOthers = Convert.ToString(Convert.ToDouble(txtGross.Text) - 1850);
                dOthers = Convert.ToString(Convert.ToDouble(dOthers) - Convert.ToDouble(txtOtherAllowance.Text));
                txtBasic.Text = Convert.ToString(Convert.ToDouble(dOthers) /1.5);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtPF.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .08);
                txtConveyance.Text = "0";
                txtFood.Text = "900";
                txtMedical.Text = "600";
                txtTransportation.Text = "350";
            }
            else if(hidEmpDepartment.Value=="Sales")
            {
                sOA = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text));
                txtBasic.Text = Convert.ToString(Convert.ToDouble(sOA) * .6);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtMedical.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .1);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtPF.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .08);
                txtConveyance.Text = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtBasic.Text) - Convert.ToDouble(txtHousing.Text) - Convert.ToDouble(txtMedical.Text));
                txtFood.Text = "150";
                txtTransportation.Text = "0";
            } 
            else
            {
                sOA = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text));
                txtBasic.Text = Convert.ToString(Convert.ToDouble(sOA) * .6);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtMedical.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .1);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtPF.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .08);
                txtConveyance.Text = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtBasic.Text) - Convert.ToDouble(txtHousing.Text) - Convert.ToDouble(txtMedical.Text));
                txtFood.Text = "0";
                txtTransportation.Text = "0";
            }
            
            txtBasic.Text = Convert.ToString(Convert.ToDecimal(txtBasic.Text).ToString("####.00"));
            txtMedical.Text = Convert.ToString(Convert.ToDecimal(txtMedical.Text).ToString("####.00"));
            txtHousing.Text = Convert.ToString(Convert.ToDecimal(txtHousing.Text).ToString("####.00"));
            txtPF.Text = Convert.ToString(Convert.ToDecimal(txtPF.Text).ToString("####.00"));
            txtConveyance.Text = Convert.ToString(Convert.ToDecimal(txtConveyance.Text).ToString("####.00"));
            txtFood.Text = Convert.ToString(Convert.ToDecimal(txtFood.Text).ToString("####.00"));
            txtTransportation.Text = Convert.ToString(Convert.ToDecimal(txtTransportation.Text).ToString("####.00"));



        }


        //===========End===============         
    }
}