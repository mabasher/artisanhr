﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmSalaryInformationN.aspx.cs" Inherits="Saas.Hr.WebForm.HrPayroll.FrmSalaryInformationN" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Salary Information
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Select Employee" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-3 hidden">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" Height="40px" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 15px;">
                                <asp:Label ID="Label18" runat="server" Text="Gross Salary :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtGross" ReadOnly="true" Font-Size="13" runat="server"
                                        AutoPostBack="true" OnTextChanged="txtGross_TextChanged"
                                        CssClass="form-control input-sm"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtGross" />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Other Allowance:" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtOtherAllowance" Font-Size="13" runat="server" AutoPostBack="true" OnTextChanged="txtOtherAllowance_TextChanged" CssClass="form-control input-sm"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtOtherAllowance" />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label30" runat="server" Text="Basic Salary :" CssClass="col-sm-3 control-label"></asp:Label>
                                <%--<div class="col-sm-2">
                                    <asp:TextBox ID="txtBasicPercentage" Font-Size="13"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                   </div>
                                <asp:Label ID="Label10" runat="server" Text="%" CssClass="col-sm-1 control-label"></asp:Label>
                                --%>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtBasic" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtBasic_TextChanged"></asp:TextBox>
                                    <%--  <asp:Label ID="lblBasicNote" ForeColor="Red" runat="server" Text=" (Gross * 60%)" CssClass="control-label"></asp:Label>--%>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label31" runat="server" Text="Housing :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2 ">
                                    <asp:TextBox ID="txtHousingPercentage" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtHousingPercentage_TextChanged"></asp:TextBox>
                                </div>
                                <asp:Label ID="Label11" runat="server" Text="%" CssClass="col-sm-1 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtHousing" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtHousing_TextChanged"></asp:TextBox>
                                </div>
                                <%-- <div class="col-sm-4">
                                     <asp:Label ID="lblHousingNote" ForeColor="Red" runat="server" Text=" (Basic * 50%)" CssClass="control-label"></asp:Label>
                                </div>--%>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label33" runat="server" Text="Medical :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtMedicalPercentage" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtMedicalPercentage_TextChanged"></asp:TextBox>
                                </div>
                                <asp:Label ID="Label12" runat="server" Text="%" CssClass="col-sm-1 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtMedical" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtMedical_TextChanged"></asp:TextBox>
                                </div>
                                <%--<div class="col-sm-4">
                                     <asp:Label ID="lblMedicalNote" ForeColor="Red" runat="server" Text=" (Basic * 10%)" CssClass="control-label"></asp:Label>
                                </div>--%>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label32" runat="server" Text="PF :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtPFPercentage" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtPFPercentage_TextChanged"></asp:TextBox>
                                </div>
                                <asp:Label ID="Label13" runat="server" Text="%" CssClass="col-sm-1 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtPF" Font-Size="13" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtPF_TextChanged"></asp:TextBox>
                                </div>
                                <%-- <div class="col-sm-4">
                                     <asp:Label ID="lblPFNote" ForeColor="Red" runat="server" Text=" (Basic * 8%)" CssClass="control-label"></asp:Label>
                                </div>--%>
                            </div>


                        </div>
                    </div>
                    <div class="col-md-5" style="margin-top: 100px; font-size: 13px;">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Food Allowance :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtFood" Font-Size="13" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Transportation :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtTransportation" Font-Size="13" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <asp:CheckBox ID="chkFixedTransport" runat="server" Width="150px" Text=" Fixed (Y/N)" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label34" runat="server" Text="Conveyance :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtConveyance" Font-Size="13" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="lbl_issue_Date" runat="server" Text="Effective Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txteffectiveDate" CssClass="input-sm date-picker" />
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="Label8" runat="server" Text="Incr.Dt.:" CssClass="control-label"></asp:Label>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txtIncrDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Bank Account :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtBankAccount" Font-Size="13" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="Routing No :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtRoutingNo" Font-Size="13" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Payment Mode :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:CheckBox ID="chkBank" runat="server" Width="150px" Text=" Bank" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:CheckBox ID="chkCash" runat="server" Width="150px" Text=" Cash" />
                                </div>
                                <div class="col-sm-3">
                                    <asp:CheckBox ID="chktaxxable" runat="server" Width="150px" Text=" Is Taxable" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:CheckBox ID="chkOT" runat="server" Width="150px" Text=" Is OT Payable" />
                                </div>
                            </div>



                        </div>
                    </div>


                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-9">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div style="height: 250px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="Gross">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridGross" runat="server" Text='<%# Bind("sGross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Basic">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridBasic" runat="server" Text='<%# Bind("sBasic") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridHR" runat="server" Text='<%# Bind("sHR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Conveyance">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridConveyance" runat="server" Text='<%# Bind("sConveyance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Medical">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMedical" runat="server" Text='<%# Bind("sMedical") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridPF" runat="server" Text='<%# Bind("sPF") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Food">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridFood" runat="server" Text='<%# Bind("sFood") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transport">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTransportation" runat="server" Text='<%# Bind("sTransportation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Others">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridOtherAllowance" runat="server" Text='<%# Bind("sOtherAllowance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Effective Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEffectiveDate" runat="server" Text='<%# Bind("sEffectiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incr. Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridIncrDate" runat="server" Text='<%# Bind("sIncrDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAccountNumber" runat="server" Text='<%# Bind("sAccountNumber") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridSalaryInfoId" Value='<%# Bind("SalaryInfoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridPaymentMode" Value='<%# Bind("sPaymentMode") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridTaxxable" Value='<%# Bind("sIsTaxxable") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridOTPayable" Value='<%# Bind("sIsOTPayable") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridFixedTransport" Value='<%# Bind("sIsFixedTransport") %>' runat="server" />

                                            <asp:HiddenField ID="hidHousingPercentage" Value='<%# Bind("HousingPercentage") %>' runat="server" />
                                            <asp:HiddenField ID="hidPFPercentage" Value='<%# Bind("PFPercentage") %>' runat="server" />
                                            <asp:HiddenField ID="hidMedicalPercentage" Value='<%# Bind("MedicalPercentage") %>' runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Routing No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridsRoutingNumber" runat="server" Text='<%# Bind("sRoutingNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>




            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidtrAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpType" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpDepartment" runat="server" Value="" />
        <asp:HiddenField ID="hidOTPayable" runat="server" Value="N" />
        <asp:HiddenField ID="hidTaxxable" runat="server" Value="N" />
        <asp:HiddenField ID="hidPaymentMode" runat="server" Value="N" />
        <asp:HiddenField ID="hidFixedTransport" runat="server" Value="N" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployeeforSalary.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>

</asp:Content>
