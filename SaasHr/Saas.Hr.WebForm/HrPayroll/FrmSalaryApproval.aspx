﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmSalaryApproval.aspx.cs" Inherits="Saas.Hr.WebForm.HrPayroll.FrmSalaryApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Salary Verify and Approval
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
        <div>
            <div class="row row-custom">
                            
                <div class="col-sm-12">
                    <div class="form-horizontal">
                       
                        
                            <div class="form-group">
                                <asp:Label ID="lbl_issue_Date" runat="server" Text="Approve Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txteffectiveDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>
                    </div>
                </div>                   
                <div class="col-md-6">
                    <div class="form-group center">

                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Approve" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                            
                    </div>
                </div>
                
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12 ">
                    <div style="height: 550px; overflow: scroll;">
                        <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                            CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                </asp:CommandField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PIN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Designation">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>     
                                <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridJoinDate" runat="server" Text='<%# Bind("Joindate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                    <asp:TemplateField HeaderText="Gross">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridGross" runat="server" Text='<%# Bind("sGross") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    
                                    <asp:TemplateField HeaderText="Basic">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridBasic" runat="server" Text='<%# Bind("sBasic") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridHR" runat="server" Text='<%# Bind("sHR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Conveyance">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridConveyance" runat="server" Text='<%# Bind("sConveyance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    
                                    <asp:TemplateField HeaderText="Medical">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMedical" runat="server" Text='<%# Bind("sMedical") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridPF" runat="server" Text='<%# Bind("sPF") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Food">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridFood" runat="server" Text='<%# Bind("sFood") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Transportation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTransportation" runat="server" Text='<%# Bind("sTransportation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    <asp:TemplateField HeaderText="Other Allowance">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridOtherAllowance" runat="server" Text='<%# Bind("sOtherAllowance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                  
                                    <asp:TemplateField HeaderText="Effective Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEffectiveDate" runat="server" Text='<%# Bind("sEffectiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Incr. Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridIncrDate" runat="server" Text='<%# Bind("sIncrDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Account Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAccountNumber" runat="server" Text='<%# Bind("sAccountNumber") %>'></asp:Label>                                     
                                            <asp:HiddenField ID="hid_GridEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />                                    
                                            <asp:HiddenField ID="hid_GridSalaryGroupId" Value='<%# Bind("SalaryGroupId") %>' runat="server" />                                      
                                            <asp:HiddenField ID="hid_GridSalaryInfoId" Value='<%# Bind("SalaryInfoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridPaymentMode" Value='<%# Bind("sPaymentMode") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridTaxxable" Value='<%# Bind("sIsTaxxable") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridOTPayable" Value='<%# Bind("sIsOTPayable") %>' runat="server" />
                                      
                                           <%-- <asp:HiddenField ID="hidHousingPercentage" Value='<%# Bind("HousingPercentage") %>' runat="server" />
                                            <asp:HiddenField ID="hidPFPercentage" Value='<%# Bind("PFPercentage") %>' runat="server" />
                                            <asp:HiddenField ID="hidMedicalPercentage" Value='<%# Bind("MedicalPercentage") %>' runat="server" />--%>
                                        
                                            </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Routing No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridsRoutingNumber" runat="server" Text='<%# Bind("sRoutingNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>  
                                              
                            </Columns>
                            <PagerStyle CssClass="pagination-sa" />
                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                        </asp:GridView>
                    </div>
                </div>


            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
        <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
        <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
        <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
        <asp:HiddenField ID="hidDaysInMonth" runat="server" Value="0" />
        <asp:HiddenField ID="hidInputdays" runat="server" Value="0" />
        <asp:HiddenField ID="hidtrStatus" runat="server" Value="N" />
 
        <asp:HiddenField ID="hidpresent" runat="server" Value="0" />
        <asp:HiddenField ID="hidlate" runat="server" Value="0" />
        <asp:HiddenField ID="hidweekEndOff" runat="server" Value="0" />
        <asp:HiddenField ID="hidleaveWithPay" runat="server" Value="0" />
        <asp:HiddenField ID="hidleaveWithoutPay" runat="server" Value="0" />
        <asp:HiddenField ID="hidgovtleave" runat="server" Value="0" />
        <asp:HiddenField ID="hidabsent" runat="server" Value="0" />
       

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <%--<script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>--%>
</asp:Content>
