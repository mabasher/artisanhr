﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmSalaryLoan.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmSalaryLoan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Salary Loan | Advance
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">

        <div class="col-md-6">
            <fieldset>
                <legend>Salary Loan | Advance</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                     <div class="form-group">
                        <asp:Label ID="Label31" runat="server" Text="Issue Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                        </div>
                    </div>
                     <div class="form-group">
                        <asp:Label ID="Label11" runat="server" Text="Loan Id :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtLoanId" runat="server" placeholder="Loan Id.."  CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                      <div class="form-group">
                        <asp:Label ID="Label6" runat="server" Text="Purpose :"   CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtPurpose" runat="server" placeholder="Loan Purpose.." CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Loan Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtLoanAmount" runat="server" placeholder="Loan Amount.." CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtLoanAmount" />
                        </div>
                    </div>
                     <div class="form-group hidden">
                        <asp:Label ID="Label7" runat="server" Text="Interest Rate :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtInterestRate" runat="server" placeholder="Rate.." CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtInterestRate" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label9" runat="server" Text="Monthly Installment :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtMonthlyInstallment" runat="server"  AutoPostBack="true"  ontextchanged="txtMonthlyInstallment_TextChanged"  
                                placeholder="Installment.." CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtMonthlyInstallment" />
                        </div>
                    </div>
                      <div class="form-group">
                        <asp:Label ID="Label10" runat="server" Text="No Of Installment :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtNoOfInstallment" runat="server"   AutoPostBack="true"  ontextchanged="txtNoOfInstallment_TextChanged" 
                                placeholder="No Of Installment.." CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtNoOfInstallment" />
                        </div>
                    </div>                    
                    <div class="form-group">
                        <asp:Label ID="Label5" runat="server" Text="Starting Month :" CssClass="col-sm-3 control-label"></asp:Label>
                         <div class="col-sm-3">
                            <asp:DropDownList ID="ddMonth" CssClass="form-control input-sm chosen-select"  data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control input-sm" MaxLength="4"></asp:TextBox>
                        </div>
                    </div>
                    
                   <%--  <div class="form-group">
                        <asp:Label ID="Label19" runat="server" Text="Pay Head Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:DropDownList ID="ddPayHeadName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Pay Head Name" runat="server"></asp:DropDownList>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group center">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btn_save_Click" Width="80px" />
                    </div>
        </div>
        </fieldset>
    </div>
  <div class="col-md-6">
        <div style="height: 300px; overflow: scroll;">
            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                    </asp:CommandField>
         

                        <asp:TemplateField HeaderText="Loan Id ">
                            <ItemTemplate>
                                <asp:Label ID="lblGridLoanId" runat="server" Text='<%# Bind("LoanId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Loan Date ">
                            <ItemTemplate>
                                <asp:Label ID="lblGridLoanDate" runat="server" Text='<%# Bind("loanDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 

                        <asp:TemplateField HeaderText="Loan Amount ">
                            <ItemTemplate>
                                <asp:Label ID="lblGridLoanAmount" runat="server" Text='<%# Bind("loanAmount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>  
                    <asp:TemplateField HeaderText="Installment ">
                            <ItemTemplate>
                                <asp:Label ID="lblGridInstallment" runat="server" Text='<%# Bind("Installment") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>    
                    <asp:TemplateField HeaderText="Inst. No ">
                            <ItemTemplate>
                                <asp:Label ID="lblGridInstallmentNo" runat="server" Text='<%# Bind("InstallmentNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    <asp:TemplateField HeaderText="Month">
                        <ItemTemplate>
                            <asp:Label ID="lblGridMonth" runat="server" Text='<%# Bind("StartMonthName") %>'></asp:Label>
                            <asp:HiddenField ID="hidGridMonthId" Value='<%# Bind("StartMonth") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Year">
                        <ItemTemplate>
                            <asp:Label ID="lblGridYear" runat="server" Text='<%# Bind("StartYear") %>'></asp:Label>
                            <asp:HiddenField ID="hidGridPurpose" Value='<%# Bind("Purpose") %>' runat="server" />
                            <asp:HiddenField ID="hidGridRemarks" Value='<%# Bind("Remark") %>' runat="server" />
                            <asp:HiddenField ID="hidGridEmpId" Value='<%# Bind("empAutoId") %>' runat="server" />
                            <asp:HiddenField ID="hidGridLoanAutoId" Value='<%# Bind("LoanAutoId") %>' runat="server" />
                            <asp:HiddenField ID="hidGridPIN" Value='<%# Bind("PIN") %>' runat="server" />
                            <asp:HiddenField ID="hidGridEmpName" Value='<%# Bind("EmployeeName") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <PagerStyle CssClass="pagination-sa" />
                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
            </asp:GridView>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-horizontal">
            <div class="form-group center">
                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
            </div>
        </div>
    </div>
  
    </div>
    
    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>
</asp:Content>
