﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmPFOpeningBalance.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmPFOpeningBalance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        PF Opening Balance
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        
        <div class="col-md-7">
                <div class="form-horizontal"> 
                        <div class="form-group">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            <asp:Button ID="btnFind" runat="server" CssClass="btn btn-warning btn-sm" Text="Find" OnClick="btnFind_Click" Width="80px" />
                        
                            <asp:Label ID="Label31" runat="server" Text="Opening Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                                </div>
                        </div>
                    </div>
       </div>

        <div class="col-md-6 hidden">
            
                <div class="form-horizontal">
                  

                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label19" runat="server" Text="Pay Head Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:DropDownList ID="ddPayHeadName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Pay Head Name" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="No Of Bonus :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtNoOfBonus" runat="server" placeholder="No Of Bonus.." CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtNoOfBonus" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                </div>

        </div>
        <div class="col-md-6 hidden" style="margin-top: 100px;">
            <fieldset>
                <legend>Employee Search..</legend>
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="From Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtSearchFromDate" CssClass="input-sm date-picker" />
                        </div>
                        <asp:Label ID="Label3" runat="server" Text="To Date :" CssClass="col-sm-2 control-label"></asp:Label>

                        <div class="col-sm-2">
                            <asp:TextBox ID="txtSearchToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                        </div>


                    </div>
                </div>
            </fieldset>
        </div>

        <div class="col-sm-12">
            <div class="form-horizontal">
                <div class="form-group center">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>
            </div>
        </div>


        <div class="col-md-7">
            <div style="height: 450px; overflow: scroll;">
                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                    CssClass="table table-striped table-bordered"   EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                        </asp:CommandField>
                        <asp:TemplateField HeaderText="PIN">
                            <ItemTemplate>
                                <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Account Head">
                            <ItemTemplate>
                                <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("AccountHead") %>'></asp:Label>
                                <asp:HiddenField ID="hidCoaId" Value='<%# Bind("CoaId") %>' runat="server" />     
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <%--<asp:Label ID="lblGridAmount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>--%>
                           
                             <asp:TextBox ID="txtAmount" runat="server" Text='<%# Eval("Amount")%>' Width="100px"></asp:TextBox>                                         
                                          <%--<asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtAmount" 
                                             Type="Double" ValidationGroup="a" runat="server" 
                                             ErrorMessage="*" 
                                          Operator="datatypecheck" ></asp:CompareValidator>--%>  
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <PagerStyle CssClass="pagination-sa" />
                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                </asp:GridView>
            </div>
        </div>


    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

</asp:Content>
