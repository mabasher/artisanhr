﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmPFOpeningBalance : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        private string sAmount = string.Empty;
        private string sTotalAmount = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");             

                commonfunctions.g_b_FillDropDownList(ddPayHeadName,
                    "PayHeadInfo",
                    "PayHeadName",
                    "PayHeadInfoId", string.Empty);                

                //v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            string empId = string.Empty;
            empId = "%";
            //if (hidEmpAutoId.Value != "0") { empId = hidEmpAutoId.Value + "X%"; }

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "ProcLoadPFOpeningBalance '" + empId + "','" + Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(txtSearchToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";
            
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;              
            gdv_costingHead.DataBind();
            sqlconnection.Close();            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            CommonFunctions.numeric_Validation NV = new CommonFunctions.numeric_Validation();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            s_save_ = "";
            foreach (GridViewRow row in gdv_costingHead.Rows)
            {
                string CoaId = string.Empty;
                string AType = string.Empty;

                CoaId = ((HiddenField)row.Cells[0].FindControl("hidCoaId")).Value;
                TextBox A = (TextBox)row.Cells[3].FindControl("txtAmount");
                AType = Convert.ToString(A.Text);
                if (AType == "")
                {
                    AType = "0";
                }               
                if (Convert.ToDouble(AType)>0)
                {
                    s_save_ = s_save_ + " Update ChartOfAccountsOpeningBalance set OpeningBalance="
                                + "'"
                                + AType
                                + "',OpeningDate='"
                                + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "' Where ChartOfAccountAutoId='" + CoaId + "'";
                }

            }

            s_save_ = s_save_ + " Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
        }

               

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //if (txtPIN.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "PIN Can Not Blank!";
            //    return false;
            //}

            //if (txtEmpName.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Employee Name Can Not Blank!";
            //    return false;
            //}                      
            
            if (gdv_costingHead.Rows.Count<1)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "List Can Not Blank!";
                return false;
            }
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFromDate.Text = "";
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
        
        protected void btnFind_Click(object sender, EventArgs e)
        {
            v_loadGridView_CostingHead();
        }


        //===========End===============         
    }
}