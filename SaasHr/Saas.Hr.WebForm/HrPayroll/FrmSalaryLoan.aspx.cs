﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmSalaryLoan : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                hidEmpAutoId.Value = "0";
                //txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                //txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                //commonfunctions.g_b_FillDropDownList(ddPayHeadName,
                //    "PayHeadInfo",
                //    "PayHeadName",
                //    "PayHeadInfoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddMonth,
                    "T_Month",
                    "MonthName",
                    "MonthNum", "Order by MonthNum");
                txtYear.Text = DateTime.Now.ToString("yyyy");
               
                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "ProcSalaryLoanSelect '" + hidEmpAutoId.Value + "'";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;                
            gdv_costingHead.DataBind();
            sqlconnection.Close();            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {                
                s_Update = "[ProcSalaryLoanINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtLoanId.Text.Trim())
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtPurpose.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtLoanAmount.Text.Trim())
                        + "','0','"
                        + HttpUtility.HtmlDecode(txtMonthlyInstallment.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtNoOfInstallment.Text.Trim())
                        + "','"
                        + ddMonth.SelectedItem.Text
                        + "','"
                        + HttpUtility.HtmlDecode(txtYear.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        Response.Redirect(Request.RawUrl);
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcSalaryLoanINSERT]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txtLoanId.Text.Trim())
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtPurpose.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtLoanAmount.Text.Trim())
                        + "','0','"
                        + HttpUtility.HtmlDecode(txtMonthlyInstallment.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtNoOfInstallment.Text.Trim())
                        + "','"
                        + ddMonth.SelectedItem.Text
                        + "','"
                        + HttpUtility.HtmlDecode(txtYear.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "','I'";
                    s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;

                        Response.Redirect(Request.RawUrl);
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridLoanAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                ddMonth.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridMonthId")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }
            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }
            if (txtLoanId.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Loan Id Can Not Blank!";
                return false;
            }
            if (txtLoanAmount.Text == "" || txtLoanAmount.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Amount Can Not Blank!";
                return false;
            }
            if (ddMonth.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Month Name Can Not Blank!";
                return false;
            }
            if (txtYear.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Year Can Not Blank!";
                return false;
            }



            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtLoanId.Text = string.Empty;
            txtMonthlyInstallment.Text = string.Empty;
            txtNoOfInstallment.Text = string.Empty;
            txtLoanAmount.Text = string.Empty;
            txtPurpose.Text = string.Empty;
            txtAdditionalNote.Text = string.Empty;
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        hidCardid.Value= drow["CardNo"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_CostingHead();
        }

        protected void txtNoOfInstallment_TextChanged(object sender, EventArgs e)
        {
            string sval1 = string.Empty;
            string sval2 = string.Empty;
            if (txtLoanAmount.Text == "")
            { sval1 = "0"; }
            else
            { sval1 = txtLoanAmount.Text; }
            if (txtNoOfInstallment.Text == "")
            { sval2 = "0"; }
            else
            { sval2 = txtNoOfInstallment.Text; }

            txtMonthlyInstallment.Text = Convert.ToString(Convert.ToDouble(sval1) / Convert.ToDouble(sval2));
            txtMonthlyInstallment.Text = Convert.ToString(Convert.ToDecimal(txtMonthlyInstallment.Text).ToString("####.00"));
        }
        protected void txtMonthlyInstallment_TextChanged(object sender, EventArgs e)
        {
            string sval1 = string.Empty;
            string sval2 = string.Empty;
            if (txtLoanAmount.Text=="")
            { sval1 = "0"; }
            else
            {sval1 = txtLoanAmount.Text; }
            if (txtMonthlyInstallment.Text == "")
            { sval2 = "0"; }
            else
            { sval2 = txtMonthlyInstallment.Text; }

            txtNoOfInstallment.Text = Convert.ToString(Convert.ToDouble(sval1)/ Convert.ToDouble(sval2));
            txtNoOfInstallment.Text = Convert.ToString(Convert.ToDecimal(txtNoOfInstallment.Text).ToString("####"));

        }




        //===========End===============         
    }
}