﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrPayroll
{
    public partial class FrmSalaryApproval : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txteffectiveDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                v_loadGridView_CostingHead();

            }
        }
        
        private void v_loadGridView_CostingHead()
        {           
            
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            //qry = "SELECT SalaryInfoId,sGross,sBasic,sHR,sConveyance,sMedical,sPF,sFood,sTransportation,sOtherAllowance,sEffectiveDate,sIncrDate,sAccountNumber,sPaymentMode,sIsTaxxable,sIsOTPayable,sRoutingNumber From SalaryInfoDummy Where sExported='N' order by sEffectiveDate desc";

            qry = "ProcGetSalaryForApproval";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;             
            gdv_costingHead.DataBind();
            sqlconnection.Close();            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            s_save_ = "";
            foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            {
                string s_empAutoId = string.Empty;
                string s_salaryInfoId = string.Empty;
                
                s_empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hid_GridEmpAutoId")).Value;
                s_salaryInfoId = ((HiddenField)gdvRow.Cells[0].FindControl("hid_GridSalaryInfoId")).Value;
                
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
                if (checkbox.Checked == true)
                {
                    s_save_ = s_save_+ " Execute ProcSalaryInfoINSERTFromDummy"
                            + "'"
                            + s_salaryInfoId
                            + "','"
                            + s_empAutoId
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "','"
                            + SessionUserIP
                            + "'";
                    
                     }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    //lblMsg.Visible = true;
                    //lblMsg.Text = "Successfully Done!";
                    //InsertMode();
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
            ///Insert Option

        }
                
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            s_save_ = "";
            foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            {
                string s_empAutoId = string.Empty;
                string s_salaryInfoId = string.Empty;

                s_empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hid_GridEmpAutoId")).Value;
                s_salaryInfoId = ((HiddenField)gdvRow.Cells[0].FindControl("hid_GridSalaryInfoId")).Value;

                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
                if (checkbox.Checked == true)
                {
                    s_save_ = s_save_ + " Execute ProcSalaryInfoDummyDELETE"
                            + "'"
                            + s_salaryInfoId
                            + "','"
                            + s_empAutoId
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "','"
                            + SessionUserIP
                            + "'";

                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    //lblMsg.Visible = true;
                    //lblMsg.Text = "Successfully Done!";
                    //InsertMode();
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
            ///Insert Option

        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            hidEmpAutoId.Value = "%";
            hidDepartmentId.Value = "%";
            hidSectionId.Value = "%";
            hidLineId.Value = "%";
            hidpresent.Value = "0";
            hidlate.Value = "0";
            hidweekEndOff.Value = "0";
            hidleaveWithPay.Value = "0";
            hidleaveWithoutPay.Value = "0";
            hidgovtleave.Value = "0";
            hidabsent.Value = "0";
            hidInputdays.Value = "0";

            //if (txtPresent.Text != ""){ hidpresent.Value = txtPresent.Text; }
            //if (txtlate.Text != "") {hidlate.Value = txtlate.Text; }
            //if (txtweekend.Text != ""){ hidweekEndOff.Value = txtweekend.Text; }
            //if (txtleave.Text != "") { hidleaveWithPay.Value = txtleave.Text; }
            //if (txtwp.Text != "") { hidleaveWithoutPay.Value = txtwp.Text; }
            //if (txtgl.Text != "") { hidgovtleave.Value = txtgl.Text; }
            //if (txtabsent.Text != "") { hidabsent.Value = txtabsent.Text; }
            //hidInputdays.Value = Convert.ToString(Convert.ToInt32(hidpresent.Value)+ Convert.ToInt32(hidweekEndOff.Value)+ Convert.ToInt32(hidleaveWithPay.Value)+ Convert.ToInt32(hidleaveWithoutPay.Value)+ Convert.ToInt32(hidgovtleave.Value) + Convert.ToInt32(hidabsent.Value));
            //hidDaysInMonth.Value=Convert.ToString(DateTime.DaysInMonth(Convert.ToInt32(txtYear.Text), Convert.ToInt32(ddMonth.SelectedValue)));
            
            //if (Convert.ToInt32(hidInputdays.Value) != Convert.ToInt32(hidDaysInMonth.Value))
            //{ return false; } 

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
        }
        

        //===========End===============         
    }
}