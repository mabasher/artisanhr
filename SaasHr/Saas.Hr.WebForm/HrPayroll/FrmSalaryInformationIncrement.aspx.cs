﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrPayroll
{

    public partial class FrmSalaryInformationIncrement : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        public double dWorkerExistingHrAmount = 0;
        public double dincrnewAmount = 0;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txteffectiveDate.Text = DateTime.Now.ToString();
                txtIncrDate.Text = DateTime.Now.ToString();

                v_loadGridSalary();
                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("SELECT SalaryInfoId,sGross,sBasic,sHR,sConveyance,sMedical,sPF,sFood,sTransportation,sOtherAllowance,sEffectiveDate,sIncrDate,sAccountNumber,sPaymentMode,sIsTaxxable,sIsOTPayable,sRoutingNumber,sIsFixedTransport From SalaryInfoDummy Where EmpAutoId='" + hidEmpAutoId.Value + "' order by sEffectiveDate desc", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }

        private void v_loadGridSalary()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("SELECT top 1 SalaryInfoId,sGross,sBasic,sHR,sConveyance,sMedical,sPF,sFood,sTransportation,sOtherAllowance,sEffectiveDate,sIncrDate,sAccountNumber,sPaymentMode,sIsTaxxable,sIsOTPayable,sRoutingNumber,sIsFixedTransport From SalaryInfo Where EmpAutoId='" + hidEmpAutoId.Value + "' and sEffectiveDate=(Select Max(s.sEffectiveDate) From SalaryInfo s Where  s.EmpAutoId='" + hidEmpAutoId.Value + "') ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvSalary.DataSource = ds;
            gdvSalary.DataBind();
            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcSalaryInfoINSERT-1]"
                        + "'"
                        + hidtrAutoId.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','0','0','"
                        + HttpUtility.HtmlDecode(txtGross.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBasic.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtHousing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtConveyance.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMedical.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPF.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtFood.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtTransportation.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOtherAllowance.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txteffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtIncrDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtBankAccount.Text.Trim())
                        + "','"
                        + hidPaymentMode.Value
                        + "','"
                        + hidTaxxable.Value
                        + "','"
                        + hidOTPayable.Value
                        + "','Y','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U','"
                        + HttpUtility.HtmlDecode(txtRoutingNo.Text.Trim()) + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcSalaryInfoDummyINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','0','0','"
                        + HttpUtility.HtmlDecode(txtGross.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBasic.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtHousing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtConveyance.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMedical.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPF.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtFood.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtTransportation.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOtherAllowance.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txteffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtIncrDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtBankAccount.Text.Trim())
                        + "','"
                        + hidPaymentMode.Value
                        + "','"
                        + hidTaxxable.Value
                        + "','"
                        + hidOTPayable.Value
                        + "','"
                        + hidFixedTransport.Value
                        + "','N','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I','"
                        + HttpUtility.HtmlDecode(txtRoutingNo.Text.Trim())
                        + "'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }


        protected void gdvSalary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvSalary, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdvSalary_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidtrAutoId.Value = "0";
                //hidtrAutoId.Value = ((HiddenField)gdvSalary.Rows[gdv_costingHead.SelectedIndex].FindControl("hidSalaryInfoId")).Value;
                double dIncrRate = 0;
                double dIncrAmount = 0;

                if (txtIncrRate.Text == "") { txtIncrRate.Text = "0"; }
                if (txtIncrAmount.Text == "") { txtIncrAmount.Text = "0"; }
                dIncrRate = Convert.ToDouble(txtIncrRate.Text);
                dIncrAmount = Convert.ToDouble(txtIncrAmount.Text);

                if (dIncrRate > 0 || dIncrAmount > 0)
                {
                    string dHr = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsHR")).Text;
                    dWorkerExistingHrAmount = Convert.ToDouble(dHr);
                    string dBasic = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsBasic")).Text;
                    string dGross = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsGross")).Text;

                    if (dIncrAmount > 0)
                    {
                        if (hidEmpType.Value == "Worker")
                        {
                            txtBasic.Text = Convert.ToString(Convert.ToDouble(dBasic) + dIncrAmount);
                        }
                        else
                        {
                            txtGross.Text = Convert.ToString(Convert.ToDouble(dGross) + dIncrAmount);
                        }

                    }
                    else
                    {
                        if (hidEmpType.Value == "Worker")
                        {
                            dIncrAmount = Convert.ToDouble(dBasic) * dIncrRate / 100;
                            txtBasic.Text = Convert.ToString(Convert.ToDouble(dBasic) + dIncrAmount);
                        }
                        else
                        {
                            dIncrAmount = Convert.ToDouble(dGross) * dIncrRate / 100;
                            txtGross.Text = Convert.ToString(Convert.ToDouble(dGross) + dIncrAmount);
                        }
                    }

                    //txtGross.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsGross")).Text;
                    //dIncrAmount = Convert.ToDouble(txtGross.Text) * dIncrRate / 100;
                    //txtGross.Text = Convert.ToString(Convert.ToDouble(txtGross.Text)+ dIncrAmount);

                    txtOtherAllowance.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsOtherAllowance")).Text;
                    txtConveyance.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsConveyance")).Text;
                    txtTransportation.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsTransportation")).Text;
                    //txtGross_TextChanged(sender,e);// OLd system
                    getSalaryBreakDown();//New System

                    //txtBasic.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsBasic")).Text;
                    //txtHousing.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsHR")).Text;
                    //txtConveyance.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsConveyance")).Text;
                    //txtMedical.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsMedical")).Text;
                    //txtPF.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsPF")).Text;
                    //txtFood.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsFood")).Text;
                    //txtTransportation.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsTransportation")).Text;
                    //txteffectiveDate.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsEffectiveDate")).Text;
                    //txtIncrDate.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsIncrDate")).Text;

                    txtBankAccount.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsAccountNumber")).Text;
                    txtRoutingNo.Text = ((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsRoutingNumber")).Text;
                    if (((HiddenField)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("hidsPaymentMode")).Value == "B")
                    {
                        chkBank.Checked = true;
                        chkCash.Checked = true;
                    }
                    if (((HiddenField)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("hidsPaymentMode")).Value == "C")
                    {
                        chkCash.Checked = true;
                        chkBank.Checked = false;
                    }
                    chktaxxable.Checked = false;
                    if (((HiddenField)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("hidsIsTaxxable")).Value == "Y")
                    {
                        chktaxxable.Checked = true;
                    }
                    chkOT.Checked = false;
                    if (((HiddenField)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("hidsIsOTPayable")).Value == "Y")
                    {
                        chkOT.Checked = true;
                    }
                    chkFixedTransport.Checked = false;
                    if (((HiddenField)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("hidsIsFixedTransport")).Value == "Y")
                    {
                        chkFixedTransport.Checked = true;
                    }


                }
                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidtrAutoId.Value = "0";
                //hidtrAutoId.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridSalaryInfoId")).Value;
                //txtGross.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridGross")).Text;
                //txtBasic.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridBasic")).Text;
                //txtHousing.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridHR")).Text;
                //txtConveyance.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridConveyance")).Text;
                //txtMedical.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridMedical")).Text;
                //txtPF.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridPF")).Text;
                //txtFood.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridFood")).Text;
                //txtTransportation.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridTransportation")).Text;
                //txtOtherAllowance.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridOtherAllowance")).Text;

                //txteffectiveDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEffectiveDate")).Text;
                //txtIncrDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridIncrDate")).Text;
                //txtBankAccount.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAccountNumber")).Text;
                //txtRoutingNo.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridsRoutingNumber")).Text;
                //if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridPaymentMode")).Value=="B")
                //{
                //chkBank.Checked = true;
                //}
                //if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridPaymentMode")).Value == "C")
                //{
                //    chkCash.Checked = true;
                //}
                //if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridTaxxable")).Value == "Y")
                //{
                //    chktaxxable.Checked = true;
                //}
                //if (((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridOTPayable")).Value == "Y")
                //{
                //    chkOT.Checked = true;
                //}

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();


            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }
            if (txtGross.Text == "") { txtGross.Text = "0"; }
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }

            if (Convert.ToDouble(txtGross.Text) < 1)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Gross Can Not Blank!";
                return false;
            }

            hidFixedTransport.Value = "N";
            if (chkFixedTransport.Checked) { hidFixedTransport.Value = "Y"; }
            hidTaxxable.Value = "N";
            if (chktaxxable.Checked) { hidTaxxable.Value = "Y"; }
            hidOTPayable.Value = "N";
            if (chkOT.Checked) { hidOTPayable.Value = "Y"; }
            hidPaymentMode.Value = "C";
            if (chkBank.Checked)
            {
                hidPaymentMode.Value = "B";
                if (txtBankAccount.Text == "")
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Bank Account Can Not Blank!";
                    return false;
                }
                if (txtRoutingNo.Text == "")
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Routing No Can Not Blank!";
                    return false;
                }
            }


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtGross.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtBankAccount.Text = string.Empty;
            txtBasic.Text = string.Empty;
            txtConveyance.Text = string.Empty;
            txtPF.Text = string.Empty;
            txtFood.Text = string.Empty;
            txtTransportation.Text = string.Empty;
            txtMedical.Text = string.Empty;
            txtHousing.Text = string.Empty;
            txtRoutingNo.Text = string.Empty;
            chkBank.Checked = false;
            chkCash.Checked = false;
            chkOT.Checked = false;
            chktaxxable.Checked = false;
            chkFixedTransport.Checked = false;
        }

        protected void txtGross_TextChanged(object sender, EventArgs e)
        {
            if (txtGross.Text == "") { txtGross.Text = "0"; }
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }
            if (Convert.ToDouble(txtGross.Text) > 0)
            {
                //if (hidEmpType.Value == "Worker")
                //{
                //    txtBasic.Text = Convert.ToString((Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text)) / 1.5);
                //}
                //else
                //{
                //    txtBasic.Text = Convert.ToString((Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text)) * 0.6);
                //}
                getSalaryBreakDown();
            }
            txtOtherAllowance.Focus();
        }

        protected void txtOtherAllowance_TextChanged(object sender, EventArgs e)
        {
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }
            if (txtGross.Text == "") { txtGross.Text = "0"; }
            if (Convert.ToDouble(txtGross.Text) > 0)
            {
                //if (hidEmpType.Value == "Worker")
                //{
                //    txtBasic.Text = Convert.ToString((Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text)) / 1.5);
                //}
                //else
                //{
                //    txtBasic.Text = Convert.ToString((Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text)) * 0.6);
                //}
                getSalaryBreakDown();
            }
            txteffectiveDate.Focus();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            //s_select = "SELECT * FROM EmployeePersonalInfo e left outer join  Where EmployeePersonalInfoId='" + sEmpId + "'";
            //s_select = "SELECT 'PIN :' + PIN + char(13) + 'Name :' + EmployeeName + char(13) + 'Department :' + d.Department + char(13) + 'Emp Type :' + et.TypeName as empName FROM EmployeeOfficialInfo O left outer join EmployeePersonalInfo e ON O.EmpAutoId = e.EmployeePersonalInfoId left outer join DepartmentInfo d on O.DepartmentAutoId = d.DepartmentInfoId  left outer join EmployeeType et on O.EmployeeTypeAutoId = et.EmployeeTypeId   Where EmployeePersonalInfoId ='" + sEmpId + "'";
            s_select = "SELECT  'Department :' + d.Department + char(13) + 'Emp Type :' + et.TypeName as empName,d.Department as empDept,et.TypeName as empType FROM EmployeeOfficialInfo O left outer join EmployeePersonalInfo e ON O.EmpAutoId = e.EmployeePersonalInfoId left outer join DepartmentInfo d on O.DepartmentAutoId = d.DepartmentInfoId  left outer join EmployeeType et on O.EmployeeTypeAutoId = et.EmployeeTypeId   Where EmployeePersonalInfoId ='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        //txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["empName"].ToString();
                        //txtEmpName.Text = txtPIN.Text + " " + txtEmpName.Text;
                        hidEmpType.Value = drow["empType"].ToString();
                        hidEmpDepartment.Value = drow["empDept"].ToString();
                        //join Date for Next Incr Date Calculation
                    }
                }
            }

            v_loadGridSalary();
            v_loadGridView_CostingHead();


        }


        private void getSalaryBreakDown()
        {
            string sOA = string.Empty;
            string dOthers, tempHrAmount = string.Empty;
            sOA = "0";
            dOthers = "0";
            if (txtOtherAllowance.Text == "") { txtOtherAllowance.Text = "0"; }
            if (hidEmpType.Value == "Worker" && Convert.ToDouble(txtBasic.Text) > 0)
            {
                //dOthers = Convert.ToString(Convert.ToDouble(txtGross.Text) - 1850);
                //dOthers = Convert.ToString(Convert.ToDouble(dOthers) - Convert.ToDouble(txtOtherAllowance.Text));
                //txtBasic.Text = Convert.ToString(Convert.ToDouble(dOthers) /1.5);
                //txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                //txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .4);
                tempHrAmount = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .4);
                if (dWorkerExistingHrAmount > Convert.ToDouble(tempHrAmount))
                {
                    tempHrAmount = dWorkerExistingHrAmount.ToString();
                }
                txtHousing.Text = tempHrAmount;
                txtPF.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .08);
                txtConveyance.Text = "0";
                txtFood.Text = "900";
                txtMedical.Text = "600";
                txtTransportation.Text = "350";
                txtGross.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text)
                                                + Convert.ToDouble(txtHousing.Text)
                                                + Convert.ToDouble(txtMedical.Text)
                                                + Convert.ToDouble(txtConveyance.Text)
                                                + Convert.ToDouble(txtFood.Text)
                                                + Convert.ToDouble(txtTransportation.Text)
                                                + Convert.ToDouble(txtOtherAllowance.Text)
                                                );
            }
            else if (hidEmpDepartment.Value == "Sales" && Convert.ToDouble(txtGross.Text) > 0)
            {
                sOA = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text));
                txtBasic.Text = Convert.ToString(Convert.ToDouble(sOA) * .6);
                txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                txtMedical.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .1);
                txtPF.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .08);
                txtConveyance.Text = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtBasic.Text) - Convert.ToDouble(txtHousing.Text) - Convert.ToDouble(txtMedical.Text));
                txtFood.Text = "150";
                txtTransportation.Text = "0";
            }
            else
            {
                if (Convert.ToDouble(txtGross.Text) > 0)
                {
                    sOA = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtOtherAllowance.Text));
                    txtBasic.Text = Convert.ToString(Convert.ToDouble(sOA) * .6);
                    txtHousing.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .5);
                    txtMedical.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .1);
                    txtPF.Text = Convert.ToString(Convert.ToDouble(txtBasic.Text) * .08);
                    txtConveyance.Text = Convert.ToString(Convert.ToDouble(txtGross.Text) - Convert.ToDouble(txtBasic.Text) - Convert.ToDouble(txtHousing.Text) - Convert.ToDouble(txtMedical.Text));
                    txtFood.Text = "0";
                    txtTransportation.Text = "0";
                }
            }
            if (Convert.ToDouble(txtBasic.Text) > 0)
            {
                txtBasic.Text = Convert.ToString(Convert.ToDecimal(txtBasic.Text).ToString("####.00"));
                txtMedical.Text = Convert.ToString(Convert.ToDecimal(txtMedical.Text).ToString("####.00"));
                txtHousing.Text = Convert.ToString(Convert.ToDecimal(txtHousing.Text).ToString("####.00"));
                txtPF.Text = Convert.ToString(Convert.ToDecimal(txtPF.Text).ToString("####.00"));
                txtConveyance.Text = Convert.ToString(Convert.ToDecimal(txtConveyance.Text).ToString("####.00"));
                txtFood.Text = Convert.ToString(Convert.ToDecimal(txtFood.Text).ToString("####.00"));
                txtTransportation.Text = Convert.ToString(Convert.ToDecimal(txtTransportation.Text).ToString("####.00"));
            }

        }

        protected void txtIncrAmount_TextChanged(object sender, EventArgs e)
        {
            double dIncrRate = 0;
            double dIncrAmount = 0;
            double dBasic = 0;
            // double dBasic = 0;

            //dIncrRate = Convert.ToDouble(txtIncrRate.Text);
            //dIncrAmount = Convert.ToDouble(txtIncrAmount.Text);
            //dBasic = Convert.ToDouble(((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsBasic")).Text);

            if (Convert.ToDouble(txtIncrAmount.Text) > 0)
            {
                //txtIncrRate.Text = Convert.ToDouble(dIncrAmount) * 100 / Convert.ToDouble(Convert.ToString(dBasic));
            }

        }

        protected void txtIncrRate_TextChanged(object sender, EventArgs e)
        {  double dIncrRate = 0;
            double dIncrAmount = 0;
            double dBasic = 0;
            dIncrRate = Convert.ToDouble(txtIncrRate.Text);
            dIncrAmount = Convert.ToDouble(txtIncrAmount.Text);
            dBasic = Convert.ToDouble(((Label)gdvSalary.Rows[gdvSalary.SelectedIndex].FindControl("lblsBasic")).Text);

        }
        //===========End===============         
    }
}