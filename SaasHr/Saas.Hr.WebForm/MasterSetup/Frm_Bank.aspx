﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="Frm_Bank.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.Frm_Bank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Bank Information
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>

            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label ID="lbl_GroupName" runat="server" Text="Name :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_Name" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label12" runat="server" Text="Code :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label7" runat="server" Text="Branch Name :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtBranch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Address :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <asp:Label ID="Label13" runat="server" Text="A/C No :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtACNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label14" runat="server" Text="Swift Code :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtSwiftCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                          <div class="form-group">
                            <asp:Label ID="Label10" runat="server" Text="Routing no :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtRoutingNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Contact Person :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtContactPerson" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Country :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="Post Code :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtPostCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text="Email :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text="Phone :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label8" runat="server" Text="Web Site :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtWebSite" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label9" runat="server" Text="Remarks :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row row-custom">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="text-danger">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group">

                            <asp:Label ID="Label11" runat="server" Text="Bank :" CssClass="col-sm-2 col-lg-1 control-label align-left"></asp:Label>

                            <div class="col-sm-4 col-lg-3">
                                <asp:TextBox ID="txtBankSearch" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0px;"
                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                    CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" OnPageIndexChanging="gdv_costingHead_PageIndexChanging" ShowHeaderWhenEmpty="True">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Bank Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridSuppliername" runat="server" Text='<%# Bind("Bankname") %>'></asp:Label>
                                                <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("Bank_Id") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Branch Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridBranch" runat="server" Text='<%# Bind("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridSupplierAdress" runat="server" Text='<%# Bind("BankAdress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                   

                                        <asp:TemplateField HeaderText="A/C No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridAC01" runat="server" Text='<%# Bind("ACNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="RoutingNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridRoutingNo" runat="server" Text='<%# Bind("RoutingNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Swift Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridSwiftCode" runat="server" Text='<%# Bind("SwiftCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Contact_Person">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridContact_Person" runat="server" Text='<%# Bind("Contact_Person") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PostalCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridPostalCode" runat="server" Text='<%# Bind("PostalCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridCountry" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="phone">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridphone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Email">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Website">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridWebsite" runat="server" Text='<%# Bind("Website") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Remaks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridRemaks" runat="server" Text='<%# Bind("Remaks") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UserAutoId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserAutoId" runat="server" Text='<%# Bind("UserAutoId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-sa" />
                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_GridItemAutoId" runat="server" Value="True" />
        <asp:HiddenField ID="hid_GridUserAutoId" runat="server" Value="True" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>