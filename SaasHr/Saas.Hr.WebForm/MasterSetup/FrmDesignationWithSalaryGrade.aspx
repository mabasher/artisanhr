﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmDesignationWithSalaryGrade.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.FrmDesignationwithSalaryGrade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Designation With Salary Grade Setup
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="Label27" runat="server" Text="Designation :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label29" runat="server" Text="Salary Grade :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-5">
                                        <asp:DropDownList ID="ddSalaryGrade" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Effective Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-5">
                                       <asp:TextBox runat="server" ID="txtEffectiveDate" CssClass="input-sm date-picker" />
                                    </div>
                                </div>
                                
                                <div class="form-group center" style="margin-top:50px;">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                                
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                                </div>

                            </div>
                        </div>
                               
                    <div class="col-md-6">
                        <div style="height: 450px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SalaryGrade">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridSalaryGrade" runat="server" Text='<%# Bind("SalaryGrade") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Effective Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEffectiveDate" runat="server" Text='<%# Bind("EffectiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidDesignationId" Value='<%# Bind("DesignationId") %>' runat="server" />
                                            <asp:HiddenField ID="hidSalaryGradeId" Value='<%# Bind("SalaryGradeId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
         

            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    
</asp:Content>
