﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class Frm_HSCodeWiseExpense : Page
    {
        string filename = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(dd_user,
                    "T_CompanyInfo",
                    "compName",
                    "autoId", string.Empty);
                                
                v_loadGridView_Company();
            }
        }
        

        private void v_loadGridView_Company()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            
            SqlCommand cmd = new SqlCommand("select AutoId as AutoId, ItemCode as Code, ItemName as Product  from T_PtoductMaster Order by ItemName asc", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvCompanyList.DataSource = ds;
            gdvCompanyList.AllowPaging = true;
            gdvCompanyList.PageSize = 100;
            gdvCompanyList.DataBind();

            sqlconnection.Close();
        }


        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }


            if (btn_save.Text == "Update")
            {
                s_Update = "[proc_CommonFormForDrivedTbl_DMLqqqq]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + SessionCompanyId
                + "','"
                + dd_user.SelectedValue
                + "','T_Inv_SubCategory','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {

                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        v_loadGridView_Company();
                        InsertMode();

                    }
                }
            }
            else
            {
                string s_select = string.Empty;
                string s_Select_returnValue = string.Empty;
                string s_trautoId = string.Empty;
                foreach (GridViewRow gdvRow in gdvCompanyList.Rows)
                {
                    CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                    if (checkbox.Checked == true)
                    {
                        s_trautoId = ((HiddenField)gdvCompanyList.Rows[gdvRow.RowIndex].FindControl("hid_GridItemAutoId")).Value;
                        s_select = s_select + " INSERT INTO T_CompanyWiseProduct VALUES((Select isnull(max(AutoId),0)+1 From T_CompanyWiseProduct),'" + dd_user.SelectedValue + "','" + s_trautoId + "')";
                    }
                }
                s_save_ = "Delete T_CompanyWiseProduct where UserId='" + dd_user.SelectedValue +"'";
                s_save_ = s_save_ + " "+ s_select+" Select ''";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        v_loadGridView_Company();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }
        private void InsertMode_Msg()
        {

            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            string s_op = "N";
            if (dd_user.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "User Can Not Blank!";
                return false;
            }

            foreach (GridViewRow gdvRow in gdvCompanyList.Rows)
            {
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true)
                {
                    s_op = "Y";
                }
            }

            if (s_op == "N")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "No Company Selected!!";
                return false;
            }


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            dd_user.SelectedValue = "0";
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //CommonFunctions commonFunctions = new CommonFunctions();
            //SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            //sqlconnection.Open();

            //SqlCommand cmd = new SqlCommand("select sc.AutoId as AutoId, CategoryName as CategoryName,SubCategory as SubCategoryName, sc.code as Code, sc.Company_Id as Company_Id,c.AutoId as CategoryId from T_Inv_SubCategory sc left outer join T_InvCategory c ON sc.CategoryId=c.AutoId ", sqlconnection);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //da.Fill(ds);
            //gdv_costingHead.PageIndex = e.NewPageIndex;
            //gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            //gdv_costingHead.PageSize = 8;
            //gdv_costingHead.DataBind();
            //sqlconnection.Close();
        }

        protected void dd_user_SelectedIndexChanged(object sender, EventArgs e)
        {
            ////For Color            
            Connection connection = new Connection();
            string s_returnValue = string.Empty;
            string s_Select = string.Empty;
            string s_SelectedId = string.Empty;

            s_Select = "SELECT isnull(ProductId,0) as ProductId FROM T_CompanyWiseProduct Where Company_Id='" + dd_user.SelectedValue + "';";
            s_returnValue = connection.connection_DB(s_Select, 0, false, false, false);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        foreach (GridViewRow gdvRow in gdvCompanyList.Rows)
                        {
                            CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                            s_SelectedId = ((HiddenField)gdvCompanyList.Rows[gdvRow.RowIndex].FindControl("hid_GridItemAutoId")).Value;
                            if (s_SelectedId == drow["ProductId"].ToString())
                            {
                                checkbox.Checked = true;
                            }
                        }

                    }
                }
            }
            ///For Color
        }


    }
}