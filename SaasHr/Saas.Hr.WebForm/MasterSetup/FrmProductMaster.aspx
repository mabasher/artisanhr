﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmProductMaster.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.FrmProductMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Product Master
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="col-md-12">
        <div class="row row-custom">
            <div class="form-horizontal">
                <div class="col-md-4">

                    <div class="form-group">

                        <asp:Label ID="Label22" runat="server" Text="" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtSearch" runat="server"  AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Item Search..." CssClass="form-control input-sm"></asp:TextBox>
                            <asp:HiddenField ID="hidProductId" runat="server" Value="" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Item Category :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="dd_Category" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Item Code :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Item Name :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label9" runat="server" Text="Sub Category :" title="Item Sub Category" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddItemSubCategory" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Primary Unit :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="dd_PrimaryUnit" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label17" runat="server" Text="Brand :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddBrand" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label6" runat="server" Text="Size :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddSize" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" Text="Color :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddColor" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label23" runat="server" Text="Part Number :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txtPartNumber" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label10" runat="server" Text="Item Type :" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddItemType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label11" runat="server" Text="Sub Type :" title="Item Sub Type" CssClass="col-sm-5 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddItemSubType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="col-md-4">
                    <div class="form-group">
                        <asp:Label ID="Label12" runat="server" Text="Item Class :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddItemClass" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label13" runat="server" Text="Origin :" title="Item Origin Name" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddItemSourceName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label27" runat="server" Text="Moving Status :" title="Item Moving Status" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddMovingStatus" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label28" runat="server" Text="user Item Type :" title="User Item Type" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddUserItemType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label" runat="server" Text="Consum. Type :" title="Consumption Type" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddConsumptionType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label18" runat="server" Text="Store Name :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddStoreName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label19" runat="server" Text="Shelf Location :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddShelfLocation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label3" runat="server" Text="Secondary Unit :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddSecondaryUnit" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label5" runat="server" Text="Base Unit :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddBaseUnit" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="Conver. Factor :" title="Conversion Factor" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txtConversionFactor" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label14" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:CheckBox ID="chkUseLot" runat="server" Width="90px" Text="Use Lot" />
                            <asp:CheckBox ID="chkLotDivisible" runat="server" Width="100px" Text="Lot Divisible" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label16" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:CheckBox ID="chkSerialControl" runat="server" Width="150px" Text="Serial Control" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="col-md-4">
                    <div class="form-group">
                        <asp:Label ID="Label21" runat="server" Text="Self Life :" title="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-1">
                            <asp:CheckBox ID="chkSelfLife" runat="server" onclick="checkLifeStatus()" Text="" />
                        </div>
                        <div class="col-sm-5 hidden" id="dv_SelfLifeDays">
                            <asp:TextBox ID="txt_SelfLifeDays" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label15" runat="server" Text="Planner :" title="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-1">
                            <asp:CheckBox ID="chkPlanner" runat="server" onclick="checkPlanner()" Text="" />
                        </div>
                        <div class="col-sm-5 hidden" id="dv_Planner">
                            <asp:TextBox ID="txtPlanner" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label24" runat="server" Text="Min Level :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtMinLevel" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label25" runat="server" Text="Max Level :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtMaxLevel" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label26" runat="server" Text="Safety Level :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtSafetyLevel" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label32" runat="server" Text="Re-order level :" title="Re-order level Qty" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtReOrderlevelQty" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label20" runat="server" Text="HS Code :" title="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-1">
                            <asp:CheckBox ID="chkHSCodeEnable" runat="server" onclick="checkHSCode()" Text="" />
                        </div>
                        <div class="col-sm-5 hidden" id="dv_HSCode">
                            <asp:TextBox ID="txtHSCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label34" runat="server" Text="Cycle Counting :" title="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-1">
                            <asp:CheckBox ID="chkCycleCounting" runat="server" onclick="chkCycleCounting()" Text="" />
                        </div>
                        <div class="col-sm-2 hidden" id="dv_CycleCounting">
                            <asp:TextBox ID="txtCycleCounting" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label30" runat="server" Text="Rate in BDT :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtRateInBDT" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label31" runat="server" Text="Amount in BDT :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtAmountInBDT" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label33" runat="server" Text="Account Ledger :" title="A/C Ledger" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddItemExpenseAccountLedger" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Category" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label29" runat="server" Text="Additional Info. :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top:50px;">
        <div class="row row-custom">
            <div class="form-horizontal">
                <div class="col-md-4">
                </div>
            </div>
            <div class="form-horizontal">
                <div class="col-md-4">
                    <div class="row row-custom">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="text-danger">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="col-md-4">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script src="../Scripts/jquery-ui-1.12.1.min.js"></script>
    <script src="../Scripts/jquery-ui-1.12.1.js"></script>



    <script>
        function checkLifeStatus() {
            if (document.getElementById('<%=chkSelfLife.ClientID %>').checked) {
                document.getElementById('dv_SelfLifeDays').classList.remove('hidden');

            } else {
                document.getElementById('dv_SelfLifeDays').classList.add('hidden');
            }
        }
        function checkPlanner() {
            if (document.getElementById('<%=chkPlanner.ClientID %>').checked) {
                document.getElementById('dv_Planner').classList.remove('hidden');

            } else {
                document.getElementById('dv_Planner').classList.add('hidden');
            }
        }
        function checkHSCode() {
            if (document.getElementById('<%=chkHSCodeEnable.ClientID %>').checked) {
                document.getElementById('dv_HSCode').classList.remove('hidden');

            } else {
                document.getElementById('dv_HSCode').classList.add('hidden');
            }
        }
        function chkCycleCounting() {
            if (document.getElementById('<%=chkCycleCounting.ClientID %>').checked) {
                document.getElementById('dv_CycleCounting').classList.remove('hidden');

            } else {
                document.getElementById('dv_CycleCounting').classList.add('hidden');
            }
        }
        
    </script>
        
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/ProductService.asmx/GetProducts") %>',
                '#<%=hidProductId.ClientID %>');
        });
    </script>

</asp:Content>