﻿<%@ Page Title="Performance Bonus Setup" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmPerformanceBonusSetup.aspx.cs" Inherits="Saas.Hr.WebForm.HrAttendance.FrmPerformanceBonusSetup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Performance Bonus Setup
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="form-group ">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-3 ">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group ">
                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 20px;">
                                <asp:Label ID="Label18" runat="server" Text="Effective Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txtEffectiveDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Deposite Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txtDepositeDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtAmount" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="TDS :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtTDS" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtTDS" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Bank Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtBankAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                        ValidChars="." TargetControlID="txtBankAmount" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Challan No :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtChallanNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                              <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Bank Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                   <asp:DropDownList ID="ddBankName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Bank Name" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="Bank Branch Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                   <asp:DropDownList ID="ddBankBranchName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Bank Name" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            


                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                    <asp:Button ID="btnRemove" runat="server" Text="Remove" CausesValidation="false" CssClass="btn btn-danger btn-sm" OnClick="btnRemove_Click" />

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div style="height: 250px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="PIN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridPIN" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CardNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("CardNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EmployeeName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEmployeeName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   
                                    
                                    <asp:TemplateField HeaderText="EffectiveDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEffectiveDate" runat="server" Text='<%# Bind("SalaryDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Deposite Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDepositeDate" runat="server" Text='<%# Bind("DepositeDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAmount" runat="server" Text='<%# Bind("TaxAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BankAmount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridBankAmount" runat="server" Text='<%# Bind("BankAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TDS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTDS" runat="server" Text='<%# Bind("TDS") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    

                                    <asp:TemplateField HeaderText="Bank Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridBankName" runat="server" Text='<%# Bind("BankName") %>'></asp:Label>
                                            <asp:HiddenField ID="hidBankAutoId" Value='<%# Bind("BankAutoId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>                                   

                                    <asp:TemplateField HeaderText="Bank Branch Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridBankBranchName" runat="server" Text='<%# Bind("BankBranchName") %>'></asp:Label>
                                            <asp:HiddenField ID="hidBankBranchAutoId" Value='<%# Bind("BankBranchAutoId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                            <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("SalaryTaxSetupId") %>' runat="server" />
                                            <asp:HiddenField ID="hid_GridCompanyId" Value='<%# Bind("CompanyAutoId") %>' runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hid_GridItemAutoId" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

    </script>

</asp:Content>
