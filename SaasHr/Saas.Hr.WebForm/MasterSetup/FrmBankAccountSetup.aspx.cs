﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class FrmBankAccountSetup : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownListMultiColumn(dd_Category,
                    "T_Bank",
                     "Name",
                     "Branch",
                     "Bank_Id", string.Empty);

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select BAc.AutoId as AutoId, B.Name as BankName,BAc.AccountName as AccountName, BAc.AccountNo as AccountNo,BAc.Company_Id as Company_Id,B.Bank_Id as Bank_Id  from BankAccountNoSetup BAc left outer join T_Bank B ON BAc.BankId=B.Bank_Id ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[proc_CommonFormForDrivedTbl_DML]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                + "','"
                + SessionCompanyId
                + "','"
                + dd_Category.SelectedValue
                + "','BankAccountNoSetup','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[proc_CommonFormForDrivedTbl_DML]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + dd_Category.SelectedValue
                        + "','BankAccountNoSetup','I'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txt_GroupName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridSubCategoryName")).Text;
                txtCode.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAccountNo")).Text;
                dd_Category.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridCategoryId")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        //protected void btn_remove_Click(object sender, EventArgs e)
        //{
        //    Connection connection = new Connection();

        //    string S_Delete_T_LeaveAssignByHR = string.Empty;
        //    S_Delete_T_LeaveAssignByHR = "proc_frm_CompanyInfo_Delete_T_CompanyInfo_LCM"
        //         + "'"
        //         + gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].Cells[1].Text.Trim()
        //         + "'";

        //    string S_DeleteReturn_T_LeaveAssignByHR = string.Empty;
        //    S_DeleteReturn_T_LeaveAssignByHR = connection.connection_DB(S_Delete_T_LeaveAssignByHR, 1, false, true, true);

        //    if (S_DeleteReturn_T_LeaveAssignByHR != GlobalVariables.g_s_connectionErrorReturnValue)
        //    {
        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationSuccessfull;
        //        v_loadGridView_CostingHead();
        //        InsertMode();

        //    }
        //    else
        //    {
        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationFailed;
        //    }
        //}
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (dd_Category.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Bank Name Can Not Blank!";
                return false;
            }

            if (txtCode.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "A/C No Can Not Blank!";
                return false;
            }

            if (txt_GroupName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Account Name Can Not Blank!";
                return false;
            }
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txt_GroupName.Text = string.Empty;
            txtCode.Text = string.Empty;
            dd_Category.SelectedValue = "0";
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select BAc.AutoId as AutoId, B.Name as BankName,BAc.AccountName as AccountName, BAc.AccountNo as AccountNo,BAc.Company_Id as Company_Id,B.Bank_Id as Bank_Id  from BankAccountNoSetup BAc left outer join T_Bank B ON BAc.BankId=B.Bank_Id  ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.PageIndex = e.NewPageIndex;
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }
    }
}