﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" 
    CodeBehind="FrmAttendanceBonus.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.FrmAttendanceBonus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Attendance Bonus
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div>
            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label ID="Label18" runat="server" Text="Effective Date :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtEffectiveDate" CssClass="input-sm date-picker" />
                            </div>
                        </div>
                        <div class="form-group ">
                            <asp:Label ID="Label4" runat="server" Text="Employee Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label3" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 hidden">
                                <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label5" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text="Department :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                         <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Designation :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>                                                                 
                          <div class="form-group">
                            <asp:Label ID="Label7" runat="server" Text="Employee type :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddEmployeeType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>  
                        <div class="form-group">
                            <asp:Label ID="lbl_GroupName" runat="server" Text="Present Ratio :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtPrecentRatio" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <asp:Label ID="Label21" runat="server" Text="Bonus Amount :" title="" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtBonusAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Remarks :" title="" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                      
                        <div class="row row-custom">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="text-danger">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-custom">
                <div class="col-xs-12">
                    <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0px;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" OnPageIndexChanging="gdv_costingHead_PageIndexChanging" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="Effe. Date">
                                <ItemTemplate>                                    
                                    <asp:Label ID="lblGridEffictiveDate" runat="server" Text='<%# Convert.ToDateTime(Eval("EffictiveDate")).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Employee Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Department">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Designation">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="EmployeeType">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridEmployeeType" runat="server" Text='<%# Bind("EmpTypeName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Present Ratio">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridPresentRatio" runat="server" Text='<%# Bind("PresentRatio") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Bonus Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridBonusAmount" runat="server" Text='<%# Bind("BonusAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                    <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                    <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmployeeId") %>' runat="server" />
                                   <%-- <asp:HiddenField ID="hid_GridCompanyId" Value='<%# Bind("Company_Id") %>' runat="server" />--%>
                                    <asp:HiddenField ID="hidDesignationId" Value='<%# Bind("DesignationId") %>' runat="server" />
                                    <asp:HiddenField ID="hid_EmployeeTypeId" Value='<%# Bind("EmployeeTypeId") %>' runat="server" />
                                    <asp:HiddenField ID="hidDepartmentAutoId" Value='<%# Bind("DepartmentAutoId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />        

        <asp:HiddenField ID="hidDesignationId" runat="server" Value="0" />        
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />      
        <asp:HiddenField ID="hid_EmployeeTypeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidDepartmentAutoId" runat="server" Value="0" />
        
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

    </script>

</asp:Content>
