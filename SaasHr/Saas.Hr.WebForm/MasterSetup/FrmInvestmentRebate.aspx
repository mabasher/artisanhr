﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmInvestmentRebate.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmInvestmentRebate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Investment Rebate
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">

        <div class="col-md-8 center">
            <fieldset>
                <legend>Investment Limit</legend>

                <div class="form-horizontal">
                    <div class="form-group hidden">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label5" runat="server" Text="Percentage Of Total Salary :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtPercentageOfTotalSalary" runat="server" placeholder="" CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtPercentageOfTotalSalary" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Rebate Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtRebateAmount" runat="server" placeholder="" CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtRebateAmount" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Fixed Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtFixedAmount" runat="server" placeholder="" CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,Custom"
                                ValidChars="." TargetControlID="txtFixedAmount" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label31" runat="server" Text="Effective Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label3" runat="server" Text="Condition :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:RadioButtonList ID="RdoCondition" runat="server" RepeatDirection="Horizontal" CellPadding="2" CellSpacing="2">
                                <asp:ListItem Value="U" Selected="True">Upper</asp:ListItem>
                                <asp:ListItem Value="L">Lower</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group center">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btn_save_Click" Width="80px" />
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="col-sm-12">
            <div class="form-horizontal">
                <div class="form-group center">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div style="height: 250px; overflow: scroll;">
                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                    CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                        </asp:CommandField>
                        <asp:TemplateField HeaderText="Percentage Of Total Salary">
                            <ItemTemplate>
                                <asp:Label ID="lblGridPercentageOfTotalSalary" runat="server" Text='<%# Bind("percentageOfTotal") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                   
                        <asp:TemplateField HeaderText="Percentage Rebate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridpercentageRebate" runat="server" Text='<%# Bind("percentageRebate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Fixed Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblGridfixedAmount" runat="server" Text='<%# Bind("fixedAmount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Effictive Date">
                            <ItemTemplate>
                                <asp:Label ID="lblGrideffectiveDate" runat="server" Text='<%# Bind("effectiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Condition">
                            <ItemTemplate>
                                <asp:Label ID="lblGridcalculateCondition" runat="server" Text='<%# Bind("calculateCondition") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                                                
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                <asp:HiddenField ID="hidInvestmentRebateId" Value='<%# Bind("InvestmentRebateId") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="pagination-sa" />
                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                </asp:GridView>
            </div>
        </div>


    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>
</asp:Content>
