﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;
using System.Collections.Generic;

namespace Saas.Hr.WebForm.HrAttendance
{
    public partial class FrmSalaryTAXChallan : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtEffectiveDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                string Sessionuserlevel = string.Empty;
                Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);

                if (Sessionuserlevel == "4")
                {
                    txtSearch.Visible = false;
                    getEmpData(SessionUserId);
                }
                commonfunctions.g_b_FillDropDownList(ddBank,
                  "T_Bank",
                  "Name",
                  "Bank_Id", "Order by Name");
                commonfunctions.g_b_FillDropDownList(ddBankBranch,
                  "BankBranchInfo",
                  "BankBranchName",
                  "AutoId", "Order by BankBranchName asc");


                v_loadGridView_CostingHead();


            }
        }

        private void v_loadGridView_CostingHead()
        {
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            string Sessionuserlevel = string.Empty;
            Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);

            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcSalaryTaxChallanSelect '" + SessionCompanyId + "','" + hidEmpAutoId.Value + "%','" + Sessionuserlevel + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.DataBind();
            sqlconnection.Close();

        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcSalaryTaxChallanINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddBank.SelectedValue
                        + "','"
                        + ddBankBranch.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtChallanNo.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtAmount.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcSalaryTaxChallanINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddBank.SelectedValue
                        + "','"
                        + ddBankBranch.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtChallanNo.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtAmount.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','I'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            if (b_validationReturn == false)
            {
                return;
            }

            s_save_ = "Delete SalaryTaxChallan Where AutoId= "
                       + "'"
                       + hidAutoIdForUpdate.Value
                       + "' Select ''";

            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;

                txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridPIN")).Text;
                txtCard.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCardNo")).Text;
                txtEmpName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEmployeeName")).Text;

                ddBank.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridBankId")).Value;
                ddBankBranch.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridBankBranchId")).Value;
                txtChallanNo.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridChallanNo")).Text;
                txtAmount.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAmount")).Text;
                txtEffectiveDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEffectiveDate")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;
                hidEmpAutoId.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidEmpAutoId")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            if (hidEmpAutoId.Value == "0" || hidEmpAutoId.Value == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }
            if (txtChallanNo.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Challan Can Not Blank!";
                return false;
            }
            if (txtAmount.Text == "")
            {
                txtAmount.Text = "0";
            }
            if (Convert.ToDouble(txtAmount.Text)==0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Amount Can Not Blank!";
                return false;
            }
            if (ddBank.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Bank Can Not Blank!";
                return false;
            }
            if (ddBankBranch.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Bank Branch Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtChallanNo.Text = string.Empty;
            txtRemarks.Text = string.Empty;


        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }


        //===========End===============         
    }
}