﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmAnnualHolyday.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.FrmAnnualholyday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Annual Holiday Calendar
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

         <div class="row row-custom">
                <div class="col-md-5">
                    <div class="form-horizontal">
                                          
                            <div class="form-group"  style="margin-top: 80px;">
                                <asp:Label ID="Label5" runat="server" Text="Year :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtyear" runat="server" AutoPostBack="True" OnTextChanged="txtyear_TextChanged" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="From Date :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>                    
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Days :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtdays"  AutoPostBack="True" OnTextChanged="txtdays_TextChanged" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>  
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="To Date :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-3">
                                      <asp:TextBox ID="txtToDate" Enabled="false" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                               </div>     
                            </div>        
                             <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:CheckBox ID="chkPublic" runat="server" Text="Is Festival Holiday" />
                                    </div>
                                </div>       
                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Job Location :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddJobLocation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>        
                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text="Employee Type :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddEmployeeType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>      
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Note :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group center" style="margin-top: 20px;">
                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
                        
                            <div class="form-group center">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false" CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                <asp:Button ID="btnRemove" runat="server" Text="Remove" CausesValidation="false" CssClass="btn btn-danger btn-sm" OnClick="btnRemove_Click" Width="80px" />
                            </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-horizontal">
                            <div class="main-data-grid" style="overflow: scroll;text-align:center;">

                                <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                    CssClass="table table-striped table-bordered"
                                  OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                                    EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="From Date" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridfromDate" runat="server" Text='<%# Bind("fromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridToDate" runat="server" Text='<%# Bind("ToDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Days" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGriddays" runat="server" Text='<%# Bind("days") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Is Festival" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridIsPublicholiday" runat="server" Text='<%# Bind("IsPublicholiday") %>'></asp:Label>
                                                 <asp:HiddenField ID="hidGridAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                             </ItemTemplate>
                                        </asp:TemplateField>

                                                                               
                                        <asp:TemplateField HeaderText="Location" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridLocationName" runat="server" Text='<%# Bind("LocationName") %>'></asp:Label>
                                                 <asp:HiddenField ID="hidGridLocationInfoId" Value='<%# Bind("LocationInfoId") %>' runat="server" />
                                             </ItemTemplate>
                                        </asp:TemplateField>
                                                                               
                                        <asp:TemplateField HeaderText="Emp. Type" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridEmpTypeName" runat="server" Text='<%# Bind("TypeName") %>'></asp:Label>
                                                 <asp:HiddenField ID="hidGridEmployeeTypeId" Value='<%# Bind("EmployeeTypeId") %>' runat="server" />
                                             </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Note" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridremarks" runat="server" Text='<%# Bind("remarks") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        
                                        <asp:TemplateField HeaderText="Day" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGriddayname" runat="server" Text='<%# Bind("trdayName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                    </Columns>
                                    <PagerStyle CssClass="pagination-sa" />
                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                </asp:GridView>
                            </div>
                    </div>
                </div>                               
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidIspublicHoliday" runat="server" Value="N" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

</asp:Content>
