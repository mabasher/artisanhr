﻿using System;
using System.Data;
using System.Web;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class FrmProductMaster : System.Web.UI.Page
    {
        private string s_chk_UseLot = string.Empty;
        private string s_chk_LotDivisible = string.Empty;
        private string s_chk_SerialControl = string.Empty;
        private string s_chk_HSCodeEnable = string.Empty;
        private string s_chk_SelfLife = string.Empty;
        private string s_chk_Planner = string.Empty;
        private string s_chk_CycleCount = string.Empty;

        private string ItemAutoId = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(dd_Category,
                    "T_InvCategory",
                    "CategoryName",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemSubCategory,
                   "T_Inv_SubCategory",
                   "SubCategory",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemType,
                   "T_Inv_ItemType",
                   "ItemType",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemSubType,
                   "T_Inv_ItemSubType",
                   "ItemSubType",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemSubType,
                   "T_Inv_ItemSubType",
                   "ItemSubType",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(dd_PrimaryUnit,
                   "T_UOM",
                   "UOM",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSecondaryUnit,
                   "T_UOM",
                   "UOM",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBaseUnit,
                   "T_UOM",
                   "UOM",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSize,
                   "T_Size",
                   "Size",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddColor,
                   "T_Color",
                   "Color",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemClass,
                   "T_Inv_ItemClass",
                   "ItemClass",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemClass,
                   "T_Inv_ItemClass",
                   "ItemClass",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemSourceName,
                   "T_Inv_ItemSource",
                   "ItemSource",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddUserItemType,
                   "T_Inv_UserItemType",
                   "UserItemType",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddMovingStatus,
                   "T_Inv_ItemMovementStatus",
                   "ItemMovementStatus",
                   "AutoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddConsumptionType,
                   "T_Inv_ConsumptionType",
                   "ConsumptionType",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBrand,
                   "T_Brand",
                   "Brand",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddStoreName,
                   "T_Inv_StockName",
                   "StockName",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddShelfLocation,
                   "T_Inv_Location",
                   "LocationName",
                   "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddItemExpenseAccountLedger,
                   "T_ExpenseHead",
                   "ExpenseHead",
                   "AutoId", string.Empty);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }
            s_chk_UseLot = "N";
            if (chkUseLot.Checked)
            {
                s_chk_UseLot = "Y";
            }
            s_chk_LotDivisible = "N";
            if (chkLotDivisible.Checked)
            {
                s_chk_LotDivisible = "Y";
            }
            s_chk_SerialControl = "N";
            if (chkSerialControl.Checked)
            {
                s_chk_SerialControl = "Y";
            }
            s_chk_HSCodeEnable = "N";
            if (chkHSCodeEnable.Checked)
            {
                s_chk_HSCodeEnable = "Y";
            }
            s_chk_SelfLife = "N";
            if (chkSelfLife.Checked)
            {
                s_chk_SelfLife = "Y";
            }
            s_chk_Planner = "N";
            if (chkPlanner.Checked)
            {
                s_chk_Planner = "Y";
            }

            s_chk_CycleCount = "N";
            if (chkCycleCounting.Checked)
            {
                s_chk_CycleCount = "Y";
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcProductMasterUpdate]"
                + "'"
                + hidProductId.Value
                + "','"
                + HttpUtility.HtmlDecode(txtItemCode.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtItemName.Text.Trim().Replace("'", "''"))
                + "','"
                + dd_PrimaryUnit.SelectedValue
                + "','"
                + ddSecondaryUnit.SelectedValue
                + "','"
                + HttpUtility.HtmlDecode(txtConversionFactor.Text.Trim())
                + "','"
                + ddBaseUnit.SelectedValue
                + "','"
                + ddSize.SelectedValue
                + "','"
                + ddColor.SelectedValue
                + "','"
                + HttpUtility.HtmlDecode(txtPartNumber.Text.Trim())
                + "','"
                + dd_Category.SelectedValue
                + "','"
                + ddItemSubCategory.SelectedValue
                + "','"
                + ddItemType.SelectedValue
                + "','"
                + ddItemSubType.SelectedValue
                + "','"
                + ddItemClass.SelectedValue
                + "','"
                + ddItemSourceName.SelectedValue
                + "','"
                + ddMovingStatus.SelectedValue
                + "','"
                + ddUserItemType.SelectedValue
                + "','"
                + ddConsumptionType.SelectedValue
                + "','"
                + s_chk_UseLot
                + "','"
                + s_chk_LotDivisible
                + "','"
                + s_chk_SerialControl
                + "','"
                + ddBrand.SelectedValue
                + "','"
                + ddStoreName.SelectedValue
                + "','"
                + ddShelfLocation.SelectedValue
                + "','"
                + s_chk_SelfLife
                + "','"
                + HttpUtility.HtmlDecode(txt_SelfLifeDays.Text.Trim())
                + "','"
                + s_chk_Planner
                + "','"
                + HttpUtility.HtmlDecode(txtPlanner.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtMinLevel.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtMaxLevel.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtSafetyLevel.Text.Trim())
                + "','"
                + s_chk_HSCodeEnable
                + "','"
                + HttpUtility.HtmlDecode(txtHSCode.Text.Trim())
                + "','"
                + s_chk_CycleCount
                + "','"
                + HttpUtility.HtmlDecode(txtCycleCounting.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtRateInBDT.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtAmountInBDT.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtReOrderlevelQty.Text.Trim())
                + "','"
                + ddItemExpenseAccountLedger.SelectedValue
                + "','"
                + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                + "','"
                + SessionUserId
                + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;

                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcProductMasterInsert]"
                        + "'"
                        + HttpUtility.HtmlDecode(txtItemCode.Text.Trim())
                        + "','"
                         + HttpUtility.HtmlDecode(txtItemName.Text.Trim())
                        + "','"
                        + dd_PrimaryUnit.SelectedValue
                        + "','"
                        + ddSecondaryUnit.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtConversionFactor.Text.Trim())
                        + "','"
                        + ddBaseUnit.SelectedValue
                        + "','"
                        + ddSize.SelectedValue
                        + "','"
                        + ddColor.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtPartNumber.Text.Trim())
                        + "','"
                        + dd_Category.SelectedValue
                        + "','"
                        + ddItemSubCategory.SelectedValue
                        + "','"
                        + ddItemType.SelectedValue
                        + "','"
                        + ddItemSubType.SelectedValue
                        + "','"
                        + ddItemClass.SelectedValue
                        + "','"
                        + ddItemSourceName.SelectedValue
                        + "','"
                        + ddMovingStatus.SelectedValue
                        + "','"
                        + ddUserItemType.SelectedValue
                        + "','"
                        + ddConsumptionType.SelectedValue
                        + "','"
                        + s_chk_UseLot
                        + "','"
                        + s_chk_LotDivisible
                        + "','"
                        + s_chk_SerialControl
                        + "','"
                        + ddBrand.SelectedValue
                        + "','"
                        + ddStoreName.SelectedValue
                        + "','"
                        + ddShelfLocation.SelectedValue
                        + "','"
                        + s_chk_SelfLife
                        + "','"
                        + HttpUtility.HtmlDecode(txt_SelfLifeDays.Text.Trim())
                        + "','"
                        + s_chk_Planner
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlanner.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMinLevel.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMaxLevel.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtSafetyLevel.Text.Trim())
                        + "','"
                        + s_chk_HSCodeEnable
                        + "','"
                        + HttpUtility.HtmlDecode(txtHSCode.Text.Trim())
                        + "','"
                        + s_chk_CycleCount
                        + "','"
                        + HttpUtility.HtmlDecode(txtCycleCounting.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRateInBDT.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAmountInBDT.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtReOrderlevelQty.Text.Trim())
                        + "','"
                        + ddItemExpenseAccountLedger.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;

                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //if (txtItemCode.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Code Can Not Blank!";
            //    return false;
            //}
            if (txtItemName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Item Name Can Not Blank!";
                return false;
            }
            if (dd_Category.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Category Can Not Blank!";
                return false;
            }
            //if (dd_PrimaryUnit.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Primary Unit Can Not Blank!";
            //    return false;
            //}
            //if (ddItemExpenseAccountLedger.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Expense Ledger Name Can Not Blank!";
            //    return false;
            //}

            if (txtConversionFactor.Text == "") { txtConversionFactor.Text = "0"; }
            if (txt_SelfLifeDays.Text == "") { txt_SelfLifeDays.Text = "0"; }
            if (txtMinLevel.Text == "") { txtMinLevel.Text = "0"; }
            if (txtMaxLevel.Text == "") { txtMaxLevel.Text = "0"; }
            if (txtSafetyLevel.Text == "") { txtSafetyLevel.Text = "0"; }
            if (txtReOrderlevelQty.Text == "") { txtReOrderlevelQty.Text = "0"; }
            if (txtCycleCounting.Text == "") { txtCycleCounting.Text = "0"; }
            if (txtRateInBDT.Text == "") { txtRateInBDT.Text = "0"; }
            if (txtAmountInBDT.Text == "") { txtAmountInBDT.Text = "0"; }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            hidProductId.Value = "";
            txtSearch.Text = "";
            //dd_Category.SelectedValue = "0";
            ddItemSubCategory.SelectedValue = "0";
            dd_PrimaryUnit.SelectedValue = "0";
            ddSecondaryUnit.SelectedValue = "0";
            ddBaseUnit.SelectedValue = "0";
            ddSize.SelectedValue = "0";
            ddColor.SelectedValue = "0";
            ddItemType.SelectedValue = "0";
            ddItemSubType.SelectedValue = "0";
            ddItemClass.SelectedValue = "0";
            ddItemSourceName.SelectedValue = "0";
            ddConsumptionType.SelectedValue = "0";
            ddBrand.SelectedValue = "0";
            ddStoreName.SelectedValue = "0";
            ddShelfLocation.SelectedValue = "0";
            ddItemExpenseAccountLedger.SelectedValue = "0";
            txtItemCode.Text = string.Empty;
            txtItemName.Text = string.Empty;
            txtConversionFactor.Text = string.Empty;
            txtPlanner.Text = string.Empty;
            txtMinLevel.Text = "0";
            txtMaxLevel.Text = "0";
            txtSafetyLevel.Text = "0";
            txtHSCode.Text = string.Empty;
            txtRateInBDT.Text = "0";
            txtAmountInBDT.Text = "0";
            txtReOrderlevelQty.Text = "0";
            txtRemarks.Text = string.Empty;
        }
        
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidProductId.Value != "" && hidProductId.Value != "0")
            {
                btn_Search_Click(sender, e);
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            //s_select = "[Proc_ProductMaster_Search]"
            //            + "'"
            //            + hidProductId.Value
            //            + "';";

            s_select = "SELECT * FROM ProductMaster Where AutoId='" + hidProductId.Value + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtItemCode.Text = drow["ItemCode"].ToString();
                        txtItemName.Text = drow["ItemName"].ToString();
                        dd_PrimaryUnit.SelectedValue = drow["PrimaryUnitId"].ToString();
                        ddSecondaryUnit.SelectedValue = drow["SecondaryUnitId"].ToString();
                        txtConversionFactor.Text = drow["ConversionFactor"].ToString();
                        ddBaseUnit.SelectedValue = drow["BaseUnitId"].ToString();
                        ddSize.SelectedValue = drow["SizeId"].ToString();
                        ddColor.SelectedValue = drow["ColorId"].ToString();
                        txtPartNumber.Text = drow["PartNumber"].ToString();
                        dd_Category.SelectedValue = drow["ItemCategoryId"].ToString();
                        ddItemSubCategory.SelectedValue = drow["ItemSubCategoryId"].ToString();
                        ddItemType.SelectedValue = drow["ItemTypeId"].ToString();
                        ddItemSubType.SelectedValue = drow["ItemSubTypeId"].ToString();
                        ddItemClass.SelectedValue = drow["ItemClassId"].ToString();
                        ddItemSourceName.SelectedValue = drow["ItemSourceNameId"].ToString();
                        ddMovingStatus.SelectedValue = drow["ItemMovementStatusId"].ToString();
                        ddUserItemType.SelectedValue = drow["UserItemTypeId"].ToString();
                        ddConsumptionType.SelectedValue = drow["ConsumptionTypeId"].ToString();
                        chkUseLot.Checked = false;
                        if (drow["UseLot"].ToString() == "Y"){chkUseLot.Checked = true;}
                        chkLotDivisible.Checked = false;
                        if (drow["LotDivisible"].ToString() == "Y") {chkLotDivisible.Checked = true;}
                        chkSerialControl.Checked = false;
                        if (drow["SerialControl"].ToString() == "Y"){chkSerialControl.Checked = true;}
                        ddBrand.SelectedValue = drow["BrandId"].ToString();
                        ddStoreName.SelectedValue = drow["StoreNameId"].ToString();
                        ddShelfLocation.SelectedValue = drow["ShelfLocationId"].ToString();
                        chkSelfLife.Checked = false;
                        if (drow["SelfLife"].ToString() == "Y"){chkSelfLife.Checked = true;}
                        txt_SelfLifeDays.Text = drow["SelfLifeDays"].ToString();
                        chkPlanner.Checked = false;
                        if (drow["Planner"].ToString() == "Y"){chkPlanner.Checked = true;}
                        txtPlanner.Text = drow["PlannerType"].ToString();
                        txtMinLevel.Text = drow["MinLevel"].ToString();
                        txtMaxLevel.Text = drow["MaxLevel"].ToString();
                        txtSafetyLevel.Text = drow["SafetyLevel"].ToString();
                        txtReOrderlevelQty.Text = drow["ReOrderlevelQty"].ToString();
                        chkHSCodeEnable.Checked = false;
                        if (drow["HSCodeEnable"].ToString() == "Y"){chkHSCodeEnable.Checked = true;}             
                        chkCycleCounting.Checked = false;
                        if (drow["CycleCountEnable"].ToString() == "Y"){chkCycleCounting.Checked = true;}
                        txtCycleCounting.Text = drow["CycleCountDays"].ToString();
                        txtRateInBDT.Text = drow["RateInBDT"].ToString();
                        txtAmountInBDT.Text = drow["AmountInBDT"].ToString();
                        ddItemExpenseAccountLedger.SelectedValue = drow["ExpenseAccountLedgerId"].ToString();
                        txtRemarks.Text = drow["Remarks"].ToString();
                    }
                    btn_save.Text = "Update";
                }
            }
        }




    }
}