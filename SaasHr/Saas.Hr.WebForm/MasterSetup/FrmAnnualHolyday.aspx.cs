﻿using Saas.Hr.WebForm.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class FrmAnnualholyday : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtyear.Text = DateTime.Now.Year.ToString();

                commonfunctions.g_b_FillDropDownList(ddEmployeeType,
                    "EmployeeType",
                    "TypeName",
                    "EmployeeTypeId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddJobLocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);

                v_loadGridView_CostingHead();
            }
        }

        protected void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            qry = "select fromDate,toDate,days,remarks,IsPublicholiday,AutoId,isnull(et.EmployeeTypeId,0) as EmployeeTypeId,et.TypeName,isnull(l.LocationInfoId,0) as LocationInfoId ,l.LocationName,datename(dw,fromDate) as trdayName From AnnualHoliday h left outer join EmployeeType et on h.EmployeeTypeId=et.EmployeeTypeId left outer join LocationInfo l on h.JoblocationId=l.LocationInfoId WHERE year(fromDate)='" + txtyear.Text + "' order by fromDate desc";

            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();
            sqlconnection.Close();
        }
        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }
        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidGridAutoId")).Value;
                txtFromDate.Text = ((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGridfromDate")).Text;
                txtdays.Text = ((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGriddays")).Text;
                txtToDate.Text = ((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGridToDate")).Text;
                txtNote.Text = ((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGridremarks")).Text;
                ddJobLocation.SelectedValue = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidGridLocationInfoId")).Value;
                ddEmployeeType.SelectedValue = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidGridEmployeeTypeId")).Value;
                
                chkPublic.Checked = false;
                if (((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGridIsPublicholiday")).Text == "Y") { chkPublic.Checked = true; }
                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string s_Delete_returnValue = string.Empty;
            string s_Delete = string.Empty;
            s_Delete = "DELETE FROM	dbo.AnnualHoliday WHERE	AutoId='" + hidAutoIdForUpdate.Value + "' select ''";

            if (btn_save.Text == "Update")
            {
                s_Delete_returnValue = connection.connection_DB(s_Delete, 1, true, true, true);

                if (s_Delete_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {

                    if (s_Delete_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                    else
                    {
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }

        }
        
        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            if (b_validationReturn == false)
            {
                return;
            }

            if (chkPublic.Checked) { hidIspublicHoliday.Value = "Y"; }
            
             if (btn_save.Text == "Update")
            {
                s_Update = "[ProcAnnualHolidayINSERT]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                + "','"
                + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                + "','"
                + txtdays.Text
                + "','"
                + txtNote.Text
                + "','"
                + SessionUserId
                + "','"
                + SessionCompanyId
                + "','"
                + hidIspublicHoliday.Value
                + "','"
                + ddJobLocation.SelectedValue
                + "','"
                + ddEmployeeType.SelectedValue
                + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {

                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                    else
                    {                      
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
            else
            {
                s_save_ = "[ProcAnnualHolidayINSERT]"
                + "'0','"
                + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                + "','"
                + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                + "','"
                + txtdays.Text
                + "','"
                + txtNote.Text
                + "','"
                + SessionUserId
                + "','"
                + SessionCompanyId
                + "','"
                + hidIspublicHoliday.Value
                + "','"
                + ddJobLocation.SelectedValue
                + "','"
                + ddEmployeeType.SelectedValue
                + "','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
                ////
            }
        }


        protected void txtyear_TextChanged(object sender, EventArgs e)
        {
            v_loadGridView_CostingHead();
        }

        protected void txtdays_TextChanged(object sender, EventArgs e)
        {
            if (txtdays.Text == "")
            {
                txtdays.Text = "1";
            }
            DateTime dt = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dt = dt.AddDays(Convert.ToDouble(txtdays.Text)-1);
            txtToDate.Text = dt.ToString("dd/MM/yyyy");
        }
        ///////================
    }
}