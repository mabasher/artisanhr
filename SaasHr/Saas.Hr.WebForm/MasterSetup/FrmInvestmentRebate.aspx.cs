﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmInvestmentRebate : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                //txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                //txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
              

                //commonfunctions.g_b_FillDropDownList(ddPayHeadName,
                //    "PayHeadInfo",
                //    "PayHeadName",
                //    "PayHeadInfoId", string.Empty);                

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidCardid.Value == "0"){ hidCardid.Value = ""; }

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "select InvestmentRebateId as InvestmentRebateId,percentageOfTotal as percentageOfTotal,percentageRebate as percentageRebate, fixedAmount as fixedAmount, effectiveDate as effectiveDate,calculateCondition as calculateCondition , Remarks as Remarks from InvestmentRebate ";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;                
            gdv_costingHead.DataBind();
            sqlconnection.Close();            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {                
                s_Update = "[ProcTaxInvestmentRebateINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtPercentageOfTotalSalary.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRebateAmount.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtFixedAmount.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + RdoCondition.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcTaxInvestmentRebateINSERT]"
                        + "'"
                        + '0'
                        + "','"
                        + HttpUtility.HtmlDecode(txtPercentageOfTotalSalary.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRebateAmount.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtFixedAmount.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + RdoCondition.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I'";                
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidInvestmentRebateId")).Value;
                txtPercentageOfTotalSalary.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridPercentageOfTotalSalary")).Text;
                txtRebateAmount.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridpercentageRebate")).Text;

                txtFixedAmount.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridfixedAmount")).Text;
                txtFromDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGrideffectiveDate")).Text;
                txtAdditionalNote.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;
                
                RdoCondition.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridcalculateCondition")).Text;


                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {

            if (txtPercentageOfTotalSalary.Text == "" || txtPercentageOfTotalSalary.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Percentage Of Total Salary Can Not Blank!";
                return false;
            }

            if (txtRebateAmount.Text == "" || txtRebateAmount.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Rebate Amount Can Not Blank!";
                return false;
            }
            if (txtFixedAmount.Text == "" || txtFixedAmount.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Fixed Amount Can Not Blank!";
                return false;
            }
            //if (ddPayHeadName.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "PayHead Name Can Not Blank!";
            //    return false;
            //}
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
         //   txtAmount.Text = "0";
            txtAdditionalNote.Text = string.Empty;
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        hidCardid.Value= drow["CardNo"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_CostingHead();
        }


        //===========End===============         
    }
}