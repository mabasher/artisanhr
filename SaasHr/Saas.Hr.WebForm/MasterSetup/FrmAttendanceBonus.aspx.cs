﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class FrmAttendanceBonus : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtEffectiveDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                  "DepartmentInfo",
                  "Department",
                  "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddDesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddEmployeeType,
                    "EmployeeType",
                    "TypeName",
                    "EmployeeTypeId", string.Empty);

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcAttendanceBonusSelect", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.AllowPaging = true;
                gdv_costingHead.PageSize = 5000;
                gdv_costingHead.DataBind();
            
            sqlconnection.Close();
        }



        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionUserIP = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }


            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcAttendanceBonusSetupUpdate]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                + "','"
                + hidEmpAutoId.Value
                + "','"
                + ddDepartment.SelectedValue
                + "','"
                + ddDesignation.SelectedValue
                + "','"
                + ddEmployeeType.SelectedValue
                + "','"
                + HttpUtility.HtmlDecode(txtBonusAmount.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtPrecentRatio.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                + "','"
                + SessionCompanyId
                + "','"
                + SessionUserId
                + "','"
                + SessionUserIP
                + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {

                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();

                    }
                }
            }
            else
            {
                s_save_ = "[ProcAttendanceBonusSetupInsert]"
                        + "'"
                        + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddDepartment.SelectedValue
                        + "','"
                        + ddDesignation.SelectedValue
                        + "','"
                        + ddEmployeeType.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtBonusAmount.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPrecentRatio.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }
            #endregion gdv_ChildrenInfo_RowDataBound
        }
        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                hidEmpAutoId.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidEmpAutoId")).Value;
                txtEffectiveDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEffictiveDate")).Text;
                txtSearch.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblEmployeeName")).Text;
                ddDesignation.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidDesignationId")).Value;
                ddEmployeeType.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_EmployeeTypeId")).Value;
                ddDepartment.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidDepartmentAutoId")).Value;
                txtPrecentRatio.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridPresentRatio")).Text;
                txtBonusAmount.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridBonusAmount")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {

                lblMsg.Text = exception.Message;
            }
            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }
        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (txtleaveDays.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "leave Days Can Not Blank!";
            //    return false;
            //}
            //if (txtleaveType.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Leave Type Can Not Blank!";
            //    return false;
            //}

            //if (txtCarryOverLimit.Text == "")
            //{
            //    txtCarryOverLimit.Text = "0";
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtEffectiveDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtPrecentRatio.Text = string.Empty;
            txtBonusAmount.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            ddDepartment.SelectedValue ="0";                      
            ddDesignation.SelectedValue = "0";
            ddEmployeeType.SelectedValue = "0";
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //CommonFunctions commonFunctions = new CommonFunctions();
            //SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            //sqlconnection.Open();

            //SqlCommand cmd = new SqlCommand("select AutoId as AutoId, Color as Color, code as Code, Company_Id as Company_Id from T_Color ", sqlconnection);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //da.Fill(ds);
            //gdv_costingHead.PageIndex = e.NewPageIndex;
            //gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            //gdv_costingHead.PageSize = 8;
            //gdv_costingHead.DataBind();
            //sqlconnection.Close();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            //v_loadGridView_CostingHead();
        }

    }
}