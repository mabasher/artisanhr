﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class FrmEmployeeGroup : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);


                if (Sessionuserlevel != "2" && Sessionuserlevel !="3")
                {
                    chkMode.Visible = false;
                    btnModeCheck.Visible = false;
                }

                //if (Convert.ToString(Session[GlobalVariables.g_s_WinMode]) == "N")
                //{
                //    chkMode.Visible = false;
                //    btnModeCheck.Visible = false;
                //}
                
                getChkMode();
                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select EmployeeGroupId as EmployeeGroupId, GroupName as GroupName, '0' as Company_Id from EmployeeGroup ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 5000;
            gdv_costingHead.DataBind();
            
            sqlconnection.Close();
        }

        private void getChkMode()
        {
            Connection connection = new Connection();
            string s_ChkValue = string.Empty;
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;
            
            s_select = "SELECT * FROM T_WinMode";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        s_ChkValue = drow["trMode"].ToString();
                        chkMode.Checked = false;
                        if (s_ChkValue == "Y") { chkMode.Checked = true; }
                    }
                }
            }
        }

        protected void btnModeCheck_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string s_returnValue = string.Empty;
            string qry = string.Empty;
            qry = "delete T_WinMode Insert into T_WinMode values ('N','Dummy Mode') select ''";
            if (chkMode.Checked)
            {
                qry = "delete T_WinMode Insert into T_WinMode values ('Y','Actual Mode') select ''";
            }          
            s_returnValue = connection.connection_DB(qry, 1, true, true, true);
            s_returnValue = "";
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionUserIP = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }


            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcCommonFormHR]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                + "','"
                + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                + "',N'"
                + HttpUtility.HtmlDecode(txtNameBangla.Text.Trim())
                + "','"
                + SessionUserId
                + "','"
                + SessionUserIP
                + "','"
                + SessionUserId
                + "','"
                + SessionUserIP
                + "','0','0','EmployeeGroup','U'";
                
                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {

                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();

                    }
                }
            }
            else
            {
                s_save_ = "[ProcCommonFormHR]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txt_GroupName.Text.Trim())
                        + "',N'"
                        + HttpUtility.HtmlDecode(txtNameBangla.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','EmployeeGroup','I'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }
            #endregion gdv_ChildrenInfo_RowDataBound
        }
        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txt_GroupName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtNameBangla.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryNameBangla")).Text;
                //txtCode.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCode")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {

                lblMsg.Text = exception.Message;
            }
            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        //protected void btn_remove_Click(object sender, EventArgs e)
        //{
        //    Connection connection = new Connection();

        //    string S_Delete_T_LeaveAssignByHR = string.Empty;
        //    S_Delete_T_LeaveAssignByHR = "proc_frm_CompanyInfo_Delete_T_CompanyInfo_LCM"
        //         + "'"
        //         + gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].Cells[1].Text.Trim()
        //         + "'";


        //    string S_DeleteReturn_T_LeaveAssignByHR = string.Empty;
        //    S_DeleteReturn_T_LeaveAssignByHR = connection.connection_DB(S_Delete_T_LeaveAssignByHR, 1, false, true, true);

        //    if (S_DeleteReturn_T_LeaveAssignByHR != GlobalVariables.g_s_connectionErrorReturnValue)
        //    {


        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationSuccessfull;
        //        v_loadGridView_CostingHead();
        //        InsertMode();

        //    }
        //    else
        //    {
        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationFailed;
        //    }
        //}
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }
        private void InsertMode_Msg()
        {

            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (txtCode.Text == "")
            //{

            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Code Can Not Blank!";
            //    return false;
            //}
            if (txt_GroupName.Text == "")
            {

                lblMsg.Visible = true;
                lblMsg.Text = "Marital Status Can Not Blank!";
                return false;
            }

            //if (txtNameBangla.Text == "")
            //{

            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Project Bangla name Can Not Blank!";
            //    return false;
            //}
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txt_GroupName.Text = string.Empty;
            //txtCode.Text = string.Empty;
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //CommonFunctions commonFunctions = new CommonFunctions();
            //SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            //sqlconnection.Open();

            //SqlCommand cmd = new SqlCommand("select AutoId as AutoId, Color as Color, code as Code, Company_Id as Company_Id from T_Color ", sqlconnection);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataSet ds = new DataSet();
            //da.Fill(ds);
            //gdv_costingHead.PageIndex = e.NewPageIndex;
            //gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            //gdv_costingHead.PageSize = 8;
            //gdv_costingHead.DataBind();
            //sqlconnection.Close();
        }
    }
}