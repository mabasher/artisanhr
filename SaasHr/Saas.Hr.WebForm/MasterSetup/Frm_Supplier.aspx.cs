﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.MasterSetup
{
    public partial class Frm_Supplier : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                v_loadGridView_BankInfo();
            }
        }

        private void v_loadGridView_BankInfo()
        {

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            //SqlCommand cmd = new SqlCommand("proc_frm_SupplierInfo_Select_T_Supplier", sqlconnection);
            SqlCommand cmd = new SqlCommand("proc_frm_SupplierInfo_Select_T_Supplier '%" + txtBankSearch.Text + "%'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }
        

        protected void btn_save_Click(object sender, EventArgs e)
        {

            Connection connection = new Connection();
            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;


            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }


            if (btn_save.Text == "Update")
            {
                s_Update = "[proc_frm_SupplierInfo_Update_T_Supplier]"
                + "'"
                + hidAutoIdForUpdate.Value
                + "','"
                + HttpUtility.HtmlDecode(txt_Name.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtAddress.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtPostCode.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtCountry.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtPhone.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtFax.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtEmail.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtWebSite.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                + "','"
                + SessionUserId
                + "','"
                + HttpUtility.HtmlDecode(txtTallyLedgerName.Text.Trim().Replace("'", "''"))
                + "','"
                + HttpUtility.HtmlDecode(txtCode.Text.Trim().Replace("'", "''"))
                + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);


                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        v_loadGridView_BankInfo();
                        InsertMode();

                    }
                }
            }
            else
            {
                s_save_ = "[proc_frm_SupplierInfo_Insert_T_Supplier]"
                        + "'"
                        + HttpUtility.HtmlDecode(txt_Name.Text.Trim().Replace("'", "''"))
                        + "','"
                         + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPostCode.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCountry.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPhone.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtFax.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtWebSite.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionUserId
                        + "','"
                        + HttpUtility.HtmlDecode(txtTallyLedgerName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCode.Text.Trim())
                        + "'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);


                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        v_loadGridView_BankInfo();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }

                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }



            }


        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }
            #endregion gdv_ChildrenInfo_RowDataBound
        }
        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txt_Name.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridSuppliername")).Text;
                txtAddress.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridSupplierAdress")).Text;
                txtPostCode.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridPostalCode")).Text;
                txtCountry.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCountry")).Text;
                txtPhone.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridphone")).Text;
                txtFax.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridFax")).Text;
                txtEmail.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEmail")).Text;
                txtWebSite.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridWebsite")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemaks")).Text;
                txtContactPerson.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridContact_Person")).Text;
                txtTallyLedgerName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridTallyLedgerName")).Text;
                txtCode.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCode")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {

                lblMsg.Text = exception.Message;
            }
            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        //protected void btn_remove_Click(object sender, EventArgs e)
        //{
        //    Connection connection = new Connection();

        //    string S_Delete_T_LeaveAssignByHR = string.Empty;
        //    S_Delete_T_LeaveAssignByHR = "proc_frm_CompanyInfo_Delete_T_CompanyInfo_LCM"
        //         + "'"
        //         + gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].Cells[1].Text.Trim()
        //         + "'";


        //    string S_DeleteReturn_T_LeaveAssignByHR = string.Empty;
        //    S_DeleteReturn_T_LeaveAssignByHR = connection.connection_DB(S_Delete_T_LeaveAssignByHR, 1, false, true, true);

        //    if (S_DeleteReturn_T_LeaveAssignByHR != GlobalVariables.g_s_connectionErrorReturnValue)
        //    {


        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationSuccessfull;
        //        v_loadGridView_CostingHead();
        //        InsertMode();

        //    }
        //    else
        //    {
        //        lblMsg.Text = GlobalVariables.g_s_deleteOperationFailed;
        //    }
        //}
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }
        private void InsertMode_Msg()
        {

            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (txt_Name.Text == "")
            {

                lblMsg.Visible = true;
                lblMsg.Text = "Name Can Not Blank!";
                return false;
            }
            if (txtAddress.Text == "")
            {

                lblMsg.Visible = true;
                lblMsg.Text = "Address Can Not Blank!";
                return false;
            }
            

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txt_Name.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtTallyLedgerName.Text = string.Empty;
            txtCode.Text = string.Empty;
        }

        protected void gdv_costingHead_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("proc_frm_SupplierInfo_Select_T_Supplier '%" + txtBankSearch.Text + "%'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.PageIndex = e.NewPageIndex;

            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }
    }
}