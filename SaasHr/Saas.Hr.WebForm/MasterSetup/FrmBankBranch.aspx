﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmBankBranch.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.FrmBankBranchInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Bank Branch Information
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group hidden">
                            <asp:Label ID="Label1" runat="server" Text="Code :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <asp:Label ID="lbl_GroupName" runat="server" Text="Bank Branch Name :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_GroupName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group hidden">
                            <asp:Label ID="Label2" runat="server" Text="Project (Bangla) :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtNameBangla" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row row-custom">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="text-danger">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-custom">
                <div class="col-lg-6 col-sm-8 col-xs-12">
                    <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0px;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" OnPageIndexChanging="gdv_costingHead_PageIndexChanging" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="Bank Branch">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridCategoryName" runat="server" Text='<%# Bind("ProjectName") %>'></asp:Label>
                                    <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                    <asp:HiddenField ID="hid_GridCompanyId" Value='<%# Bind("Company_Id") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            

                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>