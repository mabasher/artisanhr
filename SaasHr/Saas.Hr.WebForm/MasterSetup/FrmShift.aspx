﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmShift.aspx.cs" Inherits="Saas.Hr.WebForm.MasterSetup.FrmShiftInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Shift Information
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Shift Name :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtShift" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                                                
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text="In Time :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtInTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>       
                            <div class="col-sm-1">
                                <asp:Label ID="Label2" runat="server" Text="Out Time :"></asp:Label>
                            </div>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtOutTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>          

                        <div class="form-group">
                           <asp:Label ID="Label3" runat="server" Text="Last In Time :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtLastInTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                         <div class="form-group">  
                               <asp:Label ID="Label10" runat="server" style="background-color:cornsilk;" Text="First Session :" CssClass="col-sm-2 control-label"></asp:Label>
                          </div>

                        <%--<fieldset>
                            <legend> First Session</legend>--%>
                            
                        <div class="form-group">  
                               <asp:Label ID="Label4" runat="server" Text="In Time :" CssClass="col-sm-2 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtFSInTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="Label6" runat="server" Text="Out Time :"></asp:Label>
                                </div>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtFSOutTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                        </div>
                       <%-- </fieldset>--%>
                        
                         <div class="form-group">  
                               <asp:Label ID="Label7" runat="server" style="background-color:cornsilk;" Text="Second Session :" CssClass="col-sm-2 control-label"></asp:Label>
                          </div>
                        <div class="form-group">  
                               <asp:Label ID="Label8" runat="server" Text="In Time :" CssClass="col-sm-2 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtSSInTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-1">
                                    <asp:Label ID="Label9" runat="server" Text="Out Time :"></asp:Label>
                                </div>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtSSOutTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                        </div>


                        <div class="row row-custom">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="text-danger">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="margin-top:50px;">
                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-custom">
                <div class="col-lg-6 col-sm-8 col-xs-12">
                    <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0px;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" OnPageIndexChanging="gdv_costingHead_PageIndexChanging" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Shift Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridCategoryName" runat="server" Text='<%# Bind("ShiftName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="In Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridinTime" runat="server" Text='<%# Bind("inTime","{0:t}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Out Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridoutTime" runat="server" Text='<%# Bind("outTime","{0:t}") %>'></asp:Label>
                                    <asp:HiddenField ID="hidlastInTime" Value='<%# Bind("lastInTime") %>' runat="server" />
                                    <asp:HiddenField ID="hidGridAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                    <asp:HiddenField ID="hidfirstSessionInTime" Value='<%# Bind("firstSessionInTime") %>' runat="server" />
                                    <asp:HiddenField ID="hidfirstSessionOutTime" Value='<%# Bind("firstSessionOutTime") %>' runat="server" />
                                    <asp:HiddenField ID="hidsecondSessionInTime" Value='<%# Bind("secondSessionInTime") %>' runat="server" />
                                    <asp:HiddenField ID="hidsecondSessionOutTime" Value='<%# Bind("secondSessionOutTime") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                         </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>