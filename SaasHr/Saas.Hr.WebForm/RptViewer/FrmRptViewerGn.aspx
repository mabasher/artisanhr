﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmRptViewerGn.aspx.cs" Inherits="Saas.Hr.WebForm.RptViewer.FrmRptViewerGn" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%; margin: auto;">
            <asp:ScriptManager
                ID="scrptmgrReport"
                runat="server">
            </asp:ScriptManager>
            <br />
            <asp:Label ID="lbl_message" runat="server" Text=""></asp:Label>
            <CR:CrystalReportViewer
                ID="crvRpt"
                runat="server"
                AutoDataBind="true"
                OnInit="crvRpt_Init"
                OnUnload="crvRpt_Unload" />
        </div>
    </form>
</body>
</html>