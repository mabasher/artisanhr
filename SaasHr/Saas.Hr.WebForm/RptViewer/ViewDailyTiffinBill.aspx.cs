﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewDailyTiffinBill : System.Web.UI.Page
    {
        string sdaysMonth = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Daily Tiffin Bill";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period = Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:left;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<td>" + g_s_rptCompanyName + "</td>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<td>" + g_s_rptName + "</td>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<td>" + g_s_Period + "</td>");
            html.Append("</tr>");
            //html.Append("<tr>");
            //html.Append("<td style='text-align:right;'>PD=Present Day,AD=Absent Days,WH=Weekly Holiday,TD=Total Day</td>");
            //html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td colspan='30' style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double SL, qty, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14, q15, q16, q17, q18, q19, q20, q21, q22, q23, q24, q25, q26, q27, q28, q29, q30, q31,
                tq1, tq2, tq3, tq4, tq5, tq6, tq7, tq8, tq9, tq10, tq11, tq12, tq13, tq14, tq15, tq16, tq17, tq18, tq19, tq20,
                tq21, tq22, tq23, tq24, tq25, tq26, tq27, tq28, tq29, tq30, tq31 = 0,tPayable = 0, Payable = 0;


            qty = 0; q1 = 0; q2 = 0; q3 = 0; q4 = 0; q5 = 0; q6 = 0; q7 = 0; q8 = 0; q9 = 0; q10 = 0; q11 = 0; q12 = 0; q13 = 0; q14 = 0; q15 = 0; q16 = 0; q17 = 0;
            q18 = 0; q19 = 0; q20 = 0; q21 = 0; q22 = 0; q23 = 0; q24 = 0; q25 = 0; q26 = 0; q27 = 0; q28 = 0; q29 = 0; q30 = 0; q31 = 0;
            tq1 = 0; tq2 = 0; tq3 = 0; tq4 = 0; tq5 = 0; tq6 = 0; tq7 = 0; tq8 = 0; tq9 = 0; tq10 = 0; tq11 = 0; tq12 = 0; tq13 = 0; tq14 = 0; tq15 = 0; tq16 = 0; tq17 = 0;
            tq18 = 0; tq19 = 0; tq20 = 0; tq21 = 0; tq22 = 0; tq23 = 0; tq24 = 0; tq25 = 0; tq26 = 0; tq27 = 0; tq28 = 0; tq29 = 0; tq30 = 0; tq31 = 0; tPayable = 0; Payable = 0;

            DataView dtview = new DataView(dt);
            DataTable GDT = dtview.ToTable(true, "department");
            foreach (DataRow grow in GDT.Rows)
            {
                SL = 0;
                Heading = (grow["department"]).ToString();

                html.Append("<table class='table-data' style='width:100%;'>");
                //Building the Header row.
                html.Append("<tr>");
                html.Append("<th> Department : " + Heading + " </th>");
                html.Append("</tr>");
                //Table end.
                html.Append("</table>");

                //Table start.                
                html.Append("<table class='table-data' style='width:100%;font-size: 7pt;font-family:verdana;'>");
                //Building the Header row.

                html.Append("<tr style='background-color:silver;'>");
                html.Append("<td style='width:2%;text-align:center;'>SL#</td>");
                html.Append("<td style='width:7%;text-align:center;'>PIN</td>");
                html.Append("<td style='width:15%;'>Employee Name</td>");
                html.Append("<td style='width:2%;text-align:center;'>1</td>");
                html.Append("<td style='width:2%;text-align:center;'>2</td>");
                html.Append("<td style='width:2%;text-align:center;'>3</td>");
                html.Append("<td style='width:2%;text-align:center;'>4</td>");
                html.Append("<td style='width:2%;text-align:center;'>5</td>");
                html.Append("<td style='width:2%;text-align:center;'>6</td>");
                html.Append("<td style='width:2%;text-align:center;'>7</td>");
                html.Append("<td style='width:2%;text-align:center;'>8</td>");
                html.Append("<td style='width:2%;text-align:center;'>9</td>");
                html.Append("<td style='width:2%;text-align:center;'>10</td>");
                html.Append("<td style='width:2%;text-align:center;'>11</td>");
                html.Append("<td style='width:2%;text-align:center;'>12</td>");
                html.Append("<td style='width:2%;text-align:center;'>13</td>");
                html.Append("<td style='width:2%;text-align:center;'>14</td>");
                html.Append("<td style='width:2%;text-align:center;'>15</td>");
                html.Append("<td style='width:2%;text-align:center;'>16</td>");
                html.Append("<td style='width:2%;text-align:center;'>17</td>");
                html.Append("<td style='width:2%;text-align:center;'>18</td>");
                html.Append("<td style='width:2%;text-align:center;'>19</td>");
                html.Append("<td style='width:2%;text-align:center;'>20</td>");
                html.Append("<td style='width:2%;text-align:center;'>21</td>");
                html.Append("<td style='width:2%;text-align:center;'>22</td>");
                html.Append("<td style='width:2%;text-align:center;'>23</td>");
                html.Append("<td style='width:2%;text-align:center;'>24</td>");
                html.Append("<td style='width:2%;text-align:center;'>25</td>");
                html.Append("<td style='width:2%;text-align:center;'>26</td>");
                html.Append("<td style='width:2%;text-align:center;'>27</td>");
                html.Append("<td style='width:2%;text-align:center;'>28</td>");
                if (sdaysMonth == "29")
                {
                    html.Append("<td style='width:2%;text-align:center;'>29</td>");
                }
                if (sdaysMonth == "30")
                {
                    html.Append("<td style='width:2%;text-align:center;'>29</td>");
                    html.Append("<td style='width:2%;text-align:center;'>30</td>");
                }
                if (sdaysMonth == "31")
                {
                    html.Append("<td style='width:2%;text-align:center;'>29</td>");
                    html.Append("<td style='width:2%;text-align:center;'>30</td>");
                    html.Append("<td style='width:2%;text-align:center;'>31</td>");
                }

                html.Append("<td style='width:2%;text-align:center;'>PD</td>");
                html.Append("<td style='width:2%;text-align:center;'>Rate</td>");
                html.Append("<td style='width:2%;text-align:center;'>Payable</td>");
                html.Append("</tr>");
                //foreach (DataRow row in dt.Rows)

                qty = 0; q1 = 0; q2 = 0; q3 = 0; q4 = 0; q5 = 0; q6 = 0; q7 = 0; q8 = 0; q9 = 0; q10 = 0; q11 = 0; q12 = 0; q13 = 0; q14 = 0; q15 = 0; q16 = 0; q17 = 0;
                q18 = 0; q19 = 0; q20 = 0; q21 = 0; q22 = 0; q23 = 0; q24 = 0; q25 = 0; q26 = 0; q27 = 0; q28 = 0; q29 = 0; q30 = 0; q31 = 0; Payable = 0;
                foreach (DataRow row in dt.Select("Department = '" + Heading + "'"))
                {
                    SL = SL + 1;
                    html.Append("<tr>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(SL);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["EmpId"]);
                    html.Append("</td>");
                    html.Append("<td>");
                    html.Append(row["EmpName"]);
                    html.Append("</td>");

                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["1"]);
                    if (row["1"].ToString() != "") { q1 = q1 + 1; tq1 = tq1 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["2"]);
                    if (row["2"].ToString() != "") { q2 = q2 + 1; tq2= tq2 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["3"]);
                    if (row["3"].ToString() != "") { q3 = q3 + 1; tq3 = tq3 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["4"]);
                    if (row["4"].ToString() != "") { q4 = q4 + 1; tq4 = tq4 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["5"]);
                    if (row["5"].ToString() != "") { q5 = q5 + 1; tq5 = tq5 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["6"]);
                    if (row["6"].ToString() != "") { q6 = q6 + 1; tq6 = tq6 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["7"]);
                    if (row["7"].ToString() != "") { q7 = q7 + 1; tq7 = tq7 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["8"]);
                    if (row["8"].ToString() != "") { q8 = q8 + 1; tq8 = tq8 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["9"]);
                    if (row["9"].ToString() != "") { q9 = q9 + 1; tq9 = tq9 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["10"]);
                    if (row["10"].ToString() != "") { q10 = q10 + 1; tq10 = tq10 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["11"]);
                    if (row["11"].ToString() != "") { q11 = q11 + 1; tq11 = tq11 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["12"]);
                    if (row["12"].ToString() != "") { q12 = q12 + 1; tq12 = tq12 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["13"]);
                    if (row["13"].ToString() != "") { q13 = q13 + 1; tq13 = tq13 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["14"]);
                    if (row["14"].ToString() != "") { q14 = q14 + 1; tq14 = tq14 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["15"]);
                    if (row["15"].ToString() != "") { q15 = q15 + 1; tq15 = tq15 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["16"]);
                    if (row["16"].ToString() != "") { q16 = q16 + 1; tq16 = tq16 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["17"]);
                    if (row["17"].ToString() != "") { q17 = q17 + 1; tq17 = tq17 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["18"]);
                    if (row["18"].ToString() != "") { q18 = q18 + 1; tq18 = tq18 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["19"]);
                    if (row["19"].ToString() != "") { q19 = q19 + 1; tq19 = tq19 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["20"]);
                    if (row["20"].ToString() != "") { q20 = q20 + 1; tq20 = tq20 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["21"]);
                    if (row["21"].ToString() != "") { q21 = q21 + 1; tq21 = tq21 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["22"]);
                    if (row["22"].ToString() != "") { q22 = q22 + 1; tq22 = tq22 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["23"]);
                    if (row["23"].ToString() != "") { q23 = q23 + 1; tq23 = tq23 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["24"]);
                    if (row["24"].ToString() != "") { q24 = q24 + 1; tq24 = tq24 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["25"]);
                    if (row["25"].ToString() != "") { q25 = q25 + 1; tq25 = tq25 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["26"]);
                    if (row["26"].ToString() != "") { q26 = q26 + 1; tq26 = tq26 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["27"]);
                    if (row["27"].ToString() != "") { q27 = q27 + 1; tq27 = tq27 + 1; }
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["28"]);
                    if (row["28"].ToString() != "") { q28 = q28 + 1; tq28 = tq28 + 1; }
                    html.Append("</td>");

                    if (sdaysMonth == "29")
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["29"]);
                        if (row["29"].ToString() != "") { q29 = q29 + 1; tq29 = tq29 + 1; }
                        html.Append("</td>");
                    }
                    if (sdaysMonth == "30")
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["29"]);
                        if (row["29"].ToString() != "") { q29 = q29 + 1; tq29 = tq29 + 1; }
                        html.Append("</td>");
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["30"]);
                        if (row["30"].ToString() != "") { q30 = q30 + 1; tq30= tq30 + 1; }
                        html.Append("</td>");
                    }
                    if (sdaysMonth == "31")
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["29"]);
                        if (row["29"].ToString() != "") { q29 = q29 + 1; tq29 = tq29 + 1; }
                        html.Append("</td>");
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["30"]);
                        if (row["30"].ToString() != "") { q30 = q30 + 1; tq30 = tq30 + 1; }
                        html.Append("</td>");
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["31"]);
                        if (row["31"].ToString() != "") { q31 = q31 + 1; tq31 = tq31 + 1; }
                        html.Append("</td>");
                    }
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["Present"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:right;'>");
                    html.Append(row["Rate"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:right;'>");
                    html.Append(row["Payable"]);
                    Payable = Payable + Convert.ToDouble(row["Payable"]);
                    tPayable = tPayable + Convert.ToDouble(row["Payable"]);
                    html.Append("</td>");
                    html.Append("</tr>");

                }


                html.Append("<tr>");
                html.Append("<td style='text-align:center;' colspan='3'>");
                html.Append("Sub Total");
                html.Append("</td>");

                html.Append("<td style='text-align:center;'>");
                html.Append(q1);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q2);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q3);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q4);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q5);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q6);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q7);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q8);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q9);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q10);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q11);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q12);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q13);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q14);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q15);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q16);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q17);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q18);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q19);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q20);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q21);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q22);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q23);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q24);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q25);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q26);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q27);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(q28);
                html.Append("</td>");
                if (sdaysMonth == "29")
                {
                    html.Append("<td style='text-align:center;'>");
                    html.Append(q29);
                    html.Append("</td>");
                }
                if (sdaysMonth == "30")
                {
                    html.Append("<td style='text-align:center;'>");
                    html.Append(q29);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(q30);
                    html.Append("</td>");
                }
                if (sdaysMonth == "31")
                {
                    html.Append("<td style='text-align:center;'>");
                    html.Append(q29);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(q30);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(q31);
                    html.Append("</td>");
                }
                html.Append("<td style='text-align:center;' colspan='2'>");
                html.Append("");
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(Payable);
                html.Append("</td>");
                html.Append("</tr>");


            }

            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td style='text-align:center;' colspan='3'>");
            html.Append("Grand Total");
            html.Append("</td>");

            html.Append("<td style='text-align:center;'>");
            html.Append(tq1);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq2);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq3);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq4);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq5);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq6);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq7);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq8);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq9);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq10);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq11);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq12);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq13);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq14);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq15);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq16);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq17);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq18);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq19);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq20);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq21);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq22);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq23);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq24);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq25);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq26);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq27);
            html.Append("</td>");
            html.Append("<td style='text-align:center;'>");
            html.Append(tq28);
            html.Append("</td>");
            if (sdaysMonth == "29")
            {
                html.Append("<td style='text-align:center;'>");
                html.Append(tq29);
                html.Append("</td>");
            }
            if (sdaysMonth == "30")
            {
                html.Append("<td style='text-align:center;'>");
                html.Append(tq29);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(tq30);
                html.Append("</td>");
            }
            if (sdaysMonth == "31")
            {
                html.Append("<td style='text-align:center;'>");
                html.Append(tq29);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(tq30);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(tq31);
                html.Append("</td>");
            }
            html.Append("<td style='text-align:center;' colspan='2'>");
            html.Append("");
            html.Append("</td>");
            html.Append("<td style='text-align:right;'>");
            html.Append(tPayable);
            html.Append("</td>");
            html.Append("</tr>");


            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {
            string sQry = string.Empty;
            string sFromDate = Session[GlobalVariables.g_s_rptfromDate].ToString();
            string sToDate = Session[GlobalVariables.g_s_rpttoDate].ToString();
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string sJobLocationId = Session[GlobalVariables.g_s_rptforJobLocationId].ToString();
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sSectionId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sCompanyId = Session[GlobalVariables.g_s_CompanyAutoId].ToString();
            string srptStatus = Session[GlobalVariables.g_s_rptStatus].ToString();
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            string s_trShift = Convert.ToString(Session[GlobalVariables.g_s_rptShift]);
            string s_Userlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            int sdaysofMonth = 0;
            int iMonth = 0;
            int iYear = 0;
            iYear = Convert.ToInt32(Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("yyyy"));
            iMonth = Convert.ToInt32(Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("MM"));
            sdaysofMonth = System.DateTime.DaysInMonth(iYear, iMonth);
            sdaysMonth = sdaysofMonth.ToString();
            srptStatus = "AnA";//All type Attendance

            //@companyAutoId varchar(50),
            //@UserLevel as varchar(50),
            //@employeeId varchar(50),
            //@sectionId varchar(50),
            //@LineId varchar(50),
            //@departmentId varchar(250),
            //@month varchar(2),
            //@year char(4),
            //@daysInMonth int

            sQry = "procHTMLDailyTiffinBill '" + sCompanyId + "','" + s_Userlevel + "','" + sEmpId + "','%','" + sJobLocationId + "','" + sDepartmentId + "','"
                                            + iMonth.ToString() + "','" + iYear.ToString() + "','" + sdaysofMonth.ToString() + "'";
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=ProjectTarget_" + CampaignID + ".xls");
            Response.AddHeader("content-disposition", "attachment;filename=DailyTiffin.xls");
            Response.Charset = "";
            //Response.ContentType = "application/vnd.xls";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}