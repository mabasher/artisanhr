﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewAppraisalReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Performance Appraisal Report";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period =  Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th>" + g_s_rptCompanyName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th>" + g_s_rptName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th>" + g_s_Period + "</th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            if (Session[GlobalVariables.g_s_rptforMonth].ToString() == "0")
            {
                getRptDataYearly();
            }
            else
            {
                getRptData();
            }

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }

        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0;  double SL = 0;  double ttpresent = 0;   double tpresent = 0;  double tpresentpercent = 0;
            double ttabsent = 0;  double tabsent = 0;  double tabsentpercent = 0;  double ttleave = 0;  double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty;  late = "N";  sStatus = ""; sIntime = "";


            html.Append("<table class='table-data' style='width:100%;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th> </th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            //PIN,empname,Designation,Department,PresentBasic,grade,RevisedBasic,IncrementAmount,Note

            //Table start.                
            html.Append("<table class='table-data' style='width:100%;font-size: 7pt;font-family:verdana;'>");
            //Building the Header row.
            
            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td>SL#</td>");
            html.Append("<td>PIN</td>");
            html.Append("<td>Employee Name</td>");
            html.Append("<td>Designation</td>");
            html.Append("<td>Department</td>");
            html.Append("<td style='text-align:center;'>Present Score</td>");
            html.Append("<td style='text-align:center;'>Late Score</td>");
            html.Append("<td style='text-align:center;'>Absent Score</td>");
            html.Append("<td style='text-align:center;'>Education Score</td>");
            html.Append("<td style='text-align:center;'>Professional Knowledge</td>");
            html.Append("<td style='text-align:center;'>Quality Accuracy</td>");
            html.Append("<td style='text-align:center;'>Sincerity Discipline</td>");
            html.Append("<td style='text-align:center;'>Performance</td>");
            html.Append("<td style='text-align:center;'>Total</td>");
            html.Append("</tr>");
            foreach (DataRow row in dt.Rows)
            {                
                SL = SL + 1; tpresent = 0; tleave = 0; tabsent = 0;
                html.Append("<tr>");
                html.Append("<td>");
                html.Append(SL);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PIN"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["empname"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["Designation"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["Department"]);
                html.Append("</td>");                
                html.Append("<td style='text-align:right;'>");
                html.Append(row["PresentDayScore"]);
                //tpresent =Convert.ToDouble(row["PresentBasic"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["LateDayScore"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["AbsentDayScore"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["EducationScore"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["ProfessionalKnowledge"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["QualityAccuracy"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["SincerityDiscipline"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["Performance"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["Total"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            //html.Append("<tr style='background-color:silver;'>");
            //html.Append("<td colspan='5' style='text-align:center;'>");
            //html.Append("Total : ");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append(ttpresent.ToString("#,##0.00"));
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("</tr>");
            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private void getRptDataYearly()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0; double SL = 0; double ttpresent = 0; double tpresent = 0; double tpresentpercent = 0;
            double ttabsent = 0; double tabsent = 0; double tabsentpercent = 0; double ttleave = 0; double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty; late = "N"; sStatus = ""; sIntime = "";


            html.Append("<table class='table-data' style='width:100%;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th> </th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            //PIN,empname,Designation,Department,PresentBasic,grade,RevisedBasic,IncrementAmount,Note

            //Table start.                
            html.Append("<table class='table-data' style='width:100%;font-size: 7pt;font-family:verdana;'>");
            //Building the Header row.

            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td>SL#</td>");
            html.Append("<td>PIN</td>");
            html.Append("<td>Employee Name</td>");
            html.Append("<td>Designation</td>");
            html.Append("<td>Department</td>");
            html.Append("<td style='text-align:center;'>January</td>");
            html.Append("<td style='text-align:center;'>February</td>");
            html.Append("<td style='text-align:center;'>March</td>");
            html.Append("<td style='text-align:center;'>April</td>");
            html.Append("<td style='text-align:center;'>May</td>");
            html.Append("<td style='text-align:center;'>June</td>");
            html.Append("<td style='text-align:center;'>July</td>");
            html.Append("<td style='text-align:center;'>August</td>");
            html.Append("<td style='text-align:center;'>September</td>");
            html.Append("<td style='text-align:center;'>October</td>");
            html.Append("<td style='text-align:center;'>November</td>");
            html.Append("<td style='text-align:center;'>December</td>");
            html.Append("<td style='text-align:center;'>Total</td>");
            html.Append("<td style='text-align:center;'>Grade</td>");
            html.Append("</tr>");
            foreach (DataRow row in dt.Rows)
            {
                SL = SL + 1; tpresent = 0; tleave = 0; tabsent = 0;
                html.Append("<tr>");
                html.Append("<td>");
                html.Append(SL);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PIN"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["empname"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["Designation"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["Department"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sJan"]);
                //tpresent =Convert.ToDouble(row["PresentBasic"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sFeb"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sMar"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["SApr"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sMay"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sJun"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sJul"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sAug"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sSep"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sOct"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sNov"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["sDec"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["Total"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["Grade"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            //html.Append("<tr style='background-color:silver;'>");
            //html.Append("<td colspan='5' style='text-align:center;'>");
            //html.Append("Total : ");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append(ttpresent.ToString("#,##0.00"));
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("<td style='text-align:center;'>");
            //html.Append("");
            //html.Append("</td>");
            //html.Append("</tr>");
            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string s_Userlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
            string sYear = Session[GlobalVariables.g_s_rptforYear].ToString();
            string sMonthId = Session[GlobalVariables.g_s_rptforMonth].ToString();
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand("procHTMLPerformanceAppraisal '" + sDepartmentId + "','" + sYear + "','" + sMonthId + "','"+ s_Userlevel + "'", sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=ProjectTarget_" + CampaignID + ".xls");
            Response.AddHeader("content-disposition", "attachment;filename=SAAS_DailyAttendance.xls");
            Response.Charset = "";
            //Response.ContentType = "application/vnd.xls";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}