﻿<%@ Page Title="Leave Summary Report" Language="C#" MasterPageFile="~/SiteRpt.Master" AutoEventWireup="true" CodeBehind="ViewLeaveSummary.aspx.cs" Inherits="Saas.Hr.WebForm.RptViewer.ViewLeaveSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
    <style type="text/css">
                
        .marginTop
        {
        	 margin-top:3.3px;
        	}
        	.table-data
        	{
        		border-collapse: collapse;
        		}
        	.table-data, table.table-data td, table.table-data th
        	{
        		border: 1px solid gray;
                
        		}
        
            .print {	           
            }
             @media print {
	            .print {display:block}
	            .btn-print {display:none;}
	            .btnExport {display:none;}
	            .rptvw {display:none;}
                
                    @page {
                        size: auto;
                    }
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="rptvw" style="width:100%;text-align:center;background-color:#1e1e2d; text-align:right;font-size:20px;font-weight:bold;color:white;height:40px;">
        <p> Report Viewer</p>
    </div>
    <div style="width: 100%; text-align: center; background-color: #1e1e2d; text-align: right; ">
     <asp:Button runat="server" class="btn-print" Text="Print Data" OnClientClick="window.print()"/>
     <asp:Button ID="btnExport" class="btnExport" runat="server" Text="Export To Excel" OnClick="ExportToExcel" />                    
    </div>
                        
    <div style="background-color:White;" class="print">
    <asp:PlaceHolder id="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>
