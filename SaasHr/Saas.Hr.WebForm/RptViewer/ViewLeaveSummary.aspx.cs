﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewLeaveSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Leave Summary Information";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period =  Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th>" + g_s_rptCompanyName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th>" + g_s_rptName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th>" + g_s_Period + "</th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0;  double SL = 0;  double ttpresent = 0;   double tpresent = 0;  double tpresentpercent = 0;
            double ttabsent = 0;  double tabsent = 0; double ttleaveabsent = 0; double tleaveabsent = 0; double tabsenteeism = 0;
            double ttmanpower = 0; double tmanpower = 0;
            double tabsentpercent = 0;  double ttleave = 0;  double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty;  late = "N";  sStatus = ""; sIntime = "";


            html.Append("<table class='table-data' style='width:100%;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th> </th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");


            //Table start.                
            html.Append("<table class='table-data' style='width:100%;font-size: 9pt;font-family:verdana;'>");
            //Building the Header row.



            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td>SL</td>");
            html.Append("<td style='text-align:center;'>PIN</td>");
            html.Append("<td style='text-align:center;'>Employee Name</td>");
            html.Append("<td style='text-align:center;'>Designation</td>");
            html.Append("<td style='text-align:center;'>Department</td>");

            html.Append("<td style='text-align:center;'>CL Limit</td>");
            html.Append("<td style='text-align:center;'>CL Enjoy</td>");
            html.Append("<td style='text-align:center;'>CL Balance</td>");

            html.Append("<td style='text-align:center;'>EL Limit</td>");
            html.Append("<td style='text-align:center;'>EL Enjoy</td>");
            html.Append("<td style='text-align:center;'>EL Balance</td>");

            html.Append("<td style='text-align:center;'>SL Limit</td>");
            html.Append("<td style='text-align:center;'>SL Enjoy</td>");
            html.Append("<td style='text-align:center;'>SL Balance</td>");

            html.Append("<td style='text-align:center;'>Remarks</td>");
            html.Append("</tr>");
            foreach (DataRow row in dt.Rows)
            {                
                SL = SL + 1; tpresent = 0; tleave = 0; tabsent = 0; tmanpower = 0; tleaveabsent = 0;
                html.Append("<tr>");
                html.Append("<td>");
                html.Append(SL);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["PIN"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["empName"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["Designation"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["Department"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#F8F9F9;'>");
                html.Append(row["CLLimit"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#F8F9F9;'>");
                html.Append(row["CLTaken"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#F8F9F9;'>");
                html.Append(row["CLBalance"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#EAEDED;'>");
                html.Append(row["ELLimit"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#EAEDED;'>");
                html.Append(row["ELTaken"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#EAEDED;'>");
                html.Append(row["ELBalance"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#F8F9F9;'>");
                html.Append(row["SLLimit"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#F8F9F9;'>");
                html.Append(row["SLTaken"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;background-color:#F8F9F9;'>");
                html.Append(row["SLBalance"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append("");
                html.Append("</td>");
                

                //         PIN,empName,Designation, Department,joinDate,
                //CLLimit,CLTaken,CLBalance,ELLimit, ELTaken, ELBalance, SLLimit,SLTaken,SLBalance


                html.Append("</tr>");
            }

            html.Append("</table>");

            
            //html.Append("<table class='table-data' style='width:100%;margin-top:20px;'>");
            //html.Append("<tr style='background-color:silver;'>");

            //html.Append("<td style='text-align:center;'>Total Manpower</td>");
            //html.Append("<td style='text-align:center;'>Total Present</td>");
            //html.Append("<td style='text-align:center;'>Total Leave</td>");
            //html.Append("<td style='text-align:center;'>Total Absent</td>");
            //html.Append("<td style='text-align:center;'>Remarks</td>");

            //html.Append("<td colspan='2' style='text-align:center;'>");
            //html.Append("Total : ");
            //html.Append("</td>");

            //html.Append("</tr>");

            ////Table end.
            //html.Append("</table>");
            
            //Total : 220 Present :104 (70%) Abent :20 (10%) Leave : 30 (20%) 
            //Append the HTML string to Placeholder.

            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {
            string sFromDate = Session[GlobalVariables.g_s_rptfromDate].ToString();
            string sToDate = Session[GlobalVariables.g_s_rpttoDate].ToString();
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string sJobLocationId = Session[GlobalVariables.g_s_rptforJobLocationId].ToString();
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sCompanyId = Session[GlobalVariables.g_s_CompanyAutoId].ToString();

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            //ProcRptHTMLEmpWiseLeaveSummary
            //procHTMLMonthlyAttendance
            SqlCommand cmd = new SqlCommand("ProcRptHTMLEmpWiseLeaveSummary '" + sJobLocationId + "','" + sDepartmentId + "','" + sEmpId + "','" + sCompanyId + "','" + Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("yyyy") + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }

        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=ProjectTarget_" + CampaignID + ".xls");
            Response.AddHeader("content-disposition", "attachment;filename=Leave.xls");
            Response.Charset = "";
            //Response.ContentType = "application/vnd.xls";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}