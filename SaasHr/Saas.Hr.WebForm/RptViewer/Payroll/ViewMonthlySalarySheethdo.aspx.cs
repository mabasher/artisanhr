﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer.Payroll
{
    public partial class ViewMonthlySalarySheetOTHoliDay : System.Web.UI.Page
    {
        string sdaysMonth = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Monthly Salary Sheet (Holiday OT)";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period =  Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th colspan='14'>" + g_s_rptCompanyName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th colspan='14'>" + g_s_rptName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th colspan='14'>" + g_s_Period + "</th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td colspan='14' style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0;  double SL = 0;  double ttpresent = 0;   double tpresent = 0;  double tpresentpercent = 0;
            double ttabsent = 0;  double tabsent = 0;  double tabsentpercent = 0;  double ttleave = 0;  double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty;  late = "N";  sStatus = ""; sIntime = "";

            double ttSalary = 0; double ttlSalary = 0; double GrandTtlSalary = 0;
            //             [SlNo],[Section],[Department],[EmpId],[CardId],[EmpName],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],
            //[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31],[Present],[Absent],[LeavePay],[Leave],[WeeklyOff],[Total]


            DataView dtview = new DataView(dt);
            DataTable GDT = dtview.ToTable(true, "department");
            foreach (DataRow grow in GDT.Rows)
            {
                SL = 0;
                ttSalary = 0;
                ttlSalary = 0;
                Heading = (grow["department"]).ToString();

                html.Append("<table class='table-data' style='width:100%;'>");
                //Building the Header row.
                html.Append("<tr>");
                html.Append("<th colspan='14'> Department : " + Heading + " </th>");
                html.Append("</tr>");
                //Table end.
                html.Append("</table>");


                //Table start.                
                html.Append("<table class='table-data' style='width:100%;font-size: 10pt;font-family:verdana;'>");
                //Building the Header row.


                html.Append("<tr>");
                html.Append("<td  colspan='8' style='text-align:center;'>Particulars</td>");
                html.Append("<td  colspan='6' style='text-align:center;'>Salary Details</td>");
                html.Append("</tr>");


                html.Append("<tr style='background-color:silver;'>");
                    html.Append("<td style='text-align:center;'>SL</td>");
                    html.Append("<td>Employee Name</td>");
                    html.Append("<td style='text-align:center;'>Designation</td>");
                    html.Append("<td style='text-align:center;'>Section</td>");
                    html.Append("<td style='text-align:center;'>Id No</td>");
                    html.Append("<td style='text-align:center;'>Grade</td>");
                    html.Append("<td style='text-align:center;'>Join Date</td>");
                    html.Append("<td style='text-align:center;'>Month days</td>");

                    html.Append("<td style='text-align:center;'>Gross</td>");
                    html.Append("<td style='text-align:center;'>Basic</td>");
                    html.Append("<td style='text-align:center;'>Hour</td>");
                    html.Append("<td style='text-align:center;'>Rate</td>");
                    html.Append("<td style='text-align:center;'>Net Payable</td>");
                    html.Append("<td style='text-align:center;'>Signature</td>");
                html.Append("</tr>");
                //foreach (DataRow row in dt.Rows)

                foreach (DataRow row in dt.Select("Department = '" + Heading + "'"))
                    {
                    SL = SL + 1; tpresent = 0; tleave = 0; tabsent = 0;
                    html.Append("<tr>");
                  
                    html.Append("<td style='text-align:center;'>");
                    html.Append(SL);
                    html.Append("</td>");
                   
                    html.Append("<td>");
                    html.Append(row["EmployeeName"]);
                    html.Append("</td>");
                    
                    html.Append("<td>");
                    html.Append(row["Designation"]);
                    html.Append("</td>");
                   
                    html.Append("<td>");
                    html.Append(row["Department"]);
                    html.Append("</td>");
                    
                    html.Append("<td>");
                    html.Append(row["PIN"]);
                    html.Append("</td>");
                   
                    html.Append("<td>");
                    html.Append(row["SalaryGrade"]);
                    html.Append("</td>");
                   
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["JoinDate"]);
                    html.Append("</td>");
                   
                    html.Append("<td style='text-align:center;>");
                    html.Append(row["daysInMonth"]);
                    html.Append("</td>");
                    
                    html.Append("<td style='text-align:right;'>");
                    html.Append(Convert.ToDouble(row["sGross"]).ToString("#,###0"));
                    html.Append("</td>");
                   
                    html.Append("<td style='text-align:right;'>");
                    html.Append(Convert.ToDouble(row["sBasic"]).ToString("#,###0"));
                    html.Append("</td>");
                  
                    html.Append("<td style='text-align:center;'>");
                    //html.Append(row["sOT"]);
                    html.Append(row["sOTHoliday"]);
                    html.Append("</td>");
                  
                    html.Append("<td style='text-align:center;'>");
                    html.Append(Convert.ToDouble(row["sOTRate"]).ToString("#,###0.00"));
                    html.Append("</td>");                 
                   
                    html.Append("<td style='text-align:right;'>");
                    //html.Append(Convert.ToDouble(row["NettPayable"]).ToString("#,###0"));
                    html.Append(Convert.ToDouble(row["NettPayableHoliday"]).ToString("#,###0"));                    
                    html.Append("</td>");
                    ttSalary = Convert.ToDouble(row["NettPayableHoliday"]);
                    ttlSalary = ttSalary + ttlSalary;

                    html.Append("<td style='Height:80px;'>");
                    html.Append("&nbsp;");
                    html.Append("</td>");

                    html.Append("</tr>");
                }
                html.Append("<tr>");
                html.Append("<td colspan='12' style='text-align:right;'>");
                html.Append("Department Wise Total :");
                html.Append("</td>");
                html.Append("<td style='text-align:right; Font-Bold=true'>");
                html.Append(Convert.ToString(Convert.ToDecimal(ttlSalary).ToString("#,###.00")));
                html.Append("</td>");

                GrandTtlSalary = ttlSalary + GrandTtlSalary;
            }

            html.Append("<tr>");
                html.Append("<td colspan='12' style='text-align:right;'>");
                html.Append("Grand Total :");
                html.Append("</td>");
                html.Append("<td style='text-align:right; Font-Bold=true'>");
                html.Append(Convert.ToString(Convert.ToDecimal(GrandTtlSalary).ToString("#,###.00")));
                html.Append("</td>");
            html.Append("</tr>");

            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {
            string sQry = string.Empty;
            string sSalaryDate = Session[GlobalVariables.g_s_rptSalaryDate].ToString();
            string sFromDate = Session[GlobalVariables.g_s_rptfromDate].ToString();
            string sToDate = Session[GlobalVariables.g_s_rpttoDate].ToString();
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sCompanyId = Session[GlobalVariables.g_s_CompanyAutoId].ToString();
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            string s_Userlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
            string s_RptType = Convert.ToString(Session[GlobalVariables.g_s_rptTpe]);
            string iMonth = Convert.ToString(Session[GlobalVariables.g_s_rptforMonth]);
            string iYear = Convert.ToString(Session[GlobalVariables.g_s_rptforYear]);
            //ProcSalarySheet
            //ProcSalarySheetForWeb

            sQry = "ProcSalarySheetwith2hourOTWeb '" + sCompanyId 
                                            + "','" 
                                            + sEmpId 
                                            + "','" 
                                            + sDepartmentId 
                                            + "','" 
                                            + iMonth
                                            + "','" 
                                            + iYear 
                                            + "','"
                                            + Convert.ToDateTime(sSalaryDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                            + "','"
                                            + Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                            + "','"
                                            + Convert.ToDateTime(sToDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                            + "','"
                                            + s_Userlevel + "','"+ s_RptType + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=SalarySheet.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}