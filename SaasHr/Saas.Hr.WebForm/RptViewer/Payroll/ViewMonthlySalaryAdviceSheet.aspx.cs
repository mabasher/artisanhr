﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewMonthlySalaryAdviceSheet : System.Web.UI.Page
    {
        string sdaysMonth = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Monthly Salary Sheet";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period = Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            //html.Append("<tr>");
            //html.Append("<th colspan='38'>" + g_s_rptCompanyName + "</th>");
            //html.Append("</tr>");
            //html.Append("<tr>");
            //html.Append("<th colspan='38'>" + g_s_rptName + "</th>");
            //html.Append("</tr>");
            //html.Append("<tr>");
            //html.Append("<th colspan='38'>" + g_s_Period + "</th>");
            //html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            //htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            //htmlFooter.Append("<tr>");
            //htmlFooter.Append("<td colspan='38' style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("</tr>");
            //htmlFooter.Append("</table>");
            //PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0; double SL = 0; double ttpresent = 0; double tpresent = 0; double tpresentpercent = 0;
            double ttabsent = 0; double tabsent = 0; double tabsentpercent = 0; double ttleave = 0; double tleave = 0; double tleavepercent = 0;
            double ttSalary = 0; double ttlSalary = 0; double GrandTtlSalary = 0;
            string sStatus = string.Empty; string late = string.Empty; late = "N"; sStatus = ""; sIntime = "";
            string BankCode, Note, sOp5, BankACNo = string.Empty;
            string FirstPartyName = "MAINETTI PACKAGING BD PVT LTD";
            string FirstPartyInformationLine1 = "129 131 DEPZ";
            string FirstPartyInformationLine2 = "SAVER";
            string FirstPartyInformationLine3 = "DHAKA";

            DataView dtview = new DataView(dt);
            DataTable GDT = dtview.ToTable(true, "BankCode", "Note", "sOp5", "BankACNo"); ;
            foreach (DataRow grow in GDT.Rows)
            {
                BankCode = (grow["BankCode"]).ToString();
                Note = (grow["Note"]).ToString();
                sOp5 = (grow["sOp5"]).ToString();
                BankACNo = (grow["BankACNo"]).ToString();

                html.Append("<div class='header'>");
                html.Append("<table  style='width:100%;margin-top:10px;font-family:Calibri;'>");

                html.Append("<tr>");
                html.Append("<td style='width:15%;font-size:14px;font-weight:bold;'>Debit Account</td>");
                html.Append("<td style='width:35%;font-size:14px;'>" + BankACNo + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>Payment Set Code</td>");              
                html.Append("<td style='width:20%;font-size:14px;'>" + Note + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>Value Date</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + sOp5 + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>Batch Ref</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + Note + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>First Party Name</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + FirstPartyName + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>First Party Information Line 1</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + FirstPartyInformationLine1 + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>First Party Information Line 2</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + FirstPartyInformationLine2 + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td style='width:11%;font-size:14px;font-weight:bold;'>First Party Information Line 3</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + FirstPartyInformationLine3 + "</td>");
                html.Append("</tr>");

                html.Append("</table>");
            }

            //Table start.                
            html.Append("<table class='table-data' style='width:100%;font-size: 7pt;font-family:verdana;'>");
            //Building the Header row.
            html.Append("<thead>");
            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td style='width:5%;text-align:center;'>SL</td>");
            html.Append("<td style='width:10%;'>Reference</td>");
            html.Append("<td style='width:25%;text-align:center;'>Beneficiary Name</td>");
            html.Append("<td style='width:15%;text-align:center;'>Credit Account</td>");
            html.Append("<td style='width:15%;text-align:center;'>Amount</td>");
            html.Append("<td style='width:15%;text-align:center;'>Email Address</td>");
            html.Append("<td style='width:15%;text-align:center;'>Ben Bank Code/ Routing No</td>");
            html.Append("</tr>");
            html.Append("</thead>");
            html.Append("<tbody>");
            foreach (DataRow row in dt.Rows)
            {
                SL = SL + 1;
                if (SL == 25 || (SL - 25) % 30 == 0)
                    html.Append("<tr style='page-break-after: always;'>");
                else
                    html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(SL);
                html.Append("</td>");

                html.Append("<td>");
                html.Append(row["CardNo"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["EmployeeName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["sAccountNumber"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(Convert.ToDouble(row["Netvalue"]).ToString("#,###0"));
                html.Append("</td>");

                html.Append("<td>");
                html.Append(row["OfficialMail"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["sRoutingNumber"]);
                html.Append("</td>");


                html.Append("</tr>");
            }

            html.Append("</tr>");
            html.Append("</tbody>");
            html.Append("</table>");
            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {
            string sQry = string.Empty;
            sQry = Session[GlobalVariables.g_s_rptQry].ToString();
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=SalarySheet.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}