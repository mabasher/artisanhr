﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewArrivalListReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Daily Attendance";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period = Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th colspan='8'>" + g_s_rptCompanyName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th colspan='8'>" + g_s_rptName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th colspan='8'>" + g_s_Period + "</th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td colspan='8' style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0; double SL = 0; double ttpresent = 0; double tpresent = 0; double tpresentpercent = 0;
            double ttabsent = 0; double tabsent = 0; double tabsentpercent = 0; double ttleave = 0; double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty; late = "N"; sStatus = ""; sIntime = "";

            //department empId   empName Designation accessDate inTime  outTime standardAccessTime  lateStatus Shift

            DataView dtview = new DataView(dt);
            DataTable GDT = dtview.ToTable(true, "department");
            foreach (DataRow grow in GDT.Rows)
            {
                SL = 0;
                Heading = (grow["department"]).ToString();

                html.Append("<table class='table-data' style='width:100%;'>");
                //Building the Header row.
                html.Append("<tr>");
                html.Append("<th> Department : " + Heading + " </th>");
                html.Append("</tr>");
                //Table end.
                html.Append("</table>");

                //Table start.                
                html.Append("<table class='table-data' style='width:100%;font-size: 8pt;font-family:verdana;'>");
                //Building the Header row.

                html.Append("<tr style='background-color:silver;'>");
                html.Append("<td style='width:3%;text-align:center;'>SL#</td>");
                html.Append("<td style='width:7%;text-align:center;'>PIN</td>");
                html.Append("<td style='width:20%;'>Employee Name</td>");
                html.Append("<td style='width:10%;'>Designation</td>");
                html.Append("<td style='width:10%;'>Department</td>");
                html.Append("<td style='text-align:center;'>Access Date</td>");
                html.Append("<td style='text-align:center;'>Standard Time</td>");
                html.Append("<td style='text-align:center;'>In Time</td>");
                html.Append("<td style='text-align:center;'>Out Time</td>");
                html.Append("<td style='text-align:center;'>OT (h:mm)</td>");
                html.Append("<td style='text-align:center;'>WH (h:mm)</td>");
                html.Append("</tr>");
                html.Append(GetReportString(dt.Select("department = '" + Heading + "'")));
                html.Append("</table>");
                html.Append("<div class='page-break'></div>");
            }
            

            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private StringBuilder GetReportString(DataRow[] dataList) 
        {
            var html = new StringBuilder();
            var SL = 1; //tpresent = 0; tleave = 0; tabsent = 0;
            foreach (DataRow row in dataList)
            {
                html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(SL);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["EmpId"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["EmpName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["Designation"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["department"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["AccessDate"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["StandardAccessTime"]);
                html.Append("</td>");

                if (row["InTime"].ToString() == "00:00:00")
                {
                    html.Append("<td style='text-align:center;'>");
                    html.Append("");
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append("");
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append("");
                    html.Append("</td>");
                }
                else
                {
                    if (row["lateStatus"].ToString() == "Y")
                    {
                        html.Append("<td style='text-align:center;color:red;'>");
                        html.Append(row["InTime"]);
                        html.Append("</td>");
                    }
                    else
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["InTime"]);
                        html.Append("</td>");
                    }

                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["OutTime"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["Shift"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["WH"]);
                    html.Append("</td>");
                    
                }
                html.Append("</tr>");
                SL++;
            }
            return html;
        }

        private DataTable GetData()
        {
            string sQry = string.Empty;
            string sFromDate = Session[GlobalVariables.g_s_rptfromDate].ToString();
            string sToDate = Session[GlobalVariables.g_s_rpttoDate].ToString();
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string sJobLocationId = Session[GlobalVariables.g_s_rptforJobLocationId].ToString();
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sSectionId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sCompanyId = Session[GlobalVariables.g_s_CompanyAutoId].ToString();
            string srptStatus = Session[GlobalVariables.g_s_rptStatus].ToString();
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            string s_trShift = Convert.ToString(Session[GlobalVariables.g_s_rptShift]);
            srptStatus = "AnA";//All type Attendance

            sQry = "ProcRptDailyAttendance '" + sCompanyId + "','" + sEmpId + "','%','"+ sJobLocationId + "','" + sDepartmentId + "','" + s_trShift + "','"
                                            + Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','"
                                            + Convert.ToDateTime(sToDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','Web','" + s_trMode + "'";
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=ProjectTarget_" + CampaignID + ".xls");
            Response.AddHeader("content-disposition", "attachment;filename=SAAS_ArrivalList.xls");
            Response.Charset = "";
            //Response.ContentType = "application/vnd.xls";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}