﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewMonthlyAttendanceSummary : System.Web.UI.Page
    {
        string sdaysMonth = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Monthly Attendance Summary";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period =  Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:left;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<td>" + g_s_rptCompanyName + "</td>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<td>" + g_s_rptName + "</td>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<td>" + g_s_Period+ "</td>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<td style='text-align:right;'>PD=Present Day,AD=Absent Days,WH=Weekly Holiday,TD=Total Day</td>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td colspan='30' style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0;  double SL = 0;  double ttpresent = 0;   double tpresent = 0;  double tpresentpercent = 0;
            double ttabsent = 0;  double tabsent = 0;  double tabsentpercent = 0;  double ttleave = 0;  double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty;  late = "N";  sStatus = ""; sIntime = "";

//[SlNo],[Section],[Department],[EmpId],[CardId],[EmpName],[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],
//[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31],[Present],[Absent],[LeavePay],[Leave],[WeeklyOff],[Total]


        DataView dtview = new DataView(dt);
            DataTable GDT = dtview.ToTable(true, "department");
            foreach (DataRow grow in GDT.Rows)
            {
                SL = 0;
                Heading = (grow["department"]).ToString();

                html.Append("<table class='table-data' style='width:100%;'>");
                //Building the Header row.
                html.Append("<tr>");
                html.Append("<th> Department : " + Heading + " </th>");
                html.Append("</tr>");
                //Table end.
                html.Append("</table>");


                //Table start.                
                html.Append("<table class='table-data' style='width:100%;font-size: 7pt;font-family:verdana;'>");
                //Building the Header row.

                html.Append("<tr style='background-color:silver;'>");

                html.Append("<td style='width:2%;text-align:center;'>SL#</td>");
                html.Append("<td style='width:7%;text-align:center;'>PIN</td>");
                html.Append("<td style='width:15%;'>Employee Name</td>");
                html.Append("<td style='width:2%;text-align:center;'>1</td>");
                html.Append("<td style='width:2%;text-align:center;'>2</td>");
                html.Append("<td style='width:2%;text-align:center;'>3</td>");
                html.Append("<td style='width:2%;text-align:center;'>4</td>");
                html.Append("<td style='width:2%;text-align:center;'>5</td>");
                html.Append("<td style='width:2%;text-align:center;'>6</td>");
                html.Append("<td style='width:2%;text-align:center;'>7</td>");
                html.Append("<td style='width:2%;text-align:center;'>8</td>");
                html.Append("<td style='width:2%;text-align:center;'>9</td>");
                html.Append("<td style='width:2%;text-align:center;'>10</td>");
                html.Append("<td style='width:2%;text-align:center;'>11</td>");
                html.Append("<td style='width:2%;text-align:center;'>12</td>");
                html.Append("<td style='width:2%;text-align:center;'>13</td>");
                html.Append("<td style='width:2%;text-align:center;'>14</td>");
                html.Append("<td style='width:2%;text-align:center;'>15</td>");
                html.Append("<td style='width:2%;text-align:center;'>16</td>");
                html.Append("<td style='width:2%;text-align:center;'>17</td>");
                html.Append("<td style='width:2%;text-align:center;'>18</td>");
                html.Append("<td style='width:2%;text-align:center;'>19</td>");
                html.Append("<td style='width:2%;text-align:center;'>20</td>");
                html.Append("<td style='width:2%;text-align:center;'>21</td>");
                html.Append("<td style='width:2%;text-align:center;'>22</td>");
                html.Append("<td style='width:2%;text-align:center;'>23</td>");
                html.Append("<td style='width:2%;text-align:center;'>24</td>");
                html.Append("<td style='width:2%;text-align:center;'>25</td>");
                html.Append("<td style='width:2%;text-align:center;'>26</td>");
                html.Append("<td style='width:2%;text-align:center;'>27</td>");
                html.Append("<td style='width:2%;text-align:center;'>28</td>");
                if (sdaysMonth == "29")
                {
                    html.Append("<td style='width:2%;text-align:center;'>29</td>");
                }
                if (sdaysMonth == "30")
                {
                    html.Append("<td style='width:2%;text-align:center;'>29</td>");
                    html.Append("<td style='width:2%;text-align:center;'>30</td>");
                }
                if (sdaysMonth == "31")
                {
                    html.Append("<td style='width:2%;text-align:center;'>29</td>");
                    html.Append("<td style='width:2%;text-align:center;'>30</td>");
                    html.Append("<td style='width:2%;text-align:center;'>31</td>");
                }

                html.Append("<td style='width:2%;text-align:center;'>PD</td>");
                html.Append("<td style='width:2%;text-align:center;'>AD</td>");
                html.Append("<td style='width:2%;text-align:center;'>CL</td>");
                html.Append("<td style='width:2%;text-align:center;'>SL</td>");
                html.Append("<td style='width:2%;text-align:center;'>EL</td>");
                html.Append("<td style='width:2%;text-align:center;'>WH</td>");
                html.Append("<td style='width:2%;text-align:center;'>TD</td>");
                html.Append("</tr>");
                //foreach (DataRow row in dt.Rows)

                    foreach (DataRow row in dt.Select("Department = '" + Heading + "'"))
                    {
                    SL = SL + 1; tpresent = 0; tleave = 0; tabsent = 0;
                    html.Append("<tr>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(SL);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["EmpId"]);
                    html.Append("</td>");
                    html.Append("<td>");
                    html.Append(row["EmpName"]);
                    html.Append("</td>");

                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["1"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["2"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["3"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["4"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["5"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["6"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["7"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["8"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["9"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["10"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["11"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["12"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["13"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["14"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["15"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["16"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["17"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["18"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["19"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["20"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["21"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["22"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["23"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["24"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["25"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["26"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["27"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["28"]);
                    html.Append("</td>");
                    
                    if (sdaysMonth == "29")
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["29"]);
                        html.Append("</td>");
                    }
                    if (sdaysMonth == "30")
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["29"]);
                        html.Append("</td>");
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["30"]);
                        html.Append("</td>");
                    }
                    if (sdaysMonth == "31")
                    {
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["29"]);
                        html.Append("</td>");
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["30"]);
                        html.Append("</td>");
                        html.Append("<td style='text-align:center;'>");
                        html.Append(row["31"]);
                        html.Append("</td>");
                    }
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["Present"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["Absent"]);
                    html.Append("</td>");

                    html.Append("<td style='text-align:center;'>");
                    //html.Append(row["LeavePay"]);
                    html.Append(row["CL"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["SL"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["EL"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["WeeklyOff"]);
                    html.Append("</td>");
                    html.Append("<td style='text-align:center;'>");
                    html.Append(row["Total"]);
                    html.Append("</td>");

                    html.Append("</tr>");
                }
                html.Append("</table>");
            }


            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {
            string sQry = string.Empty;
            string sFromDate = Session[GlobalVariables.g_s_rptfromDate].ToString();
            string sToDate = Session[GlobalVariables.g_s_rpttoDate].ToString();
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string sJobLocationId = Session[GlobalVariables.g_s_rptforJobLocationId].ToString();
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sSectionId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sCompanyId = Session[GlobalVariables.g_s_CompanyAutoId].ToString();
            string srptStatus = Session[GlobalVariables.g_s_rptStatus].ToString();
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            string s_trShift = Convert.ToString(Session[GlobalVariables.g_s_rptShift]);
            string s_Userlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            int sdaysofMonth = 0;
            int iMonth = 0;
            int iYear = 0;
            iYear = Convert.ToInt32(Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("yyyy"));
            iMonth = Convert.ToInt32(Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("MM"));
            sdaysofMonth = System.DateTime.DaysInMonth(iYear, iMonth);
            sdaysMonth = sdaysofMonth.ToString();
            srptStatus = "AnA";//All type Attendance

            //@companyAutoId varchar(50),
            //@UserLevel as varchar(50),
            //@employeeId varchar(50),
            //@sectionId varchar(50),
            //@LineId varchar(50),
            //@departmentId varchar(250),
            //@month varchar(2),
            //@year char(4),
            //@daysInMonth int

            sQry = "ProcRptAttendanceSummary '" + sCompanyId + "','" + s_Userlevel + "','" + sEmpId + "','%','" + sJobLocationId + "','" + sDepartmentId + "','" 
                                            + iMonth.ToString() + "','" + iYear.ToString() + "','" + sdaysofMonth.ToString() + "'";
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=ProjectTarget_" + CampaignID + ".xls");
            Response.AddHeader("content-disposition", "attachment;filename=SAAS_DailyAttendance.xls");
            Response.Charset = "";
            //Response.ContentType = "application/vnd.xls";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}