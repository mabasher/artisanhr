﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class ViewMonthlyTiffinBill : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string g_s_UserName = "";// UserInforation.UserName.ToString();
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;

            g_s_UserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            g_s_rptCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            g_s_rptName = "Monthly Tiffin Bill";// Convert.ToString(Session[GlobalVariables.g_s_rptName]);
            g_s_Period =  Convert.ToString(Session[GlobalVariables.g_s_Period]);

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th>" + g_s_rptCompanyName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th>" + g_s_rptName + "</th>");
            html.Append("</tr>");
            html.Append("<tr>");
            html.Append("<th>" + g_s_Period + "</th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            getRptData();

            //Footer
            htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%;'>");
            htmlFooter.Append("<tr>");
            htmlFooter.Append("<td style='text-align:left;'> Printed by : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            //htmlFooter.Append("<td style='text-align:right;'> Developed By : STM Software Ltd.</td>");
            htmlFooter.Append("</tr>");
            htmlFooter.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer

        }


        private void getRptData()
        {
            DataTable dt = this.GetData();
            StringBuilder html = new StringBuilder();
            string Heading = string.Empty; string sIntime = string.Empty;
            double ttQty = 0;  double SL = 0;  double ttpresent = 0;   double tpresent = 0;  double tpresentpercent = 0;
            double ttabsent = 0;  double tabsent = 0; double ttleaveabsent = 0; double tleaveabsent = 0; double tabsenteeism = 0;
            double ttmanpower = 0; double tmanpower = 0;
            double tabsentpercent = 0;  double ttleave = 0;  double tleave = 0; double tleavepercent = 0;
            string sStatus = string.Empty; string late = string.Empty;  late = "N";  sStatus = ""; sIntime = "";


            html.Append("<table class='table-data' style='width:100%;'>");
            //Building the Header row.
            html.Append("<tr>");
            html.Append("<th> </th>");
            html.Append("</tr>");
            //Table end.
            html.Append("</table>");


            //Table start.                
            html.Append("<table class='table-data' style='width:100%;font-size: 9pt;font-family:verdana;'>");
            //Building the Header row.

            //trDate,trManpower,trLeave,trAbsent,trTotalLeaveandAbsent,trPresent,PercentofPresent,PercentofLeave,PercentofAbsent

   //         PIN,
			//e.EmployeeName as eName,
			//isnull(desg.Designation, '') as Designation,
			//isnull(dept.Department, '') as Department,
			//isnull(se.Section, '') as Section,
			//isnull((Select dbo.func_GetpresentDaysForTiffinAllowance(e.CardNo, @fromDate, e.PIN)),0) as trDay,
			//trRate =case when e.PIN like '%S%' then 25 else 20 end,
			//'' as eSign



            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td>SL#</td>");
            html.Append("<td style='text-align:center;'>PIN</td>");
            html.Append("<td style='text-align:center;'>Name</td>");
            html.Append("<td style='text-align:center;'>Designation</td>");
            html.Append("<td style='text-align:center;'>Department</td>");
            html.Append("<td style='text-align:center;'>Section</td>");
            html.Append("<td style='text-align:center;'>Total Day</td>");
            html.Append("<td style='text-align:center;'>Payable Amount</td>");
            html.Append("<td style='text-align:center;'>Signature</td>");
            html.Append("</tr>");
            foreach (DataRow row in dt.Rows)
            {                
                SL = SL + 1; tpresent = 0; tleave = 0; tabsent = 0; tmanpower = 0; tleaveabsent = 0;
                html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(SL);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["PIN"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["eName"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["Designation"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["Department"]);
                html.Append("</td>");
                html.Append("<td style='text-align:left;'>");
                html.Append(row["Section"]);
                html.Append("</td>");
                html.Append("<td style='text-align:right;'>");
                html.Append(row["trDay"]);
                html.Append("</td>");

                tleave = Convert.ToDouble(row["trDay"]) * Convert.ToDouble(row["trRate"]);

                html.Append("<td style='text-align:right;'>");
                html.Append(tleave);
                html.Append("</td>");

                html.Append("<td style='text-align:center;'>");
                html.Append("");
                html.Append("</td>");

                //tmanpower = Convert.ToDouble(row["trManpower"]);
                //tleaveabsent = Convert.ToDouble(row["trTotalLeaveandAbsent"]);
                //tpresent =Convert.ToDouble(row["trPresent"]);
                //tabsent = Convert.ToDouble(row["trAbsent"]);


                //ttpresent = ttpresent + tpresent;
                //ttabsent = ttabsent + tabsent;
                ttleave = ttleave + tleave;
                //ttleaveabsent = ttleaveabsent + tleaveabsent;
                //ttmanpower = ttmanpower + tmanpower;

                html.Append("</tr>");
            }

            html.Append("<tr style='background-color:silver;'>");
            html.Append("<td colspan='7' style='text-align:Right;'>");
            html.Append("Total : ");
            html.Append("</td>");
            html.Append("<td style='text-align:Right;'>");
            html.Append(ttleave.ToString("#,##0.00"));
            html.Append("</td>");
            html.Append("<td  style='text-align:center;'>");
            html.Append("");
            html.Append("</td>");
            html.Append("</tr>");

            html.Append("</table>");

            
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
        }

        private DataTable GetData()
        {

            //Officier 25 tk worker 20 tk

            string sFromDate = Session[GlobalVariables.g_s_rptfromDate].ToString();
            string sToDate = Session[GlobalVariables.g_s_rpttoDate].ToString();
            string sDepartmentId = Session[GlobalVariables.g_s_rptforDeparmentId].ToString();
            string sJobLocationId = Session[GlobalVariables.g_s_rptforJobLocationId].ToString();
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sCompanyId = Session[GlobalVariables.g_s_CompanyAutoId].ToString();
            string srptStatus = Session[GlobalVariables.g_s_rptStatus].ToString();
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            srptStatus = "AnA";//All type Attendance
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand("procHTMLMonthlyTiffinBill '" + sJobLocationId + "','" + sDepartmentId + "','" + sEmpId + "','" + sCompanyId + "','" + Convert.ToDateTime(sFromDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(sToDate, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + srptStatus + "','"+ s_trMode + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }

        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            //Response.AddHeader("content-disposition", "attachment;filename=ProjectTarget_" + CampaignID + ".xls");
            Response.AddHeader("content-disposition", "attachment;filename=MOnthlyTiffin.xls");
            Response.Charset = "";
            //Response.ContentType = "application/vnd.xls";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }



    }
}