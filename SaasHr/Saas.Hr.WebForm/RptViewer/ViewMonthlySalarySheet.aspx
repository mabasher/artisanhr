﻿<%@ Page Title="Monthly Salary Sheet" Language="C#" MasterPageFile="~/SiteRpt.Master" AutoEventWireup="true" CodeBehind="ViewMonthlySalarySheet.aspx.cs" Inherits="Saas.Hr.WebForm.RptViewer.ViewMonthlySalarySheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
    <style type="text/css">
        .marginTop {
            margin-top: 3.3px;
        }

        .table-data {
            border-collapse: collapse;
        }

        .table-data, table.table-data td, table.table-data th {
            border: 1px solid gray;
        }

        .print {
        }

        .rotate-cell {
            /* FF3.5+ */
            -moz-transform: rotate(-90.0deg);
            /* Opera 10.5 */
            -o-transform: rotate(-90.0deg);
            /* Saf3.1+, Chrome */
            -webkit-transform: rotate(-90.0deg);
            /* IE6,IE7 */
            filter: progid: DXImageTransform.Microsoft.BasicImage(rotation=0.083);
            /* IE8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)";
            /* Standard */
            transform: rotate(-90.0deg);
        }

        @media print {
            .print {
                display: block
            }

            .btn-print {
                display: none;
            }

            .btnExport {
                display: none;
            }

            .rptvw {
                display: none;
            }

            /*@page {
                        size: legal;
                    }*/
            @page {
                size: auto;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="rptvw" style="width: 100%; text-align: center; background-color: #1e1e2d; text-align: right; font-size: 20px; font-weight: bold; color: white; height: 40px;">
        <p>Report Viewer</p>
    </div>
    <div style="width: 100%; text-align: center; background-color: #1e1e2d; text-align: right;">
        <asp:Button runat="server" class="btn-print" Text="Print Data" OnClientClick="window.print()" />
        <asp:Button ID="btnExport" class="btnExport" runat="server" Text="Export To Excel" OnClick="ExportToExcel" />
    </div>

    <div style="background-color: White;" class="print">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>
