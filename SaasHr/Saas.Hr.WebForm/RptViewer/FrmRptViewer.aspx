﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmRptViewer.aspx.cs" Inherits="Saas.Hr.WebForm.RptViewer.FrmRptViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 1000px; margin: auto;">
            <asp:ScriptManager ID="scrptmgrReport" runat="server"></asp:ScriptManager>
            <br />
            <asp:Label ID="lbl_message" runat="server" Text=""></asp:Label>
            <CR:CrystalReportViewer ID="crvRpt" runat="server" AutoDataBind="true" OnInit="crvRpt_Init" />
        </div>
    </form>


</body>
</html>