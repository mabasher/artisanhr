﻿<%@ Page Title="Employee CV" Language="C#"  MasterPageFile="~/SiteRpt.Master"  AutoEventWireup="true" CodeBehind="ViewEmployeeCV.aspx.cs" Inherits="Saas.Hr.WebForm.RptViewer.HR.ViewEmployeeCV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
    <a href="../../Content/Site.scss" media="screen"></a>

    <style type="text/css">
       
        .marginTop {
            margin-top: 3.3px;
        }

        .table-data {
            border-collapse: collapse;
        }

        .table-data, table.table-data td, table.table-data th {
            border: 1px solid gray;
        }

        .print {
        }

        @media print {
            .print {
                width: 80%;
                display: block;
                font-size: 14px;
                height: 100vh;
            }

            .btn-print {
                display: none;
            }

            .btnExport {
                display: none;
            }

            .print-footer {
                width: 100%;
                position: fixed;
                bottom: 0px;
            }

            .rptvw {
                display: none;
            }

            @page {
                size: auto;
                margin-top: 3cm;
                margin-bottom: 2cm;
                margin-left: 1cm;
                margin-right: 1cm;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="rptvw" style="width: 100%; text-align: center; background-color: #1e1e2d; text-align: right; font-size: 20px; font-weight: bold; color: white; height: 40px;">
        <p>Report Viewer</p>
    </div>
    <div style="width: 100%; text-align: center; background-color: #1e1e2d; text-align: right;">
        <asp:Button runat="server" class="btn-print" Text="Print Data" OnClientClick="window.print()" />
        <asp:Button ID="btnExport" class="btnExport" runat="server" Text="Export To Excel" OnClick="ExportToExcel" />
    </div>

    <div style="background-color: White; width: 100%;" class="print">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    </div>

</asp:Content>
