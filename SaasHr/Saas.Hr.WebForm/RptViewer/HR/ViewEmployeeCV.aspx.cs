﻿using Saas.Hr.WebForm.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Saas.Hr.WebForm.Common.CommonFunctions;

namespace Saas.Hr.WebForm.RptViewer.HR
{
    public partial class ViewEmployeeCV : BasePage
    {
        private string SLNo = string.Empty;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            string g_s_rptCompanyName = string.Empty;
            string g_s_rptCompanyBIN = string.Empty;
            string g_s_rptName = string.Empty;
            string g_s_Period = string.Empty;
            g_s_rptName = "EmployeeCV";

            StringBuilder htmlFooter = new StringBuilder();
            StringBuilder html = new StringBuilder();
            html.Append("<table style='width:100%;text-align:Center;border:none;'>");
            //Building the Header row.
            //html.Append("<tr>");
            //html.Append("<th>" + UserInforation.CompanyName.ToString() + "</th>");
            //html.Append("</tr>");
            //html.Append("<tr>");
            //html.Append("<th>" + UserInforation.CompanyAddress.ToString() + "</th>");
            //html.Append("</tr>");
            //html.Append("<tr>");
            //html.Append("<th>&nbsp;</th>");
            //html.Append("</tr>");
            //html.Append("<tr>");
            //html.Append("<th>" + g_s_rptName + "</th>");
            //html.Append("</tr>");

            //Table end.
            html.Append("</table>");
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

            if (Request.QueryString["EmployeePersonalInfoId"] != null)
            {
                SLNo = Request.QueryString["EmployeePersonalInfoId"];
                Session[GlobalVariables.g_s_rptforEmpId] = SLNo.ToString();
            }
            getRptData();

            //Footer
            //htmlFooter.Append("<table style='text-align:left; margin-top:30px;font-size: 8pt;font-family:verdana;width:100%; position: fixed; bottom: 30px;'>");
            //htmlFooter.Append("<tr>");
            //htmlFooter.Append("<td style='text-align:left;'> Created By : " + g_s_UserName + " | Date & Time :" + DateTime.Now.ToString() + " | Developed By : STM Software Ltd.</td>");
            // htmlFooter.Append("</tr>");
            //htmlFooter.Append("</table>");
            //PlaceHolder1.Controls.Add(new Literal { Text = htmlFooter.ToString() });
            //Footer
        }

        private void getRptData()
        {
            DataTable dt = this.GetData();
            DataTable dtAcademicInfo = this.GetAcademicInfoData();
            DataTable dtTrainingInfo = this.GetTrainingInformationData();
            DataTable dtWorkExperience = this.GetWorkExperienceData();
            DataTable dtEmployeeNominee = this.GetEmployeeNomineeData();
            DataTable dtEmployeeChild = this.GetEmployeeChildData();

            Numeric_To_String ns = new Numeric_To_String();
            string Imagepic, sig, appSignature, vatNote = string.Empty;
            string EmployeeId , CardNo, EmployeeName, EmployeeNeekName ,FatherName,
	             MotherName,SpouseName, PersonalContact, OfficialContact, EmergencyContact, ContactPerson,
	             PersonalMail , OfficialMail , PresentAddress, PermanentAddress, EmergencyAddress, DOB,
	             PlaceOfBirth, BirthCertificate, NID, TIN , PassportNo, DrivingLicense, ExtraCurricularActivities, Remark,
                 Designation, DesignationBangla, Department, DepartmentBangla, EmpTypeName, GenderNameBangla, NationalityName,
                 ShiftName, inTime, outTime, Section, SectionBangla, BloodGroupName, GenderName, LocationName, LineNum, LineBangla,
                  ProjectName, ReligionName, ReligionNameBangla, MaritalStatusName, EmpGroupName, FloorName, FloorBangla, BranchName, BranchNameBangla,
                 JoinDate,AppointmentDate,ConfirmationDate,ProbationPeriod,PFEnable,PFDate = string.Empty;

            double ttQty = 0; double sl = 0; double tQty = 0; double ttTotal = 0; double ttNetTotal = 0; double ttDiscountTotal = 0; double ttRft = 0; double tRft = 0; double ttMt = 0; double tMt = 0;
            double ttVAT = 0;

            StringBuilder html = new StringBuilder();
            DataView dtview = new DataView(dt);
            DataTable GDT = dtview.ToTable(true, "EmployeeId", "CardNo", "EmployeeName", "EmployeeNeekName", "FatherName",
             "MotherName", "SpouseName", "PersonalContact", "OfficialContact", "EmergencyContact", "ContactPerson",
             "PersonalMail", "OfficialMail", "PresentAddress", "PermanentAddress", "EmergencyAddress", "DOB",
             "PlaceOfBirth", "BirthCertificate", "NID", "TIN", "PassportNo", "DrivingLicense", "ExtraCurricularActivities", "Remark",
             "Designation", "DesignationBangla", "Department", "DepartmentBangla", "GenderName", "GenderNameBangla", "NationalityName",
             "ShiftName", "inTime", "outTime", "Section", "SectionBangla", "BloodGroupName", "EmpTypeName", "LocationName", "LineNum", "LineBangla",
              "ProjectName", "ReligionName", "ReligionNameBangla", "MaritalStatusName", "EmpGroupName", "FloorName", "FloorBangla", "BranchName", "BranchNameBangla",
             "JoinDate", "AppointmentDate", "ConfirmationDate", "ProbationPeriod", "PFEnable", "PFDate");

            EmployeeName = ""; EmployeeId = ""; FatherName = ""; MotherName = ""; PresentAddress = ""; PermanentAddress = "";
            DOB = ""; PlaceOfBirth = "";     BirthCertificate = "";     NID = "";   TIN = "";
            PassportNo = ""; ExtraCurricularActivities = "";   Remark = ""; Designation = "";       DesignationBangla = "";   Department = "";   DepartmentBangla = "";      GenderName = "";    GenderNameBangla = "";    NationalityName = "";
            ShiftName = "";   inTime = "";  outTime = "";Section = ""; SectionBangla = "";    BloodGroupName = "";  EmpTypeName = "";
            LocationName = ""; LineNum = "";  LineBangla = "";
            ProjectName = "";   ReligionName = "";  ReligionNameBangla = "";  MaritalStatusName = ""; EmpGroupName = "";
            FloorName = ""; FloorBangla = ""; BranchName = ""; BranchNameBangla = "";  JoinDate = "";
            AppointmentDate = "";  ConfirmationDate =""; ProbationPeriod = "";   PFEnable =""; PFDate = "";

            foreach (DataRow grow in GDT.Rows)
            {
                EmployeeId = (grow["EmployeeId"]).ToString();
                CardNo = (grow["CardNo"]).ToString();
                EmployeeName = (grow["EmployeeName"]).ToString();
                EmployeeNeekName = (grow["EmployeeNeekName"]).ToString();
                FatherName = (grow["FatherName"]).ToString();
                MotherName = (grow["MotherName"]).ToString();
                SpouseName = (grow["SpouseName"]).ToString();
                PersonalContact = (grow["PersonalContact"]).ToString();
                OfficialContact = (grow["OfficialContact"]).ToString();

                EmergencyContact = (grow["EmergencyContact"]).ToString();
                ContactPerson = (grow["ContactPerson"]).ToString();
                PersonalMail = (grow["PersonalMail"]).ToString();
                PresentAddress = (grow["PresentAddress"]).ToString();
                PermanentAddress = (grow["PermanentAddress"]).ToString();
                EmergencyAddress = (grow["EmergencyAddress"]).ToString();
                DOB = (grow["DOB"]).ToString();
                PlaceOfBirth = (grow["PlaceOfBirth"]).ToString();
                BirthCertificate = (grow["BirthCertificate"]).ToString();
                NID = (grow["NID"]).ToString();
                TIN = (grow["TIN"]).ToString();
                PassportNo = (grow["PassportNo"]).ToString();
                ExtraCurricularActivities = (grow["ExtraCurricularActivities"]).ToString();
                Remark = (grow["Remark"]).ToString();
                Designation = (grow["Designation"]).ToString();
                DesignationBangla = (grow["DesignationBangla"]).ToString();
                Department = (grow["Department"]).ToString();
                DepartmentBangla = (grow["DepartmentBangla"]).ToString();
                GenderName = (grow["GenderName"]).ToString();
                GenderNameBangla = (grow["GenderNameBangla"]).ToString();
                NationalityName = (grow["NationalityName"]).ToString();
                ShiftName = (grow["ShiftName"]).ToString();
                inTime = (grow["inTime"]).ToString();
                outTime = (grow["outTime"]).ToString();
                Section = (grow["Section"]).ToString();
                SectionBangla = (grow["SectionBangla"]).ToString();
                BloodGroupName = (grow["BloodGroupName"]).ToString();
                EmpTypeName = (grow["EmpTypeName"]).ToString();
                LocationName = (grow["LocationName"]).ToString();
                LineNum = (grow["LineNum"]).ToString();
                LineBangla = (grow["LineBangla"]).ToString();
                ProjectName = (grow["ProjectName"]).ToString();
                ReligionName = (grow["ReligionName"]).ToString();
                ReligionNameBangla = (grow["ReligionNameBangla"]).ToString();
                MaritalStatusName = (grow["MaritalStatusName"]).ToString();
                EmpGroupName = (grow["EmpGroupName"]).ToString();
                FloorName = (grow["FloorName"]).ToString();
                FloorBangla = (grow["FloorBangla"]).ToString();
                BranchName = (grow["BranchName"]).ToString();
                BranchNameBangla = (grow["BranchNameBangla"]).ToString();
                JoinDate = (grow["JoinDate"]).ToString();
                AppointmentDate = (grow["AppointmentDate"]).ToString();
                ConfirmationDate = (grow["ConfirmationDate"]).ToString();
                ProbationPeriod = (grow["ProbationPeriod"]).ToString();
                PFEnable = (grow["PFEnable"]).ToString();
                PFDate = (grow["PFDate"]).ToString();
            }

                html.Append("<table style='width:100%;margin-top:10px;font-family:Calibri;'>");
                html.Append("<tr>");
                //html.Append("<td colspan='4' style='text-align:center;font-weight:bold;font-size:20px'>" + UserInforation.CompanyName + "</td>");
                //html.Append("</tr>");
                //html.Append("<tr>");
                //html.Append("<td colspan='4' style='text-align:center;font-weight:bold;font-size:15px'>" + UserInforation.CompanyAddress + "</td>");
                html.Append("</tr>");
                html.Append("<tr>");
                html.Append("<td colspan='4' style='text-align:center;font-weight:bold;font-size:20px'>Employee Personal Information</td>");
                html.Append("</tr>");
                html.Append("</table>");


                html.Append("<table class='table-data' style='width:100%;margin-top:10px;font-family:Calibri;'>");

                html.Append("<tr>");
                html.Append("<td style='width:10%;font-size:14px;'>Employee Name :</td>");
                html.Append("<td style='width:35%;font-size:14px;'>" + EmployeeName + "</td>");
                html.Append("<td style='width:10%;font-size:14px;'>Employee Id :</td>");
                html.Append("<td style='width:20%;font-size:14px;'>" + EmployeeId + "</td>");
                html.Append("</tr>");

                html.Append("<tr>");
                html.Append("<td>Father Name :</td>");
                html.Append("<td>" + FatherName + "</td>");
                html.Append("<td>Mother Name : </td>");
                html.Append("<td> " + MotherName + "</td>");
                html.Append("</tr>");

                html.Append("<tr>");
                html.Append("<td> Present Address : </td>");
                html.Append("<td>" + PresentAddress + "</td>");
                html.Append("<td>Parmanent Address : </td>");
                html.Append("<td> " + PermanentAddress + "</td>");
                html.Append("</tr>");

                html.Append("<tr>");
                html.Append("<td> DOB : </td>");
                html.Append("<td>" + DOB + "</td>");
                html.Append("<td>Birth Certificate : </td>");
                html.Append("<td> " + BirthCertificate + "</td>");
                html.Append("</tr>");

                html.Append("<tr>");
                html.Append("<td> NID : </td>");
                html.Append("<td>" + NID + "</td>");
                html.Append("<td>TIN : </td>");
                html.Append("<td> " + TIN + "</td>");
                html.Append("</tr>");
            html.Append("<tr>");
                html.Append("<td> Blood Group : </td>");
                html.Append("<td>" + BloodGroupName + "</td>");
                html.Append("<td>Nationality : </td>");
                html.Append("<td> " + NationalityName + "</td>");
            html.Append("</tr>");

            html.Append("<tr>");
                html.Append("<td> Gender Name  : </td>");
                html.Append("<td>" + GenderName + "</td>");
                html.Append("<td>Project Name : </td>");
                html.Append("<td> " + ProjectName + "</td>");
                html.Append("</tr>");

                html.Append("</table>");      
            
            //Table start.
            html.Append("<table class='table-data' style='width:100%;margin-top:20px;font-family:Calibri;'>");
            //Building the Header row.

            html.Append("<tr>");
                html.Append("<td style='width:10%;font-size:14px; text-align:center;'>Official Information</td>");
            html.Append("</tr>");
            html.Append("<tr>");
                html.Append("<td style='width:10%;font-size:14px;'>Designation</td>");
                html.Append("<td style='width:10%;font-size:14px;'>" + Designation + "</td>");
                html.Append("<td style='width:10%;font-size:14px;'>Department</td>");
                html.Append("<td style='width:10%;font-size:14px;'>" + Department + "</td>");
            html.Append("</tr>");

            html.Append("<tr>");
                html.Append("<td>Section Name</td>");
                html.Append("<td>" + Section + "</td>");
                html.Append("<td>Employee Type </td>");
                html.Append("<td>" + EmpTypeName + "</td>");
            html.Append("</tr>");

            html.Append("<tr>");
                html.Append("<td>Branch Name </td>");
                html.Append("<td>" + BranchName + "</td>");
                html.Append("<td>Job Location</td>");
                html.Append("<td>" + LocationName + "</td>");
            html.Append("</tr>");
            
            html.Append("<tr>");
                html.Append("<td>Join Date</td>");
                html.Append("<td>" + JoinDate + "</td>");
                html.Append("<td>Confirmation Date </td>");
                html.Append("<td>" + ConfirmationDate + "</td>");
            html.Append("</tr>");

            html.Append("<tr>");
            //html.Append("<td>Confirmation Date </td>");
            //html.Append("<td>" + ConfirmationDate + "</td>");
            html.Append("</tr>");            

            //Table end.
            html.Append("</table>");

            ///////////AcademicInfo//////////
            html.Append("<table class='table-data' style='width:100%;margin-top:20px;font-family:Calibri;'>");
            //Building the Header row.

            html.Append("<thead>");
            html.Append("<tr>");
                    html.Append("<td> </td>");
                    html.Append("<td style='width:20%;font-size:14px; text-align:center;'>Academic Information</td>");
            html.Append("</tr>");
            html.Append("<tr style='background-color: silver; font-size:11px; '>");
                    html.Append("<td style='text-align:center;'>SL</td>");
                    html.Append("<td style='text-align:center;'>Exam Name</td>");
                    html.Append("<td style='text-align:center;'>Institute Name</td>");
                    html.Append("<td style='text-align:center;'>Board University</td>");
                    html.Append("<td style='text-align:center;'>Major Group</td>");
                    html.Append("<td style='text-align:center;'>Year Of Passing</td>");
                    html.Append("<td style='text-align:center;'>Result</td>");
                html.Append("</tr>");
            html.Append("</thead>");
            html.Append("<tbody>");
            foreach (DataRow row in dtAcademicInfo.Rows)
            {
                sl = sl + 1;
                //if (sl == 20 || (sl - 20) % 30 == 0)
                //    html.Append("<tr style='page-break-after: always;'>");
                //else
                //    html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(sl);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["ExamName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["InstituteName"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["BoardUniversityName"]);
                html.Append("</td>");
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["MajorGroupName"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["YearOfPassing"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["Result"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            html.Append("</tbody>");

            html.Append("</table>");

            ///////////TrainingInformation//////////
            html.Append("<table class='table-data' style='width:100%;margin-top:20px;font-family:Calibri;'>");
            //Building the Header row.

            html.Append("<thead>");
            html.Append("<tr>");
            html.Append("<td> </td>");
            html.Append("<td style='width:20%;font-size:14px; text-align:center;'>Training Information</td>");
            html.Append("</tr>");
            html.Append("<tr style='background-color: silver; font-size:11px; '>");
            html.Append("<td style='text-align:center;'>SL</td>");
            html.Append("<td style='text-align:center;'>Training Title</td>");
            html.Append("<td style='text-align:center;'>Topics Covered</td>");
            html.Append("<td style='text-align:center;'>Organized By</td>");
            html.Append("<td style='text-align:center;'>From Date</td>");
            html.Append("<td style='text-align:center;'>ToDate</td>");
            html.Append("</tr>");
            html.Append("</thead>");
            html.Append("<tbody>");
            foreach (DataRow row in dtTrainingInfo.Rows)
            {
                sl = sl + 1;
                //if (sl == 20 || (sl - 20) % 30 == 0)
                //    html.Append("<tr style='page-break-after: always;'>");
                //else
                //    html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(sl);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["TrainingTitle"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["TopicsCovered"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["OrganizedBy"]);
                html.Append("</td>");
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["FromDate"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["ToDate"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            html.Append("</tbody>");

            html.Append("</table>");

            ///////////WorkExperience//////////
            html.Append("<table class='table-data' style='width:100%;margin-top:20px;font-family:Calibri;'>");
            //Building the Header row.

            html.Append("<thead>");
            html.Append("<tr>");
            html.Append("<td> </td>");
            html.Append("<td style='width:20%;font-size:14px; text-align:center;'>Work Experience</td>");
            html.Append("</tr>");
            html.Append("<tr style='background-color: silver; font-size:11px; '>");
            html.Append("<td style='text-align:center;'>SL</td>");
            html.Append("<td style='text-align:center;'>Company Name</td>");
            html.Append("<td style='text-align:center;'>Company Business</td>");
            html.Append("<td style='text-align:center;'>Company Address</td>");
            html.Append("<td style='text-align:center;'>Designation</td>");
            html.Append("<td style='text-align:center;'>department</td>");
            html.Append("<td style='text-align:center;'>Job Nature</td>");
            html.Append("<td style='text-align:center;'>Job Responsibilities</td>");
            html.Append("<td style='text-align:center;'>Last Salary</td>");
            html.Append("<td style='text-align:center;'>From Date</td>");
            html.Append("<td style='text-align:center;'>To Date</td>");
            html.Append("<td style='text-align:center;'>Remark</td>");
            html.Append("</tr>");
            html.Append("</thead>");
            html.Append("<tbody>");
            foreach (DataRow row in dtWorkExperience.Rows)
            {
                sl = sl + 1;
                //if (sl == 20 || (sl - 20) % 30 == 0)
                //    html.Append("<tr style='page-break-after: always;'>");
                //else
                //    html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(sl);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["companyName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["companyBusiness"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["companyAddress"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["designation"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["department"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["jobNature"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["jobResponsibilities"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["lastSalary"]);
                html.Append("</td>");

                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["FromDate"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["ToDate"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["remark"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            html.Append("</tbody>");

            html.Append("</table>");


            ///////////EmployeeNominee//////////
            html.Append("<table class='table-data' style='width:100%;margin-top:20px;font-family:Calibri;'>");
            //Building the Header row.

            html.Append("<thead>");
            html.Append("<tr>");
            html.Append("<td> </td>");
            html.Append("<td style='width:20%;font-size:14px; text-align:center;'>Nominee Information</td>");
            html.Append("</tr>");
            html.Append("<tr style='background-color: silver; font-size:11px; '>");
            html.Append("<td style='text-align:center;'>SL</td>");
            html.Append("<td style='text-align:center;'>Nominee Name</td>");
            html.Append("<td style='text-align:center;'>Father Name</td>");
            html.Append("<td style='text-align:center;'>Mother Name</td>");
            html.Append("<td style='text-align:center;'>DOB</td>");
            html.Append("<td style='text-align:center;'>Permanent Address</td>");
            html.Append("<td style='text-align:center;'>Personal Contact</td>");
            html.Append("<td style='text-align:center;'>Personal Mail</td>");
            html.Append("<td style='text-align:center;'>NID</td>");
            html.Append("<td style='text-align:center;'>Relation</td>");
            html.Append("<td style='text-align:center;'>Share</td>");
            html.Append("</tr>");
            html.Append("</thead>");
            html.Append("<tbody>");
            foreach (DataRow row in dtEmployeeNominee.Rows)
            {
                sl = sl + 1;
                //if (sl == 20 || (sl - 20) % 30 == 0)
                //    html.Append("<tr style='page-break-after: always;'>");
                //else
                //    html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(sl);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["NomineeName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["FatherName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["MotherName"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["DOB"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PermanentAddress"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PersonalContact"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PersonalMail"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["NID"]);
                html.Append("</td>");

                html.Append("</td>");
                html.Append("<td >");
                html.Append(row["Relation"]);
                html.Append("</td>");
                html.Append("<td >");
                html.Append(row["Share"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            html.Append("</tbody>");

            html.Append("</table>");

            

            ///////////EmployeeChild//////////
            html.Append("<table class='table-data' style='width:100%;margin-top:20px;font-family:Calibri;'>");
            //Building the Header row.

            html.Append("<thead>");
            html.Append("<tr>");
            html.Append("<td> </td>");
            html.Append("<td style='width:20%;font-size:14px; text-align:center;'>Child Information</td>");
            html.Append("</tr>");
            html.Append("<tr style='background-color: silver; font-size:11px; '>");
            html.Append("<td style='text-align:center;'>SL</td>");
            html.Append("<td style='text-align:center;'>Child Name</td>");
            html.Append("<td style='text-align:center;'>Father Name</td>");
            html.Append("<td style='text-align:center;'>Mother Name</td>");
            html.Append("<td style='text-align:center;'>DOB</td>");
            html.Append("<td style='text-align:center;'>Permanent Address</td>");
            html.Append("<td style='text-align:center;'>Personal Contact</td>");
            html.Append("<td style='text-align:center;'>Personal Mail</td>");
            html.Append("<td style='text-align:center;'>NID</td>");
            html.Append("<td style='text-align:center;'>Relation</td>");
            html.Append("</tr>");
            html.Append("</thead>");
            html.Append("<tbody>");
            foreach (DataRow row in dtEmployeeChild.Rows)
            {
                sl = sl + 1;
                //if (sl == 20 || (sl - 20) % 30 == 0)
                //    html.Append("<tr style='page-break-after: always;'>");
                //else
                //    html.Append("<tr>");
                html.Append("<td style='text-align:center;'>");
                html.Append(sl);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["ChildName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["FatherName"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["MotherName"]);
                html.Append("</td>");
                html.Append("<td style='text-align:center;'>");
                html.Append(row["DOB"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PermanentAddress"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PersonalContact"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["PersonalMail"]);
                html.Append("</td>");
                html.Append("<td>");
                html.Append(row["NID"]);
                html.Append("</td>");

                html.Append("</td>");
                html.Append("<td >");
                html.Append(row["Relation"]);
                html.Append("</td>");

                html.Append("</tr>");
            }
            html.Append("</tbody>");

            html.Append("</table>");
            //Append the HTML string to Placeholder.
            PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

        }

        private DataTable GetData()
        {
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sQry = string.Empty;

            sQry = "ProcRptEmployeePersonalCV'" + sEmpId + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }
        private DataTable GetAcademicInfoData()
        {
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sQry = string.Empty;

            sQry = "ProcRptEmpAcademicInformation'" + sEmpId + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }
        private DataTable GetTrainingInformationData()
        {
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sQry = string.Empty;

            sQry = "ProcRptTrainingInformationNew'" + sEmpId + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }
        private DataTable GetWorkExperienceData()
        {
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sQry = string.Empty;
            sQry = "ProcRptWorkExperienceNew'" + sEmpId + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }
        private DataTable GetEmployeeNomineeData()
        {
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sQry = string.Empty;
            sQry = "ProcRptEmployeeNomineeNew'" + sEmpId + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }
        private DataTable GetEmployeeChildData()
        {
            string sEmpId = Session[GlobalVariables.g_s_rptforEmpId].ToString();
            string sQry = string.Empty;
            sQry = "ProcRptEmployeeChildNew'" + sEmpId + "'";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Dispose();
            DataSet ds = new DataSet();
            using (DataTable dt = new DataTable())
            {
                da.Fill(dt);
                return dt;
            }
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=SAASERP_CV.xls");
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            PlaceHolder1.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }

    }
}