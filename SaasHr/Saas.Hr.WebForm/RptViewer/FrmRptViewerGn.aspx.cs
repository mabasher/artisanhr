﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.RptViewer
{
    public partial class FrmRptViewerGn : Page
    {
        private ReportDocument reportDocument;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string strPrintOpt = string.Empty;
            strPrintOpt = Convert.ToString(Session[GlobalVariables.g_s_printopt]);
            var rpt = (ReportDocument)Session["rpt"];
            if (strPrintOpt == "P")
            {
                rpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, "Report");
            }
            else
            {
                crvRpt.ReportSource = rpt;
            }

            //var rpt = (ReportDocument)Session["rpt"];
            //crvRpt.ReportSource = rpt;
        }

        protected void crvRpt_Init(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Session[SessionKeys.s_sqlQuery] = TableColumnName.Rpt_ErpReport.g_s_sql_query;
                    Session[SessionKeys.s_reportFilePath] = TableColumnName.Rpt_ErpReport.g_s_rptFilePath;
                    Session[SessionKeys.dct_reportFormulaField] = TableColumnName.Rpt_ErpReport.dct_reportFormulaField;

                    reportDocument = new ReportDocument();
                    reportDocument.Load(Server.MapPath(Session[SessionKeys.s_reportFilePath].ToString()));

                    foreach (KeyValuePair<string, string> value in (Dictionary<string, string>)(Session[SessionKeys.dct_reportFormulaField]))
                    {
                        reportDocument.DataDefinition.FormulaFields[value.Key].Text = "'" + value.Value + "'";
                    }

                    Session[SessionKeys.s_reportDocument] = reportDocument;
                }
                if (Session[SessionKeys.s_sqlQuery].ToString() != string.Empty)
                {
                    //ReportDocument reportDocument = new ReportDocument();
                    //reportDocument.Load(Server.MapPath(Session[SessionKeys.s_reportFilePath].ToString()));
                    //Session[SessionKeys.s_reportDocument] = reportDocument;
                    setLogonInfo();
                }
            }
            catch (Exception exception)
            {
                lbl_message.Text = exception.Message;
            }
        }

        private class SessionKeys
        {
            private SessionKeys()
            { }

            public const string s_reportDocument = "s_reportDocument";
            public const string dt_dataTableForReport = "dt_dataTableForReport";
            public const string dct_reportFormulaField = "dct_reportFormulaField";
            public const string s_sqlQuery = "s_sqlQuery";
            public const string s_reportFilePath = "s_reportFilePath";
        }

        public void setLogonInfo()
        {
            try
            {
                TableLogOnInfo logOnInfo = new TableLogOnInfo();
                TableLogOnInfos infos = new TableLogOnInfos();
                ConnectionInfo connectionInfo = new ConnectionInfo();
                ReportDocument reportDocument = new ReportDocument();

                logOnInfo.ConnectionInfo.ServerName = new Connection().ServerName;
                logOnInfo.ConnectionInfo.DatabaseName = new Connection().DatabaseName;
                logOnInfo.ConnectionInfo.UserID = new Connection().UserId;
                logOnInfo.ConnectionInfo.Password = new Connection().Password;
                logOnInfo.TableName = "LogonInfoTable";
                infos.Add(logOnInfo);

                reportDocument = (ReportDocument)Session[SessionKeys.s_reportDocument];
                reportDocument.Database.Tables[0].ApplyLogOnInfo(logOnInfo);

                crvRpt.LogOnInfo = infos;
            }
            catch (Exception exception)
            {
                lbl_message.Text = exception.Message;
            }
        }

        protected void crvRpt_Unload(object sender, EventArgs e)
        {
            //reportDocument.Dispose();
            //reportDocument.Close();
        }
    }
}