﻿using Saas.Hr.WebForm.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmPersonalProfile : Page
    {
        private string _imageUrl = string.Empty;
        private string _imageFileName = string.Empty;
        private string _sigUrl = string.Empty;
        private string _sigFileName = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                string Sessionuserlevel = string.Empty;
                Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtDOB.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddGender,
                    "GenderInfo",
                    "GenderName",
                    "GenderInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddNationallity,
                    "NationalityInfo",
                    "NationalityName",
                    "NationalityInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBlood,
                    "BloodGroupInfo",
                    "BloodGroupName",
                    "BloodGroupInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddReligion,
                    "ReligionInfo",
                    "ReligionName",
                    "ReligionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddMaritalStatus,
                    "MaritalStatusInfo",
                    "MaritalStatusName",
                    "MaritalStatusInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddProject,
                    "BranchInfo",
                    "BranchName",
                    "BranchInfoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddEmployeeType,
                //      "EmployeeGroup",
                //      "GroupName",
                //      "EmployeeGroupId", string.Empty);

                if (Sessionuserlevel == "4")
                {
                    ddProject.Enabled = false;
                    PInfo.Visible = false;
                    txtSearch.Visible = false;
                    getEmpData(SessionUserId);
                    ControlEnable();
                }

                v_loadGridViewList();
            }
        }

        private void maxSerial()
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;
            string s_sCompId = string.Empty;
            s_sCompId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "ProcMaxAutoSerialNoHRISABNB '" + ddProject.SelectedValue + "'";

            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["SLNo"].ToString();
                    }
                }
            }
        }

        protected void ddProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (btn_save.Text != "Update")
            {
                if (ddProject.SelectedValue != "0")
                {
                    maxSerial();
                }
            }
        }
        private void ControlEnable()
        {
            txtPIN.Enabled = false;
            txtCard.Enabled = false;
            txtOfficialEmail.Enabled = false;
        }
        protected void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            v_loadGridViewList();
        }
        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {

            hfAutoIdId.Value = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hid_GridMasterId")).Value;
            getEmpData(hfAutoIdId.Value);

        }
        private void v_loadGridViewList()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string s_sCompId = string.Empty;
            //s_sCompId = UserInforation.CompanyId.ToString();
            s_sCompId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SqlCommand cmd = new SqlCommand("Proc_Load_MasterGrid_Saas 'PersonalInfo','" + HttpUtility.HtmlDecode(txtEmpSearch.Text.Trim()) + "','" + s_sCompId + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();
            sqlconnection.Close();
        }
        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }
            if (btn_save.Text == "Update")
            {
                if (imgUpload.HasFile)
                {
                    if (!string.IsNullOrEmpty(hidURL.Value))
                    {
                        var imgFilePath = Server.MapPath("~/EmployeeImage/" + hidURL.Value);
                        if (File.Exists(imgFilePath))
                        {
                            File.Delete(imgFilePath);
                        }
                        _imageFileName = hidURL.Value;
                        _imageUrl = "~/EmployeeImage/" + _imageFileName;
                    }
                    else
                    {
                        _imageFileName = Path.GetFileName(imgUpload.PostedFile.FileName);
                        if (_imageFileName != "")
                        {
                            _imageFileName = txtPIN.Text + "_P_" + _imageFileName;
                            _imageUrl = "~/EmployeeImage/" + _imageFileName;
                        }
                    }
                }
                if (imgSigUpload.HasFile)
                {
                    if (!string.IsNullOrEmpty(hidSigURL.Value))
                    {
                        var sigFilePath = Server.MapPath("~/EmployeeSig/" + hidSigURL.Value);
                        if (File.Exists(sigFilePath))
                        {
                            File.Delete(sigFilePath);
                        }
                        _sigFileName = hidSigURL.Value;
                        _sigUrl = "~/EmployeeSig/" + _sigFileName;
                    }
                    else
                    {
                        _sigFileName = Path.GetFileName(imgSigUpload.PostedFile.FileName);
                        if (_sigFileName != "")
                        {
                            _sigFileName = txtPIN.Text + "_S_" + _sigFileName;
                            _sigUrl = "~/EmployeeSig/" + _sigFileName;
                        }
                    }
                }
                s_Update = "[ProcPersonalProfileINSERT]"
                        + "'"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtPIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCard.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmpName.Text.Trim().Replace("'", "''"))
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFather.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtMother.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtSpouse.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmergencyContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim().Replace("'", "''"))
                        + "','','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlaceofBirth.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtBirthCertificate.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNationalId.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtTIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassport.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDrivingLicense.Text.Trim().Replace("'", "''"))
                        + "','','','"//@ExtraCurricularActivities,@Remark
                        + _imageFileName //_imageUrl//_imageFileName//PIC imageUrl
                        + "','"
                        + _sigFileName //_sigUrl//_sigFileName//SIG imageUrl
                        + "','"
                        + ddGender.SelectedValue
                        + "','"
                        + ddNationallity.SelectedValue
                        + "','"
                        + ddReligion.SelectedValue
                        + "','"
                        + ddBlood.SelectedValue
                        + "','"
                        + ddMaritalStatus.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U','"
                        + ddProject.SelectedValue + "' ";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;

                        if (imgUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imgUpload.PostedFile.InputStream);
                            StmImageProcessor.Save250Jpeg(Server.MapPath(_imageUrl), bmpPostedImage);
                        }

                        if (imgSigUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedSig = new System.Drawing.Bitmap(imgSigUpload.PostedFile.InputStream);
                            StmImageProcessor.Save80Jpeg(Server.MapPath(_sigUrl), bmpPostedSig);
                        }
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
            else
            {
                _imageFileName = Path.GetFileName(imgUpload.PostedFile.FileName);
                if (_imageFileName != "")
                {
                    _imageFileName = txtPIN.Text + "_P_" + _imageFileName;
                    _imageUrl = "~/EmployeeImage/" + _imageFileName;
                }

                _sigFileName = Path.GetFileName(imgSigUpload.PostedFile.FileName);
                if (_sigFileName != "")
                {
                    _sigFileName = txtPIN.Text + "_S_" + _sigFileName;
                    _sigUrl = "~/EmployeeSig/" + _sigFileName;
                }
                s_save_ = "[ProcPersonalProfileINSERT]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txtPIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCard.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmpName.Text.Trim().Replace("'", "''"))
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFather.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtMother.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtSpouse.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmergencyContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlaceofBirth.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtBirthCertificate.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNationalId.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtTIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassport.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDrivingLicense.Text.Trim().Replace("'", "''"))
                        + "','','','"//@ExtraCurricularActivities,@Remark
                        + _imageFileName//_imageUrl//_imageFileName//PIC imageUrl
                        + "','"
                        + _sigFileName//_sigUrl//_sigFileName//SIG imageUrl
                        + "','"
                        + ddGender.SelectedValue
                        + "','"
                        + ddNationallity.SelectedValue
                        + "','"
                        + ddReligion.SelectedValue
                        + "','"
                        + ddBlood.SelectedValue
                        + "','"
                        + ddMaritalStatus.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I','"
                        + ddProject.SelectedValue + "' ";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        
                        if (imgUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imgUpload.PostedFile.InputStream);
                            StmImageProcessor.Save250Jpeg(Server.MapPath(_imageUrl), bmpPostedImage);
                        }
                        if (imgSigUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedSig = new System.Drawing.Bitmap(imgSigUpload.PostedFile.InputStream);
                            StmImageProcessor.Save80Jpeg(Server.MapPath(_sigUrl), bmpPostedSig);
                        }
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (ddGender.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Gender Can Not Blank!";
                return false;
            }

            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }

            if (txtPresentAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Present Address Can Not Blank!";
                return false;
            }
            if (txtPermanentAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Permanent Address Can Not Blank!";
                return false;
            }

            if (txtPersonalContact.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Contact No Can Not Blank!";
                return false;
            }

            //            if (imgUpload.HasFile)
            //            {
            //                int imgSize = imgUpload.PostedFile.ContentLength;
            //                string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
            //                if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
            //                {
            //                    lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
            //                    lblMsg.Visible = true;
            //                    return false;
            //                }
            //                else if (imgSize > 800000)//1048576)//1048576 bit=1MB
            //                {
            //                    lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
            //                    lblMsg.Visible = true;
            //                    return false;
            //                }
            //            }
            //else
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "User Photo Can Not Blank!";
            //    return false;
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtPresentAddress.Text = string.Empty;
            txtPermanentAddress.Text = string.Empty;
            txtFather.Text = string.Empty;
            txtMother.Text = string.Empty;
            txtNationalId.Text = string.Empty;
            txtPassport.Text = string.Empty;
            txtBirthCertificate.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            ddGender.SelectedValue = "0";
            ddNationallity.SelectedValue = "0";
            ddMaritalStatus.SelectedValue = "0";
            ddBlood.SelectedValue = "0";
            ddReligion.SelectedValue = "0";
            hidURL.Value = string.Empty;
            hidSigURL.Value = string.Empty;
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        hidEmpAutoId.Value = drow["EmployeePersonalInfoId"].ToString();
                        txtPIN.Text = drow["PIN"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        //txtCard.Text = drow["EmployeeNeekName"].ToString();
                        txtFather.Text = drow["FatherName"].ToString();
                        txtMother.Text = drow["MotherName"].ToString();
                        txtSpouse.Text = drow["SpouseName"].ToString();
                        txtPersonalContact.Text = drow["PersonalContact"].ToString();
                        txtOfficialContact.Text = drow["OfficialContact"].ToString();
                        txtEmergencyContact.Text = drow["EmergencyContact"].ToString();
                        txtContactPerson.Text = drow["ContactPerson"].ToString();
                        txtPersonalEmail.Text = drow["PersonalMail"].ToString();
                        txtOfficialEmail.Text = drow["OfficialMail"].ToString();
                        txtPresentAddress.Text = drow["PresentAddress"].ToString();
                        txtPermanentAddress.Text = drow["PermanentAddress"].ToString();
                        //txtCard.Text = drow["EmergencyAddress"].ToString();
                        //txtDOB.Text = drow["DOB"].ToString();
                        //txtDOB.Text = String.Format("{0:dd/MM/yyyy}", drow["DOB"].ToString());
                        txtDOB.Text = ((DateTime)drow["DOB"]).ToString("dd/MM/yyyy");

                        txtPlaceofBirth.Text = drow["PlaceOfBirth"].ToString();
                        txtBirthCertificate.Text = drow["BirthCertificate"].ToString();
                        txtNationalId.Text = drow["NID"].ToString();
                        txtTIN.Text = drow["TIN"].ToString();
                        txtPassport.Text = drow["PassportNo"].ToString();
                        txtDrivingLicense.Text = drow["DrivingLicense"].ToString();
                        //txtCard.Text = drow["ExtraCurricularActivities"].ToString();
                        //txtCard.Text = drow["Remark"].ToString();
                        //txtCard.Text = drow["PicUrl"].ToString();
                        //txtCard.Text = drow["SigUrl"].ToString();
                        ddGender.SelectedValue = drow["GenderAutoId"].ToString();
                        ddNationallity.SelectedValue = drow["NationalityAutoId"].ToString();
                        ddReligion.SelectedValue = drow["ReligionAutoId"].ToString();
                        ddBlood.SelectedValue = drow["BloodGroupAutoId"].ToString();
                        ddMaritalStatus.SelectedValue = drow["MaritalStatusAutoId"].ToString();
                        ImgPic.ImageUrl = "~/EmployeeImage/" + drow["PicUrl"].ToString();
                        ImgSig.ImageUrl = "~/EmployeeSig/" + drow["SigUrl"].ToString();
                        hidURL.Value = drow["PicUrl"].ToString();
                        hidSigURL.Value = drow["SigUrl"].ToString();
                        ddProject.SelectedValue = drow["ProjectId"].ToString();
                    }
                    btn_save.Text = "Update";
                }
            }
        }





    }
}