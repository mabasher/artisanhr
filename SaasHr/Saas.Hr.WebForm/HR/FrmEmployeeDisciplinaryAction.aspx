﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmEmployeeDisciplinaryAction.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmEmployeeDisciplinaryActionInformation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Employee Disciplinary Action Information 
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-7">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>                                                                                 
                            <div class="form-group">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                               <div class="col-sm-3 hidden">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            
                            <%--<hr />--%>
                            
                            <div class="form-group" style="margin-top: 20px;">
                                <asp:Label ID="lbl_issue_Date" runat="server" Text="Date of Issue :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox runat="server" ID="txtDOB" CssClass="input-sm date-picker" />
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label11" runat="server" Text="Action Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddReleaseType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <asp:Label ID="Label42" runat="server" Text="Penalty/Reward Tk. :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtFinalPayment"  runat="server" Text="0"  CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                                    <ajaxToolkit:filteredtextboxextender id="FilteredTextBoxExtender10" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtFinalPayment" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="Disciplinary Note :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPermanentAddress" TextMode="MultiLine" Height="80" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-2 hidden">
                        <div class="form-horizontal">
                            
                            
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Relation :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddGender" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Contact No :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtPersonalContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="E-mail Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtPersonalEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <asp:Label ID="Label14" runat="server" Text="National Id :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtNationalId" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label23" runat="server" Text="Share (%) :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtTIN" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label18" runat="server" Text="Nominee Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:DropDownList ID="ddReligion" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                               
                                    <asp:TextBox ID="txtNomineeName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>      
                            
                            <div class="form-group">
                                <asp:Label ID="Label19" runat="server" Text="Father's Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtFather" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label20" runat="server" Text="Mother's Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtMother" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label21" runat="server" Text="Spouse Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSpouse" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label17" runat="server" Text="Official Email :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOfficialEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text="Blood Group :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddBlood" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text="Marital Status :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddMaritalStatus" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text="Nationality :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddNationallity" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label15" runat="server" Text="Passport No :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtPassport" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label24" runat="server" Text="Driving License :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtDrivingLicense" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Official Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOfficialContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Emergency Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtEmergencyContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label25" runat="server" Text="Contact Person :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtContactPerson" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label26" runat="server" Text="Birth Certificate :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtBirthCertificate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label22" runat="server" Text="Place of Birth :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPlaceofBirth" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label9" runat="server" Text="Present Address:" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPresentAddress" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>


                            <div class="form-group" style="text-align:center;">
                                <asp:Image ID="ImgPic" runat="server" Height="120" BorderWidth="1" Width="120" />
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:FileUpload ID="imgUpload" runat="server" />
                                    <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Picture Image size <100KB)"></asp:Label>
                                </div>
                            </div>

                             <div class="form-group" style="text-align:center;">
                                <asp:Image ID="ImgSig" runat="server" Height="120" BorderWidth="1" Width="120" />
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:FileUpload ID="imgSigUpload" runat="server" />
                                    <asp:Label ID="Label28" ForeColor="#F22613" runat="server" Text="(Signature Image size <100 KB)"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row row-custom" style="padding-top: 20px;">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                        </div>
                        
                        <div class="form-group center">
                             &nbsp;
                        </div>
                        <div class="form-group center hidden">
                                 <asp:CheckBox ID="chkBangla" Text="&nbsp; Bangla" runat="server" />
                            &nbsp;&nbsp;
                                 <asp:CheckBox ID="CheckBox1" Text="&nbsp; English" runat="server" />
                            <asp:Button ID="btnPreview" runat="server" CssClass="btn btn-success btn-sm" Text="Issue Letter" OnClick="btnPreview_Click" Width="100px" />
                          
                        </div>
                    </div>
                </div>
            </div>

                                       
                    <div class="col-md-12">
                        <div style="height: 150px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="Action Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridActionType" runat="server" Text='<%# Bind("ActionType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Issue Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridIssueDate" runat="server" Text='<%# Bind("IssueDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tk.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridPenalty" runat="server" Text='<%# Bind("FinalPayment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Disciplinary Note">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridNote" runat="server" Text='<%# Bind("Note") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hid_GridEmpId" Value='<%# Bind("EmpId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridActionTypeId" Value='<%# Bind("ActionTypeId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>




            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="0" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>

</asp:Content>
