﻿using Saas.Hr.WebForm.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmPersonalInformation : Page
    {
        private string _imageUrl = string.Empty;
        private string _imageFileName = string.Empty;
        private string _sigUrl = string.Empty;
        private string _sigFileName = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                string Sessionuserlevel = string.Empty;
                Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtDOB.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtChildDOB.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtNomineeDOB.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtWorkExperienceToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtWorkExperienceFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddGender,
                    "GenderInfo",
                    "GenderName",
                    "GenderInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddNationallity,
                    "NationalityInfo",
                    "NationalityName",
                    "NationalityInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBlood,
                    "BloodGroupInfo",
                    "BloodGroupName",
                    "BloodGroupInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddReligion,
                    "ReligionInfo",
                    "ReligionName",
                    "ReligionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddMaritalStatus,
                    "MaritalStatusInfo",
                    "MaritalStatusName",
                    "MaritalStatusInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddProject,
                    "BranchInfo",
                    "BranchName",
                    "BranchInfoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddEmployeeType,
                //      "EmployeeGroup",
                //      "GroupName",
                //      "EmployeeGroupId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddExamName,
                    "ExamNameInfo",
                    "ExamName",
                    "ExamNameId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddTrainingTitle,
                 "TrainingTitleInfo",
                 "TrainingTitle",
                 "TrainingTitleId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddNomineeRelation,
                    "T_Relation",
                    "Relation",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddChildRelation,
                    "T_Relation",
                    "Relation",
                    "AutoId", string.Empty);

                if (Sessionuserlevel == "4")
                {
                    ddProject.Enabled= false;
                    PInfo.Visible = false;
                    txtSearch.Visible = false;
                    getEmpData(SessionUserId);
                    ControlEnable();
                }
                
                v_loadGridViewList();
                v_loadGridView_CostingHead();
                v_loadGridView_gdvTrainingInformation();
                v_loadGridView_gdvWorkExperience();
                v_loadGridView_gdvNomineeInfo();
                v_loadGridView_gdvChildInfo();
            }
        }

        private void maxSerial()
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;
            string s_sCompId = string.Empty;
            s_sCompId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "ProcMaxAutoSerialNoHRISABNB '" + ddProject.SelectedValue + "'";

            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["SLNo"].ToString();
                    }
                }
            }
        }

        protected void ddProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (btn_save.Text != "Update")
            {
                if (ddProject.SelectedValue != "0")
                {
                    maxSerial();
                }
            }
        }
        private void ControlEnable()
        {
            txtPIN.Enabled = false;
            txtCard.Enabled = false;
            txtOfficialEmail.Enabled = false;
        }
        protected void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            v_loadGridViewList();
        }
        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {

            hfAutoIdId.Value = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hid_GridMasterId")).Value;
            getEmpData(hfAutoIdId.Value);

        }
        private void v_loadGridViewList()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string s_sCompId = string.Empty;
            //s_sCompId = UserInforation.CompanyId.ToString();
            s_sCompId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SqlCommand cmd = new SqlCommand("Proc_Load_MasterGrid_Saas 'PersonalInfo','" + HttpUtility.HtmlDecode(txtEmpSearch.Text.Trim()) + "','" + s_sCompId + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();
            sqlconnection.Close();
        }
        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                if (imgUpload.HasFile)
                {
                    if (!string.IsNullOrEmpty(hidURL.Value))
                    {
                        var imgFilePath = Server.MapPath("~/EmployeeImage/" + hidURL.Value);
                        //var imgFilePath = Server.MapPath(hidURL.Value);
                        if (File.Exists(imgFilePath))
                        {
                            File.Delete(imgFilePath);
                        }
                        _imageFileName = hidURL.Value;
                        _imageUrl = "~/EmployeeImage/" + _imageFileName;
                    }
                    else
                    {
                        _imageFileName = Path.GetFileName(imgUpload.PostedFile.FileName);
                        if (_imageFileName != "")
                        {
                            _imageFileName = txtPIN.Text + "_P_" + _imageFileName;
                            _imageUrl = "~/EmployeeImage/" + _imageFileName;
                        }
                        //_imageFileName = "";
                    }
                }


                if (imgSigUpload.HasFile)
                {
                    if (!string.IsNullOrEmpty(hidSigURL.Value))
                    {
                        var sigFilePath = Server.MapPath("~/EmployeeSig/" + hidSigURL.Value);
                        //var sigFilePath = Server.MapPath(hidSigURL.Value);
                        if (File.Exists(sigFilePath))
                        {
                            File.Delete(sigFilePath);
                        }
                        _sigFileName = hidSigURL.Value;
                        _sigUrl = "~/EmployeeSig/" + _sigFileName;
                    }
                    else
                    {
                        _sigFileName = Path.GetFileName(imgSigUpload.PostedFile.FileName);
                        if (_sigFileName != "")
                        {
                            _sigFileName = txtPIN.Text + "_S_" + _sigFileName;
                            _sigUrl = "~/EmployeeSig/" + _sigFileName;
                        }
                        //_sigFileName = "";
                    }
                }




                s_Update = "[ProcPersonalProfileINSERT]"
                        + "'"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtPIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCard.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmpName.Text.Trim().Replace("'", "''"))
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFather.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtMother.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtSpouse.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmergencyContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim().Replace("'", "''"))
                        + "','','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlaceofBirth.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtBirthCertificate.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNationalId.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtTIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassport.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDrivingLicense.Text.Trim().Replace("'", "''"))
                        + "','','','"//@ExtraCurricularActivities,@Remark
                        + _imageFileName //_imageUrl//_imageFileName//PIC imageUrl
                        + "','"
                        + _sigFileName //_sigUrl//_sigFileName//SIG imageUrl
                        + "','"
                        + ddGender.SelectedValue
                        + "','"
                        + ddNationallity.SelectedValue
                        + "','"
                        + ddReligion.SelectedValue
                        + "','"
                        + ddBlood.SelectedValue
                        + "','"
                        + ddMaritalStatus.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U','"
                        + ddProject.SelectedValue +"' ";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;

                        if (imgUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imgUpload.PostedFile.InputStream);
                            StmImageProcessor.Save250Jpeg(Server.MapPath(_imageUrl), bmpPostedImage);
                        }

                        if (imgSigUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedSig = new System.Drawing.Bitmap(imgSigUpload.PostedFile.InputStream);
                            StmImageProcessor.Save80Jpeg(Server.MapPath(_sigUrl), bmpPostedSig);
                        }

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                }
            }
            else
            {
                _imageFileName = Path.GetFileName(imgUpload.PostedFile.FileName);
                if (_imageFileName != "")
                {
                    _imageFileName = txtPIN.Text + "_P_" + _imageFileName;
                    _imageUrl = "~/EmployeeImage/" + _imageFileName;
                    //_imageFileName = "";
                }

                _sigFileName = Path.GetFileName(imgSigUpload.PostedFile.FileName);
                if (_sigFileName != "")
                {
                    _sigFileName = txtPIN.Text + "_S_" + _sigFileName;
                    _sigUrl = "~/EmployeeSig/" + _sigFileName;
                }
                //_sigFileName = "";
                s_save_ = "[ProcPersonalProfileINSERT]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txtPIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCard.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmpName.Text.Trim().Replace("'", "''"))
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFather.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtMother.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtSpouse.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmergencyContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlaceofBirth.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtBirthCertificate.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNationalId.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtTIN.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassport.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDrivingLicense.Text.Trim().Replace("'", "''"))
                        + "','','','"//@ExtraCurricularActivities,@Remark
                        + _imageFileName//_imageUrl//_imageFileName//PIC imageUrl
                        + "','"
                        + _sigFileName//_sigUrl//_sigFileName//SIG imageUrl
                        + "','"
                        + ddGender.SelectedValue
                        + "','"
                        + ddNationallity.SelectedValue
                        + "','"
                        + ddReligion.SelectedValue
                        + "','"
                        + ddBlood.SelectedValue
                        + "','"
                        + ddMaritalStatus.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I','"
                        + ddProject.SelectedValue + "' ";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(imageUrl));

                        if (imgUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imgUpload.PostedFile.InputStream);
                            StmImageProcessor.Save250Jpeg(Server.MapPath(_imageUrl), bmpPostedImage);
                        }

                        if (imgSigUpload.HasFile)
                        {
                            System.Drawing.Bitmap bmpPostedSig = new System.Drawing.Bitmap(imgSigUpload.PostedFile.InputStream);
                            StmImageProcessor.Save80Jpeg(Server.MapPath(_sigUrl), bmpPostedSig);
                        }

                        //System.Drawing.Bitmap bmpPostedImage = new System.Drawing.Bitmap(imgUpload.PostedFile.InputStream);
                        //StmImageProcessor.Save250Jpeg(Server.MapPath(_imageUrl), bmpPostedImage);

                        //System.Drawing.Bitmap bmpPostedSig = new System.Drawing.Bitmap(imgSigUpload.PostedFile.InputStream);
                        //StmImageProcessor.Save80Jpeg(Server.MapPath(_sigUrl), bmpPostedSig);

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (ddGender.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Gender Can Not Blank!";
                return false;
            }

            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }

            if (txtPresentAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Present Address Can Not Blank!";
                return false;
            }
            if (txtPermanentAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Permanent Address Can Not Blank!";
                return false;
            }

            if (txtPersonalContact.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Contact No Can Not Blank!";
                return false;
            }

            //            if (imgUpload.HasFile)
            //            {
            //                int imgSize = imgUpload.PostedFile.ContentLength;
            //                string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
            //                if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
            //                {
            //                    lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
            //                    lblMsg.Visible = true;
            //                    return false;
            //                }
            //                else if (imgSize > 800000)//1048576)//1048576 bit=1MB
            //                {
            //                    lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
            //                    lblMsg.Visible = true;
            //                    return false;
            //                }
            //            }
            //else
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "User Photo Can Not Blank!";
            //    return false;
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtPresentAddress.Text = string.Empty;
            txtPermanentAddress.Text = string.Empty;
            txtFather.Text = string.Empty;
            txtMother.Text = string.Empty;
            txtNationalId.Text = string.Empty;
            txtPassport.Text = string.Empty;
            txtBirthCertificate.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            ddGender.SelectedValue = "0";
            ddNationallity.SelectedValue = "0";
            ddMaritalStatus.SelectedValue = "0";
            ddBlood.SelectedValue = "0";
            ddReligion.SelectedValue = "0";
            hidURL.Value = string.Empty;
            hidSigURL.Value = string.Empty;
        }
             
        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        hidEmpAutoId.Value = drow["EmployeePersonalInfoId"].ToString();
                        txtPIN.Text = drow["PIN"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        //txtCard.Text = drow["EmployeeNeekName"].ToString();
                        txtFather.Text = drow["FatherName"].ToString();
                        txtMother.Text = drow["MotherName"].ToString();
                        txtSpouse.Text = drow["SpouseName"].ToString();
                        txtPersonalContact.Text = drow["PersonalContact"].ToString();
                        txtOfficialContact.Text = drow["OfficialContact"].ToString();
                        txtEmergencyContact.Text = drow["EmergencyContact"].ToString();
                        txtContactPerson.Text = drow["ContactPerson"].ToString();
                        txtPersonalEmail.Text = drow["PersonalMail"].ToString();
                        txtOfficialEmail.Text = drow["OfficialMail"].ToString();
                        txtPresentAddress.Text = drow["PresentAddress"].ToString();
                        txtPermanentAddress.Text = drow["PermanentAddress"].ToString();
                        //txtCard.Text = drow["EmergencyAddress"].ToString();
                        //txtDOB.Text = drow["DOB"].ToString();
                        //txtDOB.Text = String.Format("{0:dd/MM/yyyy}", drow["DOB"].ToString());
                        txtDOB.Text = ((DateTime)drow["DOB"]).ToString("dd/MM/yyyy");

                        txtPlaceofBirth.Text = drow["PlaceOfBirth"].ToString();
                        txtBirthCertificate.Text = drow["BirthCertificate"].ToString();
                        txtNationalId.Text = drow["NID"].ToString();
                        txtTIN.Text = drow["TIN"].ToString();
                        txtPassport.Text = drow["PassportNo"].ToString();
                        txtDrivingLicense.Text = drow["DrivingLicense"].ToString();
                        //txtCard.Text = drow["ExtraCurricularActivities"].ToString();
                        //txtCard.Text = drow["Remark"].ToString();
                        //txtCard.Text = drow["PicUrl"].ToString();
                        //txtCard.Text = drow["SigUrl"].ToString();
                        ddGender.SelectedValue = drow["GenderAutoId"].ToString();
                        ddNationallity.SelectedValue = drow["NationalityAutoId"].ToString();
                        ddReligion.SelectedValue = drow["ReligionAutoId"].ToString();
                        ddBlood.SelectedValue = drow["BloodGroupAutoId"].ToString();
                        ddMaritalStatus.SelectedValue = drow["MaritalStatusAutoId"].ToString();
                        ImgPic.ImageUrl = "~/EmployeeImage/" + drow["PicUrl"].ToString();
                        ImgSig.ImageUrl = "~/EmployeeSig/" + drow["SigUrl"].ToString();
                        hidURL.Value = drow["PicUrl"].ToString();
                        hidSigURL.Value = drow["SigUrl"].ToString();
                        ddProject.SelectedValue = drow["ProjectId"].ToString();
                     
                        v_loadGridView_CostingHead();
                        v_loadGridView_gdvTrainingInformation();
                        v_loadGridView_gdvWorkExperience();
                        v_loadGridView_gdvNomineeInfo();
                        v_loadGridView_gdvChildInfo();

                    }
                    btn_save.Text = "Update";
                }
            }
        }

        //--------------AcademicInfo-------------------

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")   { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcEmpAcademicInformation'" + hidEmpAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.DataBind();
            sqlconnection.Close();

        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsgAcademic.Text = exception.Message;
            }
        }
        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {           
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidEmpAcademicInfoId")).Value;

                ddExamName.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidExamNameId")).Value;
                txtInstitute.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridInstituteName")).Text;
                txtBoardUniversity.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridBoardUniversityName")).Text;
                txtMajorGroup.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridMajorGroupName")).Text;
                txtResult.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridResult")).Text;
                txtCGPAMarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCGPAMarks")).Text;
                txtScaleOutOff.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridScaleOutOff")).Text;
                txtYearOfPassing.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridYearOfPassing")).Text;
                txtDuration.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDuration")).Text;
                txtAchievement.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAchievement")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;

                btnAcademicSave.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsgAcademic.Text = exception.Message;
            }

        }
        protected void btnAcademicSave_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValidAcademic();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btnAcademicSave.Text == "Update")
            {                
                s_Update = "[ProcAcademicInfoINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddExamName.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtInstitute.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBoardUniversity.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMajorGroup.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtResult.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCGPAMarks.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtScaleOutOff.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtYearOfPassing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDuration.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAchievement.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsgAcademic.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsgAcademic.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        InsertModeAcademic();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcAcademicInfoINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddExamName.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtInstitute.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBoardUniversity.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMajorGroup.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtResult.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCGPAMarks.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtScaleOutOff.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtYearOfPassing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDuration.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAchievement.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsgAcademic.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        InsertModeAcademic();
                    }
                    else
                    {
                        lblMsgAcademic.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsgAcademic.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }
        protected void btnAcademicRefresh_Click(object sender, EventArgs e)
        {
            InsertModeAcademic();
            InsertMode_MsgAcademic();
        }
        private void InsertModeAcademic()
        {
            btnAcademicSave.Text = "Save";
            ddExamName.SelectedValue = "0";
            txtInstitute.Text = string.Empty;
            txtBoardUniversity.Text = string.Empty;
            txtMajorGroup.Text = string.Empty;
            txtResult.Text = string.Empty;
            txtCGPAMarks.Text = string.Empty;
            txtScaleOutOff.Text = string.Empty;
            txtYearOfPassing.Text = string.Empty;
            txtDuration.Text = string.Empty;
            txtAchievement.Text = string.Empty;
            txtRemarks.Text = string.Empty;
        }
        private void InsertMode_MsgAcademic()
        {
            btnAcademicSave.Text = "Save";
            lblMsgAcademic.Text = string.Empty;
        }
        private Boolean isValidAcademic()
        {
            if (ddExamName.SelectedValue == "0")
            {
                lblMsgAcademic.Visible = true;
                lblMsgAcademic.Text = "Exam Name Can Not Blank!";
                return false;
            }
            //if (txtInstitute.Text == "")
            //{
            //    lblMsgAcademic.Visible = true;
            //    lblMsgAcademic.Text = "Institute Can Not Blank!";
            //    return false;
            //}

            if (txtBoardUniversity.Text == "")
            {
                lblMsgAcademic.Visible = true;
                lblMsgAcademic.Text = "Board University Can Not Blank!";
                return false;
            }
            //if (txtMajorGroup.Text == "")
            //{
            //    lblMsgAcademic.Visible = true;
            //    lblMsgAcademic.Text = "Major Group Name Can Not Blank!";
            //    return false;
            //}


            return true;
        }
                 
        //---------Training Information--------------
        private void v_loadGridView_gdvTrainingInformation()
        {
            if (hidEmpAutoId.Value == "")  { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcSelectTrainingInformation'" + hidEmpAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvTrainingInformation.DataSource = ds;
            gdvTrainingInformation.DataBind();
            sqlconnection.Close();
        }
        protected void gdvTrainingInformation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvTrainingInformation, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsgTraining.Text = exception.Message;
            }
        }
        protected void gdvTrainingInformation_SelectedIndexChanged(object sender, EventArgs e)
        {           
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("hidTrainingInfoId")).Value;

                ddTrainingTitle.SelectedValue = ((HiddenField)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("hidTrainingTitleId")).Value;
                txtOrganizedBy.Text = ((Label)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("lblGridOrganizedBy")).Text;
                txtTopicsCovered.Text = ((Label)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("lblGridTopicsCovered")).Text;
                txtFromDate.Text = ((Label)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("lblGridFromDate")).Text;
                txtToDate.Text = ((Label)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("lblGridToDate")).Text;
                txtTrainingRemarks.Text = ((Label)gdvTrainingInformation.Rows[gdvTrainingInformation.SelectedIndex].FindControl("lblGridRemarks")).Text;

                btnTrainingSave.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsgTraining.Text = exception.Message;
            }

        }
        protected void btnTrainingRefresh_Click(object sender, EventArgs e)
        {
            InsertModeTraining();
            InsertMode_MsgTraining();
        }
        private void InsertMode_MsgTraining()
        {
            btnTrainingSave.Text = "Save";
            lblMsgTraining.Text = string.Empty;
        }
        private Boolean isValidTraining()
        {
            
            if (ddTrainingTitle.SelectedValue == "0")
            {
                lblMsgTraining.Visible = true;
                lblMsgTraining.Text = "Training Title Can Not Blank!";
                return false;
            }

            if (txtTopicsCovered.Text == "")
            {
                lblMsgTraining.Visible = true;
                lblMsgTraining.Text = "Topics Covered Can Not Blank!";
                return false;
            }
            return true;
        }
        private void InsertModeTraining()
        {
            btnTrainingSave.Text = "Save";
            ddTrainingTitle.SelectedValue = "0";
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtOrganizedBy.Text = string.Empty;
            txtTopicsCovered.Text = string.Empty;
            txtTrainingRemarks.Text = string.Empty;
        }
        protected void btnTrainingSave_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValidTraining();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btnTrainingSave.Text == "Update")
            {
                s_Update = "[ProcTrainingInformationINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddTrainingTitle.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtTopicsCovered.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOrganizedBy.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtTrainingRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsgTraining.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsgTraining.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        InsertModeTraining();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcTrainingInformationINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddTrainingTitle.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtTopicsCovered.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOrganizedBy.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtTrainingRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsgTraining.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        InsertModeTraining();
                    }
                    else
                    {
                        lblMsgTraining.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsgTraining.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }



        //-----------WorkExperienceInfo--------------

        private void v_loadGridView_gdvWorkExperience()
        {
            if (hidEmpAutoId.Value == "") { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("SELECT WorkExperienceInfoId as AutoId,companyName as companyName,ni.companyBusiness as companyBusiness,ni.companyAddress as companyAddress,ni.designation as designation,ni.department as department, ni.jobNature as jobNature, ni.jobResponsibilities as jobResponsibilities, ni.lastSalary as lastSalary, ni.fromDate as fromDate, ni.toDate as toDate, ni.continuing as continuing , ni.remark as remark FROM WorkExperienceInfo ni left outer join EmployeePersonalInfo ei on ni.EmployeePersonalInfoId=ei.EmployeePersonalInfoId  Where ni.EmployeePersonalInfoId='" + hidEmpAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvWorkExperience.DataSource = ds;
            gdvWorkExperience.AllowPaging = true;
            gdvWorkExperience.PageSize = 8;
            gdvWorkExperience.DataBind();
            sqlconnection.Close();
        }
        protected void gdvWorkExperience_RowDataBound(object sender, GridViewRowEventArgs e)
        {          
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvWorkExperience, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblWorkExperience.Text = exception.Message;
            }

        }

        protected void gdvWorkExperience_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtCompanyName.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridCompanyName")).Text;

                txtCompanyBusiness.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridCompanyBusiness")).Text;
                txtCompanyAddress.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridCompanyAddress")).Text;
                txtDesignation.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridDesignation")).Text;
                txtDepartment.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridDepartment")).Text;
                txtJobNature.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridJobNature")).Text;
                txtJobResponsibilities.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridjobResponsibilities")).Text;
                txtLastSalary.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridlastSalary")).Text;
                txtWorkExperienceFromDate.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridfromDate")).Text;
                txtWorkExperienceToDate.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridtoDate")).Text;
                txtContinuing.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridcontinuing")).Text;
                txtWorkExperienceRemarks.Text = ((Label)gdvWorkExperience.Rows[gdvWorkExperience.SelectedIndex].FindControl("lblGridremark")).Text;

                btnWorkExperienceSave.Text = "Update";
            }
            catch (Exception exception)
            {
                lblWorkExperience.Text = exception.Message;
            }

        }

        protected void btnWorkExperienceRefresh_Click(object sender, EventArgs e)
        {
            InsertModeWorkExperience();
            InsertMode_MsgWorkExperience();
        }

        private void InsertMode_MsgWorkExperience()
        {
            btnWorkExperienceSave.Text = "Save";
            lblWorkExperience.Text = string.Empty;
        }

        private Boolean isValidWorkExperience()
        {
            if (txtCompanyName.Text == "")
            {
                lblWorkExperience.Visible = true;
                lblWorkExperience.Text = "Company Name Can Not Blank!";
                return false;
            }
            if (txtDesignation.Text == "")
            {
                lblWorkExperience.Visible = true;
                lblWorkExperience.Text = "Designation Can Not Blank!";
                return false;
            }
            if (txtDepartment.Text == "")
            {
                lblWorkExperience.Visible = true;
                lblWorkExperience.Text = "Department Can Not Blank!";
                return false;
            }
            //if (txtJobResponsibilities.Text == "")
            //{
            //    lblWorkExperience.Visible = true;
            //    lblWorkExperience.Text = "Job Responsibilities Can Not Blank!";
            //    return false;
            //}
            return true;
        }

        private void InsertModeWorkExperience()
        {
            btnWorkExperienceSave.Text = "Save";
            txtCompanyName.Text = string.Empty;
            txtCompanyBusiness.Text = string.Empty;
            txtCompanyAddress.Text = string.Empty;
            txtDesignation.Text = string.Empty;
            txtDepartment.Text = string.Empty;
            txtJobNature.Text = string.Empty;
            txtJobResponsibilities.Text = string.Empty;
            txtLastSalary.Text = string.Empty;
            txtContinuing.Text = string.Empty;
            txtWorkExperienceRemarks.Text = string.Empty;

        }

        protected void btnWorkExperienceSave_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValidWorkExperience();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btnWorkExperienceSave.Text == "Update")
            {     
                s_Update = "[ProcWorkExperienceInfoINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyBusiness.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDesignation.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDepartment.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobNature.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobResponsibilities.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtLastSalary.Text.Trim().Replace("'", "''"))
                        + "','"
                         + Convert.ToDateTime(txtWorkExperienceFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtWorkExperienceToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtContinuing.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtWorkExperienceRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblWorkExperience.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblWorkExperience.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        InsertModeWorkExperience(); 
                    }
                }
            }
            else
            {             

                s_save_ = "[ProcWorkExperienceInfoINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyBusiness.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDesignation.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDepartment.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobNature.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobResponsibilities.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtLastSalary.Text.Trim().Replace("'", "''"))
                        + "','"
                        + Convert.ToDateTime(txtWorkExperienceFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtWorkExperienceToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtContinuing.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtWorkExperienceRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblWorkExperience.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        InsertModeWorkExperience();
                    }
                    else
                    {
                        lblWorkExperience.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblWorkExperience.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }




        //----  NomineeInfo------------------

        private void v_loadGridView_gdvNomineeInfo()
        {
            if (hidEmpAutoId.Value == "") { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("SELECT EmployeeNomineeInfoId as AutoId,NomineeName as NomineeName,ni.FatherName as FatherName,ni.MotherName as MotherName,ni.DOB as DOB,ni.PermanentAddress as PermanentAddress,ni.PersonalContact as PersonalContact,ni.PersonalMail as PersonalMail,ni.NID as NID,r.Relation as Relation,r.AutoId as RelationId ,ni.Share as Share FROM EmployeeNomineeInfo ni left outer join EmployeePersonalInfo ei on ni.EmployeePersonalInfoId=ei.EmployeePersonalInfoId left outer join T_Relation r on ni.RelationId=r.AutoId Where ni.EmployeePersonalInfoId='" + hidEmpAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvNomineeInfo.DataSource = ds;
            gdvNomineeInfo.AllowPaging = true;
            gdvNomineeInfo.PageSize = 8;
            gdvNomineeInfo.DataBind();
            sqlconnection.Close();

        }
        protected void gdvNomineeInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvNomineeInfo, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblNomineeMsg.Text = exception.Message;
            }
        }

        protected void gdvNomineeInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtNomineeName.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridName")).Text;
                txtFatherNomineeName.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridFather")).Text;
                txtMotherNomineeName.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridMother")).Text;
                txtNomineeContactNo.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridContact")).Text;
                txtNomineeNationalId.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridNid")).Text;
                txtNomineeDOB.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridDOB")).Text;
                txtNomineeEMail.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridMail")).Text;
                txtNomineeAddress.Text = ((HiddenField)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("hidGridAddress")).Value;
                txtNomineeShare.Text = ((Label)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("lblGridShare")).Text;
                ddNomineeRelation.SelectedValue = ((HiddenField)gdvNomineeInfo.Rows[gdvNomineeInfo.SelectedIndex].FindControl("hidRelationId")).Value;

                btnNomineeSave.Text = "Update";
            }
            catch (Exception exception)
            {
                lblNomineeMsg.Text = exception.Message;
            }

        }
        protected void btnNomineeRefresh_Click(object sender, EventArgs e)
        {
            InsertModeNominee();
            InsertMode_MsgNominee();
        }

        private void InsertMode_MsgNominee()
        {
            btnNomineeSave.Text = "Save";
            lblNomineeMsg.Text = string.Empty;
        }

        private Boolean isValidNominee()
        {

            if (ddNomineeRelation.SelectedValue == "0")
            {
                lblNomineeMsg.Visible = true;
                lblNomineeMsg.Text = "Relation Can Not Blank!";
                return false;
            }
          
            if (txtNomineeName.Text == "")
            {
                lblNomineeMsg.Visible = true;
                lblNomineeMsg.Text = "Nominee Name Can Not Blank!";
                return false;
            }

            if (txtNomineeAddress.Text == "")
            {
                lblNomineeMsg.Visible = true;
                lblNomineeMsg.Text = "Nominee Address Can Not Blank!";
                return false;
            }

            if (txtNomineeShare.Text == "")
            {
                lblNomineeMsg.Visible = true;
                lblNomineeMsg.Text = "Share Can Not Blank!";
                return false;
            }

            if (txtNomineeContactNo.Text == "")
            {
                lblNomineeMsg.Visible = true;
                lblNomineeMsg.Text = "Contact No Can Not Blank!";
                return false;
            }        
            return true;
        }

        private void InsertModeNominee()
        {
            btnNomineeSave.Text = "Save";
            txtNomineeName.Text = string.Empty;
            txtFatherNomineeName.Text = string.Empty;
            txtMotherNomineeName.Text = string.Empty;
            txtNomineeContactNo.Text = string.Empty;
            txtNomineeNationalId.Text = string.Empty;
            txtNomineeDOB.Text = string.Empty;
            txtNomineeEMail.Text = string.Empty;
            txtNomineeAddress.Text = string.Empty;
            txtNomineeShare.Text = string.Empty;
            ddNomineeRelation.SelectedValue = "0";
        }
        protected void btnNomineeSave_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValidNominee();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btnNomineeSave.Text == "Update")
            {  
                s_Update = "[ProcNomineeInformationINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtFatherNomineeName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtMotherNomineeName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + Convert.ToDateTime(txtNomineeDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeContactNo.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeEMail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeNationalId.Text.Trim().Replace("'", "''"))
                        + "','"
                        + ddNomineeRelation.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeShare.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblNomineeMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblNomineeMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        InsertModeNominee();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcNomineeInformationINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtFatherNomineeName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtMotherNomineeName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + Convert.ToDateTime(txtNomineeDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeContactNo.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeEMail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeNationalId.Text.Trim().Replace("'", "''"))
                        + "','"
                        + ddNomineeRelation.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtNomineeShare.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblNomineeMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        InsertModeNominee();
                    }
                    else
                    {
                        lblNomineeMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblNomineeMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }





        // ------ChildInfo---------------

        private void v_loadGridView_gdvChildInfo()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("SELECT EmployeeChildInfoId as AutoId,ChildName as ChildName,ni.FatherName as FatherName,ni.MotherName as MotherName,ni.DOB as DOB,ni.PermanentAddress as PermanentAddress,ni.PersonalContact as PersonalContact,ni.PersonalMail as PersonalMail,ni.NID as NID,r.Relation as Relation,r.AutoId as RelationId  FROM EmployeeChildInfo ni left outer join EmployeePersonalInfo ei on ni.EmployeePersonalInfoId=ei.EmployeePersonalInfoId left outer join T_Relation r on ni.RelationId=r.AutoId Where ni.EmployeePersonalInfoId='" + hidEmpAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvChildInfo.DataSource = ds;
            gdvChildInfo.AllowPaging = true;
            gdvChildInfo.PageSize = 8;
            gdvChildInfo.DataBind();
            sqlconnection.Close();

        }
        protected void gdvChildInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvChildInfo, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblChildMsg.Text = exception.Message;
            }

        }

        protected void gdvChildInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtChildName.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridName")).Text;

                txtChildFatherName.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridFather")).Text;
                txtChildMotherName.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridMother")).Text;
                txtChildContact.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridContact")).Text;
                txtChildNID.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridNid")).Text;
                txtChildDOB.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridDOB")).Text;
                txtChildEmail.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridMail")).Text;
                txtChildAddress.Text = ((Label)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("lblGridPermanentAddress")).Text;
                ddChildRelation.SelectedValue = ((HiddenField)gdvChildInfo.Rows[gdvChildInfo.SelectedIndex].FindControl("hidRelationId")).Value;
                btnChildInfoSave.Text = "Update";
            }
            catch (Exception exception)
            {
                lblChildMsg.Text = exception.Message;
            }

        }
        protected void btnChildInfoRefresh_Click(object sender, EventArgs e)
        {
            InsertModeChild();
            InsertMode_MsgChild();
        }

        private void InsertMode_MsgChild()
        {
            btnChildInfoSave.Text = "Save";
            lblChildMsg.Text = string.Empty;
        }

        private Boolean isValidChild()
        {

            if (ddChildRelation.SelectedValue == "0")
            {
                lblChildMsg.Visible = true;
                lblChildMsg.Text = "Relation Can Not Blank!";
                return false;
            }      
            if (txtChildName.Text == "")
            {
                lblChildMsg.Visible = true;
                lblChildMsg.Text = "Child Name Can Not Blank!";
                return false;
            }

            if (txtChildAddress.Text == "")
            {
                lblChildMsg.Visible = true;
                lblChildMsg.Text = "Address Can Not Blank!";
                return false;
            }           

            //if (txtChildContact.Text == "")
            //{
            //    lblChildMsg.Visible = true;
            //    lblChildMsg.Text = "Contact No Can Not Blank!";
            //    return false;
            //}
                 
            return true;
        }

        private void InsertModeChild()
        {
            btnChildInfoSave.Text = "Save";

            txtChildName.Text = string.Empty;
            txtChildFatherName.Text = string.Empty;
            txtChildMotherName.Text = string.Empty;
            txtChildAddress.Text = string.Empty;
            txtChildContact.Text = string.Empty;
            txtChildEmail.Text = string.Empty;
            txtChildNID.Text = string.Empty;

            ddChildRelation.SelectedValue = "0";
        }

        protected void btnChildInfoSave_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValidChild();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btnChildInfoSave.Text == "Update")
            {               

                s_Update = "[ProcChildInformationINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildFatherName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildMotherName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + Convert.ToDateTime(txtChildDOB.Text.Trim(), new CultureInfo("en-GB")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildNID.Text.Trim().Replace("'", "''"))
                        + "','"
                        + ddChildRelation.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblChildMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblChildMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        InsertModeChild();
                    }
                }
            }
            else
            {                
                s_save_ = "[ProcChildInformationINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildFatherName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildMotherName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + Convert.ToDateTime(txtChildDOB.Text.Trim(), new CultureInfo("en-GB")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildContact.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildEmail.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtChildNID.Text.Trim().Replace("'", "''"))
                        + "','"
                        + ddChildRelation.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblChildMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        InsertModeChild();
                    }
                    else
                    {
                        lblChildMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblChildMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }





    }
}