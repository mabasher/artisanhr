﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmOfficialInformation : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo", 
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddDesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddEmployeeGroup,
                    "EmployeeGroup",
                    "GroupName",
                    "EmployeeGroupId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddEmployeeType,
                    "EmployeeType",
                    "TypeName",
                    "EmployeeTypeId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddShift,
                    "ShiftInformation",
                    "ShiftName",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddFloor,
                    "FloorInfo",
                    "FloorName",
                    "FloorInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBlock,
                    "BlockInfo",
                    "Block",
                    "BlockInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownListMultiColumn(ddSupervisor,
                    "EmployeePersonalInfo",
                    "PIN",
                    "EmployeeName",
                    "EmployeePersonalInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddJobLocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBranch,
                    "BranchInfo",
                    "BranchName",
                    "BranchInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddProject,
                    "ProjectInfo",
                    "ProjectName",
                    "ProjectInfoId", string.Empty);

                txtDOB.Text = DateTime.Now.ToString();
                txtDOPF.Text = DateTime.Now.ToString();
                txtDOConFirmation.Text = DateTime.Now.ToString();

                btn_save.Text = "Save";
                v_loadGridViewList();
            }
        }
        protected void txtEmpSearch_TextChanged(object sender, EventArgs e)
        {
            v_loadGridViewList();
        }
        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAutoIdId.Value = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hid_GridMasterId")).Value;
            getEmpData(hfAutoIdId.Value);
        }
        private void v_loadGridViewList()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string s_sCompId = string.Empty;
            s_sCompId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SqlCommand cmd = new SqlCommand("Proc_Load_MasterGrid_Saas 'EmployeeOfficialInfo','" + HttpUtility.HtmlDecode(txtEmpSearch.Text.Trim()) + "','" + s_sCompId + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();
            sqlconnection.Close();
        }
        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "") { hidEmpAutoId.Value = "0"; }
                CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();
            
                SqlCommand cmd = new SqlCommand("SELECT EmployeeNomineeInfoId as AutoId,NomineeName as NomineeName,ni.FatherName as FatherName,ni.MotherName as MotherName,ni.DOB as DOB,ni.PermanentAddress as PermanentAddress,ni.PersonalContact as PersonalContact,ni.PersonalMail as PersonalMail,ni.NID as NID,r.Relation as Relation,r.AutoId as RelationId ,ni.Share as Share FROM EmployeeNomineeInfo ni left outer join EmployeePersonalInfo ei on ni.EmployeePersonalInfoId=ei.EmployeePersonalInfoId left outer join T_Relation r on ni.RelationId=r.AutoId Where ni.EmployeePersonalInfoId='"+ hidEmpAutoId.Value + "'", sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.AllowPaging = true;
                gdv_costingHead.PageSize = 8;
                gdv_costingHead.DataBind();
                sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_PFEnable = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();
            s_PFEnable = "N";
            if (chkPFEnable.Checked) { s_PFEnable = "Y"; }
            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {                
                s_Update = "[ProcOfficialInformationINSERT]"
                        + "'"
                        + hidAutoId.Value 
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddProject.SelectedValue
                        + "','"
                        + ddBranch.SelectedValue
                        + "','"
                        + ddJobLocation.SelectedValue
                        + "','"
                        + ddDepartment.SelectedValue
                        + "','"
                        + ddDesignation.SelectedValue
                        + "','"
                        + ddEmployeeGroup.SelectedValue
                        + "','"
                        + ddEmployeeType.SelectedValue
                        + "','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtProbationPeriod.Text.Trim())
                        + "','Month','"
                        + Convert.ToDateTime(txtDOConFirmation.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtDOConFirmation.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + s_PFEnable
                        + "','"
                        + Convert.ToDateTime(txtDOConFirmation.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")//txtDOPF
                        + "','"
                        + ddSupervisor.SelectedValue
                        + "','"
                        + ddShift.SelectedValue
                        + "','"
                        + ddSection.SelectedValue
                        + "','"
                        + ddFloor.SelectedValue
                        + "','"
                        + ddBlock.SelectedValue
                        + "','"
                        + ddLine.SelectedValue
                        + "','','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;                        
                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcOfficialInformationINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddProject.SelectedValue
                        + "','"
                        + ddBranch.SelectedValue
                        + "','"
                        + ddJobLocation.SelectedValue
                        + "','"
                        + ddDepartment.SelectedValue
                        + "','"
                        + ddDesignation.SelectedValue
                        + "','"
                        + ddEmployeeGroup.SelectedValue
                        + "','"
                        + ddEmployeeType.SelectedValue
                        + "','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtProbationPeriod.Text.Trim())
                        + "','Month','"
                        + Convert.ToDateTime(txtDOConFirmation.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtDOConFirmation.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + s_PFEnable
                        + "','"
                        + Convert.ToDateTime(txtDOConFirmation.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")//txtDOPF
                        + "','"
                        + ddSupervisor.SelectedValue
                        + "','"
                        + ddShift.SelectedValue
                        + "','"
                        + ddSection.SelectedValue
                        + "','"
                        + ddFloor.SelectedValue
                        + "','"
                        + ddBlock.SelectedValue
                        + "','"
                        + ddLine.SelectedValue
                        + "','','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I'";
                               
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        
                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (ddDepartment.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Department Can Not Blank!";
                return false;
            }
            if (ddDesignation.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Designation Can Not Blank!";
                return false;
            }
            if (ddEmployeeGroup.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Grade Can Not Blank!";
                return false;
            }
            if (ddEmployeeType.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Type Can Not Blank!";
                return false;
            }
            if (ddSupervisor.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Supervisor Can Not Blank!";
                return false;
            }
            if (ddShift.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Shift Can Not Blank!";
                return false;
            }
            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtNomineeName.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtPresentAddress.Text = string.Empty;
            txtPermanentAddress.Text = string.Empty;
            txtFather.Text = string.Empty;
            txtMother.Text = string.Empty;
            txtNationalId.Text = string.Empty;
            txtPassport.Text = string.Empty;
            txtBirthCertificate.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            txtTIN.Text = string.Empty;

            ddGender.SelectedValue = "0";

        }

        protected void txtProbationPeriod_TextChanged(object sender, EventArgs e)
        {
            if (txtProbationPeriod.Text == "")
            {
                txtProbationPeriod.Text = "6";
            }
            DateTime dt = DateTime.ParseExact(txtDOB.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dt = dt.AddDays(Convert.ToDouble(txtProbationPeriod.Text)*30);
            txtDOConFirmation.Text = dt.ToString("dd/MM/yyyy");

            dt = DateTime.ParseExact(txtDOConFirmation.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dt = dt.AddDays(90);
            txtDOPF.Text = dt.ToString("dd/MM/yyyy");
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            btn_save.Text = "Save";

            s_select = "SELECT isnull(o.EmployeeOfficialInfoId,0) as AutoId,e.PIN,e.EmployeeName,isnull(o.DepartmentAutoId,0) as deptId,isnull(DesignationAutoId,0) as dgId,isnull(o.EmployeeGroupAutoId,0) as empgroupId,isnull(o.EmployeeTypeAutoId,0) as empTypeId";
            s_select = s_select+" "+ ",isnull(o.SectionAutoId,0) as secId,isnull(o.FloorAutoId,0) as floorId,isnull(o.BlockAutoId,0) as blockId,isnull(o.LineAutoId,0) as lineId";
            s_select = s_select + " " + ",isnull(o.SupervisorAutoId,0) as supervisorId,isnull(o.ShiftAutoId,0) as shiftId,isnull(o.LocationAutoId,0) as locationId,isnull(o.BranchAutoId,0) as branchId";
            s_select = s_select + " " + ",isnull(o.ProjectAutoId,0) as projectId,isnull(JoinDate,getdate()) as joindt,isnull(ProbationPeriod,6) as probday,isnull(o.ConfirmationDate,getdate()) as confirmdt";
            s_select = s_select + " " + ",isnull(o.PFDate,getdate()) as pfdt,isnull(PFEnable,'N') as pfenable";
            s_select = s_select + " " + "FROM EmployeePersonalInfo e left outer join EmployeeOfficialInfo o on e.EmployeePersonalInfoId=o.EmpAutoId  Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();

                        ddSupervisor.SelectedValue = drow["supervisorId"].ToString();
                        ddShift.SelectedValue = drow["shiftId"].ToString();
                        ddJobLocation.SelectedValue = drow["locationId"].ToString();
                        ddBranch.SelectedValue = drow["branchId"].ToString();                        
                        ddProject.SelectedValue = drow["projectId"].ToString();
                        ddDepartment.SelectedValue = drow["deptId"].ToString();
                        ddDesignation.SelectedValue = drow["dgId"].ToString();
                        ddEmployeeGroup.SelectedValue = drow["empgroupId"].ToString();
                        ddEmployeeType.SelectedValue = drow["empTypeId"].ToString();
                        ddSection.SelectedValue = drow["secId"].ToString();
                        ddFloor.SelectedValue = drow["floorId"].ToString();
                        ddBlock.SelectedValue = drow["blockId"].ToString();
                        ddLine.SelectedValue = drow["lineId"].ToString();
                        txtDOB.Text= ((DateTime)drow["joindt"]).ToString("dd/MM/yyyy");
                        txtDOPF.Text = ((DateTime)drow["pfdt"]).ToString("dd/MM/yyyy");
                        txtDOConFirmation.Text = ((DateTime)drow["confirmdt"]).ToString("dd/MM/yyyy");
                        txtProbationPeriod.Text = drow["probday"].ToString();
                        hidAutoId.Value= drow["AutoId"].ToString();
                        hidEmpAutoId.Value = sEmpId;
                        if (hidAutoId.Value != "0")
                        {
                            btn_save.Text = "Update";
                        }
                        //pfenable

                    }
                }
            }
            //v_loadGridView_CostingHead();
        }


      //===========End===============         
    }
}