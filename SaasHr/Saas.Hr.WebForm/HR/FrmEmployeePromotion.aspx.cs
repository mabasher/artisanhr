﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmEmployeePromotionInformation : BasePage
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(ddReleaseType,
                    "EmployeeDisciplinaryActionType",
                    "DisciplinaryActionType",
                    "DisciplinaryActionTypeId", string.Empty);

                commonfunctions.g_b_FillDropDownList(dddepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(dddesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddemployeegroup,
                    "EmployeeGroup",
                    "GroupName",
                    "EmployeeGroupId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddemployeetype,
                    "EmployeeType",
                    "TypeName",
                    "EmployeeTypeId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddlocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddbranch,
                    "BranchInfo",
                    "BranchName",
                    "BranchInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddproject,
                    "ProjectInfo",
                    "ProjectName",
                    "ProjectInfoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddOdepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddOdesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddOemployeegroup,
                    "EmployeeGroup",
                    "GroupName",
                    "EmployeeGroupId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddOemployeetype,
                    "EmployeeType",
                    "TypeName",
                    "EmployeeTypeId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddOlocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddObranch,
                    "BranchInfo",
                    "BranchName",
                    "BranchInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddOproject,
                    "ProjectInfo",
                    "ProjectName",
                    "ProjectInfoId", string.Empty);

                txtDOB.Text = DateTime.Now.ToString();

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
                CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();
                string sQry = string.Empty;
                sQry = "SELECT ep.EmployeePromotionInfoId as AutoId,ep.PromotionDate as PromotionDate,dt.Department as Department,dg.Designation as Designation,ep.Remarks as Remarks,EmpAutoId,ProjectAutoId,BranchAutoId,LocationAutoId,DepartmentAutoId,DesignationAutoId,EmployeeGroupAutoId,EmployeeTypeAutoId FROM EmployeePromotionInfo ep left outer join DepartmentInfo dt on ep.DepartmentAutoId=dt.DepartmentInfoId left outer join DesignationInfo dg on ep.DesignationAutoId=dg.DesignationInfoId left outer join LocationInfo l on ep.LocationAutoId=l.LocationInfoId left outer join BranchInfo b on ep.BranchAutoId=b.BranchInfoId left outer join EmployeeGroup eg on ep.EmployeeGroupAutoId=eg.EmployeeGroupId left outer join EmployeeType et on ep.EmployeeTypeAutoId=et.EmployeeTypeId Where ep.EmpAutoId='" + hidEmpAutoId.Value + "'";
                       
                SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.DataBind();
                sqlconnection.Close();
            
        }
        
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_PFEnable = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();
            s_PFEnable = "N";

             s_save_ = "Delete dbo.EmployeePromotionInfo WHERE	EmployeePromotionInfoId='" + hidAutoIdForUpdate.Value +"'";
            s_save_ = s_save_ + " Delete EmployeeOfficialInfoHistory where trAutoId='" + hidAutoIdForUpdate.Value + "' and trType='p' select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;

                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_PFEnable = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();
            s_PFEnable = "N";
            
            if (btn_save.Text == "Update")
            {                
                s_Update = "[ProcEmployeePromotionINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddproject.SelectedValue
                        + "','"
                        + ddbranch.SelectedValue
                        + "','"
                        + ddlocation.SelectedValue
                        + "','"
                        + dddepartment.SelectedValue
                        + "','"
                        + dddesignation.SelectedValue
                        + "','"
                        + ddemployeegroup.SelectedValue
                        + "','"
                        + ddemployeetype.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        
                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcEmployeePromotionINSERT]"
                        + "'0','"
                        + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddproject.SelectedValue
                        + "','"
                        + ddbranch.SelectedValue
                        + "','"
                        + ddlocation.SelectedValue
                        + "','"
                        + dddepartment.SelectedValue
                        + "','"
                        + dddesignation.SelectedValue
                        + "','"
                        + ddemployeegroup.SelectedValue
                        + "','"
                        + ddemployeetype.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I'";


                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        
                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }


        protected void btnPreview_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Session[GlobalVariables.g_s_printopt] = "N";

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "Termination Letter");

                if (chkBangla.Checked == true)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptYearlyIncrementLetterEnglish.rpt";
                }
                else
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptYearlyIncrementLetter.rpt";
                }
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcSalaryYearlyIncrementLetter"
                                                              + "'"
                                                              + SessionCompanyId
                                                              + "','"
                                                              + hidEmpAutoId.Value
                                                              + "','"
                                                              + SessionUserLevel
                                                              + "'";

                //if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtDOB.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridIssueDate")).Text;
                txtPermanentAddress.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridNote")).Text;
                dddesignation.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridDesignationAutoId")).Value;
                dddepartment.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridDepartmentAutoId")).Value;
                ddlocation.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridLocationAutoId")).Value;
                ddbranch.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridBranchAutoId")).Value;
                ddproject.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridProjectAutoId")).Value;
                ddemployeegroup.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridEmployeeGroupAutoId")).Value;
                ddemployeetype.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridEmployeeTypeAutoId")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (ddproject.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Project Can Not Blank!";
                return false;
            }
            if (ddbranch.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Branch Can Not Blank!";
                return false;
            }
            if (dddepartment.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Department Can Not Blank!";
                return false;
            }
            if (dddesignation.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Designation Can Not Blank!";
                return false;
            }
            if (ddlocation.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Location Can Not Blank!";
                return false;
            }
            if (ddemployeetype.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "type Can Not Blank!";
                return false;
            }
            if (ddemployeegroup.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Group Can Not Blank!";
                return false;
            }
            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFinalPayment.Text = string.Empty;
            txtPermanentAddress.Text = string.Empty;
            ddReleaseType.SelectedValue = "0";
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }

            s_select = "SELECT * FROM EmployeeOfficialInfo Where EmpAutoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        ddOproject.SelectedValue = drow["ProjectAutoId"].ToString();
                        ddObranch.SelectedValue = drow["BranchAutoId"].ToString();
                        ddOlocation.SelectedValue = drow["LocationAutoId"].ToString();
                        ddOdepartment.SelectedValue = drow["DepartmentAutoId"].ToString();
                        ddOdesignation.SelectedValue = drow["DesignationAutoId"].ToString();
                        ddOemployeegroup.SelectedValue = drow["EmployeeGroupAutoId"].ToString();
                        ddOemployeetype.SelectedValue = drow["EmployeeTypeAutoId"].ToString();                        
                    }
                }
            }

            v_loadGridView_CostingHead();
        }


      //===========End===============         
    }
}