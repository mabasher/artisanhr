﻿<%@ Page Title="Personal Information" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmPersonalInformationNew.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmPersonalInformationNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Employee Information
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div>

                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-nav">
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active">
                                <a href="#generalInformation" aria-controls="one" role="tab" style="background-color: #28526b; color: white; font-weight: bold;" data-toggle="tab">General Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#academicInformation" aria-controls="two" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Academic Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#trainingInformation" aria-controls="three" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Training Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#workExperience" aria-controls="four" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Work Experience</a>
                            </li>
                            <li role="presentation">
                                <a href="#nomineeInformation" aria-controls="five" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Nominee Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#childInformation" aria-controls="six" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Child Info</a>
                            </li>
                            <li role="presentation">
                                <a href="#image" aria-controls="seven" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Image</a>
                            </li>
                            <li role="presentation">
                                <a href="#officialInformation" aria-controls="eight" role="tab" data-toggle="tab" style="background-color: #28526b; color: white; font-weight: bold;">Official Info</a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="generalInformation" style="background-color: #c8f7f8;">
                                <div class="row row-custom">
                                    <div class="col-md-5">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group " style="margin-top: 30px;">
                                                <asp:Label ID="Label34" runat="server" Text="Company Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddProject" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddProject_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtPIN" runat="server" Enabled="false" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                                <asp:Label ID="Label18" runat="server" Text="Card No" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtEmpName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="Father's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtFather" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label20" runat="server" Text="Mother's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtMother" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label21" runat="server" Text="Spouse Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtSpouse" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbl_issue_Date" runat="server" Text="Date of Birth :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox runat="server" ID="txtDOB" CssClass="input-sm date-picker" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="Birth Certificate :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtBirthCertificate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label22" runat="server" Text="Place of Birth :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtPlaceofBirth" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label9" runat="server" Text="Present Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtPresentAddress" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label16" runat="server" Text="Permanent Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtPermanentAddress" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="margin-top: 20px;">
                                            <div class="form-horizontal">
                                                <asp:Label ID="Label27" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="form-group center ">
                                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-horizontal">

                                            <div class="form-group">
                                                <asp:Label ID="Label8" runat="server" Text="Personal Contact :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtPersonalContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label6" runat="server" Text="Official Contact :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtOfficialContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="Emergency Contact :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtEmergencyContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label25" runat="server" Text="Contact Person :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtContactPerson" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Personal Email :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtPersonalEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label17" runat="server" Text="Official Email :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtOfficialEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label5" runat="server" Text="Gender :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddGender" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label10" runat="server" Text="Blood Group :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddBlood" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label11" runat="server" Text="Religion :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddReligion" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label12" runat="server" Text="Marital Status :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddMaritalStatus" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label13" runat="server" Text="Nationality :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ddNationallity" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label14" runat="server" Text="NID :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtNationalId" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label15" runat="server" Text="Passport No :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtPassport" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label24" runat="server" Text="Driving License :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtDrivingLicense" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label23" runat="server" Text="TIN :" CssClass="col-sm-5 control-label"></asp:Label>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="txtTIN" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-custom" style="margin-top: 10px;">
                                    <div class="col-sm-12">
                                        <div class="form-horizontal">
                                            <div class="form-group center">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" id="PInfo" runat="server">
                                    <div class="form-horizontal">
                                        <div class="main-data-grid">
                                            <div style="height: 500px; overflow: scroll;">
                                                <asp:TextBox ID="txtEmpSearch" runat="server" Width="100%" placeholder="Search here... "
                                                    onfocus="this.style.backgroundColor='pink'" onblur="this.style.backgroundColor='white'"
                                                    OnTextChanged="txtEmpSearch_TextChanged">
                                                </asp:TextBox>
                                                <asp:HiddenField ID="hfAutoIdId" runat="server" />

                                                <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                                    CssClass="table table-striped table-bordered" OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                                                    EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">

                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                        </asp:CommandField>

                                                        <asp:TemplateField HeaderText="Emp Id">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridSL" runat="server" Text='<%# Bind("Party") %>'></asp:Label>
                                                                <asp:HiddenField ID="hid_GridMasterId" Value='<%# Bind("AutoId") %>' runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Employee Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("SLNO") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Father Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridFatherName" runat="server" Text='<%# Bind("FatherName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Mother Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridMotherName" runat="server" Text='<%# Bind("MotherName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Present Address">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridPresentAddress" runat="server" Text='<%# Bind("PresentAddress") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkgrdviewCV" Target="_blank" runat="server" NavigateUrl='<%# "~/RptViewer/HR/ViewEmployeeCV.aspx?EmployeePersonalInfoId=" + Eval("AutoId") %>'><i class="fa fa-user fa-3x" ></i></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                    <PagerStyle CssClass="pagination-sa" />
                                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                                </asp:GridView>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="academicInformation" style="background-color: #c8f7f8;">
                                Academic Information
                         <div class="row row-custom" id="AcademicCollapsable">
                             <div class="col-lg-12">
                                 <div class="col-md-4">
                                     <div class="form-horizontal">

                                         <div class="form-group">
                                             <asp:Label ID="Label33" runat="server" Text="Exam Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                             <div class="col-sm-8">
                                                 <asp:DropDownList ID="ddExamName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label35" runat="server" Text="Institute :" CssClass="col-sm-4 control-label"></asp:Label>
                                             <div class="col-sm-8">
                                                 <asp:TextBox ID="txtInstitute" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label36" runat="server" Text="Board/University :" CssClass="col-sm-4   control-label"></asp:Label>
                                             <div class="col-sm-8">
                                                 <asp:TextBox ID="txtBoardUniversity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label37" runat="server" Text="Major Group :" CssClass="col-sm-4 control-label"></asp:Label>
                                             <div class="col-sm-8">
                                                 <asp:TextBox ID="txtMajorGroup" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>

                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="form-horizontal">
                                         <div class="form-group">
                                             <asp:Label ID="Label38" runat="server" Text="Result :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-4">
                                                 <asp:TextBox ID="txtResult" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label39" runat="server" Text="CGPA/Marks% :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-4">
                                                 <asp:TextBox ID="txtCGPAMarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label40" runat="server" Text="Scale/Out Off :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-4">
                                                 <asp:TextBox ID="txtScaleOutOff" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label41" runat="server" Text="Year Of Passing :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-4">
                                                 <asp:TextBox ID="txtYearOfPassing" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="form-horizontal">
                                         <div class="form-group">
                                             <asp:Label ID="Label42" runat="server" Text="Duration :" CssClass="col-sm-4 control-label"></asp:Label>
                                             <div class="col-sm-7">
                                                 <asp:TextBox ID="txtDuration" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label43" runat="server" Text="Achievement :" CssClass="col-sm-4 control-label"></asp:Label>
                                             <div class="col-sm-7">
                                                 <asp:TextBox ID="txtAchievement" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label44" runat="server" Text="Remarks :" CssClass="col-sm-4 control-label"></asp:Label>
                                             <div class="col-sm-7">
                                                 <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="row row-custom" style="padding-top: 20px;">
                                     <div class="col-sm-12">
                                         <div class="form-horizontal">
                                             <div class="form-group center">
                                                 <asp:Label ID="lblMsgAcademic" runat="server"></asp:Label>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="col-sm-12">
                                         <div class="form-horizontal">
                                             <div class="form-group center">
                                                 <asp:Button ID="btnAcademicSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnAcademicSave_Click" Width="80px" />
                                                 <asp:Button ID="btnAcademicRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                                     CssClass="btn btn-default btn-sm" Width="80px" OnClick="btnAcademicRefresh_Click" />

                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-12">
                                     <div style="height: 150px; overflow: scroll;">
                                         <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                             AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                             CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                             <Columns>
                                                 <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                     <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                     <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                 </asp:CommandField>

                                                 <asp:TemplateField HeaderText="Exam Name">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridExamName" runat="server" Text='<%# Bind("ExamName") %>'></asp:Label>
                                                         <asp:HiddenField ID="hidExamNameId" Value='<%# Bind("ExamNameId") %>' runat="server" />
                                                     </ItemTemplate>
                                                 </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Institute Name">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridInstituteName" runat="server" Text='<%# Bind("InstituteName") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Board University Name">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridBoardUniversityName" runat="server" Text='<%# Bind("BoardUniversityName") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Major Group">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridMajorGroupName" runat="server" Text='<%# Bind("MajorGroupName") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Result">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridResult" runat="server" Text='<%# Bind("Result") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="CGPA/Marks">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridCGPAMarks" runat="server" Text='<%# Bind("CGPAMarks") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Scale/Out Off">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridScaleOutOff" runat="server" Text='<%# Bind("ScaleOutOff") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Year Of Passing">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridYearOfPassing" runat="server" Text='<%# Bind("YearOfPassing") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Duration">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Achievement">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridAchievement" runat="server" Text='<%# Bind("Achievement") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Remarks">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                                         <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />

                                                         <asp:HiddenField ID="hidEmpAcademicInfoId" Value='<%# Bind("EmpAcademicInfoId") %>' runat="server" />

                                                         <asp:HiddenField ID="hidInstituteId" Value='<%# Bind("InstituteId") %>' runat="server" />
                                                         <asp:HiddenField ID="hidBoardUniversityId" Value='<%# Bind("BoardUniversityId") %>' runat="server" />
                                                         <asp:HiddenField ID="hidMajorGroupId" Value='<%# Bind("MajorGroupId") %>' runat="server" />
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                             </Columns>
                                             <PagerStyle CssClass="pagination-sa" />
                                             <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                             <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                             <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                         </asp:GridView>
                                     </div>
                                 </div>
                             </div>
                         </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="trainingInformation" style="background-color: #c8f7f8;">
                                <div class="row row-custom" id="TrainingCollapsable">
                                    <div class="col-lg-12">

                                        <div class="col-md-5">
                                            <div class="form-horizontal">

                                                <div class="form-group">
                                                    <asp:Label ID="Label67" runat="server" Text="Training Title :" CssClass="col-sm-4 control-label"></asp:Label>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="ddTrainingTitle" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label45" runat="server" Text="Topics Covered :" CssClass="col-sm-4 control-label"></asp:Label>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="txtTopicsCovered" TextMode="MultiLine" Height="40" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <div class="form-horizontal">
                                                <div class="form-group" style="margin-top: 10px;">
                                                    <asp:Label ID="Label46" runat="server" Text="Organized By :" CssClass="col-sm-4   control-label"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtOrganizedBy" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label47" runat="server" Text="From Date :" CssClass="col-sm-4 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <asp:Label ID="Label48" runat="server" Text="To Date :" CssClass="control-label"></asp:Label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label49" runat="server" Text="Remarks :" CssClass="col-sm-4 control-label"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtTrainingRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row row-custom" style="padding-top: 20px;">
                                            <div class="col-sm-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group center">
                                                        <asp:Label ID="lblMsgTraining" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group center">
                                                        <asp:Button ID="btnTrainingSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnTrainingSave_Click" Width="80px" />
                                                        <asp:Button ID="btnTrainingRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                                            CssClass="btn btn-default btn-sm" OnClick="btnTrainingRefresh_Click" Width="80px" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div style="height: 150px; overflow: scroll;">
                                                <asp:GridView ID="gdvTrainingInformation" runat="server" Style="width: 100%; margin-left: 0;"
                                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                                    CssClass="table table-striped table-bordered" OnRowDataBound="gdvTrainingInformation_RowDataBound" OnSelectedIndexChanged="gdvTrainingInformation_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                                    <Columns>
                                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                        </asp:CommandField>

                                                        <asp:TemplateField HeaderText="Training Title">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridTrainingTitle" runat="server" Text='<%# Bind("TrainingTitle") %>'></asp:Label>
                                                                <asp:HiddenField ID="hidTrainingTitleId" Value='<%# Bind("TrainingTitleId") %>' runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Topics Covered">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridTopicsCovered" runat="server" Text='<%# Bind("TopicsCovered") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Organized By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridOrganizedBy" runat="server" Text='<%# Bind("OrganizedBy") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="From Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridFromDate" runat="server" Text='<%# Bind("FromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="To Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridToDate" runat="server" Text='<%# Bind("ToDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Remarks">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                                                <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                                                <asp:HiddenField ID="hidTrainingInfoId" Value='<%# Bind("TrainingInfoId") %>' runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle CssClass="pagination-sa" />
                                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="workExperience" style="background-color: #c8f7f8;">
                                Work Experience
                         <div class="row row-custom">
                             <div class="col-lg-12">
                                 <div class="col-md-4">
                                     <div class="form-horizontal">

                                         <div class="form-group" style="margin-top: 10px;">
                                             <asp:Label ID="Label51" runat="server" Text="Company Name :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label52" runat="server" Text="Company Business :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtCompanyBusiness" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label53" runat="server" Text="Company Address :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtCompanyAddress" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label54" runat="server" Text="Designation :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>


                                     </div>
                                 </div>
                                 <div class="col-md-4" style="margin-top: 10px;">
                                     <div class="form-horizontal">
                                         <div class="form-group ">
                                             <asp:Label ID="Label55" runat="server" Text="Department :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>

                                         <div class="form-group">
                                             <asp:Label ID="Label56" runat="server" Text="Job Nature :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtJobNature" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label57" runat="server" Text="Job Responsibilities :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtJobResponsibilities" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label59" runat="server" Text="Last Salary :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtLastSalary" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>

                                     </div>
                                 </div>
                                 <div class="col-md-4" style="margin-top: 10px;">
                                     <div class="form-horizontal">
                                         <div class="form-group">
                                             <asp:Label ID="Label60" runat="server" Text="From Date :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-4">
                                                 <asp:TextBox runat="server" ID="txtWorkExperienceFromDate" CssClass="input-sm date-picker" />
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label61" runat="server" Text="To Date :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-4">
                                                 <asp:TextBox runat="server" ID="txtWorkExperienceToDate" CssClass="input-sm date-picker" />
                                             </div>
                                         </div>

                                         <div class="form-group">
                                             <asp:Label ID="Label62" runat="server" Text="Continuing :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtContinuing" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <asp:Label ID="Label63" runat="server" Text="Remarks :" CssClass="col-sm-5 control-label"></asp:Label>
                                             <div class="col-sm-6">
                                                 <asp:TextBox ID="txtWorkExperienceRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="row row-custom" style="padding-top: 20px;">
                                     <div class="col-sm-12">
                                         <div class="form-horizontal">
                                             <div class="form-group center">
                                                 <asp:Label ID="lblWorkExperience" runat="server" CssClass="text-danger"></asp:Label>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="col-sm-12">
                                         <div class="form-horizontal">
                                             <div class="form-group center">
                                                 <asp:Button ID="btnWorkExperienceSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnWorkExperienceSave_Click" Width="80px" />
                                                 <asp:Button ID="btnWorkExperienceRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                                     CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-12">
                                     <div style="height: 150px; overflow: scroll;">
                                         <asp:GridView ID="gdvWorkExperience" runat="server" Style="width: 100%; margin-left: 0;"
                                             AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                             CssClass="table table-striped table-bordered" OnRowDataBound="gdvWorkExperience_RowDataBound" OnSelectedIndexChanged="gdvWorkExperience_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                             <Columns>
                                                 <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                     <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                                     <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                 </asp:CommandField>

                                                 <asp:TemplateField HeaderText="Company Name">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridCompanyName" runat="server" Text='<%# Bind("CompanyName") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="CompanyBusiness">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridCompanyBusiness" runat="server" Text='<%# Bind("CompanyBusiness") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Company Address">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridCompanyAddress" runat="server" Text='<%# Bind("CompanyAddress") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Designation">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Department">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Job Nature">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridJobNature" runat="server" Text='<%# Bind("JobNature") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Job Responsibilities">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridjobResponsibilities" runat="server" Text='<%# Bind("jobResponsibilities") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Last Salary">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridlastSalary" runat="server" Text='<%# Bind("lastSalary") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="fromDate">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridfromDate" runat="server" Text='<%# Bind("fromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="To Date">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridtoDate" runat="server" Text='<%# Bind("toDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Continuing">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridcontinuing" runat="server" Text='<%# Bind("continuing") %>'></asp:Label>
                                                     </ItemTemplate>
                                                 </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Remark">
                                                     <ItemTemplate>
                                                         <asp:Label ID="lblGridremark" runat="server" Text='<%# Bind("remark") %>'></asp:Label>
                                                         <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                                     </ItemTemplate>
                                                 </asp:TemplateField>
                                             </Columns>
                                             <PagerStyle CssClass="pagination-sa" />
                                             <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                             <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                             <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                         </asp:GridView>
                                     </div>
                                 </div>
                             </div>
                         </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="nomineeInformation" style="background-color: #c8f7f8;">
                                Nominee Information
                         <div class="row row-custom">

                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                 <ContentTemplate>
                                     <div class="col-lg-12">

                                         <div class="col-md-4">
                                             <div class="form-horizontal">

                                                 <div class="form-group" style="margin-top: 10px;">
                                                     <asp:Label ID="Label64" runat="server" Text="Nominee Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-8">
                                                         <asp:TextBox ID="txtNomineeName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                                 <div class="form-group">
                                                     <asp:Label ID="Label65" runat="server" Text="Father's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-8">
                                                         <asp:TextBox ID="txtFatherNomineeName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                                 <div class="form-group">
                                                     <asp:Label ID="Label66" runat="server" Text="Mother's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-8">
                                                         <asp:TextBox ID="txtMotherNomineeName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                                 <div class="form-group">
                                                     <asp:Label ID="Label68" runat="server" Text="Date of Birth :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-4">
                                                         <asp:TextBox runat="server" ID="txtNomineeDOB" CssClass="input-sm date-picker" />
                                                     </div>
                                                 </div>

                                             </div>
                                         </div>
                                         <div class="col-md-4" style="margin-top: 10px;">
                                             <div class="form-horizontal">
                                                 <div class="form-group">
                                                     <asp:Label ID="Label69" runat="server" Text="Address:" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-8">
                                                         <asp:TextBox ID="txtNomineeAddress" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                                 <div class="form-group">
                                                     <asp:Label ID="Label70" runat="server" Text="Contact No :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-8">
                                                         <asp:TextBox ID="txtNomineeContactNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                                 <div class="form-group">
                                                     <asp:Label ID="Label71" runat="server" Text="E-mail Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-8">
                                                         <asp:TextBox ID="txtNomineeEMail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>

                                             </div>
                                         </div>
                                         <div class="col-md-4" style="margin-top: 10px;">
                                             <div class="form-horizontal">
                                                 <div class="form-group">
                                                     <asp:Label ID="Label72" runat="server" Text="National Id :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-7">
                                                         <asp:TextBox ID="txtNomineeNationalId" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                                 <div class="form-group">
                                                     <asp:Label ID="Label73" runat="server" Text="Relation :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-7">
                                                         <asp:DropDownList ID="ddNomineeRelation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                     </div>
                                                 </div>

                                                 <div class="form-group">
                                                     <asp:Label ID="Label74" runat="server" Text="Share (%) :" CssClass="col-sm-4 control-label"></asp:Label>
                                                     <div class="col-sm-7">
                                                         <asp:TextBox ID="txtNomineeShare" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>

                                         <div class="row row-custom" style="padding-top: 20px;">
                                             <div class="col-sm-12">
                                                 <div class="form-horizontal">
                                                     <div class="form-group center">
                                                         <asp:Label ID="lblNomineeMsg" runat="server" CssClass="text-danger"></asp:Label>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="col-sm-12">
                                                 <div class="form-horizontal">
                                                     <div class="form-group center">
                                                         <asp:Button ID="btnNomineeSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnNomineeSave_Click" Width="80px" />
                                                         <asp:Button ID="btnNomineeRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                                             CssClass="btn btn-default btn-sm" OnClick="btnNomineeRefresh_Click" Width="80px" />
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <div style="height: 150px; overflow: scroll;">
                                                 <asp:GridView ID="gdvNomineeInfo" runat="server" Style="width: 100%; margin-left: 0;"
                                                     AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                                     CssClass="table table-striped table-bordered" OnRowDataBound="gdvNomineeInfo_RowDataBound" OnSelectedIndexChanged="gdvNomineeInfo_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                                     <Columns>
                                                         <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                             <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                                             <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                         </asp:CommandField>

                                                         <asp:TemplateField HeaderText="Nominee Name">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("NomineeName") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>

                                                         <asp:TemplateField HeaderText="Father Name">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridFather" runat="server" Text='<%# Bind("FatherName") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Mother Name">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridMother" runat="server" Text='<%# Bind("MotherName") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Contact No">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridContact" runat="server" Text='<%# Bind("PersonalContact") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Email Address">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridMail" runat="server" Text='<%# Bind("PersonalMail") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="NID">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridNid" runat="server" Text='<%# Bind("NID") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="DOB">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridDOB" runat="server" Text='<%# Bind("DOB","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Relation">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridRelation" runat="server" Text='<%# Bind("Relation") %>'></asp:Label>
                                                             </ItemTemplate>
                                                         </asp:TemplateField>

                                                         <asp:TemplateField HeaderText="Share(%)">
                                                             <ItemTemplate>
                                                                 <asp:Label ID="lblGridShare" runat="server" Text='<%# Bind("Share") %>'></asp:Label>
                                                                 <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                                                 <asp:HiddenField ID="hidRelationId" Value='<%# Bind("RelationId") %>' runat="server" />
                                                                 <asp:HiddenField ID="hidGridAddress" Value='<%# Bind("PermanentAddress") %>' runat="server" />
                                                             </ItemTemplate>
                                                         </asp:TemplateField>
                                                     </Columns>
                                                     <PagerStyle CssClass="pagination-sa" />
                                                     <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                                     <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                     <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                                 </asp:GridView>
                                             </div>
                                         </div>
                                     </div>
                                 </ContentTemplate>
                             </asp:UpdatePanel>
                         </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="childInformation" style="background-color: #c8f7f8;">
                                Child Information
                          <div class="row row-custom">
                              <div class="col-lg-12">

                                  <div class="col-md-4">
                                      <div class="form-horizontal">

                                          <div class="form-group" style="margin-top: 10px;">
                                              <asp:Label ID="Label29" runat="server" Text="Child Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <asp:Label ID="Label30" runat="server" Text="Father's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildFatherName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <asp:Label ID="Label31" runat="server" Text="Mother's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildMotherName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>

                                      </div>
                                  </div>
                                  <div class="col-md-4" style="margin-top: 10px;">
                                      <div class="form-horizontal">

                                          <div class="form-group">
                                              <asp:Label ID="Label77" runat="server" Text="Date of Birth :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-4">
                                                  <asp:TextBox runat="server" ID="txtChildDOB" CssClass="input-sm date-picker" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <asp:Label ID="Label85" runat="server" Text="Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildAddress" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <asp:Label ID="Label86" runat="server" Text="Contact No :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="col-md-4" style="margin-top: 10px;">
                                      <div class="form-horizontal">
                                          <div class="form-group">
                                              <asp:Label ID="Label87" runat="server" Text="E-mail Address :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <asp:Label ID="Label88" runat="server" Text="NID/ Birth Reg. :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:TextBox ID="txtChildNID" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                              </div>
                                          </div>
                                          <div class="form-group ">
                                              <asp:Label ID="Label89" runat="server" Text="Relation :" CssClass="col-sm-4 control-label"></asp:Label>
                                              <div class="col-sm-7">
                                                  <asp:DropDownList ID="ddChildRelation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                              </div>
                                          </div>


                                      </div>
                                  </div>

                                  <div class="row row-custom" style="padding-top: 20px;">
                                      <div class="col-sm-12">
                                          <div class="form-horizontal">
                                              <div class="form-group center">
                                                  <asp:Label ID="lblChildMsg" runat="server" CssClass="text-danger"></asp:Label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="col-sm-12">
                                          <div class="form-horizontal">
                                              <div class="form-group center">
                                                  <asp:Button ID="btnChildInfoSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnChildInfoSave_Click" Width="80px" />
                                                  <asp:Button ID="btnChildInfoRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                                      CssClass="btn btn-default btn-sm" OnClick="btnChildInfoRefresh_Click" Width="80px" />
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                      <div style="height: 150px; overflow: scroll;">
                                          <asp:GridView ID="gdvChildInfo" runat="server" Style="width: 100%; margin-left: 0;"
                                              AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                              CssClass="table table-striped table-bordered" OnRowDataBound="gdvChildInfo_RowDataBound" OnSelectedIndexChanged="gdvChildInfo_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                              <Columns>
                                                  <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                      <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                                      <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                  </asp:CommandField>

                                                  <asp:TemplateField HeaderText="Child Name">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("ChildName") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>

                                                  <asp:TemplateField HeaderText="Father Name">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridFather" runat="server" Text='<%# Bind("FatherName") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Mother Name">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridMother" runat="server" Text='<%# Bind("MotherName") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Contact No">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridContact" runat="server" Text='<%# Bind("PersonalContact") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Email Address">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridMail" runat="server" Text='<%# Bind("PersonalMail") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="NID">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridNid" runat="server" Text='<%# Bind("NID") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="DOB">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridDOB" runat="server" Text='<%# Bind("DOB","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Address">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridPermanentAddress" runat="server" Text='<%# Bind("PermanentAddress") %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>

                                                  <asp:TemplateField HeaderText="Relation">
                                                      <ItemTemplate>
                                                          <asp:Label ID="lblGridRelation" runat="server" Text='<%# Bind("Relation") %>'></asp:Label>
                                                          <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                                          <asp:HiddenField ID="hidRelationId" Value='<%# Bind("RelationId") %>' runat="server" />
                                                          <asp:HiddenField ID="hidGridAddress" Value='<%# Bind("PermanentAddress") %>' runat="server" />
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                              </Columns>
                                              <PagerStyle CssClass="pagination-sa" />
                                              <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                              <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                              <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                          </asp:GridView>
                                      </div>
                                  </div>
                              </div>
                          </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="image" style="background-color: #c8f7f8;">
                                Image Information                        
                         <div class="row row-custom">
                             <div class="col-md-12">
                                 <div class="form-horizontal">
                                     <div class="col-md-6">
                                         <div class="form-group" style="text-align: left;">
                                             <asp:Image ID="ImgPic" runat="server" Height="120" BorderWidth="1" Width="120" />
                                         </div>
                                         <div class="form-group">
                                             <div class="col-sm-12">
                                                 <asp:FileUpload ID="imgUpload" runat="server" />
                                                 <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Picture Image size <100KB)"></asp:Label>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="col-md-6">
                                         <div class="form-group" style="text-align: left;">
                                             <asp:Image ID="ImgSig" runat="server" Height="120" BorderWidth="1" Width="120" />
                                         </div>
                                         <div class="form-group">
                                             <div class="col-sm-12">
                                                 <asp:FileUpload ID="imgSigUpload" runat="server" />
                                                 <asp:Label ID="Label28" ForeColor="#F22613" runat="server" Text="(Signature Image size <100 KB)"></asp:Label>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                             </div>
                             <div class="col-sm-8">
                                 <div class="form-horizontal">
                                     <div class="form-group center">
                                         <asp:Button ID="btnImageSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnImageSave_Click" Width="80px" />
                                         <asp:Button ID="btnImageRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                             CssClass="btn btn-default btn-sm" OnClick="btnChildInfoRefresh_Click" Width="80px" />
                                     </div>
                                 </div>
                             </div>

                         </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="officialInformation" style="background-color: #c8f7f8;">
                                Official Information
                        <div class="row row-custom">
                            <div class="col-lg-12">
                                <div class="col-md-5">
                                    <div class="form-horizontal">


                                        <div class="form-group">
                                            <asp:Label ID="Label75" runat="server" Text="Department :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label76" runat="server" Text="Designation :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <%--CssClass="form-control input-sm chosen-select"--%>
                                                <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label78" runat="server" Text="Employee Grade :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddEmployeeGroup" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label79" runat="server" Text="Employee Type :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddEmployeeType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label80" runat="server" Text="Section :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label81" runat="server" Text="Floor :" CssClass="col-sm-5 control-label "></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddFloor" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label82" runat="server" Text="Block :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddBlock" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label83" runat="server" Text="Line No :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row row-custom" style="padding-top: 20px;">
                                            <div class="col-sm-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group center">
                                                        <asp:Label ID="Label90" runat="server" CssClass="text-danger"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-horizontal">
                                                    <div class="form-group center">
                                                        <asp:Button ID="btnOfficialSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnOfficialSave_Click" Width="80px" />
                                                        <asp:Button ID="btnOfficialRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <asp:Label ID="Label84" runat="server" Text="Supervisor :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddSupervisor" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label91" runat="server" Text="Date of Joining :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:TextBox runat="server" ID="txtJoinDate" CssClass="input-sm date-picker" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label92" runat="server" Text="Probation Period:" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txtProbationPeriod" runat="server" Text="6" CssClass="form-control input-sm" FilterType="Numbers,Custom" AutoPostBack="True" OnTextChanged="txtProbationPeriod_TextChanged"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,Custom"
                                                    ValidChars="." TargetControlID="txtProbationPeriod" />
                                            </div>
                                            <asp:Label ID="Label93" runat="server" Text="Month" CssClass="control-label"></asp:Label>

                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label94" runat="server" Text="Date of Confirmation :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:TextBox runat="server" ID="txtDOConFirmation" CssClass="input-sm date-picker" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label95" runat="server" Text="PF Enable (Y/N) :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:CheckBox ID="chkPFEnable" runat="server" />
                                                <asp:TextBox runat="server" ID="txtDOPF" CssClass="input-sm date-picker hidden" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label96" runat="server" Text="Shift :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddShift" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label97" runat="server" Text="Job Location :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddJobLocation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label98" runat="server" Text="Company Name :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddBranch" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label99" runat="server" Text="Project :" CssClass="col-sm-5 control-label"></asp:Label>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddProjectName" CssClass="form-control input-sm chosen-select " data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row row-custom" style="margin-top: 10px;">
                                    <div class="col-sm-12">
                                        <div class="form-horizontal">
                                            <div class="form-group center">
                                                <asp:Label ID="Label107" runat="server" CssClass="text-danger"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>


                        </div>
                    </div>
                </div>

                <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
                <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
                <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
                <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
                <asp:HiddenField ID="hidURL" runat="server" Value="" />
                <asp:HiddenField ID="hidSigURL" runat="server" Value="" />
                <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
                <asp:HiddenField ID="hidAutoIdOfficial" runat="server" Value="0" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <style>
        .chosen-container.chosen-container-single {
            width: 100% !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

    </script>
    <style type="text/css">
        legend {
            width: 100%;
            border: 1px solid black;
            padding: 3px 6px;
            cursor: pointer;
            display: inline-block;
        }

            legend::after {
                content: "▼";
                float: right;
            }
    </style>

</asp:Content>
