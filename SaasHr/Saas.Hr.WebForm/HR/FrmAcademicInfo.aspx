﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmAcademicInfo.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmAcademicInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Academic Information
    </div>
<asp:UpdatePanel ID="UpdatePanel1" runat="server"></asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-5">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-3 hidden">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>   

                            <div class="form-group hidden" style="margin-top: 20px; ">
                                <asp:Label ID="Label18" runat="server" Text="Exam Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtExamName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group" style="margin-top: 20px;">
                                <asp:Label ID="Label19" runat="server" Text="Exam Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:DropDownList ID="ddExamName" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label20" runat="server" Text="Institute :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtInstitute" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label21" runat="server" Text="Board/University :" CssClass="col-sm-4   control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtBoardUniversity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Major Group :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtMajorGroup" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Result :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txtResult" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label14" runat="server" Text="CGPA/Marks% :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtCGPAMarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="margin-top: 100px;">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Scale/Out Off :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtScaleOutOff" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label23" runat="server" Text="Year Of Passing :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtYearOfPassing" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label17" runat="server" Text="Duration :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtDuration" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text="Achievement :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAchievement" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="Remarks :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 hidden">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <asp:Label ID="Label11" runat="server" Text="Religion :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddReligion" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text="Marital Status :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddMaritalStatus" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text="Nationality :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddNationallity" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label15" runat="server" Text="Passport No :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtPassport" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label24" runat="server" Text="Driving License :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtDrivingLicense" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Official Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtOfficialContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Emergency Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtEmergencyContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label25" runat="server" Text="Contact Person :" CssClass="col-sm-4 control-label"></asp:Label>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtContactPerson" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label26" runat="server" Text="Birth Certificate :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtBirthCertificate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label22" runat="server" Text="Place of Birth :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPlaceofBirth" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label9" runat="server" Text="Present Address:" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPresentAddress" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>


                            <div class="form-group" style="text-align: center;">
                                <asp:Image ID="ImgPic" runat="server" Height="120" BorderWidth="1" Width="120" />
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:FileUpload ID="imgUpload" runat="server" />
                                    <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Picture Image size <100KB)"></asp:Label>
                                </div>
                            </div>

                            <div class="form-group" style="text-align: center;">
                                <asp:Image ID="ImgSig" runat="server" Height="120" BorderWidth="1" Width="120" />
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:FileUpload ID="imgSigUpload" runat="server" />
                                    <asp:Label ID="Label28" ForeColor="#F22613" runat="server" Text="(Signature Image size <100 KB)"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div style="height: 150px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="Exam Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridExamName" runat="server" Text='<%# Bind("ExamName") %>'></asp:Label>
                                            <asp:HiddenField ID="hidExamNameId" Value='<%# Bind("ExamNameId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Institute Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInstituteName" runat="server" Text='<%# Bind("InstituteName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Board University Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridBoardUniversityName" runat="server" Text='<%# Bind("BoardUniversityName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Major Group">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMajorGroupName" runat="server" Text='<%# Bind("MajorGroupName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Result">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridResult" runat="server" Text='<%# Bind("Result") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CGPA/Marks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCGPAMarks" runat="server" Text='<%# Bind("CGPAMarks") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Scale/Out Off">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridScaleOutOff" runat="server" Text='<%# Bind("ScaleOutOff") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Year Of Passing">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridYearOfPassing" runat="server" Text='<%# Bind("YearOfPassing") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Duration">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Achievement">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAchievement" runat="server" Text='<%# Bind("Achievement") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                            <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                            
                                            <asp:HiddenField ID="hidEmpAcademicInfoId" Value='<%# Bind("EmpAcademicInfoId") %>' runat="server" />
                                            
                                            <asp:HiddenField ID="hidInstituteId" Value='<%# Bind("InstituteId") %>' runat="server" />
                                            <asp:HiddenField ID="hidBoardUniversityId" Value='<%# Bind("BoardUniversityId") %>' runat="server" />
                                            <asp:HiddenField ID="hidMajorGroupId" Value='<%# Bind("MajorGroupId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAcademicInfoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidExamNameId" runat="server" Value="0" />
        <asp:HiddenField ID="hidInstituteId" runat="server" Value="0" />
        <asp:HiddenField ID="hidBoardUniversityId" runat="server" Value="0" />
        <asp:HiddenField ID="hidMajorGroupId" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
        /////////ExamName///////////
        $(function () {
            makeAutoComplete('#<%=txtExamName.ClientID %>',
                '<%=ResolveUrl("~/Services/GetExamName.asmx/GetExam") %>',
                '#<%=hidExamNameId.ClientID %>');
        });
        /////////InstituteName///////////
        $(function () {
            makeAutoComplete('#<%=txtInstitute.ClientID %>',
                '<%=ResolveUrl("~/Services/InstituteName.asmx/GetInstituteName") %>',
                '#<%=hidInstituteId.ClientID %>');
        });
        /////////BoardUniversity///////////
        $(function () {
            makeAutoComplete('#<%=txtBoardUniversity.ClientID %>',
                '<%=ResolveUrl("~/Services/BoardUniversity.asmx/GetBoardUniversity") %>',
                '#<%=hidBoardUniversityId.ClientID %>');
        });
        /////////MajorGroup///////////
        $(function () {
            makeAutoComplete('#<%=txtMajorGroup.ClientID %>',
                '<%=ResolveUrl("~/Services/MajorGroup.asmx/GetMajorGroup") %>',
                '#<%=hidMajorGroupId.ClientID %>');
        });


    </script>

</asp:Content>
