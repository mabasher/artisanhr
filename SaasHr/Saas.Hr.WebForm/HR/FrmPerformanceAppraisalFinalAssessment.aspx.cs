﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmPerformanceAppraisalFinalAssessment : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                int cy = DateTime.Today.Year - 10;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                ddYear.DataSource = years;
                ddYear.DataBind();

            
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
                txtToTime.Text = DateTime.Now.ToString("hh:mm:ss tt");

                //commonfunctions.g_b_FillDropDownList(ddEmployeeType,
                //    "EmployeeType",
                //    "TypeName",
                //    "EmployeeTypeId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddDepartment,
                //    "DepartmentInfo",
                //    "Department",
                //    "DepartmentInfoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddSection,
                //    "SectionInfo",
                //    "Section",
                //    "SectionInfoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddLine,
                //    "LineInfo",
                //    "Line",
                //    "LineInfoId", string.Empty);

                v_loadGridView_gdv_Attendence();
            }
        }

        //private void v_loadGridView_CostingHead()
        //{
        //    if (hidCardid.Value == "0") { hidCardid.Value = ""; }
        //    hidDepartmentId.Value = "%";
        //    hidSectionId.Value = "%";
        //    hidLineId.Value = "%";
        //    if (ddDepartment.SelectedValue != "0") { hidDepartmentId.Value = ddDepartment.SelectedValue + "X%"; }
        //    if (ddSection.SelectedValue != "0") { hidSectionId.Value = ddSection.SelectedValue + "X%"; }
        //    if (ddLine.SelectedValue != "0") { hidLineId.Value = ddLine.SelectedValue + "X%"; }

        //    CommonFunctions commonFunctions = new CommonFunctions();
        //    SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
        //    sqlconnection.Open();
        //    string qry = string.Empty;
        //    qry = "ProcDailyAttandenceSelect '" + hidDepartmentId.Value + "','" + hidSectionId.Value + "','" + hidLineId.Value + "','" + hidCardid.Value + "%','" + Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(txtSearchToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";
        //    SqlCommand cmd = new SqlCommand(qry, sqlconnection);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);
        //    gdv_costingHead.DataSource = ds;
        //    gdv_costingHead.DataBind();
        //    sqlconnection.Close();
        //}

        private void v_loadGridView_gdv_Attendence()
        {
            if (hidCardid.Value == "0") { hidCardid.Value = ""; }

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            //qry = "ProcSelectPerformanceAppraisal'" + hidCardid.Value + "','2020'";
            qry = "[dbo].[ProcSelectPerformanceAppraisal]";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_Attendence.DataSource = ds;
            gdv_Attendence.DataBind();
            sqlconnection.Close();

            //int count = ds.Tables.Count;  //it will give the total number of tables in your dataset.
            //count = ds.Tables[0].Rows.Count;
            //lblCounter.Text = "Row No : "+count.ToString();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcDailyAttandenceINSERT11]"
                        + "'"
                        + hidEmpAutoId.Value
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
            else
            {
                foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
                {
                    string s_empAutoId = string.Empty;
                    string s_CardId = string.Empty;
                    string s_fromDate = string.Empty;
                    string s_InTime = string.Empty;
                    string s_OutTime = string.Empty;
                    string s_InOutStatus = string.Empty;

                    s_InOutStatus = "A";
                    //   s_empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hidempAutoId")).Value;
                    s_CardId = ((TextBox)gdvRow.Cells[0].FindControl("txtcid")).Text;
                    s_fromDate = ((TextBox)gdvRow.Cells[0].FindControl("txtDate")).Text;
                    s_InTime = ((TextBox)gdvRow.Cells[0].FindControl("txtInTime")).Text;
                    s_OutTime = ((TextBox)gdvRow.Cells[0].FindControl("txtOutTime")).Text;

                    if (s_InTime!="" && s_OutTime !="")
                    {
                        s_InOutStatus = "A";
                    }
                    if (s_InTime == "" && s_OutTime != "")
                    {
                        s_InOutStatus = "O";
                    }
                    if (s_InTime != "" && s_OutTime == "")
                    {
                        s_InOutStatus = "I";
                    }

                    CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                    if (checkbox.Checked == true)
                    {
                      s_save_ = s_save_ + "Execute ProcDailyAttandenceINSERT "
                                    + "'"
                                    + s_CardId//txtCard.Text.Trim()
                                    + "','"
                                    + s_CardId//txtCard.Text.Trim()
                                    + "','"
                                    + s_CardId//txtCard.Text.Trim()
                                    + "','"
                                    + s_CardId//txtCard.Text.Trim()
                                    + "','"
                                    + Convert.ToDateTime(s_fromDate.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                    + "','"
                                    + HttpUtility.HtmlDecode(s_InTime.Trim())
                                    + "','"
                                    + HttpUtility.HtmlDecode(s_OutTime.Trim())
                                    + "','"
                                    + s_InOutStatus
                                    + "'";
                    }
                }
                //Insert Option
                s_save_ = s_save_ + "  Select ''";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
                        
            //foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            //    {
            //        string s_Card = string.Empty;
            //        string s_fromDate = string.Empty;

            //    s_Card = ((Label)gdvRow.Cells[0].FindControl("lblGridCardNo")).Text;
            //    s_fromDate = ((Label)gdvRow.Cells[0].FindControl("lblGridDOB")).Text;

            //        CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
            //        if (checkbox.Checked == true)
            //        {
            //            s_save_ = s_save_ + "Execute ProcDailyAttandenceDELETE "
            //                          + "'"
            //                          + s_Card
            //                          + "','"
            //                          + Convert.ToDateTime(s_fromDate.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
            //                          + "'";
            //        }
            //    }
                //Insert Option
                s_save_ = s_save_ + "  Select ''";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }


        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {

            //if (txtPIN.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "PIN Can Not Blank!";
            //    return false;
            //}
            //if (txtEmpName.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Employee Name Can Not Blank!";
            //    return false;
            //}

            string s_t = string.Empty;
            s_t = "N";
            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string s_CardId = string.Empty;
                s_CardId = ((TextBox)gdvRow.Cells[0].FindControl("txtcid")).Text;

                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true && s_CardId != "")
                {
                    s_t = "Y";
                }
            }

            if (s_t == "N")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee List Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFromDate.Text = "";
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        hidCardid.Value = drow["CardNo"].ToString();
                    }
                }
            }


        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_gdv_Attendence();
        }


        //===========End===============         
    }
}