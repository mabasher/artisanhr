﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmAppraisal.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmAppraisal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">

    <script type="text/javascript">
        function ValidateText(val) {
            var intValue = parseInt(val.value, 10);

            if (isNaN(intValue)) {
                alert("please enter only number");
            }
    </script>
    <style>
        .GridHeader {
            text-align: center !important;
        }
    </style>
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Employee Monthly Performance Appraisal 
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <style>
            .date-picker {
                width: 100% !important;
            }
        </style>
        <div class="row row-custom">
            <div class="col-lg-12">
                <div class="row row-custom">
                    <div class="col-lg-12">
                        <fieldset>
                            <div class="form-horizontal">

                                <div class="col-md-12">

                                    <div class="col-sm-12 hidden">
                                        <div class="form-horizontal">
                                            <div class="form-group center" style="background-color: #EEE8AA;">
                                                <asp:Label ID="Label6" runat="server" Text="PK=Professional knowledge." CssClass="control-label"></asp:Label>

                                                <asp:Label ID="Label7" runat="server" Text="QA=Quality and accuracy" CssClass="control-label"></asp:Label>

                                                <asp:Label ID="Label9" runat="server" Text="SD=Sincerity and discipline." CssClass="control-label"></asp:Label>

                                                <asp:Label ID="Label10" runat="server" Text="LB=Loyalty and Behavior." CssClass="control-label"></asp:Label>

                                                <asp:Label ID="Label11" runat="server" Text="PD=Present days" CssClass="control-label"></asp:Label>

                                                <asp:Label ID="Label12" runat="server" Text="LD=Late days." CssClass="control-label"></asp:Label>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group hidden">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group hidden ">
                                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2 ">
                                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label17" runat="server" Text="Effective Date :" CssClass="col-sm-2  control-label"></asp:Label>
                                        <div class="col-sm-2">
                                            <asp:TextBox runat="server" ID="txtDate" CssClass="input-sm date-picker" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label4" runat="server" Text="Department :" CssClass="col-sm-2  control-label"></asp:Label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label16" runat="server" Text="Month :" CssClass="col-sm-12  control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddMonth" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label2" runat="server" Text="Year :" CssClass="col-sm-12  control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:DropDownList ID="ddYear" Visible="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1 ">
                                            <asp:CheckBox ID="chkUpdate" runat="server" Text=" View Data" CssClass="text-danger" />
                                        </div>
                                    </div>

                                    <div class="form-group hidden">
                                        <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4" style="text-align: left; color: red;">
                                            <asp:Label ID="lblCounter" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-xs" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                                CssClass="btn btn-default btn-xs" OnClick="btn_refresh_Click" Width="80px" />
                                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" Visible="false" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                                            <asp:Button ID="btnPreview" runat="server" CssClass="btn btn-primary btn-xs" Visible="false" Text="Preview" OnClick="btnPreview_Click" Width="80px" />
                                            <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Visible="false" Text=" View to PDF" CssClass="text-danger" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-horizontal">
                                            <div class="form-group center">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div>

                                        <asp:GridView ID="gdv_Attendence" runat="server" Style="width: 100%; margin-left: 0;"
                                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle GridHeader"
                                            EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                </asp:CommandField>

                                                <asp:TemplateField HeaderText="PIN" ItemStyle-Width="7%"  HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPIN" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("PIN") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                        <asp:HiddenField ID="hidGridEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name" ItemStyle-Width="15%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEmpName" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("EmpName") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Designation" ItemStyle-Width="10%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDesignation" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("designation") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Present <br/> Day" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPresentDay" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("PresentDay") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="(10)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPresentDayScore" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("PresentDayScore") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Late <br/> Day" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtLateDay" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("LateDay") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="(5)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtLateDayScore" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("LateDayScore") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Absent<br/>Day" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAbsentDay" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("AbsentDay") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="(5)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtAbsentDayScore" Enabled="false" runat="server" CssClass="form-control input-sm" onkeyup="ValidateText(this);"
                                                            Text='<% # Eval("AbsentDayScore") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Educa <br/> tion" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEducation" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("Education") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="(10)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEducationScore" Enabled="false" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("EducationScore") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Professional <br/> knowledge <br/> (10)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtProfessionalKnowledge" runat="server" CssClass="form-control input-sm" onkeyup="ValidateText(this);"
                                                            Text='<% # Eval("ProfessionalKnowledge") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Quality <br/> and <br/> accuracy <br/> (10)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQualituAccuracy" runat="server" CssClass="form-control input-sm" onkeyup="ValidateText(this);"
                                                            Text='<% # Eval("QualityAccuracy") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Sincerity <br/> and <br/> discipline <br/> (10)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSincerityDiscipline" runat="server" CssClass="form-control input-sm" onkeyup="ValidateText(this);"
                                                            Text='<% # Eval("SincerityDiscipline") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Perfor<br/>mance<br/>(10)" ItemStyle-Width="5%" HeaderStyle-CssClass="GridHeader">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtPerformance" runat="server" CssClass="form-control input-sm" onkeyup="ValidateText(this);"
                                                            Text='<% # Eval("Performance") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total" ItemStyle-Width="6%" HeaderStyle-CssClass="GridHeader" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTotal" Enabled="false" runat="server" CssClass="form-control input-sm" onkeyup="ValidateText(this);"
                                                            Text='<% # Eval("Total") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'">
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <PagerStyle CssClass="pagination-sa" />
                                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" Wrap="true" />
                                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>



                            </div>

                        </fieldset>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-lg-12 hidden">
            <div class="row row-custom">
                <div class="col-md-8">
                    <div class="row row-custom">
                        <div class="col-md-12">

                            <div class="form-horizontal">

                                <div class="form-group hidden">
                                    <asp:Label ID="Label31" runat="server" Text="Transaction Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label1" runat="server" Text="In Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label5" runat="server" Text="Out Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmployeeTypeId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
    <asp:HiddenField ID="hidForAll" runat="server" Value="" />

    <asp:HiddenField ID="hidQryA" runat="server" Value="" />
    <asp:HiddenField ID="hidQryB" runat="server" Value="" />
    <asp:HiddenField ID="hidQryC" runat="server" Value="" />
    <asp:HiddenField ID="hidQryD" runat="server" Value="" />
    <asp:HiddenField ID="hidQryE" runat="server" Value="" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
            $(function () {
                makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/GetAllEmp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
            });

            function checkAll(objRef) {
                var GridView = objRef.parentNode.parentNode.parentNode;
                var inputList = GridView.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    var row = inputList[i].parentNode.parentNode;
                    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                        if (objRef.checked) {
                            row.style.backgroundColor = "aqua";
                            inputList[i].checked = true;
                        }
                        else {
                            if (row.rowIndex % 2 == 0) {
                                row.style.backgroundColor = "#C2D69B";
                            }
                            else {
                                row.style.backgroundColor = "white";
                            }
                            inputList[i].checked = false;
                        }
                    }
                }
            }

            function MouseEvents(objRef, evt) {
                var checkbox = objRef.getElementsByTagName("input")[0];
                if (evt.type == "mouseover") {
                    objRef.style.backgroundColor = "orange";
                }
                else {
                    if (checkbox.checked) {
                        objRef.style.backgroundColor = "aqua";
                    }
                    else if (evt.type == "mouseout") {
                        if (objRef.rowIndex % 2 == 0) {
                            objRef.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            objRef.style.backgroundColor = "white";
                        }
                    }
                }
            }
    </script>
        
    <script type="text/javascript">

            $(function () {
                $("[id*=txt]").keyup(function () {
                    var rmValue = $(this).closest('tr').find('[id*=txtRMValue]').val();
                    var AddValue = $(this).closest('tr').find('[id*=txtATypeQty]').val();

                    $(this).closest('tr').find('[id*=txtBTypeQty]').val(
                        (parseFloat(AddValue) * 100)
                        / parseFloat(rmValue));

                    $(this).closest('tr').find('[id*=txtVAT]').val(
                        parseFloat(rmValue)
                        + parseFloat(AddValue));
                });

                $("[id*=txtBTypeQty]").keyup(function () {
                    var rmValue = $(this).closest('tr').find('[id*=txtRMValue]').val();
                    var AddRate = $(this).closest('tr').find('[id*=txtBTypeQty]').val();

                    $(this).closest('tr').find('[id*=txtATypeQty]').val(
                        (parseFloat(AddRate) * parseFloat(rmValue))
                        / 100);

                    var AddValue = $(this).closest('tr').find('[id*=txtATypeQty]').val();
                    $(this).closest('tr').find('[id*=txtVAT]').val(
                        parseFloat(rmValue)
                        + parseFloat(AddValue));
                });

                $("[id*=txtVAT]").keyup(function () {
                    var rmValue = $(this).closest('tr').find('[id*=txtRMValue]').val();
                    var aVat = $(this).closest('tr').find('[id*=txtVAT]').val();

                    $(this).closest('tr').find('[id*=txtATypeQty]').val(
                        (parseFloat(aVat) - parseFloat(rmValue)));

                    var AddValue = $(this).closest('tr').find('[id*=txtATypeQty]').val();
                    $(this).closest('tr').find('[id*=txtBTypeQty]').val(
                        (parseFloat(AddValue) * 100)
                        / parseFloat(rmValue));
                });

            })
    </script>


</asp:Content>
