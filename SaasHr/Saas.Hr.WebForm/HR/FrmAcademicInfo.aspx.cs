﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmAcademicInfo : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //tt
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(ddExamName,
                    "ExamNameInfo",
                    "ExamName",
                    "ExamNameId", string.Empty);

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
                CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();
            
                SqlCommand cmd = new SqlCommand("ProcEmpAcademicInformation'" + hidEmpAutoId.Value + "'", sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.DataBind();
                sqlconnection.Close();
            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                //var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                //if (File.Exists(filePath))
                //{
                //    File.Delete(filePath);
                //}

                //filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                //filename = txtPIN.Text + "_" + filename;
                //URL = "~/UserPic/" + filename;
                //filename = "";
                
                s_Update = "[ProcAcademicInfoINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddExamName.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtInstitute.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBoardUniversity.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMajorGroup.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtResult.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCGPAMarks.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtScaleOutOff.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtYearOfPassing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDuration.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAchievement.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";
                
                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                //filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                //filename = txtPIN.Text + "_" + filename;
                //URL = "~/UserPic/" + filename;
                //filename = "";

                s_save_ = "[ProcAcademicInfoINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddExamName.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(txtInstitute.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBoardUniversity.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMajorGroup.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtResult.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCGPAMarks.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtScaleOutOff.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtYearOfPassing.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDuration.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAchievement.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";                
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidEmpAcademicInfoId")).Value;

                ddExamName.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidExamNameId")).Value;
                txtInstitute.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridInstituteName")).Text;
                txtBoardUniversity.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridBoardUniversityName")).Text;
                txtMajorGroup.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridMajorGroupName")).Text;
                txtResult.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridResult")).Text;
                txtCGPAMarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCGPAMarks")).Text;
                txtScaleOutOff.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridScaleOutOff")).Text;
               
                txtYearOfPassing.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridYearOfPassing")).Text;
                txtDuration.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDuration")).Text;
                txtAchievement.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAchievement")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            
            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }

            if (ddExamName.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Exam Name Can Not Blank!";
                return false;
            }

            if (txtInstitute.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Institute Can Not Blank!";
                return false;
            }

            if (txtBoardUniversity.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Board University Can Not Blank!";
                return false;
            }

            if (txtMajorGroup.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Major Group Name Can Not Blank!";
                return false;
            }

            //if (imgUpload.HasFile)
            //{
            //    int imgSize = imgUpload.PostedFile.ContentLength;
            //    string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
            //    if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
            //    {
            //        lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //    else if (imgSize > 800000)//1048576)//1048576 bit=1MB
            //    {
            //        lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //}
            

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            ddExamName.SelectedValue = "0";
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtInstitute.Text = string.Empty;
            txtBoardUniversity.Text = string.Empty;
            txtMajorGroup.Text = string.Empty;
            txtResult.Text = string.Empty;
            txtCGPAMarks.Text = string.Empty;
            txtScaleOutOff.Text = string.Empty;
            txtYearOfPassing.Text = string.Empty;
            txtDuration.Text = string.Empty;
            txtAchievement.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }


      //===========End===============         
    }
}