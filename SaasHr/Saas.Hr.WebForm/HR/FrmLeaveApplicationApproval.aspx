﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmLeaveApplicationApproval.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmLeaveApplicationApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    Leave Application Approval
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-md-4">

            <asp:Label ID="lblMsg1" runat="server" ForeColor="Red"></asp:Label>
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="lblEmployeeName" runat="server" Text="Emp. Name:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtEmployeeName" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblEmployeePIN" runat="server" Text="Emp. PIN:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtEmployeePIN" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtLeaveType" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblFromDate" runat="server" Text="From Date:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtFromDate" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblToDate" runat="server" Text="To Date:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtToDate" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblIsPartialLeave" runat="server" Text="Partial Leave:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-1">
                        <asp:CheckBox ID="chkIsPartialLeave" runat="server" onclick="return false;" />
                    </div>
                </div>
                <div class="form-group hidden" id="dvFromTime" runat="server">
                    <asp:Label ID="lblFromTime" runat="server" Text="From Time:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtFromTime" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group hidden" id="dvToTime" runat="server">
                    <asp:Label ID="lblToTime" runat="server" Text="To Time:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtToTime" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="lblSupervisor" runat="server" Text="Supervisor:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtSupervisor" runat="server" ReadOnly="True" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="lblReasonOfLeave" runat="server" Text="Description:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtReasonOfLeave" runat="server" ReadOnly="True" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group" style="margin-top: 10px;">
                    <div class="col-sm-offset-4 col-sm-10">
                        <asp:Button ID="btnApprove" runat="server" CssClass="btn btn-primary btn-sm" Text="Approve" OnClick="btnApprove_Click" Width="70px" />
                        <asp:Button ID="btnReject" runat="server" CssClass="btn btn-danger btn-sm" Text="Reject" OnClick="btnReject_Click" Width="70px" />
                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="70px" />
                    </div>
                </div>
                
                <div class="form-group" style="margin-top: 10px;">
                    <asp:Label ID="Label1" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                    </div>
                </div>

            </div>
            <asp:HiddenField ID="hidLeaveInfo" Value="0" runat="server" />
            <asp:GridView ID="gdvLeaveStatus" runat="server" Style="width: 100%; margin-left: 0;"
                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                CssClass="table table-striped table-bordered">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                    </asp:CommandField>
                    <asp:TemplateField HeaderText="Leave Type">
                        <ItemTemplate>
                            <asp:Label ID="lblGrdApproveLeaveType" runat="server" Text='<%# Bind("leaveType") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Limit">
                        <ItemTemplate>
                            <asp:Label ID="lblGrdApproveLimit" runat="server" Text='<%# Bind("Approved") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Enjoyed">
                        <ItemTemplate>
                            <asp:Label ID="lblGrdApproveEnjoyed" runat="server" Text='<%# Bind("Taken") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Balance">
                        <ItemTemplate>
                            <asp:Label ID="lblGrdApproveBalance" runat="server" Text='<%# Bind("Balance") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pagination-sa" />
                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
            </asp:GridView>
        </div>
        <div class="col-md-8">
            <fieldset>
                <legend>Pending Leave Application</legend>
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvLeaveApplication" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvLeaveApplication_RowDataBound" OnSelectedIndexChanged="gdvLeaveApplication_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Leave Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblLeaveType" runat="server" Text='<%# Bind("LeaveType.leaveType1") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridLeaveAppIdId" Value='<%# Bind("EmpLeaveApplicationId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblPIN" runat="server" Text='<%# Bind("EmployeePersonalInfo.PIN") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("EmployeePersonalInfo.EmployeeName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblFromDate" runat="server" Text='<%# Eval("FromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("ToDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Note">
                                <ItemTemplate>
                                    <asp:Label ID="lblNote" runat="server" Text='<%# Bind("ReasonOfLeave") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="URL">
                                <ItemTemplate>
                                    <asp:Label ID="lblNote" runat="server" Text='<%# Bind("ReasonOfLeave") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>