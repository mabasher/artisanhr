﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmWorkExperience : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);


                string Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
                if (Sessionuserlevel == "4")
                {
                    txtSearch.Visible = false;
                    getEmpData(SessionUserId);
                    hidEmpAutoId.Value = SessionUserId;
                }

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")  hidEmpAutoId.Value = "0"; 
                CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();           

                SqlCommand cmd = new SqlCommand("SELECT WorkExperienceInfoId as AutoId,companyName as companyName,ni.companyBusiness as companyBusiness,ni.companyAddress as companyAddress,ni.designation as designation,ni.department as department, ni.jobNature as jobNature, ni.jobResponsibilities as jobResponsibilities, ni.lastSalary as lastSalary, ni.fromDate as fromDate, ni.toDate as toDate, ni.continuing as continuing , ni.remark as remark FROM WorkExperienceInfo ni left outer join EmployeePersonalInfo ei on ni.EmployeePersonalInfoId=ei.EmployeePersonalInfoId  Where ni.EmployeePersonalInfoId='" + hidEmpAutoId.Value + "'", sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.AllowPaging = true;
                gdv_costingHead.PageSize = 8;
                gdv_costingHead.DataBind();
                sqlconnection.Close();            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = txtPIN.Text + "_" + filename;
                URL = "~/UserPic/" + filename;
                filename = "";
                
                s_Update = "[ProcWorkExperienceInfoINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyBusiness.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDesignation.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDepartment.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobNature.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobResponsibilities.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtLastSalary.Text.Trim().Replace("'", "''"))
                        + "','"
                         + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtContinuing.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";
                
                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = txtPIN.Text + "_" + filename;
                URL = "~/UserPic/" + filename;
                filename = "";

                s_save_ = "[ProcWorkExperienceInfoINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyName.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyBusiness.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtCompanyAddress.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDesignation.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtDepartment.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobNature.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtJobResponsibilities.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtLastSalary.Text.Trim().Replace("'", "''"))
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtContinuing.Text.Trim().Replace("'", "''"))
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";                
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtCompanyName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCompanyName")).Text;

                txtCompanyBusiness.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCompanyBusiness")).Text;
                txtCompanyAddress.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCompanyAddress")).Text;
                txtDesignation.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDesignation")).Text;
                txtDepartment.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDepartment")).Text;
                txtJobNature.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridJobNature")).Text;
                txtJobResponsibilities.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridjobResponsibilities")).Text;
                txtLastSalary.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridlastSalary")).Text;
                txtFromDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridfromDate")).Text;
                txtToDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridtoDate")).Text;
                txtContinuing.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridcontinuing")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridremark")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();


            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }

            //if (txtNomineeName.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Nominee Name Can Not Blank!";
            //    return false;
            //}
            
            //if (txtPermanentAddress.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Address Can Not Blank!";
            //    return false;
            //}

            //if (txtTIN.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Share Can Not Blank!";
            //    return false;
            //}

            //if (txtPersonalContact.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Contact No Can Not Blank!";
            //    return false;
            //}

            if (imgUpload.HasFile)
            {
                int imgSize = imgUpload.PostedFile.ContentLength;
                string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
                if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
                {
                    lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
                    lblMsg.Visible = true;
                    return false;
                }
                else if (imgSize > 800000)//1048576)//1048576 bit=1MB
                {
                    lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
                    lblMsg.Visible = true;
                    return false;
                }
            }
            //else
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "User Photo Can Not Blank!";
            //    return false;
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            //txtNomineeName.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtPresentAddress.Text = string.Empty;
            //txtPermanentAddress.Text = string.Empty;
            //txtFather.Text = string.Empty;
            //txtMother.Text = string.Empty;
            //txtNationalId.Text = string.Empty;
            txtPassport.Text = string.Empty;
            txtBirthCertificate.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            //txtTIN.Text = string.Empty;

           
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }


      //===========End===============         
    }
}