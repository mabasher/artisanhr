﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmOfficialInformation.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmOfficialInformation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Official Information
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="row row-custom">

            <div class="col-lg-12">

                <div class="col-md-2">
                    <div class="form-horizontal">
                        <div class="main-data-grid">
                            <div style="height: 500px; overflow: scroll;">
                                <asp:TextBox ID="txtEmpSearch" runat="server" Width="100%" placeholder="Search here... "
                                    onfocus="this.style.backgroundColor='pink'" onblur="this.style.backgroundColor='white'"
                                    OnTextChanged="txtEmpSearch_TextChanged">
                                </asp:TextBox>
                                <asp:HiddenField ID="hfAutoIdId" runat="server" />

                                <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                    CssClass="table table-striped table-bordered" OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                                    EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">

                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Card No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridSL" runat="server" Text='<%# Bind("Party") %>'></asp:Label>
                                                <asp:HiddenField ID="hid_GridMasterId" Value='<%# Bind("AutoId") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("SLNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-sa" />
                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            <%--    ------------------------%>
                <div class="col-md-5">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 hidden">
                                <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group" style="margin-top: 20px;">
                            <asp:Label ID="Label11" runat="server" Text="Department :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label27" runat="server" Text="Designation :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>


                        <div class="form-group">
                            <asp:Label ID="Label30" runat="server" Text="Employee Grade :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddEmployeeGroup" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label45" runat="server" Text="Employee Type :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddEmployeeType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label29" runat="server" Text="Section :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label31" runat="server" Text="Floor :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddFloor" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label32" runat="server" Text="Block :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddBlock" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label37" runat="server" Text="Line No :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label40" runat="server" Text="Supervisor :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddSupervisor" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                     <div class="row row-custom" style="padding-top: 20px;">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                        </div>
                    </div>
                </div>

                </div>

                <div class="col-md-5" style="margin-top: 100px;">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <asp:Label ID="lbl_issue_Date" runat="server" Text="Date of Joining :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox runat="server" ID="txtDOB" CssClass="input-sm date-picker" />
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label42" runat="server" Text="Probation Period:" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtProbationPeriod" runat="server" Text="6" CssClass="form-control input-sm" FilterType="Numbers,Custom" AutoPostBack="True" OnTextChanged="txtProbationPeriod_TextChanged"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,Custom"
                                    ValidChars="." TargetControlID="txtProbationPeriod" />
                            </div>
                            <asp:Label ID="Label43" runat="server" Text="Month" CssClass="control-label"></asp:Label>

                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label39" runat="server" Text="Date of Confirmation :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox runat="server" ID="txtDOConFirmation" CssClass="input-sm date-picker" />
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label41" runat="server" Text="PF Enable (Y/N) :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="chkPFEnable" runat="server" />
                                <asp:TextBox runat="server" ID="txtDOPF" CssClass="input-sm date-picker hidden" />
                            </div>
                        </div>


                        <div class="form-group">
                            <asp:Label ID="Label44" runat="server" Text="Shift :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-6">
                                <asp:DropDownList ID="ddShift" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label35" runat="server" Text="Job Location :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-6">
                                <asp:DropDownList ID="ddJobLocation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label36" runat="server" Text="Company Name :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-6">
                                <asp:DropDownList ID="ddBranch" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label34" runat="server" Text="Project :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-6">
                                <asp:DropDownList ID="ddProject" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-md-2 hidden">
                    <div class="form-horizontal">


                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text="Relation :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddGender" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label16" runat="server" Text="Address:" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtPermanentAddress" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label8" runat="server" Text="Contact No :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtPersonalContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="E-mail Address :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtPersonalEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label14" runat="server" Text="National Id :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtNationalId" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label23" runat="server" Text="Share (%) :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtTIN" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label18" runat="server" Text="Nominee Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddReligion" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>

                                <asp:TextBox ID="txtNomineeName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label19" runat="server" Text="Father's Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtFather" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label20" runat="server" Text="Mother's Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtMother" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label21" runat="server" Text="Spouse Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtSpouse" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label17" runat="server" Text="Official Email :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtOfficialEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label10" runat="server" Text="Blood Group :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddBlood" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label12" runat="server" Text="Marital Status :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddMaritalStatus" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label13" runat="server" Text="Nationality :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddNationallity" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label15" runat="server" Text="Passport No :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtPassport" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label24" runat="server" Text="Driving License :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtDrivingLicense" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text="Official Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtOfficialContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Emergency Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtEmergencyContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label25" runat="server" Text="Contact Person :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtContactPerson" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label26" runat="server" Text="Birth Certificate :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtBirthCertificate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label22" runat="server" Text="Place of Birth :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtPlaceofBirth" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label9" runat="server" Text="Present Address:" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtPresentAddress" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>


                        <div class="form-group" style="text-align: center;">
                            <asp:Image ID="ImgPic" runat="server" Height="120" BorderWidth="1" Width="120" />
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <asp:FileUpload ID="imgUpload" runat="server" />
                                <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Picture Image size <100KB)"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group" style="text-align: center;">
                            <asp:Image ID="ImgSig" runat="server" Height="120" BorderWidth="1" Width="120" />
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <asp:FileUpload ID="imgSigUpload" runat="server" />
                                <asp:Label ID="Label28" ForeColor="#F22613" runat="server" Text="(Signature Image size <100 KB)"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

               

                <div class="col-md-12 hidden">
                    <div style="height: 1px; overflow: scroll;">
                        <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                            CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="Nominee Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("NomineeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Father Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridFather" runat="server" Text='<%# Bind("FatherName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mother Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridMother" runat="server" Text='<%# Bind("MotherName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contact No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridContact" runat="server" Text='<%# Bind("PersonalContact") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridMail" runat="server" Text='<%# Bind("PersonalMail") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridNid" runat="server" Text='<%# Bind("NID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDOB" runat="server" Text='<%# Bind("DOB","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Relation">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridRelation" runat="server" Text='<%# Bind("Relation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Share(%)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridShare" runat="server" Text='<%# Bind("Share") %>'></asp:Label>
                                        <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                        <asp:HiddenField ID="hidRelationId" Value='<%# Bind("RelationId") %>' runat="server" />
                                        <asp:HiddenField ID="hidGridAddress" Value='<%# Bind("PermanentAddress") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-sa" />
                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>




        </div>


        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidAutoId" runat="server" Value="0" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>



</asp:Content>
