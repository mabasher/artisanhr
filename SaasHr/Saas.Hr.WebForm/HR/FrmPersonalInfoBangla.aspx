﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmPersonalInfoBangla.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmPersonalInfoBangla" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        ব্যক্তিগত তথ্য বাংলা
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-lg-12">

            <div class="col-md-5">
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-3 hidden">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 20px;">
                        <asp:Label ID="Label2" runat="server" Text="পিন বাংলা :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtPINBangla" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label18" runat="server" Text="নাম বাংলা :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label19" runat="server" Text="বাবার নাম :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtFather" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label20" runat="server" Text="মায়ের নাম :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtMother" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label21" runat="server" Text="বর্তমান ঠিকানা :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtPresentAddress" runat="server" TextMode="MultiLine" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label16" runat="server" Text="স্থায়ী ঠিকানা :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="txtPermanentAddress" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-2 hidden">
                <div class="form-horizontal">

                    <div class="form-group" style="text-align: center;">
                        <asp:Image ID="ImgPic" runat="server" Height="120" BorderWidth="1" Width="120" />
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <asp:FileUpload ID="imgUpload" runat="server" />
                            <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Picture Image size <100KB)"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group" style="text-align: center;">
                        <asp:Image ID="ImgSig" runat="server" Height="120" BorderWidth="1" Width="120" />
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <asp:FileUpload ID="imgSigUpload" runat="server" />
                            <asp:Label ID="Label28" ForeColor="#F22613" runat="server" Text="(Signature Image size <100 KB)"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-custom" style="padding-top: 20px;">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div style="height: 150px; overflow: scroll;">
                    <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="পিন বাংলা">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridPINNo" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="নাম বাংলা">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="বাবার নাম">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridFather" runat="server" Text='<%# Bind("FatherName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="মায়ের নাম">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridMother" runat="server" Text='<%# Bind("MotherName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="বর্তমান ঠিকানা">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridPresentAddress" runat="server" Text='<%# Bind("PresentAddress") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="স্থায়ী ঠিকানা">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridPermanentAddress" runat="server" Text='<%# Bind("PermanentAddress") %>'></asp:Label>
                                    <asp:HiddenField ID="hid_GridEmployeeAutoId" Value='<%# Bind("EmployeeAutoId") %>' runat="server" />
                                    <asp:HiddenField ID="hidEmployeePersonalBanglaInfoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>




    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmployeePersonalBanglaInfoId" runat="server" Value="0" />
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>

</asp:Content>
