﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmPersonalAdditionalInfo : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                commonfunctions.g_b_FillDropDownList(ddGender,
                    "GenderInfo",
                    "GenderName",
                    "GenderInfoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddNationallity,
                    "NationalityInfo",
                    "NationalityName",
                    "NationalityInfoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddBlood,
                    "BloodGroupInfo",
                    "BloodGroupName",
                    "BloodGroupInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddReligion,
                    "ReligionInfo",
                    "ReligionName",
                    "ReligionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddMaritalStatus,
                    "MaritalStatusInfo",
                    "MaritalStatusName",
                    "MaritalStatusInfoId", string.Empty);

                //v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select AutoId as AutoId, UserName as UserName,logInId as logInId,Contact as Contact,Email as Email,UserAddress as UserAddress,isnull(userlevel,0) as userlevel,pwd as pwd,ISNULL(URL,'') as URL  from T_SignIn ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = txtPIN.Text + "_" + filename;
                URL = "~/UserPic/" + filename;
                filename = "";
                s_Update = "[ProcPersonalProfileINSERT]"
                        + "'"
                        + hidEmpAutoId.Value
                        + "','"
                        + HttpUtility.HtmlDecode(txtPIN.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCard.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmpName.Text.Trim())
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFather.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMother.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtSpouse.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialContact.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmergencyContact.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialEmail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','','"
                        + HttpUtility.HtmlDecode(txtDOB.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlaceofBirth.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBirthCertificate.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtNationalId.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtTIN.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassport.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDrivingLicense.Text.Trim())
                        + "','','','"//@ExtraCurricularActivities,@Remark
                        + filename//PIC URL
                        + "','"
                        + filename//SIG URL
                        + "','"
                        + ddGender.SelectedValue
                        + "','"
                        + ddNationallity.SelectedValue
                        + "','"
                        + ddReligion.SelectedValue
                        + "','"
                        + ddBlood.SelectedValue
                        + "','"
                        + ddMaritalStatus.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";
                
                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                }
            }
            else
            {
                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = txtPIN.Text + "_" + filename;
                URL = "~/UserPic/" + filename;
                filename = "";
                s_save_ = "[ProcPersonalProfileINSERT]"
                        + "'0','"
                        + HttpUtility.HtmlDecode(txtPIN.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtCard.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmpName.Text.Trim())
                        + "','','"
                        + HttpUtility.HtmlDecode(txtFather.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtMother.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtSpouse.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialContact.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtEmergencyContact.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtContactPerson.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtOfficialEmail.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','','"
                        + HttpUtility.HtmlDecode(txtDOB.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPlaceofBirth.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtBirthCertificate.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtNationalId.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtTIN.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtPassport.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDrivingLicense.Text.Trim())
                        + "','','','"//@ExtraCurricularActivities,@Remark
                        + filename//PIC URL
                        + "','"
                        + filename//SIG URL
                        + "','"
                        + ddGender.SelectedValue
                        + "','"
                        + ddNationallity.SelectedValue
                        + "','"
                        + ddReligion.SelectedValue
                        + "','"
                        + ddBlood.SelectedValue
                        + "','"
                        + ddMaritalStatus.SelectedValue
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";                
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        //InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (ddGender.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Gender Can Not Blank!";
                return false;
            }

            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }

            if (txtPresentAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Present Address Can Not Blank!";
                return false;
            }
            if (txtPermanentAddress.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Permanent Address Can Not Blank!";
                return false;
            }

            if (txtPersonalContact.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Contact No Can Not Blank!";
                return false;
            }

            if (imgUpload.HasFile)
            {
                int imgSize = imgUpload.PostedFile.ContentLength;
                string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
                if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
                {
                    lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
                    lblMsg.Visible = true;
                    return false;
                }
                else if (imgSize > 800000)//1048576)//1048576 bit=1MB
                {
                    lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
                    lblMsg.Visible = true;
                    return false;
                }
            }
            //else
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "User Photo Can Not Blank!";
            //    return false;
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtPresentAddress.Text = string.Empty;
            txtPermanentAddress.Text = string.Empty;
            txtFather.Text = string.Empty;
            txtMother.Text = string.Empty;
            txtNationalId.Text = string.Empty;
            txtPassport.Text = string.Empty;
            txtBirthCertificate.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            txtPlaceofBirth.Text = string.Empty;
            ddGender.SelectedValue = "0";
            ddNationallity.SelectedValue = "0";
            ddMaritalStatus.SelectedValue = "0";
            ddBlood.SelectedValue = "0";
            ddReligion.SelectedValue = "0";
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            
            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        //txtCard.Text = drow["EmployeeNeekName"].ToString();
                        txtFather.Text = drow["FatherName"].ToString();
                        txtMother.Text = drow["MotherName"].ToString();
                        txtSpouse.Text = drow["SpouseName"].ToString();
                        txtPersonalContact.Text = drow["PersonalContact"].ToString();
                        txtOfficialContact.Text = drow["OfficialContact"].ToString();
                        txtEmergencyContact.Text = drow["EmergencyContact"].ToString();
                        txtContactPerson.Text = drow["ContactPerson"].ToString();
                        txtPersonalEmail.Text = drow["PersonalMail"].ToString();
                        txtOfficialEmail.Text = drow["OfficialMail"].ToString();
                        txtPresentAddress.Text = drow["PresentAddress"].ToString();
                        txtPermanentAddress.Text = drow["PermanentAddress"].ToString();
                        //txtCard.Text = drow["EmergencyAddress"].ToString();
                        txtDOB.Text = drow["DOB"].ToString();
                        txtPlaceofBirth.Text = drow["PlaceOfBirth"].ToString();
                        txtBirthCertificate.Text = drow["BirthCertificate"].ToString();
                        txtNationalId.Text = drow["NID"].ToString();
                        txtTIN.Text = drow["TIN"].ToString();
                        txtPassport.Text = drow["PassportNo"].ToString();
                        txtDrivingLicense.Text = drow["DrivingLicense"].ToString();
                        //txtCard.Text = drow["ExtraCurricularActivities"].ToString();
                        //txtCard.Text = drow["Remark"].ToString();
                        //txtCard.Text = drow["PicUrl"].ToString();
                        //txtCard.Text = drow["SigUrl"].ToString();
                        ddGender.SelectedValue = drow["GenderAutoId"].ToString();
                        ddNationallity.SelectedValue = drow["NationalityAutoId"].ToString();
                        ddReligion.SelectedValue = drow["ReligionAutoId"].ToString();
                        ddBlood.SelectedValue = drow["BloodGroupAutoId"].ToString();
                        ddMaritalStatus.SelectedValue = drow["MaritalStatusAutoId"].ToString();
                                             
                    }
                    btn_save.Text = "Update";
                }
            }
        }


               
    }
}