﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmLeaveApplication.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmLeaveApplication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    Application for Leave
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-lg-12">
            <div class="col-md-4">
                
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lblEmployee" runat="server" Text="Application for:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddEmployee" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddLeaveType" AutoPostBack="true" OnSelectedIndexChanged="ddLeaveType_SelectedIndexChanged" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="Leave Balance :" ForeColor="Red" Font-Bold="true" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtLeaveBalance" ForeColor="Red" Font-Bold="true" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>   
                        </div>                                
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblFromDate" runat="server" Text="From Date:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtFromDate" runat="server"
                                        AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"
                                CssClass="form-control input-sm date-picker"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="lbldays" runat="server" Text="Leave Days :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtDays" Font-Bold="true" AutoComplete="off" runat="server" 
                                CssClass="form-control input-sm" AutoPostBack="True" 
                                OnTextChanged="txtDays_TextChanged"></asp:TextBox>   
                        </div>                                
                    </div>

                    <div class="form-group">
                        <asp:Label ID="lblToDate" runat="server" Text="To Date:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtToDate" Enabled="false" runat="server" CssClass="form-control input-sm date-picker"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <asp:Label ID="lblIsPartialLeave" runat="server" Text="Partial Leave:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-1">
                            <asp:CheckBox ID="chkIsPartialLeave" runat="server" />
                        </div>
                    </div>
                    <div class="form-group hidden" id="dvFromTime" runat="server">
                        <asp:Label ID="lblFromTime" runat="server" Text="From Time:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group hidden" id="dvToTime" runat="server">
                        <asp:Label ID="lblToTime" runat="server" Text="To Time:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblSupervisor" runat="server" Text="Supervisor:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddSupervisor" runat="server" Enabled="false" CssClass="form-control input-sm chosen-select"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblReasonOfLeave" runat="server" Text="Description:" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtReasonOfLeave" runat="server" CssClass="form-control input-sm" Height="80" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                                        
                    <div class="form-group">
                        <asp:Label ID="Label6" runat="server" Text="Doc. Upload :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:FileUpload ID="imgUpload" runat="server" />
                        </div>
                    </div>                    
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-6">
                            <asp:Label ID="Label3" ForeColor="#F22613" runat="server" Text="***if Sick Leave (upload PDF Docs)"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-offset-4 col-sm-10">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Apply" OnClick="btnSave_Click" Width="80px" 
                                OnClientClick="this.disabled=true;" UseSubmitBehavior="false"  autopostback="false" xmlns:asp="#unknown" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="80px" />
                        </div>
                    </div>                    
                    <div class="form-group" style="margin-top: 10px;">
                        <asp:Label ID="Label1" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:Label ID="lblMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="hidLeaveApplication" Value="0" runat="server" />
            </div>
            <div class="col-md-8">
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvLeaveApplication" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvLeaveApplication_RowDataBound" OnSelectedIndexChanged="gdvLeaveApplication_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Leave Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblLeaveType" runat="server" Text='<%# Bind("LeaveType.leaveType1") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridLeaveAppIdId" Value='<%# Bind("EmpLeaveApplicationId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="PIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblPIN" runat="server" Text='<%# Bind("EmployeePersonalInfo.PIN") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("EmployeePersonalInfo.EmployeeName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="From Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("FromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("ToDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
                <div>
                    <iframe id="ifrmPiDoc" runat="server" style="border: 0; width: 100%; min-height: 500px;"></iframe>
                </div>
            </div>
        </div>
        
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
    <script>
        $(document).ready(function () {
            $("#<%= chkIsPartialLeave.ClientID %>").change(function () {
                if (this.checked) {
                    $("#<%= dvFromTime.ClientID %>").removeClass("hidden");
                    $("#<%= dvToTime.ClientID %>").removeClass("hidden");
                } else {
                    $("#<%= dvFromTime.ClientID %>").addClass("hidden");
                    $("#<%= dvToTime.ClientID %>").addClass("hidden");
                }
            });
        });
    </script>
</asp:Content>