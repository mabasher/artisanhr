﻿<%@ Page Title="Training Info" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmTrainingInfo.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmTrainingInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Training Information
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server"></asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="row row-custom">
            <div class="col-lg-12">

                <div class="col-md-5">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 hidden">
                                <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>


                        <div class="form-group" style="margin-top: 20px;">
                            <asp:Label ID="Label19" runat="server" Text="Training Title :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddTrainingTitle" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label20" runat="server" Text="Topics Covered :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtTopicsCovered" TextMode="MultiLine" Height="40" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label21" runat="server" Text="Organized By :" CssClass="col-sm-4   control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtOrganizedBy" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-top: 100px;">
                            <asp:Label ID="lbl_issue_Date" runat="server" Text="From Date :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="Label8" runat="server" Text="To Date :" CssClass="control-label"></asp:Label>
                            </div>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label16" runat="server" Text="Remarks :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row row-custom" style="padding-top: 20px;">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div style="height: 150px; overflow: scroll;">
                        <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                            CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="Training Title">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridTrainingTitle" runat="server" Text='<%# Bind("TrainingTitle") %>'></asp:Label>
                                        <asp:HiddenField ID="hidTrainingTitleId" Value='<%# Bind("TrainingTitleId") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Topics Covered">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridTopicsCovered" runat="server" Text='<%# Bind("TopicsCovered") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Organized By">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridOrganizedBy" runat="server" Text='<%# Bind("OrganizedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="From Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridFromDate" runat="server" Text='<%# Bind("FromDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="To Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridToDate" runat="server" Text='<%# Bind("ToDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Remarks">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                        <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                        <asp:HiddenField ID="hidTrainingInfoId" Value='<%# Bind("TrainingInfoId") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-sa" />
                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAcademicInfoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidExamNameId" runat="server" Value="0" />
        <asp:HiddenField ID="hidInstituteId" runat="server" Value="0" />
        <asp:HiddenField ID="hidBoardUniversityId" runat="server" Value="0" />
        <asp:HiddenField ID="hidMajorGroupId" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

    </script>

</asp:Content>
