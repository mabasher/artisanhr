﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData.Hr;
using Saas.Hr.WebForm.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmLeaveApplicationApproval : Page
    {
        private HrEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int SessionUserId = 0;
                SessionUserId = Convert.ToInt16(Session[GlobalVariables.g_s_userAutoId]);
                BindLeaveApplications(SessionUserId);

            }
        }

        private void BindLeaveApplications(int empId)
        {
            using (_context = new HrEntities())
            {
                gdvLeaveApplication.DataSource = _context.EmpLeaveApplications.Include(m => m.LeaveType).Include(f => f.EmployeePersonalInfo)
                                                    .Where(m => m.SupervisorId == empId && m.ApplicationStatus == LeaveApplicationStatus.Pending.ToString()).ToList();
                gdvLeaveApplication.DataBind();
            }
        }

        private void BindEmpLeaveStatus(decimal companyId, int empId)
        {
            using (_context = new HrEntities())
            {
                gdvLeaveStatus.DataSource = _context.ProcRptLeaveSummary(companyId, empId + "X%", DateTime.Now.Year).ToList();
                gdvLeaveApplication.DataBind();
            }
        }

        private void ClearAll()
        {
            hidLeaveInfo.Value = "0";
            chkIsPartialLeave.Checked = false;

            txtSupervisor.Text = string.Empty;
            txtLeaveType.Text = string.Empty;
            txtFromDate.Text = string.Empty;
            txtToDate.Text = string.Empty;
            txtFromTime.Text = string.Empty;
            txtToTime.Text = string.Empty;
            txtReasonOfLeave.Text = string.Empty;

            dvFromTime.Attributes.Remove("class");
            dvFromTime.Attributes.Add("class", "form-group hidden");
            dvToTime.Attributes.Remove("class");
            dvToTime.Attributes.Add("class", "form-group hidden");

            btnApprove.Text = "Save";
            lblMsg.Text = string.Empty;
            BindLeaveApplications(0);
            BindEmpLeaveStatus(0, 0);
        }

        private void UpdateApplicationStatus(LeaveApplicationStatus newStatus)
        {
            _context = new HrEntities();
            var id = int.Parse(hidLeaveInfo.Value);
            var empLeaveApplication = _context.EmpLeaveApplications.Include(la => la.LeaveType).FirstOrDefault(r => r.EmpLeaveApplicationId == id);
            var supervisor = _context.EmployeePersonalInfoes.Find(int.Parse(Session[GlobalVariables.g_s_userAutoId].ToString()));
            if (empLeaveApplication == null)
            {
                MessageBox.ShowMessageInLabel("Please select Application to Approve/Reject!!!", lblMsg,
                    MessageType.ErrorMessage);
            }
            else
            {
                empLeaveApplication.ApplicationStatus = newStatus.ToString();
                empLeaveApplication.ApproveOrRejectBy = int.Parse(Session[GlobalVariables.g_s_userAutoId].ToString());
                empLeaveApplication.ApproveOrRejectTime = DateTime.Now;
                _context.SaveChanges();
                if (newStatus == LeaveApplicationStatus.Approved)
                {
                    SaveLeave(empLeaveApplication);
                    string supervisorName = null;
                    string supervisorEmail = null;
                    if (supervisor != null)
                    {
                        supervisorName = supervisor.EmployeeName;
                        supervisorEmail = supervisor.OfficialMail;
                    }
                    SendApprovalMail(empLeaveApplication.EmpIdLeaveApplicationFor, supervisorName, supervisorEmail, empLeaveApplication.LeaveType.leaveType1);

                    ClearAll();
                    MessageBox.ShowMessageInLabel("Application Approved!!!", lblMsg, MessageType.ErrorMessage);
                }
                else if (newStatus == LeaveApplicationStatus.Rejected)
                {
                    //SaveLeave(empLeaveApplication);
                    string supervisorName = null;
                    string supervisorEmail = null;
                    if (supervisor != null)
                    {
                        supervisorName = supervisor.EmployeeName;
                        supervisorEmail = supervisor.OfficialMail;
                    }
                    SendRejectMail(empLeaveApplication.EmpIdLeaveApplicationFor, supervisorName, supervisorEmail, empLeaveApplication.LeaveType.leaveType1);

                    ClearAll();
                    MessageBox.ShowMessageInLabel("Application Rejected!!!", lblMsg, MessageType.ErrorMessage);
                }


            }
        }

        protected void gdvLeaveApplication_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvLeaveApplication, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvLeaveApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new HrEntities();
            var id = int.Parse(((HiddenField)gdvLeaveApplication.Rows[gdvLeaveApplication.SelectedIndex].FindControl("hidGridLeaveAppIdId")).Value);
            var module = _context.EmpLeaveApplications.Include(a => a.LeaveType).First(r => r.EmpLeaveApplicationId == id);
            var applicant = _context.EmployeePersonalInfoes.First(app => app.EmployeePersonalInfoId == module.EmpIdLeaveApplicationFor);
            var supervisor = _context.EmployeePersonalInfoes.First(app => app.EmployeePersonalInfoId == module.SupervisorId);

            hidLeaveInfo.Value = module.EmpLeaveApplicationId.ToString();

            txtEmployeeName.Text = applicant.EmployeeName + " (" + applicant.PIN + ")";
            txtEmployeePIN.Text = applicant.PIN;
            txtSupervisor.Text = supervisor.EmployeeName + " (" + supervisor.PIN + ")";
            txtLeaveType.Text = module.LeaveType.leaveType1;
            txtFromDate.Text = module.FromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = module.ToDate.ToString("dd/MM/yyyy");
            txtReasonOfLeave.Text = module.ReasonOfLeave.ToString();
            if (module.IsPartialLeave == true)
            {
                dvFromTime.Attributes.Remove("class");
                dvFromTime.Attributes.Add("class", "form-group");
                dvToTime.Attributes.Remove("class");
                dvToTime.Attributes.Add("class", "form-group");
                chkIsPartialLeave.Checked = true;
                if (module.FromTime != null)
                    txtFromTime.Text = StringGenerator.TimespanToAmPmFormat(module.FromTime.Value);
                if (module.ToTime != null)
                    txtToTime.Text = StringGenerator.TimespanToAmPmFormat(module.ToTime.Value);
            }

            BindEmpLeaveStatus(applicant.CompanyAutoId, applicant.EmployeePersonalInfoId);
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            UpdateApplicationStatus(LeaveApplicationStatus.Approved);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            UpdateApplicationStatus(LeaveApplicationStatus.Rejected);
        }

        private void SaveLeave(EmpLeaveApplication leaveApplication)
        {
            var connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            var leaveDuration = (leaveApplication.ToDate.Date.Subtract(leaveApplication.FromDate.Date)).Days + 1;

            s_save_ = "[ProcLeaveWithPayINSERT]"
                    + "'0','"
                    + leaveApplication.EmpIdLeaveApplicationFor
                    + "','"
                    + leaveApplication.LeaveTypeId
                    + "','"
                    + (leaveApplication.IsPartialLeave == false ? "Days" : "Hours")
                    + "','"
                    + leaveApplication.FromDate.ToString("MM/dd/yyyy")
                    + "','"
                    + leaveApplication.FromTime
                    + "','"
                    + leaveDuration //(leaveApplication.ToDate.Date - leaveApplication.FromDate.Date).Days + 1
                    + "','"
                    + leaveApplication.ToDate.ToString("MM/dd/yyyy")
                    + "','"
                    + leaveApplication.ToTime
                    + "','"
                    + leaveApplication.ReasonOfLeave
                    + "','"
                    + SessionCompanyId
                    + "','"
                    + SessionUserId
                    + "','"
                    + SessionUserIP
                    + "','I'";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                lblMsg.Text = s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue ? GlobalVariables.g_s_insertOperationSuccessfull : GlobalVariables.g_s_duplicateCheckWarningMessage;
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        private string GenerateLeaveApprovalMail(List<ProcRptLeaveSummary_Result> leaveSummaryResults, string applicant, string leaveType)
        {
            var baseUrl = string.Empty;
            if (Request.ApplicationPath != null)
            {
                baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                 Request.ApplicationPath.TrimEnd('/') + "/";
            }

            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sDays = string.Empty;
            double dDays = 0;
            sFromDate = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            sToDate = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            dDays = (Convert.ToDateTime(sToDate) - Convert.ToDateTime(sFromDate)).TotalDays;
            dDays = dDays + 1;
            sDays = dDays.ToString();
            
            var sb = new StringBuilder();
            sb.Append($@"
                    <p>Dear {applicant},</p>
                    <p>Your {leaveType} application has been approved.</p>
                    <p>Your leave period from {sFromDate} to {sToDate} for {sDays} days.</p>

                    <p>Leave status is below:</p>
                    <table style=""border-collapse: collapse; width: 400px; border: 1px solid #999999; background-color: #cce0ff;"">
                        <thead>
                            <tr style=""background-color: #99bbff;"">
                                <th style=""border: 1px solid #999999;"">Leave Type</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Limit</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Enjoyed</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Balance</th>
                            </tr>
                        </thead>
                        <tbody>");
            foreach (var leaveSummaryResult in leaveSummaryResults)
            {
                sb.Append($@"<tr>
                                <td style=""border: 1px solid #999999;"">{leaveSummaryResult.leaveType}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Approved}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Taken}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Balance}</td>
                            </tr>");
            }
            sb.Append($@"</tbody>
                    </table>
                    <p><a href=""{baseUrl}"" target=""_blank"">Click here</a> to open software.</p>
                    <p> Developed by : <a href=""http://stmsoftwareltd.com/"" target=""_blank"">STM Software Ltd</a>.</p>
                    ");

            return sb.ToString();
        }

        private void SendApprovalMail(int employeeId, string supervisorName, string supervisorEmail, string leaveType)
        {
            var receivers = new List<string>();
            var ccReceivers = new List<string>();

            using (_context = new HrEntities())
            {
                var employee =
                    _context.EmployeePersonalInfoes.FirstOrDefault(e => e.EmployeePersonalInfoId == employeeId);

                if (employee == null) return;

                if (!string.IsNullOrEmpty(employee.OfficialMail))
                    receivers.Add(employee.OfficialMail);

                if (!string.IsNullOrEmpty(supervisorEmail))
                    ccReceivers.Add(supervisorEmail);

                //receivers.Add(employee.PIN.Contains("MPS")
                //    ? ConfigurationManager.AppSettings.Get("EmailReceiverForStaffLeaveApproval")
                //    : ConfigurationManager.AppSettings.Get("EmailReceiverForWorkerLeaveApproval"));
                string EmailForStaff = string.Empty;
                string EmailForworker = string.Empty;
                EmailForStaff = ConfigurationManager.AppSettings.Get("EmailReceiverForStaffLeaveApproval");
                if (!string.IsNullOrEmpty(EmailForStaff))
                    ccReceivers.Add(EmailForStaff);
                EmailForworker = ConfigurationManager.AppSettings.Get("EmailReceiverForWorkerLeaveApproval");
                if (!string.IsNullOrEmpty(EmailForworker))
                    ccReceivers.Add(EmailForworker);
                
                var leaveStatus = _context.ProcRptLeaveSummary(
                    employee.CompanyAutoId, employee.EmployeePersonalInfoId + "X%", DateTime.Now.Year);
                var mailOptions = new MailSendingOption(receivers, ccReceivers, supervisorName, "Leave Application Approved", GenerateLeaveApprovalMail(leaveStatus.ToList(), employee.EmployeeName, leaveType), null);
                try
                {
                    MailSender.SendMail(mailOptions);
                }
                catch (Exception exception)
                {
                    MessageBox.ShowMessageInLabel(exception.Message, lblMsg, MessageType.ErrorMessage);
                }
            }
        }
        
        private void SendRejectMail(int employeeId, string supervisorName, string supervisorEmail, string leaveType)
        {
            var receivers = new List<string>();
            var ccReceivers = new List<string>();

            using (_context = new HrEntities())
            {
                var employee =
                    _context.EmployeePersonalInfoes.FirstOrDefault(e => e.EmployeePersonalInfoId == employeeId);

                if (employee == null) return;

                if (!string.IsNullOrEmpty(employee.OfficialMail))
                    receivers.Add(employee.OfficialMail);

                if (!string.IsNullOrEmpty(supervisorEmail))
                    ccReceivers.Add(supervisorEmail);

                receivers.Add(employee.PIN.Contains("MPS")
                    ? ConfigurationManager.AppSettings.Get("EmailReceiverForStaffLeaveApproval")
                    : ConfigurationManager.AppSettings.Get("EmailReceiverForWorkerLeaveApproval"));

                var leaveStatus = _context.ProcRptLeaveSummary(
                    employee.CompanyAutoId, employee.EmployeePersonalInfoId + "X%", DateTime.Now.Year);
                var mailOptions = new MailSendingOption(receivers, ccReceivers, supervisorName, "Leave Application Rejected", GenerateLeaveRejectedMail(leaveStatus.ToList(), employee.EmployeeName, leaveType), null);
                try
                {
                    MailSender.SendMail(mailOptions);
                }
                catch (Exception exception)
                {
                    MessageBox.ShowMessageInLabel(exception.Message, lblMsg, MessageType.ErrorMessage);
                }
            }
        }
        
        private string GenerateLeaveRejectedMail(List<ProcRptLeaveSummary_Result> leaveSummaryResults, string applicant, string leaveType)
        {
            var baseUrl = string.Empty;
            if (Request.ApplicationPath != null)
            {
                baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                 Request.ApplicationPath.TrimEnd('/') + "/";
            }

            var sb = new StringBuilder();
            sb.Append($@"
                    <p>Dear {applicant},</p>
                    <p>Your {leaveType} application has been rejected.</p>
                    <br />
                    <p>Leave status is below:</p>
                    <table style=""border-collapse: collapse; width: 400px; border: 1px solid #999999; background-color: #cce0ff;"">
                        <thead>
                            <tr style=""background-color: #99bbff;"">
                                <th style=""border: 1px solid #999999;"">Leave Type</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Limit</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Enjoyed</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Balance</th>
                            </tr>
                        </thead>
                        <tbody>");
            foreach (var leaveSummaryResult in leaveSummaryResults)
            {
                sb.Append($@"<tr>
                                <td style=""border: 1px solid #999999;"">{leaveSummaryResult.leaveType}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Approved}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Taken}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Balance}</td>
                            </tr>");
            }
            sb.Append($@"</tbody>
                    </table>
                    <p><a href=""{baseUrl}"" target=""_blank"">Click here</a> to open software.</p>
                    <p> Developed by : <a href=""http://stmsoftwareltd.com/"" target=""_blank"">STM Software Ltd</a>.</p>
                    ");

            return sb.ToString();
        }



    }
}