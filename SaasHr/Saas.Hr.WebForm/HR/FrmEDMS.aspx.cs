﻿using Saas.Hr.WebForm.Common;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmEDMS : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtDate.Text = DateTime.Now.ToString();

                string Sessionuserlevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
                if (Sessionuserlevel == "4")
                {
                    txtSearch.Visible = false;
                    getEmpData(SessionUserId);
                    hidEmpAutoId.Value = SessionUserId;
                    v_loadGridView_CostingHead();
                }

                v_loadGridView_CostingHead();
            }
        }

        protected void txtsearch_TextChanged(object sender, EventArgs e)
        {
            v_loadGridView_CostingHead();
        }

        private void v_loadGridView_CostingHead()
        {
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("select top 50  edms.EDMSId as AutoId,  edms.Date, ei.EmployeeName as EmployeeName, ei.PIN as PIN, ei.EmployeePersonalInfoId as EmployeePersonalInfoId ,isnull(edms.URL,'') as URL, edms.Remarks as Note from [EDMS] as edms left outer join EmployeePersonalInfo ei on edms.EmployeePersonalInfoId=ei.EmployeePersonalInfoId where ei.EmployeePersonalInfoId = '" + hidEmpAutoId.Value + "' ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 50;
            gdv_costingHead.DataBind();

            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                if (!string.IsNullOrEmpty(hidURL.Value))
                {
                    var filePath = Server.MapPath("~/EDMS/" + hidURL.Value);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }
                }
                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = hidEmpAutoId.Value + "_" + SessionCompanyId + "_" + filename;
                URL = "~/EDMS/" + filename;

                s_Update = "[ProcEDMSAttachmentINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "',N'"
                        + filename
                        + "',N'"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));
                        v_loadGridView_CostingHead();
                        InsertMode();
                        v_loadGridView_CostingHead();
                        ifrmPiDoc.Src = string.Empty;
                    }
                }
            }
            else
            {
                filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                filename = hidEmpAutoId.Value + "_" + SessionCompanyId + "_" + filename;
                URL = "~/EDMS/" + filename;

                s_save_ = "[ProcEDMSAttachmentINSERT]"
                        + "'"
                        + '0'
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "',N'"
                        + filename
                        + "',N'"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        imgUpload.SaveAs(Server.MapPath(URL));
                        InsertMode();
                        v_loadGridView_CostingHead();
                        ifrmPiDoc.Src = string.Empty;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;

                var dUrl = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridURL")).Value;
                hidURL.Value = dUrl;
                 ifrmPiDoc.Src = Page.ResolveUrl("~/EDMS/" + dUrl);

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Emp. Name Can Not Blank!";
                return false;
            }



            if (imgUpload.HasFile)
            {
                var imgSize = imgUpload.PostedFile.ContentLength;
                var getext = Path.GetExtension(imgUpload.PostedFile.FileName);
                if (getext != ".pdf" && getext != ".PDF") //
                {
                    lblMsg.Text = "Document Format Only PDF";
                    lblMsg.Visible = true;
                    return false;
                }
                               
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "EDMS Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }


        //===========End===============   

    }
}