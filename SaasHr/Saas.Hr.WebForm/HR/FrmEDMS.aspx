﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmEDMS.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmEDMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Electronic Document Management System(EDMS)
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-md-4 ">
            <div style="height: 400px; overflow: scroll;">

                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                    CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                        </asp:CommandField>

                        <asp:TemplateField HeaderText="EmployeeName">
                            <ItemTemplate>
                                <asp:Label ID="lblGridEmployeeName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="PIN">
                            <ItemTemplate>
                                <asp:Label ID="lblGridPIN" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDate" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Note">
                            <ItemTemplate>
                                <asp:Label ID="lblGridNote" runat="server" Text='<%# Bind("Note") %>'></asp:Label>
                                <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                <asp:HiddenField ID="hidGridEmployeePersonalInfoId" Value='<%# Bind("EmployeePersonalInfoId") %>' runat="server" />
                                 <asp:HiddenField ID="hidGridURL" Value='<%# Bind("URL") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="pagination-sa" />
                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                </asp:GridView>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-sm-3 hidden">
                        <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Date :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtDate" CssClass="input-sm date-picker" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" Text="Note :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-7">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" Text="File Attachment :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-6">

                        <asp:FileUpload ID="imgUpload" runat="server" />
                        <asp:Label ID="Label7" ForeColor="#F22613" runat="server" Text="(Please upload PDF files only.)"></asp:Label>
                    </div>
                </div>

                <div class="row row-custom">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="text-danger">
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="form-group" style="margin-top: 10px;">
                    <div class="col-sm-offset-4 col-sm-10">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                    </div>
                </div>
            </div>
            <div>
                <iframe id="ifrmPiDoc" runat="server" style="border: 0; width: 100%; min-height: 500px;"></iframe>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>
</asp:Content>
