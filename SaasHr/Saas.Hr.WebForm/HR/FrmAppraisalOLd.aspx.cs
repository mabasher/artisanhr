﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmAppraisalOLd : BasePage
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                int cy = DateTime.Today.Year - 10;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                ddYear.DataSource = years;
                ddYear.DataBind();
                //txtYear.Text = DateTime.Today.Year.ToString();
                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                  "DepartmentInfo",
                  "Department",
                  "DepartmentInfoId", "Order by Department");

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
                txtToTime.Text = DateTime.Now.ToString("hh:mm:ss tt");

                DataTable myDt = new DataTable();
                myDt = CreateDataTable();
                ViewState["myDatatable"] = myDt;

                v_loadGridView_gdv_Attendence();
            }
        }

        private DataTable CreateDataTable()
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Columns.Add(new DataColumn("ParameterAutoId", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Parameter", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Marks", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Note", typeof(string)));
            return myDataTable;
        }
        private void AddDataToTable(string ParameterAutoId,
                                   string Parameter,
                                   string Marks,
                                   string Note,
                                   DataTable myTable)
        {
            DataRow row = myTable.NewRow();
            row["ParameterAutoId"] = ParameterAutoId;
            row["Parameter"] = Parameter;
            row["Marks"] = Marks;
            row["Note"] = Note;
            myTable.Rows.Add(row);
        }

        private void v_loadGridView_gdv_Attendence()
        {
            string departmentId = string.Empty;
            string sMonth = string.Empty;
            string sYear, sUpdate = string.Empty;
            departmentId = "0"; sMonth = "0"; sYear = "0"; sUpdate = "I";
            if (ddDepartment.SelectedValue != "0") { departmentId = ddDepartment.SelectedValue.ToString(); }
            if (ddMonth.SelectedValue != "0") { sMonth = ddMonth.SelectedValue.ToString(); }
            if (ddYear.SelectedItem.ToString() != "") { sYear = ddYear.SelectedItem.ToString(); }
            if (chkUpdate.Checked) { sUpdate = "U"; }
            //sYear = txtYear.Text;
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;

            qry = "ProcSelectMonthlyPerformance'" + departmentId + "','" + sMonth + "','" + sYear + "','" + sUpdate + "'";

            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_Attendence.DataSource = ds;
            gdv_Attendence.DataBind();
            sqlconnection.Close();

        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;
            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            string SessionUserType = Convert.ToString(Session[GlobalVariables.g_s_userStatus]);

            string tempData = string.Empty;

            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string empAutoId, PK, QA, STO, LS, AP, BS, RD, SH, IC, OTH = string.Empty;
                empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hidGridEmpAutoId")).Value;
                PK = ((TextBox)gdvRow.Cells[0].FindControl("txtPK")).Text;
                QA = ((TextBox)gdvRow.Cells[0].FindControl("txtQA")).Text;
                STO = ((TextBox)gdvRow.Cells[0].FindControl("txtSTO")).Text;
                LS = ((TextBox)gdvRow.Cells[0].FindControl("txtLS")).Text;
                AP = ((TextBox)gdvRow.Cells[0].FindControl("txtAP")).Text;
                BS = ((TextBox)gdvRow.Cells[0].FindControl("txtBS")).Text;
                RD = ((TextBox)gdvRow.Cells[0].FindControl("txtRD")).Text;
                SH = ((TextBox)gdvRow.Cells[0].FindControl("txtSH")).Text;
                IC = ((TextBox)gdvRow.Cells[0].FindControl("txtIC")).Text;
                OTH = ((TextBox)gdvRow.Cells[0].FindControl("txtOthers")).Text;
                if (PK == "") { PK = "0"; }  if (QA == "") { QA = "0"; } if (STO == "") { STO = "0"; }  if (LS == "") { LS = "0"; }  if (AP == "") { AP = "0"; }
                if (BS == "") { BS = "0"; }  if (RD == "") { RD = "0"; } if (SH == "") { SH = "0"; }    if (IC == "") { IC = "0"; }  if (OTH == "") { OTH = "0"; }
                
                tempData = tempData + "Execute ProcEmployeeMonthlyPerformanceINSERT"
                                 + "'"
                                 + ddDepartment.SelectedValue
                                 + "','"
                                 + HttpUtility.HtmlDecode(ddYear.SelectedItem.ToString())
                                 + "','"
                                 + ddMonth.SelectedValue  + "','" + empAutoId + "','"
                                 + Convert.ToDateTime(txtDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','"
                                 + PK + "','" + QA + "','" + STO + "','" + LS + "','" + AP + "','" + BS + "','"
                                 + RD + "','" + SH + "','" + IC + "','" + OTH + "','"+ HR_AutoId + "' ";
            }
            tempData = tempData + " Select ''";
            s_returnValue = connection.connection_DB(tempData, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
            }



        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            //foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            //    {
            //        string s_Card = string.Empty;
            //        string s_fromDate = string.Empty;

            //    s_Card = ((Label)gdvRow.Cells[0].FindControl("lblGridCardNo")).Text;
            //    s_fromDate = ((Label)gdvRow.Cells[0].FindControl("lblGridDOB")).Text;

            //        CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
            //        if (checkbox.Checked == true)
            //        {
            //            s_save_ = s_save_ + "Execute ProcDailyAttandenceDELETE "
            //                          + "'"
            //                          + s_Card
            //                          + "','"
            //                          + Convert.ToDateTime(s_fromDate.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
            //                          + "'";
            //        }
            //    }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }


        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {

            if (ddDepartment.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Department Can Not Blank!";
                return false;
            }

            //if (txtYear.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Year Can Not Blank!";
            //    return false;
            //}
            //string s_t = string.Empty;
            //s_t = "N";
            //foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            //{
            //    string s_CardId = string.Empty;
            //    s_CardId = ((TextBox)gdvRow.Cells[0].FindControl("txtPIN")).Text;

            //    CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
            //    if (checkbox.Checked == true && s_CardId != "")
            //    {
            //        s_t = "Y";
            //    }
            //}

            //if (s_t == "N")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Employee List Can Not Blank!";
            //    return false;
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFromDate.Text = "";
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        hidCardid.Value = drow["CardNo"].ToString();
                    }
                }
            }


        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_gdv_Attendence();
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string SessionUserlevel = Session[GlobalVariables.g_s_userLevel].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            string empId = string.Empty;
            string sMonth = string.Empty;
            string sYear = string.Empty;
            empId = "0"; sMonth = "0"; sYear = "0";
            if (hidEmpAutoId.Value != "0") { empId = hidEmpAutoId.Value; }
            if (ddMonth.SelectedValue != "0") { sMonth = ddMonth.SelectedValue.ToString(); }
            if (ddYear.SelectedItem.ToString() != "") { sYear = ddYear.SelectedItem.ToString(); }

            //sYear = txtYear.Text;

            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "Monthly Performance Sheet");
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("8FromDate", txtFromDate.Text.ToString());


                TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptEmployeeMonthlyPerformance.rpt";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptMonthlyPerformance'" + empId + "','" + sMonth + "','" + sYear + "'";

                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }


        }


    }
}