﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmTransferInformation.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmEmployeeTransferInformation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Employee Transfer Information 
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>                                                                                 
                            <div class="form-group">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                               <div class="col-sm-1 hidden">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                                                        
                            <div class="form-group hidden">
                                <asp:Label ID="Label11" runat="server" Text="Action Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddReleaseType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="form-group hidden">
                                <asp:Label ID="Label42" runat="server" Text="Penalty/Reward Tk. :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtFinalPayment"  runat="server" Text="0"  CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                                    <ajaxToolkit:filteredtextboxextender id="FilteredTextBoxExtender10" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtFinalPayment" />
                                </div>
                            </div>

                            <%--<hr />--%>
                            
                            <div class="form-group" style="margin-top: 20px;">
                                <asp:Label ID="lbl_issue_Date" runat="server" Text="Transfer Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox runat="server" ID="txtDOB" CssClass="input-sm date-picker" />
                                </div>
                            </div>

                            
                             
                            <div class="form-group">
                                <asp:Label ID="Label17" runat="server" Text="Designation :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="dddesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>    
                            <div class="form-group">
                                <asp:Label ID="Label18" runat="server" Text="Department :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="dddepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>    
                            <div class="form-group">
                                <asp:Label ID="Label19" runat="server" Text="Employee Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddemployeetype" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>    
                            <div class="form-group">
                                <asp:Label ID="Label20" runat="server" Text="Employee Group :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddemployeegroup" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>     
                            <div class="form-group">
                                <asp:Label ID="Label15" runat="server" Text="Location :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlocation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>  
                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text="Project :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddproject" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>       
                            <div class="form-group">
                                <asp:Label ID="Label14" runat="server" Text="Branch :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddbranch" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>   

                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="Note :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPermanentAddress"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>



                        </div>
                    </div>
                    
                    <div class="col-md-6" style="margin-top:100px;">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6" style="color:red;font-weight:bold;text-align:center;"> 
                                <asp:Label ID="Label5" runat="server" Text="Employee Present Status" CssClass="control-label"></asp:Label>
                                </div>
                            </div>    
                           
                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Designation :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddOdesignation" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>    
                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Department :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddOdepartment" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>    
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="Employee Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddOemployeetype" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>    
                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text="Employee Group :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddOemployeegroup" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>   
                            
                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text="Location :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddOlocation" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>   
                             <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Project :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddOproject" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>       
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Branch :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddObranch" Enabled="false" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>     

                        </div>
                   </div>


                <div class="row row-custom" style="padding-top: 20px;">
                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                        </div>
                        
                        <div class="form-group center">
                             &nbsp;
                        </div>
                        <div class="form-group center hidden">
                                 <asp:CheckBox ID="chkBangla" Text="&nbsp; Bangla" runat="server" />
                            &nbsp;&nbsp;
                                 <asp:CheckBox ID="CheckBox1" Text="&nbsp; English" runat="server" />
                            <asp:Button ID="btnPreview" runat="server" CssClass="btn btn-success btn-sm" Text="Promotion Letter" OnClick="btnPreview_Click" Width="100px" />
                          
                        </div>
                    </div>
                </div>
            </div>

                                       
                    <div class="col-md-12">
                        <div style="height: 150px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>
                                                                        
                                    <asp:TemplateField HeaderText="Transfer Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridIssueDate" runat="server" Text='<%# Bind("TransferDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Note">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridNote" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hid_GridEmpId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridProjectAutoId" Value='<%# Bind("ProjectAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridBranchAutoId" Value='<%# Bind("BranchAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridLocationAutoId" Value='<%# Bind("LocationAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridDepartmentAutoId" Value='<%# Bind("DepartmentAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridDesignationAutoId" Value='<%# Bind("DesignationAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridEmployeeGroupAutoId" Value='<%# Bind("EmployeeGroupAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridEmployeeTypeAutoId" Value='<%# Bind("EmployeeTypeAutoId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>




            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="0" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>

</asp:Content>
