﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData.Hr;
using Saas.Hr.WebForm.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmLeaveApplication : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        private HrEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int SessionUserId = 0;
                SessionUserId = Convert.ToInt16(Session[GlobalVariables.g_s_userAutoId]);
                string SessionUserSupervisorId = "0";
                SessionUserSupervisorId = Session[GlobalVariables.g_s_userSupervisorAutoId].ToString();
                BindLeaves(SessionUserId);
                BindLeaveType();
                BindEmployee();
                BindSuperVisor();
                ddSupervisor.SelectedValue = SessionUserSupervisorId;

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            }
        }
        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            dateChange();
        }
        protected void txtDays_TextChanged(object sender, EventArgs e)
        {
            dateChange();
        }

        private void dateChange()
        {
            string sday = string.Empty;
            if (txtDays.Text == "")
            { sday = "1"; }
            else { sday = txtDays.Text; }
            DateTime dt = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dt = dt.AddDays(Convert.ToDouble(sday) - 1);
            txtToDate.Text = dt.ToString("dd/MM/yyyy");
        }


        private void BindLeaves( int empId)
        {
            using (_context = new HrEntities())
            {
                //gdvLeaveApplication.DataSource = _context.EmpLeaveApplications
                //                                    .Include(m => m.LeaveType)
                //                                    .Include(m => m.ApplicationStatus=="")
                //                                    .Include(m => m.EmpIdLeaveApplicationFor==empId).ToList();
                gdvLeaveApplication.DataSource = _context.EmpLeaveApplications
                                                    .Include(m => m.LeaveType).Include(f=>f.EmployeePersonalInfo)
                                                    .Where(m => m.EmpIdLeaveApplicationFor == empId && m.ApplicationStatus == LeaveApplicationStatus.Pending.ToString()).ToList();


                gdvLeaveApplication.DataBind();
            }
        }

        private void BindLeaveType()
        {
            using (_context = new HrEntities())
            {
                //test
                ddLeaveType.DataSource = _context.LeaveTypes.ToList();
                ddLeaveType.DataTextField = "leaveType1";
                ddLeaveType.DataValueField = "LeaveTypeId";
                ddLeaveType.DataBind();
                ddLeaveType.Items.Insert(0, new ListItem("--- Select ---", "0"));
            }
        }

        private void BindEmployee()
        {
            var commonfunctions = new CommonFunctions();
            string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
            string SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            //commonfunctions.g_b_FillDropDownListMultiColumn(ddEmployee,
            //    "EmployeePersonalInfo",
            //    "PIN",
            //    "EmployeeName",
            //    "EmployeePersonalInfoId", string.Empty);

            string Proc = string.Empty;
            if (SessionUserLevel == "5")//HR Only
            {
                Proc = "select  EmployeePersonalInfoId as ItemId, isnull(PIN, '') + ' ' + isnull(EmployeeName, '') as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
            }
            else if (SessionUserLevel == "4")//Single Employee Only
            {
                ddEmployee.Enabled = false;
                Proc = "select EmployeePersonalInfoId as ItemId, isnull(PIN, '') + ' ' + isnull(EmployeeName, '') as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                ddEmployee.SelectedValue = SessionUserId;
            }
            else
            {
                Proc = "select EmployeePersonalInfoId as ItemId, isnull(PIN, '') + ' ' + isnull(EmployeeName, '') as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
            }

        }

        private void BindSuperVisor()
        {
            var commonfunctions = new CommonFunctions();            
            commonfunctions.g_b_FillDropDownListMultiColumn(ddSupervisor,
                "EmployeePersonalInfo",
                "PIN",
                "EmployeeName",
                "EmployeePersonalInfoId", "Where PIN Like 'MPS%'");
        }

        private void ClearAll()
        {
            ddLeaveType.SelectedIndex = 0;
            ddSupervisor.SelectedIndex = 0;
            hidLeaveApplication.Value = "0";

            txtFromDate.Text = string.Empty;
            txtToDate.Text = string.Empty;
            txtFromTime.Text = string.Empty;
            txtToTime.Text = string.Empty;
            txtReasonOfLeave.Text = string.Empty;

            chkIsPartialLeave.Checked = false;

            dvFromTime.Attributes.Remove("class");
            dvFromTime.Attributes.Add("class", "form-group hidden");
            dvToTime.Attributes.Remove("class");
            dvToTime.Attributes.Add("class", "form-group hidden");

            btnSave.Text = "Save";
            lblMsg.Text = string.Empty;
            BindLeaves(0);
        }

        private bool IsModuleValid()
        {
            if (ddEmployee.SelectedIndex < 1)
            {
                MessageBox.ShowMessageInLabel("Please select leave application for!!!", lblMsg, MessageType.ErrorMessage);
                ddEmployee.Focus();
                return false;
            }
            if (ddLeaveType.SelectedValue =="0")
            {
                MessageBox.ShowMessageInLabel("Please select leave Type!!!", lblMsg, MessageType.ErrorMessage);
                ddLeaveType.Focus();
                return false;
            }
            if (txtFromDate.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("From date should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtFromDate.Focus();
                return false;
            }

            if (DateTime.Parse(txtFromDate.Text.Trim()) < DateTime.Parse(DateTime.Now.ToString()) && ddLeaveType.SelectedValue=="2")
            {
                MessageBox.ShowMessageInLabel("Back dated application not allowed for Casual Leave !!!", lblMsg, MessageType.ErrorMessage);
                txtToDate.Focus();
                return false;
            }

            if (txtToDate.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("To date should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtToDate.Focus();
                return false;
            }

            if (!DateTime.TryParse(txtFromDate.Text.Trim(), out _))
            {
                MessageBox.ShowMessageInLabel("From date must be date in format \"dd/mm/yyyy\"!!!", lblMsg, MessageType.ErrorMessage);
                txtFromDate.Focus();
                return false;
            }

            if (!DateTime.TryParse(txtToDate.Text.Trim(), out _))
            {
                MessageBox.ShowMessageInLabel("To date must be date in format \"dd/mm/yyyy\"!!!", lblMsg, MessageType.ErrorMessage);
                txtToDate.Focus();
                return false;
            }

            if (DateTime.Parse(txtFromDate.Text.Trim()) > DateTime.Parse(txtToDate.Text.Trim()))
            {
                MessageBox.ShowMessageInLabel("To date must be greater than from date!!!", lblMsg, MessageType.ErrorMessage);
                txtToDate.Focus();
                return false;
            }

            if (txtReasonOfLeave.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Reason of leave should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtReasonOfLeave.Focus();
                return false;
            }

            if (txtLeaveBalance.Text == "") { txtLeaveBalance.Text = "0"; }
            if (txtDays.Text == "") { txtDays.Text = "0"; }
            if (Convert.ToDouble(txtLeaveBalance.Text) < Convert.ToDouble(txtDays.Text))
            {
                MessageBox.ShowMessageInLabel(ddLeaveType.SelectedItem.ToString() +" Balance Not Enough...! Try Another Leave Type!!", lblMsg, MessageType.ErrorMessage);
                ddSupervisor.Focus();
                return false;
            }


            if (chkIsPartialLeave.Checked)
            {
                if (txtFromTime.Text.Trim() == string.Empty)
                {
                    MessageBox.ShowMessageInLabel("For partial leave from time is required!!!", lblMsg, MessageType.ErrorMessage);
                    txtFromTime.Focus();
                    return false;
                }

                if (!DateTime.TryParse(txtFromTime.Text.Trim(), out _))
                {
                    MessageBox.ShowMessageInLabel("From time should be in format \"02:15 PM\"!!!", lblMsg, MessageType.ErrorMessage);
                    txtFromTime.Focus();
                    return false;
                }
                if (txtToTime.Text.Trim() == string.Empty)
                {
                    MessageBox.ShowMessageInLabel("For partial leave to time is required!!!", lblMsg, MessageType.ErrorMessage);
                    txtToTime.Focus();
                    return false;
                }

                if (!DateTime.TryParse(txtToTime.Text.Trim(), out _))
                {
                    MessageBox.ShowMessageInLabel("To time should be in format \"02:15 PM\"!!!", lblMsg, MessageType.ErrorMessage);
                    txtToTime.Focus();
                    return false;
                }
            }
            if (ddSupervisor.SelectedIndex < 1)
            {
                MessageBox.ShowMessageInLabel("Please select supervisor!!!", lblMsg, MessageType.ErrorMessage);
                ddSupervisor.Focus();
                return false;
            }
            var fromDate = DateTime.ParseExact(txtFromDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var toDate = DateTime.ParseExact(txtToDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (fromDate > toDate)
            {
                MessageBox.ShowMessageInLabel("From date should not be greater than to date!!!", lblMsg, MessageType.ErrorMessage);
                txtToDate.Focus();
                return false;
            }

            var empLeaveApplicationForId = int.Parse(ddEmployee.SelectedValue.ToString());

            //Rejected
            var isApplicationExist = false;
            using (_context = new HrEntities())
            {
                isApplicationExist = _context.EmpLeaveApplications
                .Any(la => la.EmpIdLeaveApplicationFor == empLeaveApplicationForId && la.ApplicationStatus != "Rejected"
                && ((fromDate >= la.FromDate && fromDate <= la.ToDate) || (toDate >= la.FromDate && toDate <= la.ToDate)));
            }
            if (isApplicationExist)
            {
                MessageBox.ShowMessageInLabel("From date or to date conflicts with existing application!!!", lblMsg, MessageType.ErrorMessage);
                return false;
            }

            if (ddLeaveType.SelectedValue=="3")//Sick Leave
            {
                if (Convert.ToDouble(txtDays.Text) > 1)
                {
                    string sMsg = "";
                    if (imgUpload.HasFile)
                    {
                        var imgSize = imgUpload.PostedFile.ContentLength;
                        var getext = Path.GetExtension(imgUpload.PostedFile.FileName);
                        if (getext != ".pdf" && getext != ".PDF")
                        {
                            sMsg = "Documents Format will be PDF!!!";
                            MessageBox.ShowMessageInLabel(sMsg, lblMsg, MessageType.ErrorMessage);
                            return false;
                        }
                    }
                    else
                    {
                        sMsg="Please Attach your Medical Documents!!!";
                        MessageBox.ShowMessageInLabel(sMsg, lblMsg, MessageType.ErrorMessage);
                        return false;
                    }
                }
            }



            return true;
        }

        protected void gdvLeaveApplication_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvLeaveApplication, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvLeaveApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new HrEntities();
            var id = int.Parse(((HiddenField)gdvLeaveApplication.Rows[gdvLeaveApplication.SelectedIndex].FindControl("hidGridLeaveAppIdId")).Value);
            var module = _context.EmpLeaveApplications.First(r => r.EmpLeaveApplicationId == id);
            hidLeaveApplication.Value = module.EmpLeaveApplicationId.ToString();
            ddEmployee.SelectedValue = module.EmpIdLeaveApplicationFor.ToString();
            ddLeaveType.SelectedValue = module.LeaveTypeId.ToString();
            txtReasonOfLeave.Text = module.ReasonOfLeave.ToString();

            txtFromDate.Text = module.FromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = module.ToDate.ToString("dd/MM/yyyy");
            if (module.IsPartialLeave == true)
            {
                dvFromTime.Attributes.Remove("class");
                dvFromTime.Attributes.Add("class", "form-group");
                dvToTime.Attributes.Remove("class");
                dvToTime.Attributes.Add("class", "form-group");
                chkIsPartialLeave.Checked = true;
                if (module.FromTime != null)
                    txtFromTime.Text = StringGenerator.TimespanToAmPmFormat(module.FromTime.Value);
                if (module.ToTime != null)
                    txtToTime.Text = StringGenerator.TimespanToAmPmFormat(module.ToTime.Value);
            }
            ddSupervisor.SelectedValue = module.SupervisorId.ToString();
            var dUrl = module.UpdateIp.ToString();
            hidURL.Value = dUrl;
            ifrmPiDoc.Src = Page.ResolveUrl("~/HrAttachment/" + dUrl);
            btnSave.Text = "Update";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsModuleValid()) return;
            lblMsg.Text = string.Empty;
            _context = new HrEntities();
            var fromDate = DateTime.ParseExact(txtFromDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var toDate = DateTime.ParseExact(txtToDate.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            
            

            if (hidLeaveApplication.Value.Trim() == "0")
            {
                filename = "";
                URL = "";
                if (imgUpload.HasFile)
                {
                    Guid g;
                    g = Guid.NewGuid();
                    filename = g.ToString();
                    filename = filename + ".pdf";
                    URL = "~/HrAttachment/" + filename;
                }
                var empLeaveApplication = new EmpLeaveApplication
                {
                    EmpIdLeaveApplicationFor = int.Parse(ddEmployee.SelectedValue),
                    LeaveTypeId = int.Parse(ddLeaveType.SelectedValue),
                    FromDate = fromDate,
                    ToDate = toDate,
                    SupervisorId = int.Parse(ddSupervisor.SelectedValue),
                    ReasonOfLeave = txtReasonOfLeave.Text.Trim(),
                    IsPartialLeave = chkIsPartialLeave.Checked,
                    ApplicationStatus = "Pending",
                    CreateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                    CreateTime = DateTime.Now,
                    CreateIp = Convert.ToString(Session[GlobalVariables.g_s_userIP]),
                    UpdateIp= filename
                };
                if (chkIsPartialLeave.Checked)
                {
                    empLeaveApplication.FromTime = DateTime.Parse(txtFromTime.Text).TimeOfDay;
                    empLeaveApplication.ToTime = DateTime.Parse(txtToTime.Text).TimeOfDay;
                }
                _context.EmpLeaveApplications.Add(empLeaveApplication);
                _context.SaveChanges();
                if (filename != "")
                {
                    imgUpload.SaveAs(Server.MapPath(URL));
                    ifrmPiDoc.Src = string.Empty;
                }
                var supervisor = _context.EmployeePersonalInfoes.FirstOrDefault(sup =>
                    sup.EmployeePersonalInfoId == empLeaveApplication.SupervisorId);
                var employee = _context.EmployeePersonalInfoes.FirstOrDefault(sup =>
                    sup.EmployeePersonalInfoId == empLeaveApplication.EmpIdLeaveApplicationFor);
                if (supervisor != null && employee != null)
                {
                    if (!string.IsNullOrEmpty(supervisor.OfficialMail))
                    {
                        var receivers = new List<string>
                        {
                            supervisor.OfficialMail
                        };
                        using (_context = new HrEntities())
                        {
                            var leaveStatus = _context.ProcRptLeaveSummary(
                            decimal.Parse(Session[GlobalVariables.g_s_CompanyAutoId].ToString()), employee.EmployeePersonalInfoId + "X%", DateTime.Now.Year);
                            var mailOptions = new MailSendingOption(receivers, null, employee.EmployeeName, "Leave application for approval", GenerateLeaveApprovalMail(leaveStatus.ToList(), supervisor.EmployeeName, employee.EmployeeName, ddLeaveType.SelectedItem.Text), null);
                            try
                            {
                                MailSender.SendMail(mailOptions);
                            }
                            catch (Exception exception)
                            {
                                // ignored
                                MessageBox.ShowMessageInLabel(exception.Message, lblMsg, MessageType.ErrorMessage);
                            }
                        }
                    }
                }

                ClearAll();
                MessageBox.ShowMessageInLabel("Successfully Send!!!", lblMsg, MessageType.ErrorMessage);
            }
            else
            {
                var id = int.Parse(hidLeaveApplication.Value);
                var empLeaveApplication = _context.EmpLeaveApplications.FirstOrDefault(r => r.EmpLeaveApplicationId == id);
                if (empLeaveApplication == null)
                {
                    MessageBox.ShowMessageInLabel("Application not found to update!!!", lblMsg, MessageType.ErrorMessage);
                }
                else
                {
                    hidURL.Value = empLeaveApplication.UpdateIp;
                    if (!string.IsNullOrEmpty(hidURL.Value))
                    {
                        var filePath = Server.MapPath("~/HrAttachment/" + hidURL.Value);
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                    }
                    filename = "";
                    URL = "";
                    if (imgUpload.HasFile)
                    {
                        Guid g;
                        g = Guid.NewGuid();
                        filename = g.ToString();
                        filename = filename + ".pdf";
                        URL = "~/HrAttachment/" + filename;
                    }

                    empLeaveApplication.EmpIdLeaveApplicationFor = int.Parse(ddEmployee.SelectedValue);
                    empLeaveApplication.LeaveTypeId = int.Parse(ddLeaveType.SelectedValue);
                    empLeaveApplication.FromDate = fromDate;
                    empLeaveApplication.ToDate = toDate;
                    empLeaveApplication.SupervisorId = int.Parse(ddSupervisor.SelectedValue);
                    empLeaveApplication.ReasonOfLeave = txtReasonOfLeave.Text.Trim();
                    empLeaveApplication.IsPartialLeave = chkIsPartialLeave.Checked;
                    empLeaveApplication.EmpIdLeaveApplicationFor = int.Parse(ddEmployee.SelectedValue);
                    empLeaveApplication.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                    empLeaveApplication.UpdateTime = DateTime.Now;
                    empLeaveApplication.UpdateIp = filename;// Convert.ToString(Session[GlobalVariables.g_s_userIP]);

                    if (chkIsPartialLeave.Checked)
                    {
                        empLeaveApplication.FromTime = DateTime.Parse(txtFromTime.Text).TimeOfDay;
                        empLeaveApplication.ToTime = DateTime.Parse(txtToTime.Text).TimeOfDay;
                    }
                    else
                    {
                        empLeaveApplication.FromTime = null;
                        empLeaveApplication.ToTime = null;
                    }
                    _context.SaveChanges();
                    if (filename != "")
                    {
                        imgUpload.SaveAs(Server.MapPath(URL));
                        ifrmPiDoc.Src = string.Empty;
                    }

                    ClearAll();
                    MessageBox.ShowMessageInLabel("Updated Done!!!", lblMsg, MessageType.SuccessMessage);
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private string GenerateLeaveApprovalMail(List<ProcRptLeaveSummary_Result> leaveSummaryResults, string supervisor, string applicant, string leaveType)
        {
            var baseUrl = string.Empty;
            if (Request.ApplicationPath != null)
            {
                baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                 Request.ApplicationPath.TrimEnd('/') + "/";
            }

            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sDays = string.Empty;
            string sReason = string.Empty;
            sFromDate = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            sToDate = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            sDays = txtDays.Text.ToString();
            sReason = txtReasonOfLeave.Text.ToString();

            var sb = new StringBuilder();
            sb.Append($@"
                    <p>Dear {supervisor},</p>
                    <p>{applicant} is seeking your approval for his/her {leaveType} application for the period from {sFromDate} to {sToDate} for {sDays} days.</p>
                    
                    <p>Leave status is below:</p>
                    <table style=""border-collapse: collapse; width: 400px; border: 1px solid #999999; background-color: #cce0ff;"">
                        <thead>
                            <tr style=""background-color: #99bbff;"">
                                <th style=""border: 1px solid #999999;"">Leave Type</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Limit</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Enjoyed</th>
                                <th style=""border: 1px solid #999999; width: 60px;"">Balance</th>
                            </tr>
                        </thead>
                        <tbody>");
                            foreach (var leaveSummaryResult in leaveSummaryResults)
                            {
                                sb.Append($@"<tr>
                                <td style=""border: 1px solid #999999;"">{leaveSummaryResult.leaveType}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Approved}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Taken}</td>
                                <td style=""border: 1px solid #999999; width: 60px; text-align: right;"">{leaveSummaryResult.Balance}</td>
                            </tr>");
                            }
                            sb.Append($@"</tbody>
                    </table>
                    <p>Note : {sReason}.</p>
                    <p><a href=""{baseUrl}"" target=""_blank"">Click here</a> to open software.</p>
                    <p> Developed by : <a href=""http://stmsoftwareltd.com/"" target=""_blank"">STM Software Ltd</a>.</p>
                    ");

            return sb.ToString();
        }

        protected void ddLeaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
            getLeaveBalance(ddEmployee.SelectedValue.ToString(),ddLeaveType.SelectedValue.ToString());
        }

        private void getLeaveBalance(string sEmpId,string leaveTypeId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            s_select = "ProcGetEmployeeLeaveBalanceLeaveTypewise '" + sEmpId + "','" + leaveTypeId + "','"+ Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("yyyy") + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtLeaveBalance.Text = drow["Balance"].ToString();
                    }
                }
            }


        }


    }
}