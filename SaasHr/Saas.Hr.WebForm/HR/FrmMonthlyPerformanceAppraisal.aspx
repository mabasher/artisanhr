﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmMonthlyPerformanceAppraisal.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmMonthlyPerformanceAppraisal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Monthly Performance Appraisal 
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <style>
            .date-picker {
                width: 100% !important;
            }
        </style>
        <div class="row row-custom">
            <div class="col-lg-12">
                <div class="row row-custom">
                    <div class="col-lg-12">
                        <fieldset>

                            <div class="form-horizontal">

                                <div class="form-group">
                                    <div class="col-sm-3">
                                    </div>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2 ">
                                        <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-8">
                                        <asp:TextBox runat="server" ID="txtDate" CssClass="input-sm date-picker" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="Month :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddMonth" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server" ></asp:DropDownList>
                                    </div>
                                    <asp:Label ID="Label2" runat="server" Text="Year :" CssClass="col-sm-1 control-label"></asp:Label>
                                    <div class="col-sm-1">
                                        <asp:DropDownList ID="ddYear" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server" ></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4" style="text-align: left; color: red;">
                                        <asp:Label ID="lblCounter" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-8">
                                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-xs" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                            CssClass="btn btn-default btn-xs" OnClick="btn_refresh_Click" Width="80px" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                                     <asp:Button ID="btnPreview" runat="server" CssClass="btn btn-primary btn-xs" Text="Preview" OnClick="btnPreview_Click" Width="80px" />
                                        <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-horizontal">
                                        <div class="form-group center">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div style="height: 300px; overflow: scroll;">

                                        <asp:GridView ID="gdv_Attendence" runat="server" Style="width: 100%; margin-left: 0;"
                                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                            CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                </asp:CommandField>


                                                <asp:TemplateField HeaderText="Performance Evaluations Parameter" ItemStyle-Width="28%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEvaluationsParameter" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("PerformanceEvaluationsParameter") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                        <asp:HiddenField ID="hidGridPerformanceEvaluationsParameterId" Value='<%# Bind("PerformanceEvaluationsParameterId") %>' runat="server" />
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Marks" ItemStyle-Width="6%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMarks" runat="server" CssClass="form-control input-sm" Enabled="false"
                                                            Text='<% # Eval("Marks") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Marks Out of" ItemStyle-Width="6%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSelfAssessmentMarks" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("MonthlyMarks") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Note" ItemStyle-Width="40%">

                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtSelfAssessmentNote" runat="server" CssClass="form-control input-sm"
                                                            Text='<% # Eval("Note") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox></td>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <PagerStyle CssClass="pagination-sa" />
                                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>



                            </div>

                        </fieldset>
                    </div>
                  
                </div>

            </div>
        </div>
        <div class="col-lg-12 hidden">
            <div class="row row-custom">
                <div class="col-md-8">
                    <div class="row row-custom">
                        <div class="col-md-12">

                            <div class="form-horizontal">

                                <div class="form-group hidden">
                                    <asp:Label ID="Label31" runat="server" Text="Transaction Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label1" runat="server" Text="In Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label5" runat="server" Text="Out Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmployeeTypeId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
    <asp:HiddenField ID="hidForAll" runat="server" Value="" />
    
        <asp:HiddenField ID="hidQryA" runat="server" Value="" />
        <asp:HiddenField ID="hidQryB" runat="server" Value="" />
        <asp:HiddenField ID="hidQryC" runat="server" Value="" />
        <asp:HiddenField ID="hidQryD" runat="server" Value="" />
        <asp:HiddenField ID="hidQryE" runat="server" Value="" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/GetAllEmp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        if (row.rowIndex % 2 == 0) {
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "orange";
            }
            else {
                if (checkbox.checked) {
                    objRef.style.backgroundColor = "aqua";
                }
                else if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        objRef.style.backgroundColor = "#C2D69B";
                    }
                    else {
                        objRef.style.backgroundColor = "white";
                    }
                }
            }
        }
    </script>
</asp:Content>
