﻿using System.Web.Optimization;
using System.Web.UI;

namespace Saas.Hr.WebForm
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
                            "~/Scripts/WebForms/WebForms.js",
                            "~/Scripts/WebForms/WebUIValidation.js",
                            "~/Scripts/WebForms/MenuStandards.js",
                            "~/Scripts/WebForms/Focus.js",
                            "~/Scripts/WebForms/GridView.js",
                            "~/Scripts/WebForms/DetailsView.js",
                            "~/Scripts/WebForms/TreeView.js",
                            "~/Scripts/WebForms/WebParts.js"));

            bundles.Add(new ScriptBundle("~/bundles/acescript").Include(
                "~/Scripts/chosen.jquery.js",
                "~/Scripts/inputmask/inputmask/inputmask.js",
                "~/Scripts/inputmask/inputmask/jquery.inputmask.js",
                "~/Scripts/inputmask/inputmask/inputmask.extensions.js",
                "~/Scripts/inputmask/inputmask/inputmask.date.extensions.js",
                "~/Scripts/ace-elements.js",
                "~/Scripts/ace.js",
                "~/Scripts/toastr.js",
                "~/Scripts/bootbox.min.js",
                "~/Scripts/jquery-ui-1.12.1.js",
                "~/Scripts/menu.js",
                "~/Scripts/StmSoft.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/acesextracript").Include(
                "~/Scripts/ace-extra.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/shivandrespond").Include(
                "~/Scripts/html5shiv.js",
                "~/Scripts/respond.js"
            ));

            // Order is very important for these files to work, they have explicit dependencies
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            // Use the Development version of Modernizr to develop with and learn from. Then, when you’re
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-*"));

            ScriptManager.ScriptResourceMapping.AddDefinition(
                "respond",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/respond.min.js",
                    DebugPath = "~/Scripts/respond.js",
                });
        }
    }
}