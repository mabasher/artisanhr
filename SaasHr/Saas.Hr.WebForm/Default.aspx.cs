﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Web.UI;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                BindChart();
                BindChart1();
            }
        }

        private void BindChart()
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {
                dsChartData = GetChartData();

                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']}); </script>  
                      
                    <script type='text/javascript'>  
                     
                    function drawChart() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['District', 'Amount'],");

                foreach (DataRow row in dsChartData.Rows)
                {
                    strScript.Append("['" + row["District"] + "'," + row["Amount"] + "],");
                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");
                string ttl = string.Empty;
                //ttl = "Department Wise Sales For Month Of " + Convert.ToDateTime(DateTime.Now, new CultureInfo("fr-FR")).ToString("MMMM, yyyy");
                ttl = "Department Wise Employee Share";
                //strScript.Append("title:'" + ttl + "'");
                strScript.Append(@" var options = {     
                                    title: '" + ttl + @"',            
                                    is3D: true,          
                                    };");

                strScript.Append(@"var chart = new google.visualization.PieChart(document.getElementById('chart_div'));          
                                chart.draw(data, options);        
                                }    
                            google.setOnLoadCallback(drawChart);  
                            ");
                strScript.Append(" </script>");

                ltScripts.Text = strScript.ToString();
            }
            catch
            {
            }
            finally
            {
                dsChartData.Dispose();
                //strScript.Clear();
            }
        }

        private DataTable GetChartData()
        {
            DataSet dsData = new DataSet();
            string strQry = string.Empty;
            strQry = "select isnull(d.Department,'N/A') as District,isnull(count(e.EmployeePersonalInfoId),0) as Amount from EmployeePersonalInfo e inner join EmployeeOfficialInfo o on e.EmployeePersonalInfoId=o.EmpAutoId inner join DepartmentInfo d on o.DepartmentAutoId=d.DepartmentInfoId group by d.Department order by d.Department asc";

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(strQry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dsData);
            sqlconnection.Close();

            return dsData.Tables[0];
        }

        private void BindChart1()
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {
                dsChartData = GetChartData1();

                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']}); </script>  
                      
                    <script type='text/javascript'>  
                     
                    function drawChart() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['District', 'Qty'],");

                foreach (DataRow row in dsChartData.Rows)
                {
                    strScript.Append("['" + row["District"] + "'," + row["Qty"] + "],");
                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");
                string ttl = string.Empty;
                ttl = "Attendance of " + Convert.ToDateTime(DateTime.Now, new CultureInfo("fr-FR")).ToString("MMMM,dd yyyy");
                //strScript.Append("title:'" + ttl + "'");
                strScript.Append(@" var options = {     
                                    title: '" + ttl + @"',            
                                    is3D: true,          
                                    };");

                strScript.Append(@"var chart = new google.visualization.BarChart(document.getElementById('chart_div1'));          
                                chart.draw(data, options);        
                                }    
                            google.setOnLoadCallback(drawChart);  
                            ");
                strScript.Append(" </script>");

                ltScripts1.Text = strScript.ToString();
            }
            catch
            {
            }
            finally
            {
                dsChartData.Dispose();
                //strScript.Clear();
            }
        }

        private DataTable GetChartData1()
        {
            DataSet dsData = new DataSet();
            string strQry = "ProchrDashboardWeb '1','" + DateTime.Now.ToString("MM/dd/yyyy") + "'";
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(strQry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dsData);
            sqlconnection.Close();

            return dsData.Tables[0];
        }





        private void SetValueInBox()
        {
            var fn = new CommonFunctions();
            var data = fn.GetDataSetFromSp("ProcGetCountOfData");
            if (data.Rows.Count > 0)
            {
                //lblContractCount.Text = data.Rows[0]["TotalContract"].ToString();
                //lblConfirmOrderCount.Text = data.Rows[0]["TotalOrder"].ToString();
                //lblCostingCount.Text = data.Rows[0]["TotalCosting"].ToString();
                //lblPICount.Text = data.Rows[0]["TotalProformaInvoice"].ToString();
                //lblBtBLcCount.Text = data.Rows[0]["TotalBTBLc"].ToString();
                //lblMasterLcCount.Text = data.Rows[0]["TotalMasterLc"].ToString();
                //lblAcceptanceCount.Text = data.Rows[0]["TotalAcceptance"].ToString();
                //lblRealizationCount.Text = data.Rows[0]["TotalRealization"].ToString();
            }
        }
    }
}