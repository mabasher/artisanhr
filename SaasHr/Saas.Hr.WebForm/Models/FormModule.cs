﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Saas.Hr.WebForm.Models
{
    public class FormModule
    {
        public int FormId { get; set; }
        public int ModuleId { get; set; }
    }
}