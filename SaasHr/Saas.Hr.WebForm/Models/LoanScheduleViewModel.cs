﻿using System;

namespace Saas.Hr.WebForm.Models
{
    public class LoanScheduleViewModel
    {
        public int LoanId { get; set; }
        public int LoanScheduleId { get; set; }
        public DateTime ScheduledPaymentDate { get; set; }
        public decimal PrincipleAmount { get; set; }
        public decimal InterestAmount { get; set; }
        public decimal VatAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalAmount => PrincipleAmount + InterestAmount + VatAmount + TaxAmount;
    }
}