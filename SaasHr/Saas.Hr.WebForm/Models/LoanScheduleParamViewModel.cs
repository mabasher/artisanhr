﻿using System;
using Saas.Hr.WebForm.Enum;

namespace Saas.Hr.WebForm.Models
{
    public class LoanScheduleParamViewModel
    {
        public EnumLoanDeductionType DeductionType { get; set; }
        public EnumDurationType LoanDurationType { get; set; }
        public int LoanDuration { get; set; }
        public EnumDurationType InstallmentPeriodType { get; set; }
        public int InstallmentPeriod { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal InterestRate { get; set; }
        public DateTime InstallmentStartDate { get; set; }
    }
}