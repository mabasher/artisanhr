﻿using System;

namespace Saas.Hr.WebForm.Models
{
    public class EmpSigninModel
    {
        public int EmployeeId { get; set; }
        public int CompanyId { get; set; }
        public string EmpName { get; set; }
        public string EmpEmail { get; set; }
        public Guid Password { get; set; }
    }
}