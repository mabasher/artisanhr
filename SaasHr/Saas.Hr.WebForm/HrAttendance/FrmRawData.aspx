﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmRawData.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmRawData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Card Access
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

         <div class="row row-custom">
                <div class="col-md-12">
                    <div class="form-horizontal">
                            <div class="form-group">
                                
                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger" Font-Bold="true" Font-Size="Large"></asp:Label>
                                <asp:Label ID="Label2" runat="server" Text="Access Date :" CssClass="col-sm-5 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                </div>
                                 <div class="col-sm-2">
                                      <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Find" OnClick="btn_save_Click" Width="80px" />
                               </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label4" runat="server" Text="To Date :" CssClass="col-sm-5 control-label"></asp:Label>
                                <div class="col-sm-2">
                                      <asp:TextBox ID="txtToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                               </div>    
                                
                            </div>
                                                
                            <div class="form-group center hidden">
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false" CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-horizontal">
                            <div class="main-data-grid" style="overflow: scroll;text-align:center;">

                                <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                    CssClass="table table-striped table-bordered"
                                  OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                                    EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                        </asp:CommandField>
                                        
                                        <asp:TemplateField HeaderText="PIN" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridPIN" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Card No" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridCard" runat="server" Text='<%# Bind("Cardno") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Employee Name" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridempName" runat="server" Text='<%# Bind("empName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Designation" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGriddesignation" runat="server" Text='<%# Bind("designation") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Department" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGriddepartment" runat="server" Text='<%# Bind("department") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Access Date" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridDate" runat="server" Text='<%# Bind("trDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Access Time" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridTime" runat="server" Text='<%# Bind("trTime","{0:hh:mm:ss tt }") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerStyle CssClass="pagination-sa" />
                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                </asp:GridView>
                            </div>
                    </div>
                </div>                               
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

</asp:Content>
