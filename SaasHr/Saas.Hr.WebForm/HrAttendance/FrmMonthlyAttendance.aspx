﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmMonthlyAttendance.aspx.cs" Inherits="Saas.Hr.WebForm.HrAttendance.FrmMonthlyAttendance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>



<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Monthly Attendance and Approval
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
        <div>
            <div class="row row-custom">

                <div class="col-lg-12">
                    <fieldset>
                        <legend>Employee</legend>
                        <div class="row row-custom">
                            <div class="col-lg-12">
                                <div class="form-horizontal">
                                    <div class="form-group">                                        
                                        <div class="col-sm-2" style="margin-left:50px;">
                                        <asp:Label ID="Label9" runat="server" Text="Department :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-2">
                                        <asp:Label ID="Label10" runat="server" Text="Section :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-2">
                                        <asp:Label ID="Label11" runat="server" Text="Line :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-2">
                                        <asp:Label ID="Label12" runat="server" Text="Employee :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-2">
                                        <asp:Label ID="Label21" ForeColor="Red" runat="server" Text="Month :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label22" ForeColor="Red" runat="server" Text="Year :" CssClass="control-label"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">    
                                        <div class="col-sm-2" style="margin-left:50px;">
                                            <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 ">
                                            <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>                                        
                                        <div class="col-sm-2 ">
                                            <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 ">
                                            <asp:DropDownList ID="ddEmployee" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 ">
                                            <asp:DropDownList ID="ddMonth" AutoPostBack="true" onselectedindexchanged="ddMonth_SelectedIndexChanged"
                                                CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        </div>                                        
                                        <div class="col-sm-1">
                                             <asp:DropDownList ID="txtYear"
                                                CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server">
                                            </asp:DropDownList>                                          
                                        </div>
                                    </div>
                                                                        
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label8" runat="server" Text="" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label13" runat="server" Text="Present :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label14" runat="server" Text="Late :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label15" runat="server" Text="Weekend :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label16" runat="server" Text="Leave :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label18" runat="server" Text="Without pay :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label19" runat="server" Text="Govt. :" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label20" runat="server" Text="Absent :" CssClass="control-label"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">                                          
                                        <div class="col-sm-1">
                                        <asp:Label ID="Label17" runat="server" Text="" CssClass="control-label"></asp:Label>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtPresent" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtPresent" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtlate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender2" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtlate" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtweekend" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender3" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtweekend" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtleave" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender4" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtleave" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtwp" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender5" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtwp" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtgl" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender6" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtgl" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtabsent" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            <ajx:filteredtextboxextender id="FilteredTextBoxExtender7" runat="server" filtertype="Numbers,Custom"
                                        validchars="." targetcontrolid="txtabsent" />
                                        </div>
                                                                               
                                        <div class="col-sm-2 left">
                                            <asp:LinkButton ID="lnkbtnAdd" runat="server" OnClick="lnkbtnAdd_Click" CssClass="btn btn-warning btn-xs" Text="Add" Width="60px"></asp:LinkButton>
                                            <%--<asp:LinkButton ID="lnkbtnRefresh" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-default btn-xs" Text="Refresh" Width="60px"></asp:LinkButton>--%>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            </div>
                       
                    </fieldset>
                </div>
                
                <div class="col-md-6 hidden ">

                    <fieldset>
                        <legend>Employee Wise Attendence</legend>                        
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-2 ">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label31" runat="server" Text="Transaction Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                                </div>
                            </div>

                             <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="In Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                </div>  
                             </div>
                            
                             <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Out Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                     <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                               </div>  
                             </div>

                         </div>
                    </fieldset>
                </div>

               
                <div class="col-md-6">
                      <fieldset>
                        <legend>-</legend>
                            <div class="form-group center">

                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Approve" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btn_delete_Click" Width="80px" />
                            
                            </div>
                          </fieldset>
                </div>

                <div class="col-md-6">
                    <fieldset>
                        <legend>&nbsp;</legend>
                        <div class="form-horizontal">
                            <div class="form-group" >
                                <asp:Label ID="Label2" runat="server" Text="From Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox runat="server" ID="txtSearchFromDate" Enabled="false" CssClass="input-sm date-picker" />
                                </div>
                                <asp:Label ID="Label3" runat="server" Text="To Date :" CssClass="col-sm-2 control-label"></asp:Label>

                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtSearchToDate" runat="server" Enabled="false" CssClass="input-sm date-picker"></asp:TextBox>
                                </div>
                                <div class="col-sm-3 center">
                                    <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                                </div>
                            </div>                            
                        </div>
                    </fieldset>
                </div>

                <div class="col-sm-12">
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div style="height: 350px; overflow: scroll;">
                        <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                            CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                </asp:CommandField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PIN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("EmpName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Department">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Designation">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Present">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridPresent" runat="server" Text='<%# Bind("present") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Late">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridLate" runat="server" Text='<%# Bind("late") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="WO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridWeekend" runat="server" Text='<%# Bind("weekEndOff") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Leave">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridLeave" runat="server" Text='<%# Bind("leaveWithPay") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="WP">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridWP" runat="server" Text='<%# Bind("leaveWithoutPay") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridGL" runat="server" Text='<%# Bind("govtleave") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                                                
                                <asp:TemplateField HeaderText="CL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridCL" runat="server" Text='<%# Bind("CL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridEL" runat="server" Text='<%# Bind("EL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridSL" runat="server" Text='<%# Bind("SL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Absent">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridAbsent" runat="server" Text='<%# Bind("absent") %>'></asp:Label>                                        
                                        <asp:HiddenField ID="hidempAutoId" Value='<%# Bind("empAutoId") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Total">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridTotalDay" runat="server" Text='<%# Bind("TotalDay") %>'></asp:Label> 
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Year">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridYear" runat="server" Text='<%# Bind("salaryYear") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Month">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridMonth" runat="server" Text='<%# Bind("salaryMonthName") %>'></asp:Label>                                       
                                        <asp:HiddenField ID="hidGridMonthId" Value='<%# Bind("salaryMonth") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                              
                            </Columns>
                            <PagerStyle CssClass="pagination-sa" />
                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                        </asp:GridView>
                    </div>
                </div>


            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
        <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
        <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
        <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
        <asp:HiddenField ID="hidDaysInMonth" runat="server" Value="0" />
        <asp:HiddenField ID="hidInputdays" runat="server" Value="0" />
        <asp:HiddenField ID="hidtrStatus" runat="server" Value="N" />
 
        <asp:HiddenField ID="hidpresent" runat="server" Value="0" />
        <asp:HiddenField ID="hidlate" runat="server" Value="0" />
        <asp:HiddenField ID="hidweekEndOff" runat="server" Value="0" />
        <asp:HiddenField ID="hidleaveWithPay" runat="server" Value="0" />
        <asp:HiddenField ID="hidleaveWithoutPay" runat="server" Value="0" />
        <asp:HiddenField ID="hidgovtleave" runat="server" Value="0" />
        <asp:HiddenField ID="hidabsent" runat="server" Value="0" />
       

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>
</asp:Content>
