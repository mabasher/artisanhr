﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmLateApproval.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmLateApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Late Approval
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-sm-6">
            <fieldset>
                <legend>Late Approval</legend>
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-3 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Date :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-2">
                                <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                            </div>
                          <%--  <asp:Label ID="Label5" runat="server" Text="To Date :" CssClass="col-sm-2 control-label"></asp:Label>

                            <div class="col-sm-2">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                            </div>--%>

                            <div class="col-sm-3 center">
                                <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                            </div>

                        </div>
                    </div>

                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        </div>
                    </div>

                </div>
            </fieldset>
        </div>

        <div class="col-sm-12">
            <div class="form-horizontal">
                <div class="form-group center">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div style="height: 300px; overflow: scroll;">
                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                    CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound"  EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                        </asp:CommandField>
                        
                         <asp:TemplateField ItemStyle-Width="2%">
                            <%--<HeaderTemplate>
                                <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                            </HeaderTemplate>--%>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkApproval" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Emp. Id">
                            <ItemTemplate>
                                <asp:Label ID="lblGridempId" runat="server" Text='<%# Bind("empId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblGridempName" runat="server" Text='<%# Bind("empName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Designation">
                            <ItemTemplate>
                                <asp:Label ID="lblGriddesignation" runat="server" Text='<%# Bind("designation") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Access Date">
                            <ItemTemplate>
                                <asp:Label ID="lblGridaccessDate" runat="server" Text='<%# Bind("accessDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       

                        <asp:TemplateField HeaderText="In Time">
                            <ItemTemplate>
                                 <asp:Label ID="lblGridinTime" runat="server" Text='<%# Bind("inTime") %>'></asp:Label>
                                <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                <asp:HiddenField ID="hidLateStatus" Value='<%# Bind("lateStatus") %>' runat="server" />
                                  </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="standardAccessTime">
                            <ItemTemplate>
                                <asp:Label ID="lblGridstandardAccessTime" runat="server" Text='<%# Bind("standardAccessTime") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="pagination-sa" />
                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                </asp:GridView>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
        <asp:HiddenField ID="hid_Rownumber" runat="server" Value="0" />
        <asp:HiddenField ID="hidLateStatus" runat="server" Value="" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>

</asp:Content>
