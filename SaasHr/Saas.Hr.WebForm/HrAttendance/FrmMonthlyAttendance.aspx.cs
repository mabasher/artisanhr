﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrAttendance
{
    public partial class FrmMonthlyAttendance : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
                txtToTime.Text = DateTime.Now.ToString("hh:mm:ss tt");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownListMultiColumn(ddEmployee,
                    "EmployeePersonalInfo",
                    "PIN",
                    "EmployeeName",
                    "EmployeePersonalInfoId", string.Empty);                
                commonfunctions.g_b_FillDropDownList(ddMonth,
                    "T_Month",
                    "MonthName",
                    "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");
                ddMonth.SelectedValue = DateTime.Now.Month.ToString();

                int cy = DateTime.Today.Year - 10;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                txtYear.DataSource = years;
                txtYear.DataBind();

            }
        }
        
        private void v_loadGridView_CostingHead()
        {            
            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            if (hidtrStatus.Value == "Y")
            {
                if (Convert.ToInt32(hidInputdays.Value) != Convert.ToInt32(hidDaysInMonth.Value))
                { return; }
            }

            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "ProcMonthlyAttandanceForSalary '" + hidEmpAutoId.Value + "','" + hidDepartmentId.Value + "','" + ddMonth.SelectedValue + "','" + txtYear.SelectedItem.Text + "','" + Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','"+ Convert.ToDateTime(txtSearchToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','"+ SessionCompanyId + "','" + hidpresent.Value + "','" + hidabsent.Value + "','" + hidlate.Value + "','" + hidleaveWithPay.Value + "','" + hidleaveWithoutPay.Value + "','" + hidweekEndOff.Value + "','" + hidgovtleave.Value + "','" + hidtrStatus.Value + "'";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            cmd.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;             
            gdv_costingHead.DataBind();
            sqlconnection.Close();            
        }        
        protected void btn_delete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            s_save_ = "";
            foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            {
                string s_empAutoId = string.Empty;
                string s_salaryMonth = string.Empty;
                string s_salaryYear = string.Empty;
                string s_fromDate = string.Empty;
                string s_toDate = string.Empty;
                s_salaryMonth = ddMonth.SelectedValue;
                s_salaryYear = txtYear.SelectedItem.Text;
                s_empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hidempAutoId")).Value;
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
                if (checkbox.Checked == true)
                {
                    s_save_ = s_save_ + " DELETE AttendanceMonthlyData WHERE empAutoId='"
                                            + s_empAutoId
                                            + "' and salaryMonth='"
                                            + s_salaryMonth
                                            + "' and salaryYear='"
                                            + s_salaryYear
                                            + "'";

                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
            ///Insert Option

        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            s_save_ = "";
            foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            {
                string s_empAutoId = string.Empty;
                string s_salaryMonth = string.Empty;
                string s_salaryYear = string.Empty;
                string s_fromDate = string.Empty;
                string s_toDate = string.Empty;
                string s_present = string.Empty;
                string s_late = string.Empty;
                string s_weekEndOff = string.Empty;
                string s_leaveWithPay = string.Empty;
                string s_leaveWithoutPay = string.Empty;
                string s_absent = string.Empty;
                string s_userId = string.Empty;
                string s_govtleave = string.Empty;
                string s_CL = string.Empty;
                string s_EL = string.Empty;
                string s_SL = string.Empty;

                s_salaryMonth = ddMonth.SelectedValue;
                s_salaryYear = txtYear.SelectedItem.Text;
                s_empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hidempAutoId")).Value;
                s_present = ((Label)gdvRow.Cells[0].FindControl("lblGridPresent")).Text;
                s_late = ((Label)gdvRow.Cells[0].FindControl("lblGridLate")).Text;
                s_weekEndOff = ((Label)gdvRow.Cells[0].FindControl("lblGridWeekend")).Text;
                s_leaveWithPay = ((Label)gdvRow.Cells[0].FindControl("lblGridLeave")).Text;
                s_leaveWithoutPay = ((Label)gdvRow.Cells[0].FindControl("lblGridWP")).Text;
                s_absent = ((Label)gdvRow.Cells[0].FindControl("lblGridAbsent")).Text;
                s_govtleave = ((Label)gdvRow.Cells[0].FindControl("lblGridGL")).Text;
                s_CL = ((Label)gdvRow.Cells[0].FindControl("lblGridCL")).Text;
                s_EL = ((Label)gdvRow.Cells[0].FindControl("lblGridEL")).Text;
                s_SL = ((Label)gdvRow.Cells[0].FindControl("lblGridSL")).Text;
                s_userId = SessionUserId;

                s_save_ = s_save_ + " DELETE AttendanceMonthlyData WHERE empAutoId='"
                                        + s_empAutoId
                                        + "' and salaryMonth='"
                                        + s_salaryMonth
                                        + "' and salaryYear='"
                                        + s_salaryYear
                                        + "'";
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
                if (checkbox.Checked == true)
                {
                    s_save_ = s_save_ + "INSERT INTO AttendanceMonthlyData VALUES ((select isnull(max(AutoId),0)+1 From AttendanceMonthlyData), '" + s_empAutoId + "','" 
                                        + s_salaryMonth
                                        + "','" 
                                        + s_salaryYear
                                        + "','"
                                        + Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                        + "','"
                                        + Convert.ToDateTime(txtSearchToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                        + "','"
                                        + s_present
                                        + "','"
                                        + s_late
                                        + "','"
                                        + s_weekEndOff
                                        + "','"
                                        + s_leaveWithPay
                                        + "','"
                                        + s_leaveWithoutPay
                                        + "','"
                                        + s_absent
                                        + "','"
                                        + s_userId
                                        + "','"
                                        + s_govtleave
                                        + "','"
                                        + s_CL
                                        + "','"
                                        + s_EL
                                        + "','"
                                        + s_SL
                                        + "')";

                     }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    //lblMsg.Visible = true;
                    //lblMsg.Text = "Successfully Done!";
                    //InsertMode();
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
            ///Insert Option

        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            hidEmpAutoId.Value = "%";
            hidDepartmentId.Value = "%";
            hidSectionId.Value = "%";
            hidLineId.Value = "%";
            if (ddEmployee.SelectedValue != "0") { hidEmpAutoId.Value = ddEmployee.SelectedValue + "X%"; }
            if (ddDepartment.SelectedValue != "0") { hidDepartmentId.Value = ddDepartment.SelectedValue + "X%"; }
            if (ddSection.SelectedValue != "0") { hidSectionId.Value = ddSection.SelectedValue + "X%"; }
            if (ddLine.SelectedValue != "0") { hidLineId.Value = ddLine.SelectedValue + "X%"; }
            hidpresent.Value = "0";
            hidlate.Value = "0";
            hidweekEndOff.Value = "0";
            hidleaveWithPay.Value = "0";
            hidleaveWithoutPay.Value = "0";
            hidgovtleave.Value = "0";
            hidabsent.Value = "0";
            hidInputdays.Value = "0";

            if (txtPresent.Text != ""){ hidpresent.Value = txtPresent.Text; }
            if (txtlate.Text != "") {hidlate.Value = txtlate.Text; }
            if (txtweekend.Text != ""){ hidweekEndOff.Value = txtweekend.Text; }
            if (txtleave.Text != "") { hidleaveWithPay.Value = txtleave.Text; }
            if (txtwp.Text != "") { hidleaveWithoutPay.Value = txtwp.Text; }
            if (txtgl.Text != "") { hidgovtleave.Value = txtgl.Text; }
            if (txtabsent.Text != "") { hidabsent.Value = txtabsent.Text; }

            hidInputdays.Value = Convert.ToString(Convert.ToInt32(hidpresent.Value)+ Convert.ToInt32(hidweekEndOff.Value)+ Convert.ToInt32(hidleaveWithPay.Value)+ Convert.ToInt32(hidleaveWithoutPay.Value)+ Convert.ToInt32(hidgovtleave.Value) + Convert.ToInt32(hidabsent.Value));
            hidDaysInMonth.Value=Convert.ToString(DateTime.DaysInMonth(Convert.ToInt32(txtYear.SelectedItem.Text), Convert.ToInt32(ddMonth.SelectedValue)));
            
            //if (Convert.ToInt32(hidInputdays.Value) != Convert.ToInt32(hidDaysInMonth.Value))
            //{ return false; } 

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFromDate.Text = "";
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        hidCardid.Value= drow["CardNo"].ToString();
                    }
                }
            }
                       
        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            hidtrStatus.Value = "N";
            v_loadGridView_CostingHead();
        }

        protected void lnkbtnAdd_Click(object sender, EventArgs e)
        {
            hidtrStatus.Value = "Y";
            v_loadGridView_CostingHead();
        }

        protected void ddMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            dateChange();
        }
        protected void txtYear_TextChanged(object sender, EventArgs e)
        {
            dateChange();
        }

        private void dateChange()
        {
            txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            string sMonth = string.Empty;
            string stoday = string.Empty;
            sMonth = ddMonth.SelectedValue.ToString();
            if (sMonth.Length < 2) { sMonth = "0" + sMonth; }
            txtSearchFromDate.Text = "01/" + sMonth +"/"+ txtYear.SelectedItem.Text.ToString();
            stoday = (DateTime.DaysInMonth(Convert.ToInt32(txtYear.SelectedItem.Text), Convert.ToInt32(sMonth))).ToString();
            if (stoday.Length < 2) { stoday = "0" + stoday; }
            txtSearchToDate.Text = stoday + "/" + sMonth +"/"+ txtYear.SelectedItem.Text.ToString();
        }
        //===========End===============         
    }
}