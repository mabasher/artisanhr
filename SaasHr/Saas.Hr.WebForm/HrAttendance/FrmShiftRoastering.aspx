﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmShiftRoastering.aspx.cs" Inherits="Saas.Hr.WebForm.HrAttendance.FrmShiftroastering" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Shift Rostering
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    

        <div>
            <div class="row row-custom">
               
                <div class="col-lg-12">
                    <div class="form-horizontal">
                        
                        <div class="form-group">
                            <div class="col-sm-2">
                                <asp:Label ID="Label27" runat="server" Text="Department :" CssClass="control-label"></asp:Label>
                            </div>                            
                            <div class="col-sm-2">
                                <asp:Label ID="Label7" runat="server" Text="Section :" CssClass="control-label"></asp:Label>
                            </div>                            
                            <div class="col-sm-1">
                                <asp:Label ID="Label8" runat="server" Text="Line :" CssClass="control-label"></asp:Label>
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="Label5" runat="server" Text="From Shift :" CssClass="control-label"></asp:Label>
                            </div>
                            <div class="col-sm-1">
                                <asp:Label ID="Label6" runat="server" Text="" CssClass="control-label"></asp:Label>
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="Label10" runat="server" Text="To Shift :" CssClass="control-label"></asp:Label>
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="Label9" runat="server" Text="Efective Date :" CssClass="control-label"></asp:Label>
                            </div>
                        </div>                        
                        <div class="form-group">                            
                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>                                              
                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>  
                            <div class="col-sm-1">
                                <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddShiftFrom" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                            <div class="col-sm-1">
                                    <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                            </div>
                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddShiftTo" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                            <div class="col-sm-2">
                                 <asp:TextBox runat="server" ID="txtFromDate" CssClass="date-picker" />
                             </div>
                        </div>
                        

                    </div>
                </div>
                            
                                                
                    <div class="col-md-12">
                        <div style="height: 380px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered"  EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>                                    
                                    <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkEmpSelect" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PIN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridPIN" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CardNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("CardNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEmployeeName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ShiftName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridShiftName" runat="server" Text='<%# Bind("ShiftName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ShiftChangeDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridShiftChangeDate" runat="server" Text='<%# Bind("ShiftChangeDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            <asp:HiddenField ID="hidGridEmployeePersonalInfoId" Value='<%# Bind("EmployeePersonalInfoId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
         
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                            <asp:Button ID="btn_Delete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btn_Delete_Click" Width="80px" />
                            <asp:CheckBox ID="chkRoasterChecker" Text="Roaster Checking" runat="server"  AutoPostBack="True" oncheckedchanged="chkRoasterChecker_CheckedChanged" />
                        </div>
                    </div>


                                   
                <div id="IdroasterIdEnable" runat="server" class="col-lg-12">
                    <div class="form-horizontal">     
                        
                            <div class="form-group">      
                                
                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddEmployee" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                            <div class="col-sm-1">
                                    <asp:LinkButton ID="lnkBtnforRpaster" runat="server" OnClick="lnkBtnforRpaster_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                            </div>  

                            <div class="col-md-8">
                                <div style="height: 380px; overflow: scroll;">
                                    <asp:GridView ID="gdv_ShiftRoaster" runat="server" Style="width: 100%; margin-left: 0;"
                                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                        CssClass="table table-striped table-bordered"  EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                            </asp:CommandField>                                    
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkEmpSelectdelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PIN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridPIN" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CardNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("CardNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridEmployeeName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Designation">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ShiftName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridShiftName" runat="server" Text='<%# Bind("ShiftName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ShiftChangeDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridShiftChangeDate" runat="server" Text='<%# Bind("ShiftChangeDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                    <asp:HiddenField ID="hidGridRoasterAutoId" Value='<%# Bind("RoasterAutoId") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pagination-sa" />
                                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                    </asp:GridView>
                                </div>
                            </div>                 
                                   

                    </div>
              </div>
                </div>

        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidSectionId" runat="server" Value="" />
        <asp:HiddenField ID="hidLineId" runat="server" Value="" />
        <asp:HiddenField ID="hidDepartmentId" runat="server" Value="" />
        <asp:HiddenField ID="hidShiftFromId" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpId" runat="server" Value="" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
        
    <script type="text/javascript">
        
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //row.style.backgroundColor = "gray";
                        inputList[i].checked = true;
                    }
                    else {
                        //if (row.rowIndex % 2 == 0) {
                        //    row.style.backgroundColor = "#C2D69B";
                        //}
                        //else {
                        //    row.style.backgroundColor = "white";
                        //}
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "orange";
            }
            else {
                if (checkbox.checked) {
                    objRef.style.backgroundColor = "aqua";
                }
                else if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        objRef.style.backgroundColor = "#C2D69B";
                    }
                    else {
                        objRef.style.backgroundColor = "white";
                    }
                }
            }
        }
    </script>
</asp:Content>
