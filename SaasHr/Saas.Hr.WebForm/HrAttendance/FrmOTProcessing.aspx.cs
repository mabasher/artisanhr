﻿using CrystalDecisions.CrystalReports.Engine;
using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmOverTimeProcessing : BasePage
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtForDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);

                if (Convert.ToString(Session[GlobalVariables.g_s_WinMode]) == "N")
                {
                    chkExtra.Visible = false;
                    chkExtra.Checked = false;
                }

                v_loadGridView_CostingHead();
            }
        }

        protected void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            qry = "select distinct  top 60 isnull(fd.oDate,'') as accessDate from  OverTimeWorker fd  order by isnull(fd.oDate,'') desc";

            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();
            sqlconnection.Close();
        }
        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }
        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                txtForDate.Text = ((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGridDate")).Text;

            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_breakTime = string.Empty;
            s_breakTime = "N";
            if (chkTimeBreak.Checked)
            {
                s_breakTime = "Y";
            }

            s_save_ = "[ProcWorkerOTCalculation]"
                        + "'"
                        + Convert.ToDateTime(txtForDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + SessionCompanyId + "','" + s_breakTime + "'";

            s_save_ = s_save_ + " Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;

            s_save_ = "DELETE dbo.OverTimeWorker where oDate ="
                        + "'"
                        + Convert.ToDateTime(txtForDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "'";

            s_save_ = s_save_ + " Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
                string s_sCompId = string.Empty;
                string s_deptId = string.Empty;
                string s_sectionId = string.Empty;
                string s_lineId = string.Empty;
                string sAddress = string.Empty;
                string sPeriod = string.Empty;
                string sCompanyName = string.Empty;
                string s_Dummy = string.Empty;
                string strQry = string.Empty;
                string rptURL = string.Empty;
                string rptName = string.Empty;
                string procSql = string.Empty;
                var dic = new Dictionary<string, string> { };
                Session[GlobalVariables.g_s_printopt] = "P";
                string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
                s_sCompId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                sCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
                sAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);
                sPeriod = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM dd yyyy");
                sPeriod = sPeriod + " - " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM dd yyyy");
                s_deptId = "%";
                s_sectionId = "%";
                s_lineId = "%";
                if (ddDepartment.SelectedValue != "0") { s_deptId = ddDepartment.SelectedValue + "X%"; }
                if (ddSection.SelectedValue != "0") { s_sectionId = ddSection.SelectedValue + "X%"; }
                if (ddLine.SelectedValue != "0") { s_lineId = ddLine.SelectedValue + "X%"; }

                CommonFunctions fn = new CommonFunctions();

                procSql = "Proc_Rpt_Worker_Daily_OT_BGMEA";
                if (chkExtra.Checked)
                {
                    procSql = "Proc_Rpt_Worker_Daily_OT_BGMEA_ExtraOT";
                }

                rptURL = "~\\ReportFiles\\RptDailyWorkerOT.rpt";
                rptName = "Over Time Sheet";

                dic = new Dictionary<string, string>
                {
                    {"FromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                    {"ToDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                    {"DepartmentId", s_deptId},
                    {"SectionId", s_sectionId},
                    {"LineId", s_lineId},
                    {"companyAutoId", s_sCompId},
                    {"trActual", s_trMode},
                    {"chkOfficer", "N"}
                 };

                var rptData = fn.GetDataSetFromSp(procSql, dic);
                var rpt = new ReportDocument();
                rpt.Load(Server.MapPath(rptURL));
                rpt.SetDataSource(rptData);
                rpt.SetParameterValue("CompanyName", sCompanyName);
                rpt.SetParameterValue("ComAddress", sAddress);
                rpt.SetParameterValue("Period", sPeriod);
                Session["rpt"] = rpt;
                OpenGnReport();
            }

            catch (Exception)
            {

            }
        }


        ///////================
    }
}