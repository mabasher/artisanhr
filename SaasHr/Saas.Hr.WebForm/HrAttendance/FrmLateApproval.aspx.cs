﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmLateApproval : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);


                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                v_loadGridView_CostingHead();

                DataTable myDt = new DataTable();
                myDt = CreateDataTable();
                ViewState["myDatatable"] = myDt;
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
          
            SqlCommand cmd = new SqlCommand("SELECT autoId as AutoId,cardId as empId, accessDate as accessDate,inTime as inTime,standardAccessTime as standardAccessTime,lateStatus as lateStatus FROM AttendaceFilteredData Where autoId='0' ", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty; ;
            string s_saveDetails = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //s_Update = "[ProcDailyAttandenceINSERT]"
                //        + "'"
                //        + hidNomineeId.Value
                //        + "','"
                //        + hidEmpAutoId.Value
                //        + "','"
                //        + SessionCompanyId
                //        + "','0','','"
                //        + SessionUserId
                //        + "','"
                //        + SessionUserIP
                //        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }
            else
            {


                //s_save_ = "[ProcDailyAttandenceINSERT]"
                //        + "'"
                //        + txtCard.Text.Trim()
                //        + "','"
                //        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                //        + "','"
                //        + HttpUtility.HtmlDecode(txtCard.Text.Trim())
                //        + "'";                
                //s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                /// Late Approved Insert
                foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
                {
                    CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                    if (checkbox.Checked == true)
                    {
                        string autoId = string.Empty;
                        autoId = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;

                        s_saveDetails = s_saveDetails + "update AttendaceFilteredData set lateStatus= 'Y' where autoId='" + autoId + "' ";
                    }
                }
               s_saveDetails = s_saveDetails + "Select ''";
                s_returnValue = connection.connection_DB(s_saveDetails, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;

                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }


            //if (imgUpload.HasFile)
            //{
            //    int imgSize = imgUpload.PostedFile.ContentLength;
            //    string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
            //    if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
            //    {
            //        lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //    else if (imgSize > 800000)//1048576)//1048576 bit=1MB
            //    {
            //        lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //}


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            //txtNomineeName.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            //txtPresentAddress.Text = string.Empty;
            //txtPermanentAddress.Text = string.Empty;
            //txtFather.Text = string.Empty;
            //txtMother.Text = string.Empty;
            //txtNationalId.Text = string.Empty;
            //txtPassport.Text = string.Empty;
            //txtBirthCertificate.Text = string.Empty;
            //txtPlaceofBirth.Text = string.Empty;
            //txtTIN.Text = string.Empty;

            //ddGender.SelectedValue = "0";

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();
        }
        private DataTable CreateDataTable()
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Columns.Add(new DataColumn("autoId", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("empId", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("empName", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("designation", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("accessDate", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("inTime", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("standardAccessTime", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("lateStatus", typeof(string)));
            return myDataTable;
        }
        private void AddDataToTable(string autoId,
                                    string empId,
                                    string empName,
                                    string designation,
                                    string accessDate,
                                    string inTime,
                                    string standardAccessTime,
                                    string lateStatus,
                                    DataTable myTable)
        {
            DataRow row = myTable.NewRow();
            row["autoId"] = autoId;
            row["empId"] = empId;
            row["empName"] = empName;
            row["designation"] = designation;
            row["accessDate"] = accessDate;
            row["inTime"] = inTime;
            row["standardAccessTime"] = standardAccessTime;
            row["lateStatus"] = lateStatus;
            myTable.Rows.Add(row);
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            CommonFunctions commonfunctions = new CommonFunctions();
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;
            string s_status = string.Empty;

            s_status = "N";

            //if (chkal)
            //{

            //}
            /// Order Discription Part       
            s_select = "[ProcSearchInfoForfrmLateApprove]"
                                 + "'"
                                 + txtCard.Text.Trim()
                                 + "','"
                                 + SessionCompanyId
                                 + "','"
                                 + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                 + "','N'";

            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables[0] != null)
                {

                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        string autoId = string.Empty;
                        string empId = string.Empty;
                        string empName = string.Empty;
                        string designation = string.Empty;
                        string accessDate = string.Empty;
                        string inTime = string.Empty;
                        string standardAccessTime = string.Empty;
                        string lateStatus = string.Empty;

                        autoId = drow["autoId"].ToString();
                        empId = drow["empId"].ToString();
                        empName = drow["empName"].ToString();
                        designation = drow["designation"].ToString();
                        accessDate = drow["accessDate"].ToString();
                        inTime = drow["inTime"].ToString();
                        standardAccessTime = drow["standardAccessTime"].ToString();
                        lateStatus = drow["lateStatus"].ToString();

                        DataTable myDatatable = (DataTable)ViewState["myDatatable"];
                        AddDataToTable(autoId
                                    , empId
                                    , empName
                                    , designation
                                    , accessDate
                                    , inTime
                                    , standardAccessTime
                                    , lateStatus
                                    , (DataTable)ViewState["myDatatable"]
                                 );

                        this.gdv_costingHead.DataSource = ((DataTable)ViewState["myDatatable"]).DefaultView;
                        this.gdv_costingHead.DataBind();
                        hid_Rownumber.Value = Convert.ToString(Convert.ToDouble(hid_Rownumber.Value) + 1);

                    }
                }
            }




        }


        //===========End===============         
    }
}