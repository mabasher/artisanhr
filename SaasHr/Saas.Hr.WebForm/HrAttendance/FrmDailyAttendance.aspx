﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmDailyAttendance.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmDailyAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Daily Attendance
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <style>
            .date-picker {
                width: 100% !important;
            }
        </style>
        <div class="row row-custom">
            <div class="col-lg-12">
                <div class="row row-custom">
                    <div class="col-lg-7">
                        <fieldset>
                            <legend>All Employee Attendence</legend>

                            <div class="form-horizontal">
                                
                                <div class="form-group hidden">
                                    
                                    <div class="col-sm-1" style="text-align: right;">
                                        <asp:Label ID="Label8" runat="server" Text="Line : "></asp:Label>
                                    </div>
                                    <div class="col-sm-2 ">
                                        <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                        <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-2 ">
                                    </div>
                                    <div class="col-sm-2 hidden">
                                        <asp:Button ID="BtnAllSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" />

                                        <asp:Button ID="BtnAllRefresh" runat="server" Text="Refresh" CausesValidation="false" CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" />
                                    </div>
                                </div>

                                <div class="form-group" style="border-bottom:1px solid gray;">
                                    <div class="col-sm-1" style="text-align: left;">
                                        <asp:CheckBox ID="chkall" Text="All" runat="server" />
                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <asp:Label ID="Label4" runat="server" Text="Emp Type:"></asp:Label>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddEmployeeType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-2" style="text-align: right;">
                                        <asp:Label ID="Label7" runat="server" Text="Department:"></asp:Label>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                

                                <div class="form-group">
                                     <div class="col-sm-3">
                                         </div>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2 ">
                                        <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Date From :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox runat="server" ID="txtSearchFromDate" CssClass="input-sm date-picker" />
                                    </div>
                                    <asp:Label ID="Label3" runat="server" Text="To" CssClass="col-sm-1 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtSearchToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-sm-4" style="text-align: left;color:red;">
                                    <asp:Label ID="lblCounter" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-sm-8">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-xs" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-xs" OnClick="btn_refresh_Click" Width="80px" />
                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                                        
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-horizontal">
                                        <div class="form-group center">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div style="height: 300px; overflow: scroll;">
                                        <asp:GridView ID="gdv_Attendence" runat="server" Style="width: 100%; margin-left: 0;"
                                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                            CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                </asp:CommandField>

                                                <asp:TemplateField ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkApproval" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Card No"  Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtCard" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("pCard") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Emp.Id"  ItemStyle-Width="18%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtpid" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("PID") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Emp.Name"  ItemStyle-Width="18%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtcid" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("EmpName") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                        </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date"  ItemStyle-Width="18%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDate" runat="server" Enabled="false" CssClass="form-control input-sm date-picker"
                                                            Text='<% # Eval("AccessDate", "{0:dd/MM/yyyy}") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                   
                                                        </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="InTime">
                                                    <ItemTemplate>
                                                        <%-- <asp:Label ID="lblGridInTime" runat="server" Text='<%# Bind("AccessTime","{h:mm:ss tt}") %>'></asp:Label>--%>
                                                        <asp:TextBox ID="txtInTime" runat="server" CssClass="form-control input-sm timepicker"
                                                            Text='<% # Eval("inTime", "{0:HH:mm:ss tt}") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OutTime">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtOutTime" runat="server" CssClass="form-control input-sm timepicker"
                                                            Text='<% # Eval("outTime", "{0:HH:mm:ss tt}") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                        <%--<asp:HiddenField ID="hidempAutoId" Value='<%# Bind("empAutoId") %>' runat="server" />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerStyle CssClass="pagination-sa" />
                                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>



                            </div>

                        </fieldset>
                    </div>
                    <div class="col-md-5">
                        <div style="height: 450px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Card No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("CardNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Employee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    

                                    <asp:TemplateField HeaderText="Access Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridDOB" runat="server" Text='<%# Bind("AccessDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Access Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridShare" runat="server" Text='<%# Bind("AccessTime","{0:HH:mm:ss tt}") %>'></asp:Label>
                                            <asp:HiddenField ID="hidempAutoId" Value='<%# Bind("empAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="lblGridDepartment" Value='<%# Bind("Department") %>' runat="server" />
                                            <asp:HiddenField ID="lblGridDesignation" Value='<%# Bind("Designation") %>' runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-12 hidden">
            <div class="row row-custom">
                <div class="col-md-8">
                    <div class="row row-custom">
                        <div class="col-md-12">

                            <div class="form-horizontal">

                                <div class="form-group hidden">
                                    <asp:Label ID="Label31" runat="server" Text="Transaction Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label1" runat="server" Text="In Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label5" runat="server" Text="Out Time :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmployeeTypeId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
    <asp:HiddenField ID="hidForAll" runat="server" Value="" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/GetAllEmp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        if (row.rowIndex % 2 == 0) {
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "orange";
            }
            else {
                if (checkbox.checked) {
                    objRef.style.backgroundColor = "aqua";
                }
                else if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        objRef.style.backgroundColor = "#C2D69B";
                    }
                    else {
                        objRef.style.backgroundColor = "white";
                    }
                }
            }
        }
    </script>
</asp:Content>
