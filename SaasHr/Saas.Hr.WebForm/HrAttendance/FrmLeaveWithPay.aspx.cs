﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrAttendance
{
    public partial class FrmLeaveWithPay : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //tt
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
                txtToTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
                txtDays.Text = "";
                TimePart.Visible = false;

                commonfunctions.g_b_FillDropDownList(ddLeaveType,
                    "LeaveType",
                    "leaveType",
                    "LeaveTypeId", string.Empty);

                if (SessionUserLevel == "5")//HR Only
                {
                    btnDelete.Visible = false;
                }
                else if (SessionUserLevel == "4")//Single Employee Only
                {
                    btnDelete.Visible = false;
                }

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
                CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();
            
                SqlCommand cmd = new SqlCommand("ProcLeaveWithPaySelect'" + hidEmpAutoId.Value + "'", sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.DataBind();
                sqlconnection.Close();
            
        }

        protected void ddCalculateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddCalculateType.SelectedValue == "1") {
                TimePart.Visible = false;
                DatePart.Visible = true;
                lbldays.Text = "Days";
            }
            else if (ddCalculateType.SelectedValue == "2")
            {
                DatePart.Visible = false;
                TimePart.Visible = true;
                lbldays.Text = "Hours";
            }
            dateChange();

        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                //var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                //if (File.Exists(filePath))
                //{
                //    File.Delete(filePath);
                //}

                //filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                //filename = txtPIN.Text + "_" + filename;
                //URL = "~/UserPic/" + filename;
                //filename = "";
                
                s_Update = "[ProcLeaveWithPayINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddLeaveType.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(ddCalculateType.SelectedItem.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtFromTime.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDays.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtToTime.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U'";
                
                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                //filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                //filename = txtPIN.Text + "_" + filename;
                //URL = "~/UserPic/" + filename;
                //filename = "";

                s_save_ = "[ProcLeaveWithPayINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddLeaveType.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(ddCalculateType.SelectedItem.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtFromTime.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDays.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtToTime.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I'";                
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }
        
        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcLeaveWithPayINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + ddLeaveType.SelectedValue
                        + "','"
                        + HttpUtility.HtmlDecode(ddCalculateType.SelectedItem.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtFromTime.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDays.Text.Trim())
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtToTime.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','D'";//D=Delete

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_deleteOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }


        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridAutoId")).Value;
                ddLeaveType.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidleaveTypeAutoId")).Value;                
                ddCalculateType.SelectedItem.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCalType")).Text;
                txtFromDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridfromDate")).Text;
                txtDays.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridhrsDays")).Text;
                txtToDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridtoDate")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (txtEmpName.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Employee Can Not Blank!";
            //    return false;
            //}
            
            if (hidEmpAutoId.Value == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Can Not Blank!";
                return false;
            }

            
            if (ddLeaveType.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Leave Type Can Not Blank!";
                return false;
            }
            if (ddCalculateType.SelectedItem.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Calculate Type Can Not Blank!";
                return false;
            }
            if (Convert.ToDouble(txtDays.Text)<1)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Days Can Not Blank!";
                return false;
            }

            if (txtRemarks.Text.Trim() == string.Empty)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Reason of leave Can Not Blank!";
                return false;
            }
            
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            TimePart.Visible = false;
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            ddLeaveType.SelectedValue = "0";
            txtRemarks.Text = string.Empty;
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            txtToTime.Text = DateTime.Now.ToString("hh:mm:ss tt");

            txtDays.Text = "0";
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }
        

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            dateChange();
        }
        protected void txtDays_TextChanged(object sender, EventArgs e)
        {
            dateChange();
        }
        
        private void dateChange()
        {
            string sday = string.Empty;
            if (txtDays.Text == "")
            { sday = "0"; }
            else
            { sday = txtDays.Text; }

            if (ddCalculateType.SelectedItem.ToString() == "Days")
            {
                DateTime dt = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dt = dt.AddDays(Convert.ToDouble(sday) - 1);
                txtToDate.Text = dt.ToString("dd/MM/yyyy");
            }
            else
            {
                DateTime dt = DateTime.ParseExact(txtFromTime.Text, "hh:mm:ss tt", CultureInfo.InvariantCulture);
                dt = dt.AddHours(Convert.ToDouble(sday) );
                txtToTime.Text = dt.ToString("hh:mm:ss tt");
                txtToDate.Text = txtFromDate.Text;
            }

           
        }

        //===========End===============         
    }
}