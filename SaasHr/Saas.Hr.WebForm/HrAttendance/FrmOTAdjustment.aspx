﻿<%@ Page Title="Over Time Adjustment" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmOTAdjustment.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmOTAdjustment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">

        <div class="col-md-6">
            <fieldset>
                <legend>Search</legend>
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="From Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtSearchFromDate" CssClass="input-sm date-picker" />
                        </div>
                        <asp:Label ID="Label3" runat="server" Text="To Date :" CssClass="col-sm-2 control-label"></asp:Label>

                        <div class="col-sm-2">
                            <asp:TextBox ID="txtSearchToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                        </div>

                        <div class="col-sm-3 center">
                            <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>OT Adjustment (Single Entry)</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label31" runat="server" Text="OT Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Add (Hour) :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtAddHours" onkeyup="calcAddHours()" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom" ValidChars="." TargetControlID="txtAdd" />
                        </div>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtAdd" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,custom" ValidChars="." TargetControlID="txtAdd" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" Text="Minute :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtAddMinute" onkeyup="calcAddHours()" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers,custom" ValidChars="." TargetControlID="txtAdd" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label5" runat="server" Text="Deduct (Hour) :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtDeductHours" onkeyup=calcDeductHours() runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Numbers,custom" ValidChars="." TargetControlID="txtDeduct" />
                        </div>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtDeduct" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterType="Numbers,custom" ValidChars="." TargetControlID="txtDeduct" />
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label9" runat="server" Text="Minute :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtDeductMinute" onkeyup=calcDeductHours() runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom" ValidChars="." TargetControlID="txtAdd" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group center">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnRemove_Click" Width="80px" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">

            <fieldset>
                <legend>OT Adjustment (Multiple Entry)</legend>

                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" Text="Department :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-6">
                        <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                    </div>

                    <div class="col-sm-4 center">
                        <asp:LinkButton ID="lnkBtnFind" runat="server" OnClick="lnkBtnFind_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>

                        <asp:LinkButton ID="lnkBtnSave" runat="server" OnClick="lnkBtnSave_Click" CssClass="btn btn-primary btn-xs" Width="50px"><i class="fa fa-check" aria-hidden="true"></i></asp:LinkButton>

                    </div>
                </div>


                <div class="col-sm-12" style="height: 280px; overflow-y: scroll;">
                    <asp:GridView ID="gdv_Attendence" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="PIN" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPIN" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("PIN") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-Width="50%">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtName" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("empName") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Add(Min)">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtaddMin" autocomplete="off" runat="server" CssClass="form-control input-sm timepicker"
                                        Text='<% # Eval("AddMinute") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                    <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterType="Numbers" TargetControlID="txtaddMin" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Deduct(Min)">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtdeductMin" autocomplete="off" runat="server" CssClass="form-control input-sm timepicker"
                                        Text='<% # Eval("deductMinute") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                    <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterType="Numbers" TargetControlID="txtdeductMin" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </fieldset>
        </div>

        <div class="col-sm-12">
            <div class="form-horizontal">
                <div class="form-group center">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div style="min-height: 250px; overflow-y: scroll;">
                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                    CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                        </asp:CommandField>
                        <asp:TemplateField HeaderText="PIN">
                            <ItemTemplate>
                                <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("PIN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Employee Name">
                            <ItemTemplate>
                                <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Effective Date">
                            <ItemTemplate>
                                <asp:Label ID="lblGridEffictiveDate" runat="server" Text='<%# Bind("EffictiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Add Hours">
                            <ItemTemplate>
                                <asp:Label ID="lblGridAddHours" runat="server" Text='<%# Bind("AddHours") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="AddMinute">
                            <ItemTemplate>
                                <asp:Label ID="lblGridAddMinute" runat="server" Text='<%# Bind("AddMinute") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Total Add Minute">
                            <ItemTemplate>
                                <asp:Label ID="lblGridAddHour" runat="server" Text='<%# Bind("AddHour") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Deduct Hours">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDeductHours" runat="server" Text='<%# Bind("DeductHours") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="DeductMinute">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDeductMinute" runat="server" Text='<%# Bind("DeductMinute") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Deduct Minute">
                            <ItemTemplate>
                                <asp:Label ID="lblGridDeductHour" runat="server" Text='<%# Bind("DeductHour") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                <asp:HiddenField ID="hidempAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                <asp:HiddenField ID="hidPIN" Value='<%# Bind("CardNo") %>' runat="server" />
                                <asp:HiddenField ID="hidPFCloseDateId" Value='<%# Bind("AutoId") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <PagerStyle CssClass="pagination-sa" />
                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                </asp:GridView>
            </div>
        </div>


    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
    <asp:HiddenField ID="hidDateUse" runat="server" Value="" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

        function calcAddHours() {
            var temp1 = document.getElementById('<%= txtAddHours.ClientID %>').value;
              if (temp1 == "") { temp1 = ""; };
              var temp2 = document.getElementById('<%= txtAddMinute.ClientID %>').value;
            if (temp2 == "") { temp2 = ""; };
            var total = "";
            if (temp2 != "" && temp1 != "") {
                var total = (parseFloat(temp1) * 60) + parseFloat(temp2);
            }
            document.getElementById('<%= txtAdd.ClientID %>').value = total.toString();
        }
        function calcDeductHours() {
            var temp1 = document.getElementById('<%= txtDeductHours.ClientID %>').value;
               if (temp1 == "") { temp1 = ""; };
               var temp2 = document.getElementById('<%= txtDeductMinute.ClientID %>').value;
            if (temp2 == "") { temp2 = ""; };
            var total = "";
            if (temp2 != "" && temp1 != "") {
                var total = (parseFloat(temp1) * 60) + parseFloat(temp2);
            }
               document.getElementById('<%= txtDeduct.ClientID %>').value = total.toString();
           }


    </script>
</asp:Content>
