﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmOTProcessing.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmOverTimeProcessing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Daily Over Time Processing
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-md-5">

            <div style="margin-top: 50px;">
                <fieldset>
                    <legend>OT Processing </legend>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="For Date :" CssClass="col-sm-5 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtForDate" CssClass="input-sm date-picker" />
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <asp:Label ID="Label9" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                           
                            <div class="col-sm-6">
                                <asp:CheckBox ID="chkTimeBreak" Visible="false" Checked="true" runat="server" Text="" />
                            <asp:Label ID="Label7" runat="server" Text="Allowed Lunch/Tea Break (1 hour)" ForeColor="Red" CssClass="control-label"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group center" style="margin-top: 10px;">
                            <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        </div>

                        <div class="form-group center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Calculate OT" OnClick="btn_save_Click"  Width="120"/>
                            <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnDelete_Click" Width="120"  />
                       </div>

                    </div>
                </fieldset>
            </div>

            <div style="margin-top: 20px;">
                <fieldset>
                    <legend>OT Preview</legend>
                    <div class="form-horizontal">

                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Department :" CssClass="col-sm-5 control-label"></asp:Label>
                            <div class="col-sm-6">
                                <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label5" runat="server" Text="Section :" CssClass="col-sm-5 control-label"></asp:Label>
                            <div class="col-sm-6 ">
                                <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group hidden">
                            <asp:Label ID="Label6" runat="server" Text="Line :" CssClass="col-sm-5 control-label"></asp:Label>
                            <div class="col-sm-6 ">
                                <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="From Date :" CssClass="col-sm-5 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label4" runat="server" Text="To Date :" CssClass="col-sm-5 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <asp:Label ID="Label8" runat="server" Text="" CssClass="col-sm-5 control-label"></asp:Label>
                             <div class="col-sm-4">
                                 <asp:CheckBox ID="chkExtra" Text="Extra OT" runat="server" />
                            </div>
                        </div>

                        <div class="form-group center">
                            <asp:Button ID="btnPreview" runat="server" CssClass="btn btn-primary btn-sm" Text="Preview OT Sheet" OnClick="btnPreview_Click"  />
                        </div>
                    </div>

                </fieldset>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-horizontal">
                <div class="main-data-grid" style="overflow: scroll; text-align: center;">

                    <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered"
                        OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                        EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="OT Processed Date" HeaderStyle-CssClass="center">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridDate" runat="server" Text='<%# Bind("accessDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
