﻿<%@ Page Title="Leave With Out Pay" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmLeaveWithOutPay.aspx.cs" Inherits="Saas.Hr.WebForm.HrAttendance.FrmLeaveWithOutPay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Leave With Out Pay
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="form-group ">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-3 ">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group ">
                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>


                            <div class="form-group" style="margin-top: 20px;">
                                <asp:Label ID="Label11" runat="server" Text="Leave Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddLeaveType" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Calculate Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddCalculateType" CssClass="form-control chosen-select" runat="server">
                                        <asp:ListItem>Days</asp:ListItem>
                                        <asp:ListItem>Hours</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="form-group">
                                <asp:Label ID="Label18" runat="server" Text="From Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                </div>   
                                 <asp:Label ID="Label5" runat="server" Text="Time :" CssClass="col-sm-1 control-label"></asp:Label>
                                
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label23" runat="server" Text="Days :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtDays" runat="server" CssClass="form-control input-sm" AutoPostBack="True" OnTextChanged="txtDays_TextChanged"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="To Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />                                   
                                </div>
                                 <asp:Label ID="Label43" runat="server" Text="Time :" CssClass="col-sm-1 control-label"></asp:Label>
                                
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtToTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div style="height: 150px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>
                                    
                                    <asp:TemplateField HeaderText="leaveType">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridleaveType" runat="server" Text='<%# Bind("leaveType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="calType">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCalType" runat="server" Text='<%# Bind("calType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="fromDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridfromDate" runat="server" Text='<%# Bind("fromDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  <asp:TemplateField HeaderText="fromTime">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridfromTime" runat="server" Text='<%# Bind("fromTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="hrsDays">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridhrsDays" runat="server" Text='<%# Bind("hrsDays") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="toDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridtoDate" runat="server" Text='<%# Bind("toDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  <asp:TemplateField HeaderText="toTime">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridtoTime" runat="server" Text='<%# Bind("toTime") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                            <asp:HiddenField ID="hidleaveTypeAutoId" Value='<%# Bind("leaveTypeAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidLeaveWithOutPayId" Value='<%# Bind("LeaveWithOutPayId") %>' runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAcademicInfoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidExamNameId" runat="server" Value="0" />
        <asp:HiddenField ID="hidInstituteId" runat="server" Value="0" />
        <asp:HiddenField ID="hidBoardUniversityId" runat="server" Value="0" />

        <asp:HiddenField ID="hidleaveTypeAutoId" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

    </script>

</asp:Content>
