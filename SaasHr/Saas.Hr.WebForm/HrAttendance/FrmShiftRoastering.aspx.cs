﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrAttendance
{
    public partial class FrmShiftroastering : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddShiftFrom,
                    "ShiftInformation",
                    "ShiftName",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddShiftTo,
                    "ShiftInformation",
                    "ShiftName",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddEmployee,
                    "EmployeePersonalInfo",
                    "PIN",
                    "EmployeePersonalInfoId", string.Empty);

                IdroasterIdEnable.Visible = false;
                v_loadGridView_CostingHead();
                v_loadGridViewRoaster();
            }
        }


        protected void chkRoasterChecker_CheckedChanged(object sender, EventArgs e)
        {
            hidEmpId.Value = "Basher%";
            v_loadGridViewRoaster();
            if (chkRoasterChecker.Checked == true)
            {
                IdroasterIdEnable.Visible = true;
            }
            else
            {
                IdroasterIdEnable.Visible = false;
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            s_save_ = "";
            string sUserAutoId = string.Empty;
            sUserAutoId = ddShiftTo.SelectedValue.ToString();
            foreach (GridViewRow gdvRow in gdv_costingHead.Rows)
            {
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkEmpSelect");
                if (checkbox.Checked == true)
                {                    
                    string sPageAutoId = string.Empty;
                    sPageAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hidGridEmployeePersonalInfoId")).Value;
                    //s_save_ = s_save_ + "  DELETE ShiftRoastering WHERE empAutoId='" + sPageAutoId + "' and ShiftAutoId='" + sUserAutoId + "' and EffectiveDate='" + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";
                    s_save_ = s_save_ + "  DELETE ShiftRoastering WHERE empAutoId='" + sPageAutoId + "' and EffectiveDate='" + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";

                    s_save_ = s_save_ + "INSERT INTO ShiftRoastering VALUES ((select isnull(max(AutoId),0)+1 From ShiftRoastering), '" + sPageAutoId + "','" + sUserAutoId + "','"+ Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "')";
                }
            }
            //////Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    //lblMsg.Visible = true;
                    //lblMsg.Text = "Successfully Done!";
                    //InsertMode();
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
            //////Insert Option
            
        }


        protected void btn_Delete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            //Boolean b_validationReturn = true;
            //b_validationReturn = isValid();
            //if (b_validationReturn == false)
            //{
            //    return;
            //}

            s_save_ = "";
            foreach (GridViewRow gdvRow in gdv_ShiftRoaster.Rows)
            {
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkEmpSelectdelete");
                if (checkbox.Checked == true)
                {
                    string sPageAutoId = string.Empty;
                    sPageAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hidGridRoasterAutoId")).Value;
                    s_save_ = s_save_ + "  DELETE ShiftRoastering WHERE AutoId='" + sPageAutoId + "'";

                }
            }
            //////Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    //lblMsg.Visible = true;
                    //lblMsg.Text = "Successfully Done!";
                    //InsertMode();
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
            //////Insert Option

        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {            
            if (ddShiftFrom.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Shift From Can Not Blank!";
                return false;
            }
            if (ddShiftTo.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Shift To Can Not Blank!";
                return false;
            }
                                 
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            ddShiftFrom.SelectedValue = "0";
            ddShiftTo.SelectedValue = "0";
        }
        
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_CostingHead();
        }

        protected void v_loadGridView_CostingHead()
        {

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            hidDepartmentId.Value = "%";
            hidSectionId.Value = "%";
            hidLineId.Value = "%";
            hidShiftFromId.Value = "0";
            hidEmpId.Value = "%";

            if (ddDepartment.SelectedValue != "0") { hidDepartmentId.Value = ddDepartment.SelectedValue + "X%"; }
            if (ddSection.SelectedValue != "0") { hidSectionId.Value = ddSection.SelectedValue + "X%"; }
            if (ddLine.SelectedValue != "0") { hidLineId.Value = ddLine.SelectedValue + "X%"; }
            if (ddShiftFrom.SelectedValue != "0") { hidShiftFromId.Value = ddShiftFrom.SelectedValue + "X%"; }

            qry = "ProcSearchEmpforShiftchange '" + SessionCompanyId + "','%','" + hidDepartmentId.Value + "','" + hidSectionId.Value + "','" + hidLineId.Value + "','" + hidShiftFromId.Value + "','" + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";

            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }


        protected void lnkBtnforRpaster_Click(object sender, EventArgs e)
        {
            hidEmpId.Value = "Basher%";
            if (ddEmployee.SelectedValue != "0") { hidEmpId.Value = ddEmployee.SelectedValue + "X%"; }
            v_loadGridViewRoaster();
        }
        protected void v_loadGridViewRoaster()
        {

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            qry = "ProcSearchEmpforShiftchangeinRoastering '" + SessionCompanyId + "','" + hidEmpId.Value + "','" + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";

            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_ShiftRoaster.DataSource = ds;
            gdv_ShiftRoaster.DataBind();
            sqlconnection.Close();
        }

        //===========End===============         
    }
}