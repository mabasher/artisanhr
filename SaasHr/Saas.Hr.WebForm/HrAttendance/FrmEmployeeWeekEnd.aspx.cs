﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrAttendance
{
    public partial class FrmEmployeeWeekEnd : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //tt
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId, sQry = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtEffectiveDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                sQry = "select e.EmployeePersonalInfoId as ItemId,e.PIN+'-'+e.EmployeeName as ItemName from  dbo.EmployeePersonalInfo e	inner Join dbo.EmployeeOfficialInfo o	on e.EmployeePersonalInfoId = o.empAutoId where DepartmentAutoId=0";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, sQry);

                v_loadGridView_CostingHead();
                v_loadGridView_gdv_Attendence();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string sQry = string.Empty;
            sQry = "select top 180 AutoId as AutoId, trDate as effectiveDate, Note as Remarks, 0 as CompanyAutoId  from [dbo].[EmployeeWiseWeekEnd] Where EmpAutoId='0' order by trDate desc";

            if (chkWeekend.Checked == true)
            {
                sQry = "select top 180 AutoId as AutoId, trDate as effectiveDate, Note as Remarks, 0 as CompanyAutoId  from [dbo].[EmployeeWiseWeekEnd] Where EmpAutoId='" + hidEmpAutoId.Value + "' order by trDate desc";
            }
            if (chkPresent.Checked == true)
            {
                sQry = "select top 180 AutoId as AutoId, trDate as effectiveDate, Note as Remarks, 0 as CompanyAutoId  from [dbo].[EmployeeWisePresent] Where EmpAutoId='" + hidEmpAutoId.Value + "' order by trDate desc";
            }

            SqlCommand cmd = new SqlCommand(sQry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            //gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();
            sqlconnection.Close();

        }
        protected void btnFind_Click(object sender, EventArgs e)
        { v_loadGridView_CostingHead(); }              

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            string sType = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            sType = "";
            if (chkPresent.Checked == true) { sType = "P"; }
            if (chkWeekend.Checked == true) { sType = "W"; }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcEmpWeekEndINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + sType
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','D'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);
                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = "Deleted!!";
                        //imgUpload.SaveAs(Server.MapPath(URL));
                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            string sType = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            sType = "";
            if (chkPresent.Checked == true) { sType = "P"; }
            if (chkWeekend.Checked == true) { sType = "W"; }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcEmpWeekEndINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + sType
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcEmpWeekEndINSERT]"
                        + "'0','"
                        + sType
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtEffectiveDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtRemarks.Text.Trim())
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        //Response.Redirect(Request.RawUrl);
                        v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                txtEffectiveDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEffectiveDate")).Text;
                txtRemarks.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;

                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();


            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (chkWeekend.Checked == false && chkPresent.Checked == false)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Not Checked Weekend or Present option!";
                return false;
            }

            if (chkWeekend.Checked == true && chkPresent.Checked == true)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Check Weekend or Present option!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            //txtPIN.Text = string.Empty;
            //txtEmpName.Text = string.Empty;
            //txtCard.Text = string.Empty;
            //hidEmpAutoId.Value = "0";
            //txtRemarks.Text = string.Empty;

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }


        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_gdv_Attendence();
        }

        private void v_loadGridView_gdv_Attendence()
        {
            string nday = string.Empty;
            string ndayCheck = string.Empty;
            string sEmpId = string.Empty;
            nday = "";
            ndayCheck = "N";
            if (ddDays.SelectedValue != "Select")
            {
                nday = ddDays.SelectedValue.ToString();
                ndayCheck = "Y";
            }
            hidDepartmentId.Value = "%";
            sEmpId = "%";
            if (hidEmpAutoId.Value == "") { hidEmpAutoId.Value = "0"; }
            if (ddDepartment.SelectedValue != "0") { hidDepartmentId.Value = ddDepartment.SelectedValue + "X%"; }
            //if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0") { sEmpId = hidEmpAutoId.Value + "X%"; }
            if (ddEmployee.SelectedValue  != "0") { sEmpId = ddEmployee.SelectedValue.ToString() + "X%"; }

            //if (ddDepartment.SelectedValue == "0" && hidEmpAutoId.Value == "0") { sEmpId = "C%"; hidDepartmentId.Value = "C%"; }
            if (ddDepartment.SelectedValue == "0" && ddEmployee.SelectedValue == "0") { sEmpId = "C%"; hidDepartmentId.Value = "C%"; }

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "ProcGetDatebyDayName '" + sEmpId + "','" + hidDepartmentId.Value + "','" + ndayCheck + "','" + nday + "','" + Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(txtSearchToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "'";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_Attendence.DataSource = ds;
            gdv_Attendence.DataBind();
            sqlconnection.Close();
        }


        protected void btnSaveAll_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            string sType = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            string strs = string.Empty;

            strs = "N";
            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true)
                {
                    strs = "Y";
                }
            }


            sType = "";
            if (chkPresent.Checked == true) { sType = "P"; }
            if (chkWeekend.Checked == true) { sType = "W"; }

            if (sType == "")
            {
                return;
            }

            if (strs == "N")
            {
                return;
            }


            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string s_empAutoId = string.Empty;
                string s_fromDate = string.Empty;
                s_empAutoId = ((HiddenField)gdvRow.Cells[0].FindControl("hid_AttenGridEmpAutoId")).Value;
                s_fromDate = ((TextBox)gdvRow.Cells[0].FindControl("txtDate")).Text;
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true)
                {
                    s_save_ = s_save_ + "Execute ProcEmpWeekEndINSERTALL "
                                  + "'"
                                  + sType
                                  + "','"
                                  + s_empAutoId
                                  + "','"
                                  + Convert.ToDateTime(s_fromDate.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                  + "','"
                                  + SessionUserId
                                  + "','"
                                  + SessionUserIP
                                  + "'";

                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void ddDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonFunctions commonfunctions = new CommonFunctions();
            string sQry,sDeptId = string.Empty;
            sDeptId = "0";
            if (ddDepartment.SelectedValue != "0")
            { sDeptId = ddDepartment.SelectedValue.ToString(); }
                sQry = "select e.EmployeePersonalInfoId as ItemId,e.PIN+'-'+e.EmployeeName as ItemName from  dbo.EmployeePersonalInfo e	inner Join dbo.EmployeeOfficialInfo o	on e.EmployeePersonalInfoId = o.empAutoId where DepartmentAutoId='"+ sDeptId + "'";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, sQry);
            
        }


        //===========End===============         
    }
}