﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmEmployeeWeekEnd.aspx.cs" Inherits="Saas.Hr.WebForm.HrAttendance.FrmEmployeeWeekEnd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Employee Wise WeekEnd | Present
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>


            <div class="row row-custom">
                <div class="col-md-4">
                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="form-group ">
                                <asp:Label ID="Label3" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                   <asp:CheckBox ID="chkWeekend" Text="Weekend" runat="server" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <asp:CheckBox ID="chkPresent" Text="Present" runat="server" />
                                </div>
                            </div>


                            <div class="form-group ">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group hidden">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-8 ">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group ">
                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label18" runat="server" Text="Weekend :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox runat="server" ID="txtEffectiveDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label16" runat="server" Text="Note :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtRemarks"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btnDelete_Click" Width="80px" />
                                    <asp:Button ID="btnFind" runat="server" CssClass="btn btn-primary btn-sm" Text="Find" OnClick="btnFind_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div style="height: 350px; overflow: scroll;">
                            <%--OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged"--%>
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" OnRowDataBound="gdv_costingHead_RowDataBound"  EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>
                                    
                                    <asp:TemplateField HeaderText="Tr. Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEffectiveDate" runat="server" Text='<%# Bind("effectiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    

                                    <asp:TemplateField HeaderText="Note">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>

                </div>
                
                <div class="col-md-8">
                    <div class="col-md-6">
                        <div class="form-horizontal">
                            
                                <div class="form-group ">
                                    <asp:Label ID="Label8" runat="server" Text="Department:" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddDepartment" AutoPostBack="true" OnSelectedIndexChanged="ddDepartment_SelectedIndexChanged" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label9" runat="server" Text="Employee:" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddEmployee" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Day Name:" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddDays" CssClass="form-control chosen-select" runat="server">
                                            <asp:ListItem>Select</asp:ListItem>
                                            <asp:ListItem>Sunday</asp:ListItem>
                                            <asp:ListItem>Monday</asp:ListItem>
                                            <asp:ListItem>Tuesday</asp:ListItem>
                                            <asp:ListItem>Wednesday</asp:ListItem>
                                            <asp:ListItem>Thursday</asp:ListItem>
                                            <asp:ListItem>Friday</asp:ListItem>
                                            <asp:ListItem>Saturday</asp:ListItem>
                                        </asp:DropDownList>                                        
                                    </div>  
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="From Date:" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-4">
                                        <asp:TextBox runat="server" ID="txtSearchFromDate" CssClass="input-sm date-picker" />
                                    </div>
                                </div>
                    
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="To Date:" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-5">
                                        <asp:TextBox ID="txtSearchToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                                    </div>                                                                      
                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                                    </div>
                                </div>
                    
                                <div class="form-group" style="margin-top:20px;">
                                    <asp:Label ID="Label7" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-offset-3 col-sm-4">
                                    <asp:Button ID="btnSaveAll" runat="server" CssClass="btn btn-primary btn-xs" Text="Save All" OnClick="btnSaveAll_Click" Width="80px" />
                                    </div>
                                </div>
                        
                                </div>
                                </div>


                                <div class="col-md-6">
                                    <div style="height: 500px; overflow: scroll;">
                                        <asp:GridView ID="gdv_Attendence" runat="server" Style="width: 100%; margin-left: 0;"
                                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                            CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                </asp:CommandField>

                                                <asp:TemplateField ItemStyle-Width="8%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkApproval" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="PIN"  ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtpid" runat="server" Enabled="false" CssClass="form-control input-sm"
                                                            Text='<% # Eval("PIN") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                   
                                                        <asp:HiddenField ID="hid_AttenGridEmpAutoId" Value='<%# Bind("EmpAutoId") %>' runat="server" />

                                                        </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date"  ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtDate" runat="server" Enabled="false" CssClass="form-control input-sm date-picker"
                                                            Text='<% # Eval("AccessDate", "{0:dd/MM/yyyy}") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Day Name"  ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <%-- <asp:Label ID="lblGridInTime" runat="server" Text='<%# Bind("AccessTime","{h:mm:ss tt}") %>'></asp:Label>--%>
                                                        <asp:TextBox ID="txtInTime" runat="server"  Enabled="false" CssClass="form-control input-sm timepicker"
                                                            Text='<% # Eval("nDay") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <PagerStyle CssClass="pagination-sa" />
                                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                </div>

            </div>


        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAcademicInfoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidExamNameId" runat="server" Value="0" />
        <asp:HiddenField ID="hidInstituteId" runat="server" Value="0" />
        <asp:HiddenField ID="hidBoardUniversityId" runat="server" Value="0" />
        <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });

        
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        if (row.rowIndex % 2 == 0) {
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }
    </script>

</asp:Content>
