﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmFilterData.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmFilterData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Processing Attendance Data (Filtering)
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

         <div class="row row-custom">
                <div class="col-md-5">
                    <div class="form-horizontal">
                            <div class="form-group"  style="margin-top: 150px;">
                                <asp:Label ID="Label2" runat="server" Text="For Date :" CssClass="col-sm-5 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label4" runat="server" Text="To Date :" CssClass="col-sm-5 control-label"></asp:Label>
                                <div class="col-sm-3">
                                      <asp:TextBox ID="txtToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                               </div>     
                            </div>
                            <div class="form-group center" style="margin-top: 20px;">
                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
                        
                            <div class="form-group center">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Filter" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false" CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                <asp:Button ID="btnRemove" runat="server" Text="Remove" CausesValidation="false" CssClass="btn btn-danger btn-sm" OnClick="btnRemove_Click" Width="80px" />
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-horizontal">
                            <div class="main-data-grid" style="overflow-y: scroll;text-align:center;">

                                <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                    CssClass="table table-striped table-bordered"
                                  OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                                    EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Transaction Date" HeaderStyle-CssClass="center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridDate" runat="server" Text='<%# Bind("accessDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerStyle CssClass="pagination-sa" />
                                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                </asp:GridView>
                            </div>
                    </div>
                </div>                               
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

</asp:Content>
