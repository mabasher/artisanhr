﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmOTAdjustment : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtSearchToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");


                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);


                hidDateUse.Value = "N";
                v_loadGridView_CostingHead();
                v_loadGridView_gdv_Attendence();

            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidCardid.Value == "0") { hidCardid.Value = ""; }
            if (hidEmpAutoId.Value == "0") { hidEmpAutoId.Value = ""; }

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "ProcOTAdjustmentSelect '" + hidEmpAutoId.Value + "%','" + Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + Convert.ToDateTime(txtSearchToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy") + "','" + hidDateUse.Value + "'";
            hidDateUse.Value = "N";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            //gdv_costingHead.AllowPaging = true;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }

        protected void lnkBtnFind_Click(object sender, EventArgs e)
        {
            v_loadGridView_gdv_Attendence();
        }

        private void v_loadGridView_gdv_Attendence()
        {
            hidDepartmentId.Value = "0";
            if (ddDepartment.SelectedValue != "0") { hidDepartmentId.Value = ddDepartment.SelectedValue.ToString(); }

            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = string.Empty;
            qry = "SELECT p.PIN as PIN,p.EmployeeName as empName,p.EmployeePersonalInfoId as empAutoId,'' as AddMinute,'' as deductMinute From EmployeeOfficialInfo o inner join EmployeePersonalInfo p on o.EmpAutoId=p.EmployeePersonalInfoId inner join DepartmentInfo d on o.DepartmentAutoId=d.DepartmentInfoId where d.DepartmentInfoId='" + hidDepartmentId.Value + "'";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            da.Fill(ds);
            gdv_Attendence.DataSource = ds;
            gdv_Attendence.DataBind();
            sqlconnection.Close();

        }


        protected void lnkBtnSave_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            

            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string s_PIN = string.Empty;
                string s_AddMin = string.Empty;
                string s_DeductMin = string.Empty;

                s_PIN = ((TextBox)gdvRow.Cells[0].FindControl("txtPIN")).Text;
                s_AddMin = ((TextBox)gdvRow.Cells[0].FindControl("txtaddMin")).Text;
                s_DeductMin = ((TextBox)gdvRow.Cells[0].FindControl("txtdeductMin")).Text;
                if (s_AddMin == "") { s_AddMin = "0"; }
                if (s_DeductMin == "") { s_DeductMin = "0"; }


                if (Convert.ToDouble(s_AddMin) > 0 || Convert.ToDouble(s_DeductMin) > 0)
                {
                    s_save_ = s_save_ + "Execute ProcOTAdjustmentINSERT_Grid "
                                  + "'"
                                  + s_PIN
                                  + "','"
                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                  + "','"
                                  + HttpUtility.HtmlDecode(s_AddMin.Trim())
                                  + "','"
                                  + HttpUtility.HtmlDecode(s_DeductMin.Trim())
                                  + "','','"
                                + SessionCompanyId
                                + "','"
                                + SessionUserId
                                + "','"
                                + SessionUserIP
                                + "','I'";
                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }

        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                s_Update = "[ProcOTAdjustmentINSERT]"
                        + "'"
                        + hidAutoIdForUpdate.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdd.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDeduct.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','U','"
                        + HttpUtility.HtmlDecode(txtAddHours.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAddMinute.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDeductHours.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDeductMinute.Text.Trim()) 
                        + "'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                }
            }
            else
            {
                s_save_ = "[ProcOTAdjustmentINSERT]"
                        + "'"
                        + '0'
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdd.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDeduct.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','I','"
                        + HttpUtility.HtmlDecode(txtAddHours.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtAddMinute.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDeductHours.Text.Trim())
                        + "','"
                        + HttpUtility.HtmlDecode(txtDeductMinute.Text.Trim()) +"'";

                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                        //imgUpload.SaveAs(Server.MapPath(URL));

                        Response.Redirect(Request.RawUrl);
                        //v_loadGridView_CostingHead();
                        InsertMode();
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }


        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Boolean b_validationReturn = true;

            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            s_save_ = "Delete OTAdjustment Where AutoId= "
                       + "'"
                       + hidAutoIdForUpdate.Value
                       + "' Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }


        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidPFCloseDateId")).Value;
                txtCard.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCardNo")).Text;
                txtEmpName.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEmpName")).Text;
                txtPIN.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidPIN")).Value;
                hidEmpAutoId.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidempAutoId")).Value;
                txtAdditionalNote.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridRemarks")).Text;
                txtFromDate.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridEffictiveDate")).Text;

                txtAddHours.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAddHours")).Text;
                txtAddMinute.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAddMinute")).Text;
                txtAdd.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridAddHour")).Text;

                txtDeductHours.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDeductHours")).Text;
                txtDeductMinute.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDeductMinute")).Text;
                txtDeduct.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDeductHour")).Text;


                btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }
            if (txtAdd.Text == "")
            {
                txtAdd.Text = "0";
            }
            if (txtDeduct.Text == "")
            {
                txtDeduct.Text = "0";
            }
            if (Convert.ToDouble(txtAdd.Text) == 0 && Convert.ToDouble(txtDeduct.Text) == 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Add or Deduction Hour Can Not Blank!";
                return false;
            }
            if (txtAddHours.Text == "") txtAddHours.Text = "0";
            if (txtAddMinute.Text == "") txtAddMinute.Text = "0";   
            if (txtDeductHours.Text == "") txtDeductHours.Text = "0";
            if (txtDeductMinute.Text == "") txtDeductMinute.Text = "0";
           

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                        txtCard.Text = drow["CardNo"].ToString();
                        hidCardid.Value = drow["CardNo"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();


        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {

            hidDateUse.Value = "Y";
            v_loadGridView_CostingHead();
        }


        //===========End===============         
    }
}