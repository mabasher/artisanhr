﻿namespace Saas.Hr.WebForm.EntityModel
{
    public class EmiInfoModel
    {
        public int EmiInfoId { get; set; }
        public int LoanDuration { get; set; }
    }
}