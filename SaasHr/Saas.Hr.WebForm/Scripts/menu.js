(function ($) {
    $(document).ready(function () {
        $('#LeftSideBar_cssmenu > li > ul > li').each(function () {
            var href = jQuery(this).find('a').attr('href');
            if (href === window.location.pathname + ".aspx") {
                $(this).closest('li').addClass('active');
                $(this).parent().closest('li').addClass('open');
            }
        }); //t

        $('#LeftSideBar_cssmenu > li > ul > li > ul > li').each(function () {
            var href = jQuery(this).find('a').attr('href');
            if (href === window.location.pathname + ".aspx") {
                $(this).closest('li').addClass('active');
                $(this).parent().closest('li').addClass('open active');
                $(this).parent().parent().parent().closest('li').addClass('open');
            }
        });
    });
})(jQuery);