﻿$(function () {
    $('.chosen-select').chosen();
    $(".date-picker").inputmask("datetime", {
        inputFormat: "dd/mm/yyyy",
        outputFormat: "dd/mm/yyyy",
        inputEventOnly: true
    });
    //    $(".date-picker").inputmask("99/99/9999", { "placeholder": "dd.mm.yyyy" });
    //
    //    $('.date-picker').attr("data-date-format", "dd/mm/yyyy");
    //
    //    $('.date-picker').attr("value", "");
    //    //    $('input.date-picker').rules('add', { date: true });
    $(".date-picker").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:2041'
    });
    var hgt = screen.height - 250;

    $(".main-data-grid").css("height", hgt + "px");

    //$('.timepicker').timepicker({
    //    minuteStep: 1,
    //    showSeconds: true,
    //    showMeridian: false,
    //    disableFocus: true,
    //    icons: {
    //        up: 'fa fa-chevron-up',
    //        down: 'fa fa-chevron-down'
    //    }
    //}).on('focus', function () {
    //    $('.timepicker').timepicker('showWidget');
    //}).next().on(ace.click_event, function () {
    //    $(this).prev().focus();
    //});
});

function makeAutoComplete(controlInfo, serviceUrl, valuePlaceHolder) {
    $(controlInfo).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: serviceUrl,
                data: "{ 'searchTerm': '" + request.term + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d,
                        function (item) {
                            return {
                                label: item.split('@')[0],
                                val: item.split('@')[1]
                            }
                        }));
                },
                error: function (response) {
                    alert("Error happend" + response.responseText);
                },
                failure: function (response) {
                    alert("Fail happend" + response.responseText);
                }
            });
        },
        select: function (e, i) {
            $(valuePlaceHolder).val(i.item.val);
        },
        minLength: 1
    });
}

function getOneStepConfirmation(msg) {
    return confirm("Are you sure to " + msg + "!!!")
}

function getTwoStepConfirmation(msg) {
    if (getOneStepConfirmation(msg) === true) {
        return getOneStepConfirmation(msg);
    }
    return false;
}

function getThreeStepConfirmation(msg) {
    if (getTwoStepConfirmation(msg) === true) {
        return getOneStepConfirmation(msg);
    }
    return false;
}