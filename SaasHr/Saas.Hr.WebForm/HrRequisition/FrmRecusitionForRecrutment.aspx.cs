﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmRecusitionForRecrutment : BasePage
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        private string Addmode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtIssueDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddConcernDeartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddDesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddFactoryIncharge,
                  "EmployeePersonalInfo",
                  "EmployeeName",
                  "EmployeePersonalInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddApprovedBy,
                  "EmployeePersonalInfo",
                  "EmployeeName",
                  "EmployeePersonalInfoId", " Where PIN like '%MPS%'");

                DataTable myDt = new DataTable();
                myDt = CreateDataTable();
                ViewState["myDatatable"] = myDt;

                v_loadGridView_Item();
                //v_loadGridView_CostingHead();
            }
        }
        private void v_loadGridView_Item()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcRequiredManpowerwithDesignationSelectLoad", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.DataBind();

            cmd = new SqlCommand("select r.autoId as trAutoId,r.referenceNumber as RefNo,d.Department as Department from StaffRequisitionForRecruitment r inner join DepartmentInfo d on r.DepartmentAutoId = d.DepartmentInfoId", sqlconnection);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();

            sqlconnection.Close();
        }
        private void v_loadGridView_CostingHead()
        {
            if (hidStaffRequisitionAutoId.Value == "")
            { hidStaffRequisitionAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcRequiredManpowerwithDesignationSelect'" + hidStaffRequisitionAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_costingHead.DataSource = ds;
            gdv_costingHead.AllowPaging = true;
            gdv_costingHead.PageSize = 8;
            gdv_costingHead.DataBind();
            sqlconnection.Close();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            if (btn_save.Text == "Update")
            {

                s_Update = "ProcStaffRequisitionSubmitForApprove"
                + "'"
                + hidStaffRequisitionAutoId.Value
                + "','S'";//Submit option
                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        var receivers = new List<string>
                        {
                            hidOfficialMail.Value
                        };
                        var mailOptions = new MailSendingOption(receivers, null, ddApprovedBy.SelectedItem.ToString(), "Staff Requisition for approval", GenerateRequisitionInfoForMail(hidStaffRequisitionAutoId.Value), null);
                        try
                        {
                            MailSender.SendMail(mailOptions);
                        }
                        catch (Exception exception)
                        {
                            // ignored
                            MessageBox.ShowMessageInLabel(exception.Message, lblMsg, MessageType.ErrorMessage);
                        }

                    }
                }

            }

        }

        private string GenerateRequisitionInfoForMail(string reqId)
        {
            var baseUrl = string.Empty;
            if (Request.ApplicationPath != null)
            {
                baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                 Request.ApplicationPath.TrimEnd('/') + "/";
            }

            string SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            string SessionCompany = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            string reqDate = Convert.ToDateTime(txtIssueDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            var sb = new StringBuilder();
            sb.Append($@"
                    <p>Dear Mr./Mrs. {ddApprovedBy.SelectedItem.ToString()},</p>
                    <p>Please Approve the staff Requisition No. {txtRefNo.Text} and Dated {reqDate} for the concern department {ddConcernDeartment.SelectedItem.ToString()} for your kind information and evaluation.</p>
                    ");
            sb.Append($@"</tbody>
                    </table>
                    <p>Thanking You</p>
                    <p>{SessionUserName}</p>
                    <p>{SessionCompany}</p>

                    <p><a href=""{baseUrl}"" target=""_blank"">Click here</a> to open software.</p>
                    <p> Developed by : <a href=""http://stmsoftwareltd.com/"" target=""_blank"">STM Software Ltd</a>.</p>
                    ");

            return sb.ToString();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            //Boolean b_UserReturn = true;
            //b_UserReturn = isUserCheck();
            //if (b_UserReturn == false && btn_save.Text == "Update")
            //{
            //    return;
            //}

            string SessionUserType = Convert.ToString(Session[GlobalVariables.g_s_userStatus]);

            if (btn_save.Text == "Update")
            {
                dataCollectionForUpdate();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Update Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
            else
            {
                dataCollection();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Saved Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
        }
        private void dataCollection()
        {
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";
            //maxSerial();

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcStaffRequisitionForRecruitmentInsert]"
                            + "'"
                            + Convert.ToDateTime(txtIssueDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                            + "','"
                            + HttpUtility.HtmlDecode(txtRefNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + ddConcernDeartment.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtExistingManpower.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRecruitmentDescription.Text.Trim().Replace("'", "''"))
                            + "','"
                            + ddFactoryIncharge.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtFactoryInchargeComments.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtManpowerRecruimentJustification.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtMediaPublication.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtTimeFrameOfRecuritment.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtExperienceDetails.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRecomendationOfExecutiveDirector.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtApprovalOfChairman.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + ddApprovedBy.SelectedValue
                            + "','"
                            + SessionUserId
                            + "'";

            ///Master Details Data Save
            string tempData = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                string DesignationAutoId = string.Empty;
                string Designation = string.Empty;
                string ManPowerMale = string.Empty;
                string ManPowerFeMale = string.Empty;

                DesignationAutoId = Convert.ToString(dr["DesignationAutoId"]);
                Designation = Convert.ToString(dr["Designation"]);
                ManPowerMale = Convert.ToString(dr["ManPowerMale"]);
                ManPowerFeMale = Convert.ToString(dr["ManPowerFeMale"]);

                tempData = tempData + "Execute ProcStaffRequisitionForRecruitmentDetailsInsert"
                             + "'"
                             + HttpUtility.HtmlDecode(txtRefNo.Text.Trim())
                             + "','"
                             + DesignationAutoId
                             + "','"
                             + ManPowerMale
                             + "','"
                             + ManPowerFeMale
                             + "','0','','','' ";
            }
            hidQryD.Value = hidQryD.Value + tempData + " Select ''";
            tempData = "";
        }
        private void dataCollectionForUpdate()
        {
            //hidSONo.Value = txt_OrderSl.Text.Trim();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";

            ///Master Data Save Invoice
            hidQryB.Value = "";

            hidQryB.Value = "[ProcStaffRequisitionForRecruitmentUpdate]"
                           + "'"
                           + hidStaffRequisitionAutoId.Value
                           + "','"
                           + Convert.ToDateTime(txtIssueDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                           + "','"
                           + HttpUtility.HtmlDecode(txtRefNo.Text.Trim().Replace("'", "''"))
                           + "','"
                           + ddConcernDeartment.SelectedValue
                           + "','"
                           + HttpUtility.HtmlDecode(txtExistingManpower.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtRecruitmentDescription.Text.Trim().Replace("'", "''"))
                           + "','"
                           + ddFactoryIncharge.SelectedValue
                           + "','"
                           + HttpUtility.HtmlDecode(txtFactoryInchargeComments.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtManpowerRecruimentJustification.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtMediaPublication.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtTimeFrameOfRecuritment.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtExperienceDetails.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtRecomendationOfExecutiveDirector.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtApprovalOfChairman.Text.Trim().Replace("'", "''"))
                           + "','"
                           + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                           + "','"
                           + SessionCompanyId
                           + "','"
                           + ddApprovedBy.SelectedValue
                           + "','"
                           + SessionUserId
                           + "'";

            ///Master Details Data Save
            string tempData = string.Empty;
            tempData = "";
            hidQryD.Value = "";
            foreach (DataRow dr in dt.Rows)
            {
                string DesignationAutoId = string.Empty;
                string Designation = string.Empty;
                string ManPowerMale = string.Empty;
                string ManPowerFeMale = string.Empty;

                DesignationAutoId = Convert.ToString(dr["DesignationAutoId"]);
                Designation = Convert.ToString(dr["Designation"]);
                ManPowerMale = Convert.ToString(dr["ManPowerMale"]);
                ManPowerFeMale = Convert.ToString(dr["ManPowerFeMale"]);

                tempData = tempData + "Execute ProcStaffRequisitionForRecruitmentDetailsInsert"
                             + "'"
                             + HttpUtility.HtmlDecode(txtRefNo.Text.Trim())
                             + "','"
                             + DesignationAutoId
                             + "','"
                             + ManPowerMale
                             + "','"
                             + ManPowerFeMale
                             + "','0','','','' ";
            }
            hidQryD.Value = hidQryD.Value + tempData + " Select ''";
            tempData = "";
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            CommonFunctions commonFunctions = new CommonFunctions();
            DML dml = new DML();
            DataTable dtt = (DataTable)ViewState["myDatatable"];
            string s_Rqty = string.Empty;
            string s_Qry = string.Empty;

            Boolean b_required = true;
            lblMsg.Visible = false;
            b_required = isrequired();
            if (b_required == false)
            {
                return;
            }

            if (hid_Addmode.Value == "N")
            {
                btnDel_Click(sender, e);
            }

            if (txtMale.Text == "")
            {
                txtMale.Text = "0";
            }
            if (txtFemale.Text == "")
            {
                txtFemale.Text = "0";
            }
            if (txtMale.Text == "0" && txtFemale.Text == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Manpower Is Blank!";
                return;
            }

            foreach (DataRow dr in dtt.Rows)
            {
                string iRate = string.Empty;
                string iId = string.Empty;
                string sDesignationId = string.Empty;
                sDesignationId = Convert.ToString(dr["DesignationAutoId"]);

                if (sDesignationId == ddDesignation.SelectedValue)// && iRate == txtRate.Text)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Already Added!";
                    return;
                }
            }

            DataTable myDatatable = (DataTable)ViewState["myDatatable"];
            AddDataToTable(this.ddDesignation.SelectedValue
                        , this.ddDesignation.SelectedItem.ToString()
                        , this.txtMale.Text.Trim()
                        , this.txtFemale.Text.Trim()
                        , (DataTable)ViewState["myDatatable"]
                     );

            this.gdv_costingHead.DataSource = ((DataTable)ViewState["myDatatable"]).DefaultView;
            this.gdv_costingHead.DataBind();
            hid_Rownumber.Value = Convert.ToString(Convert.ToDouble(hid_Rownumber.Value) + 1);
            AddClear();
        }

        private void AddDataToTable(string DesignationAutoId,
                                    string Designation,
                                    string ManPowerMale,
                                    string ManPowerFeMale,
                                    DataTable myTable)
        {
            DataRow row = myTable.NewRow();
            row["DesignationAutoId"] = DesignationAutoId;
            row["Designation"] = Designation;
            row["ManPowerMale"] = ManPowerMale;
            row["ManPowerFeMale"] = ManPowerFeMale;
            myTable.Rows.Add(row);
        }

        private DataTable CreateDataTable()
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Columns.Add(new DataColumn("DesignationAutoId", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Designation", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("ManPowerMale", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("ManPowerFeMale", typeof(string)));
            return myDataTable;
        }
        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (gdv_costingHead.SelectedIndex != -1)
            {
                ViewState["Addmode"] = "Y";
                Addmode = Convert.ToString(ViewState["Addmode"]);
                hid_Addmode.Value = "Y";
                DataTable dt = (DataTable)ViewState["myDatatable"];
                dt.Rows[gdv_costingHead.SelectedIndex].Delete();
                dt.AcceptChanges();
                gdv_costingHead.DataSource = dt;
                gdv_costingHead.DataBind();

                if (Convert.ToDouble(hid_Rownumber.Value) > 0)
                {
                    hid_Rownumber.Value = Convert.ToString(Convert.ToDouble(hid_Rownumber.Value) - 1);
                }
            }
        }
        private void AddClear()
        {
            ddDesignation.SelectedValue = "0";
            txtMale.Text = string.Empty;
            txtFemale.Text = string.Empty;
        }
        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                ddDesignation.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidDesignationId")).Value;
                txtMale.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridManPowerMale")).Text;
                txtFemale.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridManPowerFeMale")).Text;
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }


        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidStaffRequisitionAutoId.Value = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidRefId")).Value;
                if (hidStaffRequisitionAutoId.Value != "" && hidStaffRequisitionAutoId.Value != "0")
                {
                    getEmpData(hidStaffRequisitionAutoId.Value);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }
        private Boolean isrequired()
        {
            if (string.IsNullOrEmpty(txtMale.Text.Trim()) && string.IsNullOrEmpty(txtFemale.Text.Trim()))
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Qty No Blank!";
                return false;
            }
            if (ddDesignation.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Designation Type Blank!";
                return false;
            }


            return true;
        }
        private Boolean isValid()
        {

            if (ddApprovedBy.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Approve By Can Not Blank!";
                return false;
            }
            if (ddConcernDeartment.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Concern Deartment Can Not Blank!";
                return false;
            }
            if (txtRefNo.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Ref No. Can Not Blank!";
                return false;
            }
            if (txtExistingManpower.Text == "")
            {
                txtExistingManpower.Text = "0";
            }
            //if (hid_Rownumber.Value == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Man Power List Blank!";
            //    return false;
            //}

            return true;
        }
        private void BlankforSaerch()
        {
            gdv_costingHead.DataSource = null;
            gdv_costingHead.DataBind();
            DataTable myDt = new DataTable();
            myDt = CreateDataTable();
            ViewState["myDatatable"] = myDt;
            hid_Rownumber.Value = "0";
            v_loadGridView_Item();
            txtIssueDate.Focus();
        }
        private void InsertMode()
        {
            btn_save.Text = "Save";
            //txtPIN.Text = string.Empty;
            //txtEmpName.Text = string.Empty;
            //txtCard.Text = string.Empty;
            txtMale.Text = string.Empty;
            txtIssueDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtRemarks.Text = string.Empty;

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidStaffRequisitionAutoId.Value != "" && hidStaffRequisitionAutoId.Value != "0")
            {
                getEmpData(hidStaffRequisitionAutoId.Value);
            }
        }

        private void getEmpData(string sStaffRequisitionId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            hidApproveStatus.Value = "N";
            BlankforSaerch();

            s_select = "SELECT * FROM StaffRequisitionForRecruitment r inner join EmployeePersonalInfo p on r.ApprovedbyId=p.EmployeePersonalInfoId  Where autoId='" + sStaffRequisitionId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtIssueDate.Text = drow["inDate"].ToString();
                        txtRefNo.Text = drow["referenceNumber"].ToString();
                        ddConcernDeartment.SelectedValue = drow["DepartmentAutoId"].ToString();
                        txtExistingManpower.Text = drow["ExistingManPower"].ToString();
                        txtRecruitmentDescription.Text = drow["NecessityRecruitment"].ToString();
                        ddFactoryIncharge.SelectedValue = drow["FactoryInchargeAutoId"].ToString();
                        txtFactoryInchargeComments.Text = drow["FactoryInchargeNote"].ToString();
                        txtManpowerRecruimentJustification.Text = drow["RecruitJustification"].ToString();
                        txtMediaPublication.Text = drow["RecruitPublications"].ToString();
                        txtTimeFrameOfRecuritment.Text = drow["RecruitTimeFrame"].ToString();
                        txtExperienceDetails.Text = drow["RecruitExperience"].ToString();
                        txtRecomendationOfExecutiveDirector.Text = drow["RecomendationNote"].ToString();
                        txtApprovalOfChairman.Text = drow["ApprovalNote"].ToString();
                        txtRemarks.Text = drow["Remarks"].ToString();
                        ddApprovedBy.SelectedValue = drow["ApprovedbyId"].ToString();
                        hidOfficialMail.Value = drow["OfficialMail"].ToString();
                        hidApproveStatus.Value = drow["ApprovalStatus"].ToString();
                    }

                    //PI Discription Part
                    s_select = "[ProcRequiredManpowerwithDesignationSelect] '" + hidStaffRequisitionAutoId.Value + "'";

                    s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
                    if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                    {
                        if (connection.ResultsDataSet.Tables[0] != null)
                        {
                            foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                            {
                                string DesignationAutoId = string.Empty;
                                string Designation = string.Empty;
                                string ManPowerMale = string.Empty;
                                string ManPowerFeMale = string.Empty;

                                DesignationAutoId = drow["DesignationAutoId"].ToString();
                                Designation = drow["Designation"].ToString();
                                ManPowerMale = drow["ManPowerMale"].ToString();
                                ManPowerFeMale = drow["ManPowerFeMale"].ToString();


                                DataTable myDatatable = (DataTable)ViewState["myDatatable"];
                                AddDataToTable(DesignationAutoId
                                            , Designation
                                            , ManPowerMale
                                            , ManPowerFeMale
                                            , (DataTable)ViewState["myDatatable"]
                                         );

                                this.gdv_costingHead.DataSource = ((DataTable)ViewState["myDatatable"]).DefaultView;
                                this.gdv_costingHead.DataBind();
                                hid_Rownumber.Value = Convert.ToString(Convert.ToDouble(hid_Rownumber.Value) + 1);
                            }
                        }
                    }

                    if (hidApproveStatus.Value!="N")
                    {
                        btn_save.Visible = false;

                        if (hidApproveStatus.Value == "S")
                        {
                            lblMsg.Text = "Already Submitted!!";
                        }
                        if (hidApproveStatus.Value == "A")
                        {
                            lblMsg.Text = "Already Approved!!";
                        }
                    }
                    btn_save.Text = "Update";

                }
            }
        }


        protected void btnPreview_Click(object sender, EventArgs e)
        {
            Session[GlobalVariables.g_s_rptforSLNo] = txtRefNo.Text;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../RptViewer/ViewStaffRequisitionSheet.aspx');", true);
        }


        //===========End===============         
    }
}