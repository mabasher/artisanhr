﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmCandidateInformation : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        private string Addmode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");

                commonfunctions.g_b_FillDropDownList(ddDegree,
                    "ExamName",
                    "ExamName",
                    "ExamNameId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddDesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddRequisition,
                    "StaffRequisitionForRecruitment",
                    "referenceNumber",
                    "autoId", string.Empty);



            }
        }
    


        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;
            
            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            //Boolean b_UserReturn = true;
            //b_UserReturn = isUserCheck();
            //if (b_UserReturn == false && btn_save.Text == "Update")
            //{
            //    return;
            //}

            string SessionUserType = Convert.ToString(Session[GlobalVariables.g_s_userStatus]);
            

            if (btn_save.Text == "Update")
            {
                dataCollectionForUpdate();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                hidQryD.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Update Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
            else
            {
                dataCollection();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                hidQryD.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Saved Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
        }
        private void dataCollection()
        {
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

             SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";
            //maxSerial();

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcCandidateInformationInsert]"
                            + "'"
                            + ddDesignation.SelectedValue
                            + "','"
                            + ddRequisition.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtrecruitmentId.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRecruitmentCode.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtCVSerialNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtAdmitCardNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                            + "','"
                            + HttpUtility.HtmlDecode(txtFromTime.Text.Trim()) 
                            + "','"
                            + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtVanue.Text.Trim().Replace("'", "''"))
                            + "','"
                            + ddDegree.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtExamTitle.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtInstituteName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtBoard.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtGroup.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtResult.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtCGPAMarks.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtScaleOutOf.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtPassingYear.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtDuration.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtAchivement.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtExperience.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "','" 
                            + SessionUserIP
                            + "'";
        }
        private void dataCollectionForUpdate()
        {
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcCandidateInformationInsert]"
                             + "'"
                             + hidCandidateInfoAutoId.Value
                             + "','"
                             + ddDesignation.SelectedValue
                             + "','"
                             + ddRequisition.SelectedValue
                             + "','"
                             + HttpUtility.HtmlDecode(txtName.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtrecruitmentId.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtRecruitmentCode.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtCVSerialNo.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtAdmitCardNo.Text.Trim().Replace("'", "''"))
                             + "','"
                             + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                             + "','"
                             + HttpUtility.HtmlDecode(txtFromTime.Text.Trim())
                             + "','"
                             + HttpUtility.HtmlDecode(txtPresentAddress.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtVanue.Text.Trim().Replace("'", "''"))
                             + "','"
                             + ddDegree.SelectedValue
                             + "','"
                             + HttpUtility.HtmlDecode(txtExamTitle.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtInstituteName.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtBoard.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtGroup.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtResult.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtCGPAMarks.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtScaleOutOf.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtPassingYear.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtDuration.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtAchivement.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtExperience.Text.Trim().Replace("'", "''"))
                             + "','"
                             + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                             + "','"
                             + SessionCompanyId
                             + "','"
                             + SessionUserId
                             + "','"
                             + SessionUserIP
                             + "'";
        }
        
        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }
        private Boolean isrequired()
        {
            //if (string.IsNullOrEmpty(txtMale.Text.Trim()) && string.IsNullOrEmpty(txtFemale.Text.Trim()))
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Qty No Blank!";
            //    return false;
            //}
            if (ddDesignation.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Designation Type Blank!";
                return false;
            }
            

            return true;
        }
        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (txtExistingManpower.Text=="")
            //{
            //    txtExistingManpower.Text = "0";
            //}
            if (txtName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Name Can Not Blank!";
                return false;
            }

            //if (ddDays.SelectedItem.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Days Can Not Blank!";
            //    return false;
            //}



            //if (imgUpload.HasFile)
            //{
            //    int imgSize = imgUpload.PostedFile.ContentLength;
            //    string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
            //    if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
            //    {
            //        lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //    else if (imgSize > 800000)//1048576)//1048576 bit=1MB
            //    {
            //        lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //}


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            //txtPIN.Text = string.Empty;
            //txtEmpName.Text = string.Empty;
            //txtCard.Text = string.Empty;
            //txtMale.Text = string.Empty;
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtFromTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            txtRemarks.Text = string.Empty;
            
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidCandidateInfoAutoId.Value != "" && hidCandidateInfoAutoId.Value != "0")
            {
                getEmpData(hidCandidateInfoAutoId.Value);
            }
        }

        private void getEmpData(string hidCandidateInfoAutoId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM CandidateInformation Where autoId='" + hidCandidateInfoAutoId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        ddDesignation.SelectedValue = drow["DesignationAutoId"].ToString();
                        ddRequisition.SelectedValue = drow["RequisitionAutoId"].ToString();
                        txtName.Text = drow["CandidateName"].ToString();
                        txtrecruitmentId.Text = drow["RecruitmentId"].ToString();
                        txtRecruitmentCode.Text = drow["RecruitmentCode"].ToString();
                        txtCVSerialNo.Text = drow["CVSerialNo"].ToString();
                        txtAdmitCardNo.Text = drow["AdmitCardNo"].ToString();
                        txtFromDate.Text = drow["ExamDate"].ToString();
                        txtFromTime.Text = drow["ExamTime"].ToString();
                        txtPresentAddress.Text = drow["PresentAddress"].ToString();
                        txtVanue.Text = drow["Vanue"].ToString();
                        ddDegree.SelectedValue = drow["LastDegreeId"].ToString();
                        txtExamTitle.Text = drow["ExamTitle"].ToString();
                        txtInstituteName.Text = drow["InstituteName"].ToString();
                        txtBoard.Text = drow["Board_University"].ToString();
                        txtGroup.Text = drow["Major_Group"].ToString();
                        txtResult.Text = drow["Result"].ToString();
                        txtCGPAMarks.Text = drow["CGPA_Marks"].ToString();
                        txtScaleOutOf.Text = drow["Scale_OutOf"].ToString();
                        txtPassingYear.Text = drow["PassingYear"].ToString();
                        txtDuration.Text = drow["Duration"].ToString();
                        txtAchivement.Text = drow["Achivement"].ToString();
                        txtExperience.Text = drow["Experience"].ToString();
                        txtRemarks.Text = drow["Remarks"].ToString();
                    }
                }
            }
           
        }
      //===========End===============         
    }
} 