﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.HrRequisition
{
    public partial class FrmExamResultApprove : BasePage
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtSearchFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddReqNo,
                    "StaffRequisitionForRecruitment",
                    "referenceNumber",
                    "AutoId", "Where AutoId in (Select RequisitionId from CandidateExamResult)");

                v_loadGridView_gdv_Attendence();
            }
        }

        private void v_loadGridView_gdv_Attendence()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            string qry = "ProcSelectCandidateForSelection '" + ddReqNo.SelectedValue + "'";
            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdv_Attendence.DataSource = ds;
            gdv_Attendence.DataBind();
            sqlconnection.Close();

            int count = ds.Tables.Count;  //it will give the total number of tables in your dataset.
            count = ds.Tables[0].Rows.Count;
            lblCounter.Text = "Row No : " + count.ToString();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }
            s_save_ = " Delete CandidateExamResult Where  RequisitionId = '" + ddReqNo.SelectedValue + "'";
            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string CandidateId, cComments, cResult = string.Empty;
                CandidateId = ((TextBox)gdvRow.Cells[0].FindControl("txtCandidateId")).Text;
                cComments = ((TextBox)gdvRow.Cells[0].FindControl("txtcComments")).Text;
                cResult = ((TextBox)gdvRow.Cells[0].FindControl("txtcResult")).Text;
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true && CandidateId != "")
                {
                    s_save_ = s_save_ + "Execute ProcCandidateExamResultForApprove "
                                  + "'"
                                  + CandidateId
                                  + "','"
                                  + ddReqNo.SelectedValue
                                  + "','A'";
                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string s_Card = string.Empty;
                string s_fromDate = string.Empty;


                //s_empAutoId = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidempAutoId")).Value;
                //s_fromDate = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridDOB")).Text;

                s_Card = ((Label)gdvRow.Cells[0].FindControl("lblGridCardNo")).Text;
                s_fromDate = ((Label)gdvRow.Cells[0].FindControl("lblGridDOB")).Text;

                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkSelect");
                if (checkbox.Checked == true)
                {
                    s_save_ = s_save_ + "Execute ProcDailyAttandenceDELETE-1 "
                                  + "'"
                                  + s_Card
                                  + "','"
                                  + Convert.ToDateTime(s_fromDate.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                  + "'";
                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }


        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {

            if (ddReqNo.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Req Ref No Can Not Blank!";
                return false;
            }
            string s_t = string.Empty;
            s_t = "N";
            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string s_CardId = string.Empty;
                s_CardId = ((TextBox)gdvRow.Cells[0].FindControl("txtCandidateId")).Text;

                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true && s_CardId != "")
                {
                    s_t = "Y";
                }
            }
            if (s_t == "N")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Candidate List Can Not Blank!";
                return false;
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            v_loadGridView_gdv_Attendence();
        }


        protected void btnSend_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            foreach (GridViewRow gdvRow in gdv_Attendence.Rows)
            {
                string CandidateId = string.Empty;
                CandidateId = ((TextBox)gdvRow.Cells[0].FindControl("txtCandidateId")).Text;
                CheckBox checkbox = (CheckBox)gdvRow.Cells[0].FindControl("chkApproval");
                if (checkbox.Checked == true && CandidateId != "")
                {
                    s_save_ = s_save_ + "Execute ProcCandidateExamResultForApprove "
                                  + "'"
                                  + CandidateId
                                  + "',,'S'";
                }
            }
            //Insert Option
            s_save_ = s_save_ + "  Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);
            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        var receivers = new List<string>
                        {
                            hidOfficialMail.Value
                        };
                        var mailOptions = new MailSendingOption(receivers, null, ddApprovedBy.SelectedItem.ToString(), "Candidate Confirmation for Approved", GenerateRequisitionInfoForMail(ddReqNo.SelectedValue), null);
                        try
                        {
                            MailSender.SendMail(mailOptions);
                        }
                        catch (Exception exception)
                        {
                            // ignored
                            MessageBox.ShowMessageInLabel(exception.Message, lblMsg, MessageType.ErrorMessage);
                        }

                    }
                }

            }

        }

        private string GenerateRequisitionInfoForMail(string reqId)
        {
            var baseUrl = string.Empty;
            if (Request.ApplicationPath != null)
            {
                baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                 Request.ApplicationPath.TrimEnd('/') + "/";
            }

            string SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            string SessionCompany = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            string reqDate = Convert.ToDateTime(txtSearchFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            var sb = new StringBuilder();
            sb.Append($@"
                    <p>Dear Mr./Mrs. {ddApprovedBy.SelectedItem.ToString()},</p>
                    <p>Please Approve the Candidates of staff Requisition No. {ddReqNo.SelectedItem.ToString()} and Dated {reqDate} for your kind information and evaluation.</p>
                    ");
            sb.Append($@"</tbody>
                    </table>
                    <p>Thanking You</p>
                    <p>{SessionUserName}</p>
                    <p>{SessionCompany}</p>

                    <p><a href=""{baseUrl}"" target=""_blank"">Click here</a> to open software.</p>
                    <p> Developed by : <a href=""http://stmsoftwareltd.com/"" target=""_blank"">STM Software Ltd</a>.</p>
                    ");

            return sb.ToString();
        }



    }
}