﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmRequisitionApproval.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmRequisitionApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Staff Requisition Approval
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div class="row row-custom">

            <div class="col-md-3">
                
                                <div style="height: 650px; overflow: scroll;">
                                    <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0;"
                                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                            </asp:CommandField>
                                            
                                            <asp:TemplateField HeaderText="Ref. No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridNo" runat="server" Text='<%# Bind("RefNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                                    <asp:HiddenField ID="hidRefId" Value='<%# Bind("trAutoId") %>' runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pagination-sa" />
                                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                    </asp:GridView>
                                </div>
            </div>
            <div class="col-md-9">

                <div class="col-md-12">
                    <div class="form-horizontal">
                        <div class="form-group hidden">
                            <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Requisition..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 hidden">
                                <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label18" runat="server" Text="Issue Date :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtIssueDate" CssClass="input-sm date-picker" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Ref. No :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtRefNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Concern Deartment :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddConcernDeartment" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                            <asp:Label ID="Label5" runat="server" Text="Existing Manpower :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-1">
                                <asp:TextBox ID="txtExistingManpower" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <fieldset>
                            <legend>Required Manpower with Designation</legend>

                            <div class="form-group">
                                <asp:Label ID="Label15" runat="server" Text="Designation :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                                <asp:Label ID="Label17" runat="server" Text="Male :" CssClass="col-sm-1 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtMale" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <asp:Label ID="Label23" runat="server" Text="Female :" CssClass="col-sm-1 control-label"></asp:Label>
                                <div class="col-sm-1">
                                    <asp:TextBox ID="txtFemale" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn btn-primary btn-xs" Text="Add" />
                                    <asp:Button ID="btnDel" OnClick="btnDel_Click" runat="server" CssClass="btn btn-warning btn-xs" Text="Del" />
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div style="height: 100px;">
                                    <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                        CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                            </asp:CommandField>

                                            <asp:TemplateField HeaderText="Designation">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Male">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridManPowerMale" runat="server" Text='<%# Bind("ManPowerMale") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Female">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGridManPowerFeMale" runat="server" Text='<%# Bind("ManPowerFeMale") %>'></asp:Label>
                                                    <%-- <asp:HiddenField ID="hidRecruitmentAutoId" Value='<%# Bind("RecruitmentAutoId") %>' runat="server" />--%>
                                                    <asp:HiddenField ID="hidDesignationId" Value='<%# Bind("DesignationAutoId") %>' runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pagination-sa" />
                                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                    </asp:GridView>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-group" style="margin-top: 5px;">
                            <asp:Label ID="Label6" runat="server" Text="Narration for Recruitment :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtRecruitmentDescription" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label8" runat="server" Text="Department Incharge :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddFactoryIncharge" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                            <div class="col-sm-5">
                                <asp:TextBox ID="txtFactoryInchargeComments" placeholder="Incharge Comments" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>

                        <fieldset>
                            <legend>Human Resource Department</legend>

                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Manpower Recruitment Justification :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtManpowerRecruimentJustification" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="Media to be Used for Publication :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtMediaPublication" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text="Time Frame of Recruitment :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtTimeFrameOfRecuritment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text="Brief In Experience Details :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtExperienceDetails" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-group" style="margin-top: 5px;">
                            <asp:Label ID="Label13" runat="server" Text="Recommendation By :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtRecomendationOfExecutiveDirector" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <asp:Label ID="Label14" runat="server" Text="Approval By :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtApprovalOfChairman" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label11" runat="server" Text="Approved By :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddApprovedBy" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label16" runat="server" Text="Note :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row row-custom" style="padding-top: 20px;">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-11">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" Visible="false" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" Visible="false" />
                                <asp:Button ID="btnSend" runat="server" CssClass="btn btn-primary btn-sm" Text="Approve" OnClick="btnSend_Click" Width="150px" />
                                <asp:Button ID="btnReject" runat="server" CssClass="btn btn-danger btn-sm" Text="Reject" OnClick="btnReject_Click" Width="150px" />
                                <asp:Button ID="btnPreview" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btnPreview_Click" Width="80px" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="0" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />

        <asp:HiddenField ID="hidQryA" runat="server" Value="" />
        <asp:HiddenField ID="hidQryB" runat="server" Value="" />
        <asp:HiddenField ID="hidQryC" runat="server" Value="" />
        <asp:HiddenField ID="hidQryD" runat="server" Value="" />
        <asp:HiddenField ID="hidQryE" runat="server" Value="" />
        <asp:HiddenField ID="hidOfficialMail" runat="server" Value="" />
        
        <asp:HiddenField ID="hidDesignationId" runat="server" Value="0" />

        <asp:HiddenField ID="hidStaffRequisitionAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hid_Addmode" runat="server" Value="Y" />
        <asp:HiddenField ID="hidApproveStatus" runat="server" Value="" />
        <asp:HiddenField ID="hid_Rownumber" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/StaffRequisition.asmx/GetStaffRequisition") %>',
                '#<%=hidStaffRequisitionAutoId.ClientID %>');
        });

    </script>

</asp:Content>
