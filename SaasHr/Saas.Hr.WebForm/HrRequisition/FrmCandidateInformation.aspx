﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmCandidateInformation.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmCandidateInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Candidate Information
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>


        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-8">
                        <div class="form-horizontal">
                            <div class="form-group ">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Candidate..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-sm-3 hidden">
                                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>

                            <fieldset>
                                <legend>Candidate Information</legend>
                               
                                <div class="form-group">
                                    <asp:Label ID="Label15" runat="server" Text="Requisition No :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddRequisition" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                    <asp:Label ID="Label17" runat="server" Text="Name of The Post" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="Ref Media :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtrecruitmentId" Visible="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        <asp:DropDownList ID="ddMedia" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Media" runat="server"></asp:DropDownList>
                                    </div>
                                    <asp:Label ID="Label5" runat="server" Text="Recruitment Code :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtRecruitmentCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label27" runat="server" Text="Date Of Birth :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox runat="server" ID="txtdob" CssClass="input-sm date-picker" />
                                    </div>
                                </div>

                                <div class="form-group hidden">
                                    <asp:Label ID="Label11" runat="server" Text="CV Serial No :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtCVSerialNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label18" runat="server" Text="Admit Card No :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtAdmitCardNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <asp:Label ID="Label19" runat="server" Text="Exam Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                    </div>
                                    <asp:Label ID="Label20" runat="server" Text="Time :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtFromTime" runat="server" CssClass="form-control input-sm timepicker"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label22" runat="server" Text="Present Address :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtPresentAddress" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label23" runat="server" Text="Vanue :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtVanue" TextMode="MultiLine" Height="60" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Last Academic Qualification</legend>

                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="Degree :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddDegree" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                     <asp:Label ID="Label6" runat="server" Text="Exam Title :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtExamTitle" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                               

                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="Institute Name :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtInstituteName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                     <asp:Label ID="Label12" runat="server" Text="Board/University :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtBoard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="Major/Group :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtGroup" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label9" runat="server" Text="Result :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtResult" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" Text="CGPA/Marks% :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtCGPAMarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label14" runat="server" Text="Scale/Out Of :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtScaleOutOf" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label21" runat="server" Text="Passing Year :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtPassingYear" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:Label ID="Label24" runat="server" Text="Duration :" CssClass="col-sm-2 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtDuration" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label25" runat="server" Text="Achivement :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtAchivement" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label16" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>

                            </fieldset>
                            <div class="form-group">
                                <asp:Label ID="Label26" runat="server" Text="Experience :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtExperience" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>





                        </div>
                    </div>

                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />

        <asp:HiddenField ID="hidQryA" runat="server" Value="" />
        <asp:HiddenField ID="hidQryB" runat="server" Value="" />
        <asp:HiddenField ID="hidQryC" runat="server" Value="" />
        <asp:HiddenField ID="hidQryD" runat="server" Value="" />
        <asp:HiddenField ID="hidQryE" runat="server" Value="" />

        <asp:HiddenField ID="hidDesignationId" runat="server" Value="0" />

        <asp:HiddenField ID="hidCandidateInfoAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hid_Addmode" runat="server" Value="Y" />
        <asp:HiddenField ID="hid_Rownumber" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/CandidateInformation.asmx/GetCandidateInformation") %>',
                '#<%=hidCandidateInfoAutoId.ClientID %>');
        });

    </script>

</asp:Content>
