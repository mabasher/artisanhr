﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmCandidateInfo : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        private string Addmode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtDOB.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddRequisitionNo,
                    "StaffRequisitionForRecruitment",
                    "referenceNumber",
                    "autoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddDesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddRefMedia,
                    "T_RefMedia",
                    "RefMedia",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBirthPlace,
                    "District",
                    "DistrictName",
                    "DistrictId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddNationality,
                    "NationalityInfo",
                    "NationalityName",
                    "NationalityInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddMaritalStatus,
                   "MaritalStatusInfo",
                   "MaritalStatusName",
                   "MaritalStatusInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddlastQualification,
                   "ExamName",
                   "ExamName",
                   "ExamNameId", string.Empty);
              
                commonfunctions.g_b_FillDropDownList(ddBloodGroup,
                  "BloodGroupInfo",
                    "BloodGroupName",
                    "BloodGroupInfoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddGender,
                    "GenderInfo",
                    "GenderName",
                    "GenderInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddReligion,
                   "ReligionInfo",
                   "ReligionName",
                   "ReligionInfoId", string.Empty);
                


                v_loadGridView_Item();
            }
        }
        private void v_loadGridView_Item()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("ProcCandidateInfoSelectLoad", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            gdvList.DataBind();

            sqlconnection.Close();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {

            }

        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            //Boolean b_UserReturn = true;
            //b_UserReturn = isUserCheck();
            //if (b_UserReturn == false && btn_save.Text == "Update")
            //{
            //    return;
            //}

            string SessionUserType = Convert.ToString(Session[GlobalVariables.g_s_userStatus]);


            if (btn_save.Text == "Update")
            {
                dataCollectionForUpdate();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                hidQryD.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Update Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
            else
            {
                dataCollection();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                hidQryD.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Saved Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
        }
        private void dataCollection()
        {
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";
            //maxSerial();

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcCandidateInfoInsert]"
                            + "'"
                            + ddRequisitionNo.SelectedValue
                            + "','"
                            + ddRefMedia.SelectedValue
                            + "','"
                            + ddDesignation.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtRequitmentCode.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtCandidateName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtFathersName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtMothersName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                            + "','"
                            + ddBirthPlace.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtNID.Text.Trim().Replace("'", "''"))
                            + "','"
                            + ddNationality.SelectedValue
                            + "','"
                            + ddGender.SelectedValue
                            + "','"
                            + ddReligion.SelectedValue
                            + "','"
                            + ddMaritalStatus.SelectedValue
                            + "','"
                            + ddBloodGroup.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtExpectedSalary.Text.Trim())
                            + "','"
                            + ddlastQualification.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtGroupSubject.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtResult.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtBoardUniversity.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtInstituteName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtContactNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtEmail.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "','"
                            + SessionUserIP
                            + "'";
        }
        private void dataCollectionForUpdate()
        {
            //hidSONo.Value = txt_OrderSl.Text.Trim();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcCandidateInfoUpdate]"
                             + "'"
                             + hidAutoIdForUpdate.Value
                             + "','"
                            + ddRequisitionNo.SelectedValue
                            + "','"
                            + ddRefMedia.SelectedValue
                            + "','"
                            + ddDesignation.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtRequitmentCode.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtCandidateName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtFathersName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtMothersName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + Convert.ToDateTime(txtDOB.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                            + "','"
                            + ddBirthPlace.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtNID.Text.Trim().Replace("'", "''"))
                            + "','"
                            + ddNationality.SelectedValue
                            + "','"
                            + ddGender.SelectedValue
                            + "','"
                            + ddReligion.SelectedValue
                            + "','"
                            + ddMaritalStatus.SelectedValue
                            + "','"
                            + ddBloodGroup.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtExpectedSalary.Text.Trim())
                            + "','"
                            + ddlastQualification.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtGroupSubject.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtResult.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtBoardUniversity.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtInstituteName.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtContactNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtEmail.Text.Trim().Replace("'", "''"))
                            + "','"
                            + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "','"
                            + SessionUserIP
                            + "'";
        }

        private void AddClear()
        {
            ddDesignation.SelectedValue = "0";
            //txtMale.Text = string.Empty;
            //txtFemale.Text = string.Empty;
        }
       

        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                hidStaffRequisitionAutoId.Value = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidCandidateAutoId")).Value;
                if (hidStaffRequisitionAutoId.Value != "" && hidStaffRequisitionAutoId.Value != "0")
                {
                    getEmpData(hidStaffRequisitionAutoId.Value);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }
        private Boolean isrequired()
        {
            //if (string.IsNullOrEmpty(txtMale.Text.Trim()) && string.IsNullOrEmpty(txtFemale.Text.Trim()))
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Qty No Blank!";
            //    return false;
            //}
            if (ddDesignation.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Designation Type Blank!";
                return false;
            }


            return true;
        }
        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (txtExistingManpower.Text == "")
            //{
            //    txtExistingManpower.Text = "0";
            //}
            //if (hid_Rownumber.Value == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Man Power List Blank!";
            //    return false;
            //}



            //if (txtPIN.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "PIN Can Not Blank!";
            //    return false;
            //}

            //if (ddDays.SelectedItem.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Days Can Not Blank!";
            //    return false;
            //}



            //if (imgUpload.HasFile)
            //{
            //    int imgSize = imgUpload.PostedFile.ContentLength;
            //    string getext = Path.GetExtension(imgUpload.PostedFile.FileName);
            //    if (getext != ".JPEG" && getext != ".jpeg" && getext != ".JPG" && getext != ".jpg" && getext != ".PNG" && getext != ".png" && getext != ".tif" && getext != ".tiff")
            //    {
            //        lblMsg.Text = "Please Image Format Only jpeg,jpg,png,tif,tiff";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //    else if (imgSize > 800000)//1048576)//1048576 bit=1MB
            //    {
            //        lblMsg.Text = "Image Size Too Large..!! Should be maximum 100KB";
            //        lblMsg.Visible = true;
            //        return false;
            //    }
            //}


            return true;
        }
        private void BlankforSaerch()
        {
                hid_Rownumber.Value = "0";
            v_loadGridView_Item();
            txtDOB.Focus();
        }
        private void InsertMode()
        {
            btn_save.Text = "Save";
            //txtPIN.Text = string.Empty;
            //txtEmpName.Text = string.Empty;
            //txtCard.Text = string.Empty;
            //txtMale.Text = string.Empty;
            //txtIssueDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtRemarks.Text = string.Empty;

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidCandidateInfoAutoId.Value != "" && hidCandidateInfoAutoId.Value != "0")
            {
                getEmpData(hidCandidateInfoAutoId.Value);
            }
        }

        private void getEmpData(string sStaffRequisitionId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            BlankforSaerch();

            s_select = "SELECT * FROM CandidateInfo Where autoId='" + hidStaffRequisitionAutoId.Value + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        hidAutoIdForUpdate.Value = drow["autoId"].ToString();
                        ddRequisitionNo.SelectedValue = drow["RequisitionAutoId"].ToString();
                        ddRefMedia.SelectedValue = drow["RefMediaAutoId"].ToString();
                        ddDesignation.SelectedValue = drow["DesignationAutoId"].ToString();
                        txtRequitmentCode.Text = drow["RecruitmentCode"].ToString();
                        txtDOB.Text = drow["DOB"].ToString();
                        txtCandidateName.Text = drow["CandidateName"].ToString();
                        txtFathersName.Text = drow["FatherName"].ToString();
                        txtMothersName.Text = drow["MotherName"].ToString();
                        ddBirthPlace.SelectedValue = drow["DistrictId"].ToString();
                        txtNID.Text = drow["NID"].ToString();

                        ddNationality.SelectedValue = drow["NationalityId"].ToString();
                        ddGender.SelectedValue = drow["GenderId"].ToString();
                        ddReligion.SelectedValue = drow["ReligionId"].ToString();
                        ddMaritalStatus.SelectedValue = drow["MaritalStatusId"].ToString();

                        ddBloodGroup.SelectedValue = drow["BloodGroupId"].ToString();
                        txtExpectedSalary.Text = drow["ExpectedSalary"].ToString();
                        ddlastQualification.SelectedValue = drow["LastDegreeId"].ToString();
                        txtGroupSubject.Text = drow["GroupSubject"].ToString();
                        txtResult.Text = drow["Result"].ToString();
                        txtBoardUniversity.Text = drow["Board_University"].ToString();                      
                        txtInstituteName.Text = drow["InstituteName"].ToString();
                        txtContactNo.Text = drow["ContactNo"].ToString();
                        txtEmail.Text = drow["Email"].ToString();
                        txtRemarks.Text = drow["Remarks"].ToString();
                    }

                  
                    btn_save.Text = "Update";

                }
            }
        }
        //===========End===============         
    }
}