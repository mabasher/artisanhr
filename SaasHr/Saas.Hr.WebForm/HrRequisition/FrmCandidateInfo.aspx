﻿<%@ Page Title="Candidate Info" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmCandidateInfo.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmCandidateInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Candidate Info
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div class="row row-custom">

            <div class="col-md-3">

                <div style="height: 650px; overflow: scroll;">
                    <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="Req. No">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridNo" runat="server" Text='<%# Bind("ReqNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Candidate Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridCandidateName" runat="server" Text='<%# Bind("CandidateName") %>'></asp:Label>
                                    <asp:HiddenField ID="hidCandidateAutoId" Value='<%# Bind("CandidateAutoId") %>' runat="server" />

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-md-9">


                <div class="col-md-12">
                    <div class="form-horizontal">

                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="Requisition No :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddRequisitionNo" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label19" runat="server" Text="Name of Post :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label20" runat="server" Text="Ref.Media :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddRefMedia" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label21" runat="server" Text="Requitment Code :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtRequitmentCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label22" runat="server" Text="Candidate Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtCandidateName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label24" runat="server" Text="Father's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtFathersName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label25" runat="server" Text="Mother's Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtMothersName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label26" runat="server" Text="Date of Borth :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-4">
                                        <asp:TextBox runat="server" ID="txtDOB" CssClass="input-sm date-picker" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label27" runat="server" Text="Birth Place :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddBirthPlace" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label28" runat="server" Text="NID :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtNID" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label29" runat="server" Text="Nationality :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddNationality" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="Gender :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddGender" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Religion :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddReligion" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label30" runat="server" Text="Marital Status :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddMaritalStatus" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label38" runat="server" Text="Blood Group :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddBloodGroup" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label31" runat="server" Text="Expected Salary :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtExpectedSalary" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label32" runat="server" Text="last Qualification :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlastQualification" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <asp:Label ID="Label3" runat="server" Text="Group/Subject :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtGroupSubject" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label33" runat="server" Text="Result :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtResult" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label34" runat="server" Text="Board/University :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtBoardUniversity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label35" runat="server" Text="Institute Name :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtInstituteName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label36" runat="server" Text="Contact No :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <asp:Label ID="Label37" runat="server" Text="Email :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <asp:Label ID="Label39" runat="server" Text="Remarks :" CssClass="col-sm-4 control-label"></asp:Label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                </div>


                            </div>
                        </div>



                    </div>
                </div>

                <div class="row row-custom" style="padding-top: 20px;">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-11">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                <asp:Button ID="btnSend" runat="server" Visible="false" CssClass="btn btn-danger btn-sm" Text="Submit For Approval" OnClick="btnSend_Click" Width="150px" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="0" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hidCandidateInfoAutoId" runat="server" Value="" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />

        <asp:HiddenField ID="hidQryA" runat="server" Value="" />
        <asp:HiddenField ID="hidQryB" runat="server" Value="" />
        <asp:HiddenField ID="hidQryC" runat="server" Value="" />
        <asp:HiddenField ID="hidQryD" runat="server" Value="" />
        <asp:HiddenField ID="hidQryE" runat="server" Value="" />

        <asp:HiddenField ID="hidDesignationId" runat="server" Value="0" />

        <asp:HiddenField ID="hidStaffRequisitionAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hid_Addmode" runat="server" Value="Y" />
        <asp:HiddenField ID="hid_Rownumber" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
          <%--  makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/StaffRequisition.asmx/GetStaffRequisition") %>',
                '#<%=hidStaffRequisitionAutoId.ClientID %>');--%>
        });

    </script>

</asp:Content>
