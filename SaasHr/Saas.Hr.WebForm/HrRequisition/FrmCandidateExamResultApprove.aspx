﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmCandidateExamResultApprove.aspx.cs" Inherits="Saas.Hr.WebForm.HrRequisition.FrmExamResultApprove" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Exam Result Approval
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="row row-custom">
            <div class="col-sm-8">
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Date:" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox runat="server" ID="txtSearchFromDate" CssClass="input-sm date-picker" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Requisition Ref No:" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="ddReqNo" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-sm-1">
                            <asp:LinkButton ID="lnkBtnSearch" runat="server" OnClick="lnkBtnSearch_Click" CssClass="btn btn-warning btn-xs" Width="50px"><i class="fa fa-search" aria-hidden="true"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-horizontal">
                    
                    <div class="form-group">
                        <asp:Label ID="Label3" runat="server" Text="Approved By:" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddApprovedBy" AutoPostBack="true" OnSelectedIndexChanged="ddApprovedBy_SelectedIndexChanged" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-xs" Text="Approve" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btn_refresh" runat="server" Visible="false" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-xs" OnClick="btn_refresh_Click" Width="80px" />
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-danger btn-xs" Text="Reject" OnClick="btn_save_Click" Width="80px" />
                            <asp:Button ID="btnDelete" Visible="false" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClick="btnDelete_Click" Width="80px" />

                        </div>
                        <div class="col-sm-4" style="text-align: left; color: red;">
                            <asp:Label ID="lblCounter" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group center">
                                <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div style="height: 520px; overflow: scroll;">
                    <asp:GridView ID="gdv_Attendence" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField ItemStyle-Width="3%">
                               <%-- <HeaderTemplate>
                                    <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>--%>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkApproval" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Id" Visible="false">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCandidateId" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("CandidateId") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCode" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("Code") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Candidate Name">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCandidateName" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("CandidateName") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Father">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtFatherName" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("FatherName") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Birth Place">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDistrictName" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("DistrictName") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NID">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNID" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("NID") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtContactNo" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("ContactNo") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtEmail" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("Email") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Age">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtAge" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("Age") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qualification">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLastDegree" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("LastDegree") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Result">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLastResult" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("LastResult") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Salary">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtExpectedSalary" runat="server" Enabled="false" CssClass="form-control input-sm"
                                        Text='<% # Eval("ExpectedSalary") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Comments" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtcComments" runat="server" Enabled="false" CssClass="form-control input-sm" Width="100%"
                                        Text='<% # Eval("cComments") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Result" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtcResult" runat="server" Enabled="false" CssClass="form-control input-sm"  Width="100%"
                                        Text='<% # Eval("cResult") %>' onfocus="disableautocompletion(this.id); this.style.backgroundColor='#ffff80'"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>


        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmployeeTypeId" runat="server" Value="0" />
        <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
        <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
        <asp:HiddenField ID="hidCardid" runat="server" Value="0" />
        <asp:HiddenField ID="hidForAll" runat="server" Value="" />
        <asp:HiddenField ID="hidOfficialMail" runat="server" Value="" />
        
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
