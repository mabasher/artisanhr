﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Accounts
{
    public partial class FrmJournalVoucher : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;
        private string Addmode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtIssueDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddVouchertype,
                    "VoucherType",
                    "VoucherType",
                    "AutoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddAccountHead,
                    "ChartOfAccountsInfo",
                    "AccountHead",
                    "ChartOfAccountsInfoId", "Where SubHead='N'");


                DataTable myDt = new DataTable();
                myDt = CreateDataTable();
                ViewState["myDatatable"] = myDt;

                v_loadGridView_CostingHead();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidStaffRequisitionAutoId.Value == "")
            { hidStaffRequisitionAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
                SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
                sqlconnection.Open();
            
                SqlCommand cmd = new SqlCommand("ProcRequiredManpowerwithDesignationSelect'" + hidStaffRequisitionAutoId.Value + "'", sqlconnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gdv_costingHead.DataSource = ds;
                gdv_costingHead.AllowPaging = true;
                gdv_costingHead.PageSize = 8;
                gdv_costingHead.DataBind();
                sqlconnection.Close();
            
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserName = string.Empty;
            SessionUserName = Convert.ToString(Session[GlobalVariables.g_s_userName]);
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string soupdatedayslimit = string.Empty;
            string AppUpdate = string.Empty;
            
            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            //Boolean b_UserReturn = true;
            //b_UserReturn = isUserCheck();
            //if (b_UserReturn == false && btn_save.Text == "Update")
            //{
            //    return;
            //}

            string SessionUserType = Convert.ToString(Session[GlobalVariables.g_s_userStatus]);
            

            if (btn_save.Text == "Update")
            {
                dataCollectionForUpdate();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Update Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
            else
            {
                dataCollection();
                hidQryA.Value = "SELECT ''";
                hidQryC.Value = "SELECT ''";
                DML dml = new DML();
                if (dml.g_b_funcDMLGeneralOneToManyInsert(hidQryA.Value, hidQryB.Value, hidQryD.Value, hidQryC.Value, "A", "B", "C") == false)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Data Transaction Error!";
                    return;
                }
                else
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Saved Successfull!";
                    Response.Redirect(Request.RawUrl);
                }
                return;
            }
        }
        private void dataCollection()
        {
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

             SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();
            Sdummy = "N";
            //maxSerial();

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcStaffRequisitionForRecruitmentInsert]"
                            + "'"
                            + Convert.ToDateTime(txtIssueDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                            + "','"
                            + HttpUtility.HtmlDecode(txtVoucherNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + ddVouchertype.SelectedValue
                            + "','"
                            + HttpUtility.HtmlDecode(txtRemarks.Text.Trim().Replace("'", "''"))
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "','" 
                            + SessionUserIP
                            + "'";
           

            ///Master Details Data Save
            string tempData = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                string DesignationAutoId = string.Empty;
                string Designation = string.Empty;
                string ManPowerMale = string.Empty;
                string ManPowerFeMale = string.Empty;

                DesignationAutoId = Convert.ToString(dr["DesignationAutoId"]);
                Designation = Convert.ToString(dr["Designation"]);
                ManPowerMale = Convert.ToString(dr["ManPowerMale"]);
                ManPowerFeMale = Convert.ToString(dr["ManPowerFeMale"]);

                tempData = tempData + "Execute ProcStaffRequisitionForRecruitmentDetailsInsert"
                             + "'"
                             + HttpUtility.HtmlDecode(txtVoucherNo.Text.Trim())
                             + "','"
                             + DesignationAutoId
                             + "','"
                             + ManPowerMale
                             + "','"
                             + ManPowerFeMale
                             + "','0','','','' ";
            }
            hidQryD.Value = hidQryD.Value + tempData + " Select ''";
            tempData = "";
        }
        private void dataCollectionForUpdate()
        {
            //hidSONo.Value = txt_OrderSl.Text.Trim();
            DataTable dt = (DataTable)ViewState["myDatatable"];
            string SessionUserId = string.Empty;
            string Sdummy = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            Sdummy = "N";

            ///Master Data Save Invoice
            hidQryB.Value = "";
            hidQryB.Value = "[ProcExpProformaInvoiceUpdate]"
                            + "'"
                            //+ hfAutoIdId.Value
                            + "','"
                            + HttpUtility.HtmlDecode(txtVoucherNo.Text.Trim().Replace("'", "''"))
                            + "','"
                            + Convert.ToDateTime(txtIssueDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                            + "','"
                            //+ ddContractNo.SelectedValue
                            //+ "','"
                            //+ ddSupplier.SelectedValue
                            //+ "','"
                            //+ ddCurrency.SelectedValue
                            //+ "','"
                            //+ dd_Brand.SelectedValue
                            //+ "','"
                            //+ dd_Origin.SelectedValue
                            //+ "','"
                            //+ ddPIType.SelectedValue
                            //+ "','"
                            //+ HttpUtility.HtmlDecode(txtValidity.Text.Trim())
                            //+ "','"
                            //+ HttpUtility.HtmlDecode(txtAdditionalNote.Text.Trim())
                            //+ "','"
                            //+ HttpUtility.HtmlDecode(txtFreightCharge.Text.Trim())
                            //+ "','"
                            //+ HttpUtility.HtmlDecode(txtdocCharge.Text.Trim())
                            //+ "','"
                            //+ HttpUtility.HtmlDecode(txtHandlingCharge.Text.Trim())
                            //+ "','"
                            //+ HttpUtility.HtmlDecode(txtOtherCharge.Text.Trim())
                            + "','"
                            + SessionCompanyId
                            + "','"
                            + SessionUserId
                            + "'";

            ///Master Details Data Save
            string tempData = string.Empty;
            tempData = "";
            hidQryD.Value = "";
            foreach (DataRow dr in dt.Rows)
            {
                string DesignationAutoId = string.Empty;
                string Designation = string.Empty;
                string ManPowerMale = string.Empty;
                string ManPowerFeMale = string.Empty;

                DesignationAutoId = Convert.ToString(dr["DesignationAutoId"]);
                Designation = Convert.ToString(dr["Designation"]);
                ManPowerMale = Convert.ToString(dr["ManPowerMale"]);
                ManPowerFeMale = Convert.ToString(dr["ManPowerFeMale"]);
              
                tempData = tempData + "Execute ProcExpProformaInvoiceDetailsInsert"
                             + "'"
                             + HttpUtility.HtmlDecode(txtVoucherNo.Text.Trim())
                             //+ "','"
                             //+ Item_Id
                             //+ "','"
                             //+ Style
                             //+ "','"
                             //+ Color
                             //+ "','"
                             //+ Size
                             //+ "','"
                             //+ UnitId
                             //+ "','"
                             //+ CostingId
                             //+ "','"
                             //+ Qty
                             //+ "','"
                             //+ Rate
                             //+ "','"
                             //+ Total
                             //+ "','"
                             //+ Remarks
                             + "'";
            }
            hidQryD.Value = hidQryD.Value + tempData + " Select ''";
            tempData = "";
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();
            CommonFunctions commonFunctions = new CommonFunctions();
            DML dml = new DML();
            DataTable dtt = (DataTable)ViewState["myDatatable"];
            string s_Rqty = string.Empty;
            string s_Qry = string.Empty;

            Boolean b_required = true;
            lblMsg.Visible = false;
            b_required = isrequired();
            if (b_required == false)
            {
                return;
            }


            if (hid_Addmode.Value == "N")
            {
                btnDel_Click(sender, e);
            }

            foreach (DataRow dr in dtt.Rows)
            {
                string iRate = string.Empty;
                string iId = string.Empty;
                string sAccountId = string.Empty;
                sAccountId = Convert.ToString(dr["hidAccountId"]);
               

                if (sAccountId == ddAccountHead.SelectedValue)// && iRate == txtRate.Text)
                {
                    lblMsg.Visible = true;
                    lblMsg.Text = "Already Added!";
                    return;
                }
            }

            DataTable myDatatable = (DataTable)ViewState["myDatatable"];
            AddDataToTable(this.ddAccountHead.SelectedItem.ToString()
                        , this.txtNarration.Text
                        , this.txtDebit.Text.Trim()
                        , this.txtCredit.Text.Trim()
                        , this.ddAccountHead.SelectedValue
                        , (DataTable)ViewState["myDatatable"]
                     );

            this.gdv_costingHead.DataSource = ((DataTable)ViewState["myDatatable"]).DefaultView;
            this.gdv_costingHead.DataBind();
            hid_Rownumber.Value = Convert.ToString(Convert.ToDouble(hid_Rownumber.Value) + 1);
            //AddClear();
        }

        private void AddDataToTable(string AccountHead,
                                    string Narration,
                                    string Debit,
                                    string Credit,
                                    string AccountId,
                                    DataTable myTable)
        {
            DataRow row = myTable.NewRow();
            row["AccountHead"] = AccountHead;
            row["Narration"] = Narration;
            row["Debit"] = Debit;
            row["Credit"] = Credit;
            row["AccountId"] = AccountId;
            myTable.Rows.Add(row);
        }

        private DataTable CreateDataTable()
        {
            DataTable myDataTable = new DataTable();
            myDataTable.Columns.Add(new DataColumn("AccountHead", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Narration", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Debit", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("Credit", typeof(string)));
            myDataTable.Columns.Add(new DataColumn("AccountId", typeof(string)));
            return myDataTable;
        }
        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (gdv_costingHead.SelectedIndex != -1)
            {
                ViewState["Addmode"] = "Y";
                Addmode = Convert.ToString(ViewState["Addmode"]);
                hid_Addmode.Value = "Y";
                DataTable dt = (DataTable)ViewState["myDatatable"];
                dt.Rows[gdv_costingHead.SelectedIndex].Delete();
                dt.AcceptChanges();
                gdv_costingHead.DataSource = dt;
                gdv_costingHead.DataBind();

                if (Convert.ToDouble(hid_Rownumber.Value) > 0)
                {
                    hid_Rownumber.Value = Convert.ToString(Convert.ToDouble(hid_Rownumber.Value) - 1);
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdv_costingHead, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }
        private Boolean isrequired()
        {
            if (string.IsNullOrEmpty(txtDebit.Text.Trim()) && string.IsNullOrEmpty(txtCredit.Text.Trim()))
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Debit Or Credit Blank!";
                return false;
            }
            if (ddAccountHead.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Account Head Blank!";
                return false;
            }
            
            return true;
        }
        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (ddVouchertype.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Voucher Type Blank!";
                return false;
            }



            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtDebit.Text = string.Empty;
            txtCredit.Text = string.Empty;
            txtNarration.Text = string.Empty;
            txtIssueDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtRemarks.Text = string.Empty;
            
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidStaffRequisitionAutoId.Value != "" && hidStaffRequisitionAutoId.Value != "0")
            {
                getEmpData(hidStaffRequisitionAutoId.Value);
            }
        }

        private void getEmpData(string sStaffRequisitionId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM StaffRequisitionForRecruitment Where autoId='" + sStaffRequisitionId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtIssueDate.Text = drow["inDate"].ToString();
                        txtVoucherNo.Text = drow["referenceNumber"].ToString();
                        txtRemarks.Text = drow["Remarks"].ToString();                       
                    }
                }
            }
            v_loadGridView_CostingHead();
        }
      //===========End===============         
    }
} 