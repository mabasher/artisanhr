﻿using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.EntityFrameworkData;
using Saas.Hr.WebForm.EntityFrameworkData.Accounts;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Accounts
{
    public partial class FrmChartofaccounts : Page
    {
        private AccountsEntities _context;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindModule();
                BindParentModule();
            }
        }

        private void BindModule()
        {
            using (_context = new AccountsEntities())
            {
                gdvModule.DataSource = _context.ChartOfAccountsInfoes.Include(m => m.ChartOfAccountsInfo2).ToList();
                gdvModule.DataBind();
            }
        }

        private void BindParentModule()
        {
            using (_context = new AccountsEntities())
            {
                ddParentHead.DataSource = _context.ChartOfAccountsInfoes.Where(c => c.SubHead == "Y").ToList();
                ddParentHead.DataTextField = "AccountHead";
                ddParentHead.DataValueField = "ChartOfAccountsInfoId";
                ddParentHead.DataBind();
                ddParentHead.Items.Insert(0, new ListItem("---Select---", "0"));
            }
        }

        private void ClearAll()
        {
            hidModuleInfoId.Value = "0";
            txtAccountCode.Text = string.Empty;
            txtAccountDetails.Text = string.Empty;
            txtAccountHead.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtContact.Text = string.Empty;
            btnSave.Text = "Save";
            lblMsg.Text = string.Empty;
            BindParentModule();
            BindModule();
        }

        private bool IsModuleValid()
        {
            if (txtAccountHead.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Account Head name should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtAccountHead.Focus();
                return false;
            }

            if (txtAccountCode.Text.Trim() == string.Empty)
            {
                MessageBox.ShowMessageInLabel("Account Code should not be blank!!!", lblMsg, MessageType.ErrorMessage);
                txtAccountCode.Focus();
                return false;
            }

            return true;
        }

        protected void gdvModule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(gdvModule, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }
        }

        protected void gdvModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            _context = new AccountsEntities();
            var id = int.Parse(((HiddenField)gdvModule.Rows[gdvModule.SelectedIndex].FindControl("hidGridChartOfAccountsInfoId")).Value);
            var module = _context.ChartOfAccountsInfoes.First(r => r.ChartOfAccountsInfoId == id);
            hidModuleInfoId.Value = module.ChartOfAccountsInfoId.ToString();
            txtAccountHead.Text = module.AccountHead;
            txtAccountCode.Text = module.AccountCode;
            txtAccountDetails.Text = module.AccountDetails.ToString();
            if (module.ParentKey != null)
            {
                ddParentHead.SelectedValue = module.ParentKey.ToString();
            }
            txtContact.Text = module.Contact;
            txtEmail.Text = module.Email;
            btnSave.Text = "Update";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!IsModuleValid()) return;
            lblMsg.Text = string.Empty;
            String IsParent = string.Empty;
            IsParent = "N";
            if (chkIsParent.Checked) { IsParent = "Y"; }

            _context = new AccountsEntities();
            var parentHeadId = int.Parse(ddParentHead.SelectedValue);
            var coaobi = _context.ChartOfAccountsOpeningBalances.First(c => c.ChartOfAccountAutoId == parentHeadId);
            try
            {
                if (hidModuleInfoId.Value.Trim() == "0")
                {
                    var module = new ChartOfAccountsInfo
                    {
                        AccountHead = txtAccountHead.Text.Trim(),
                        AccountCode = txtAccountCode.Text.Trim(),
                        AccountDetails = txtAccountDetails.Text.Trim(),
                        Contact = txtContact.Text.Trim(),
                        Email = txtEmail.Text.Trim(),
                        Location = "0",
                        CreditLimit = 0,
                        Level = 0,
                        SubHead = IsParent,
                        CreateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                        CreateIp = GlobalVariables.g_s_userIP,
                        CreateTime = DateTime.Now
                    };
                    if (ddParentHead.SelectedIndex > 0)
                    {
                        module.ParentKey = int.Parse(ddParentHead.SelectedValue);
                    }

                    _context.ChartOfAccountsInfoes.Add(module);
                    var caob = new ChartOfAccountsOpeningBalance
                    {
                        ChartOfAccountAutoId = module.ChartOfAccountsInfoId,
                        OpeningBalance = 0,
                        BranchAutoId = 0,
                        OpeningDate = DateTime.Now,
                        TransactionType = coaobi.TransactionType,
                        CompanyAutoId = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId])),
                        CreateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId])),
                        CreateIp = "",
                        CreateTime = DateTime.Now
                    };
                    _context.ChartOfAccountsOpeningBalances.Add(caob);

                    _context.SaveChanges();
                    ClearAll();
                    MessageBox.ShowMessageInLabel("Success!!!", lblMsg, MessageType.SuccessMessage);
                }
                else
                {
                    var id = int.Parse(hidModuleInfoId.Value);
                    var module = _context.ChartOfAccountsInfoes.Include(r => r.ChartOfAccountsOpeningBalances).FirstOrDefault(r => r.ChartOfAccountsInfoId == id);
                    if (module == null)
                    {
                        MessageBox.ShowMessageInLabel("Data not found to update!!!", lblMsg, MessageType.ErrorMessage);
                    }
                    else
                    {
                        module.AccountHead = txtAccountHead.Text.Trim();
                        module.AccountCode = txtAccountCode.Text.Trim();
                        module.AccountDetails = txtAccountDetails.Text.Trim();
                        module.Contact = txtContact.Text.Trim();
                        module.Email = txtEmail.Text.Trim();
                        module.Location = "0";
                        module.CreditLimit = 0;
                        module.Level = 0;
                        module.SubHead = IsParent;
                        if (ddParentHead.SelectedIndex > 0)
                        {
                            module.ParentKey = int.Parse(ddParentHead.SelectedValue);
                        }
                        else
                        {
                            module.ParentKey = null;
                        }
                        module.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                        module.UpdateTime = DateTime.Now;
                        module.UpdateIp = Session[GlobalVariables.g_s_userIP].ToString();

                        foreach (var item in module.ChartOfAccountsOpeningBalances)
                        {
                            item.TransactionType = coaobi.TransactionType;
                            item.UpdateBy = int.Parse(Convert.ToString(Session[GlobalVariables.g_s_userAutoId]));
                            item.UpdateTime = DateTime.Now;
                            item.UpdateIp = Session[GlobalVariables.g_s_userIP].ToString();
                        }

                        _context.SaveChanges();
                        ClearAll();
                        MessageBox.ShowMessageInLabel("Success!!!", lblMsg, MessageType.SuccessMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowMessageInLabel(ex.InnerException.Message, lblMsg, MessageType.ErrorMessage);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
    }
}