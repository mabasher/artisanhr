﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmJournalVoucher.aspx.cs" Inherits="Saas.Hr.WebForm.Accounts.FrmJournalVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
       Journal Voucher
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-lg-12">

                    <div class="col-md-12">
                        <div class="form-horizontal">
                            <div class="form-group ">
                                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Voucher" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>


                            <div class="form-group" >
                                <asp:Label ID="Label18" runat="server" Text="Issue Date :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox runat="server" ID="txtIssueDate" CssClass="input-sm date-picker" />
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Voucher No :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtVoucherNo" ReadOnly="true"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>                            

                              <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Voucher Type :" CssClass="col-sm-3 control-label"></asp:Label>
                                <div class="col-sm-3">
                                   <asp:DropDownList ID="ddVouchertype" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                           


                                <div class="form-group" style="margin-top:20px;">                                    
                                    <div class="col-sm-12">     
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label6" runat="server" Text="Account Head" CssClass="control-label"></asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                     <asp:DropDownList ID="ddAccountHead" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label7" runat="server" Text="Narration" CssClass="control-label"></asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtNarration"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label8" runat="server" Text="Debit" CssClass="control-label"></asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtDebit"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label9" runat="server" Text="Credit" CssClass="control-label"></asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtCredit"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                         <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Label11" runat="server" Text="." CssClass="control-label"></asp:Label>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" Width="60px" runat="server" CssClass="btn btn-primary btn-xs" Text="Add" />
                                                    <asp:Button ID="btnDel" OnClick="btnDel_Click" Width="60px" runat="server" CssClass="btn btn-warning btn-xs" Text="Del" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>                                   
                                    
                              </div>


                                <div class="col-md-12">
                                    <div style="height: 200px; overflow: scroll;">
                                        <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                            CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                                </asp:CommandField>

                                                <asp:TemplateField HeaderText="Account Head">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGridDesignation" runat="server" Text='<%# Bind("Designation") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Narration">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGridNarration" runat="server" Text='<%# Bind("ManPowerMale") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Debit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGridDebit" runat="server" Text='<%# Bind("ManPowerMale") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Credit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGridCredit" runat="server" Text='<%# Bind("ManPowerFeMale") %>'></asp:Label>
                                                        <asp:HiddenField ID="hidAccountId" Value='<%# Bind("DesignationAutoId") %>' runat="server" />
                                                   </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-sa" />
                                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                           



                            <div class="form-group">
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtRemarks"  runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row row-custom" style="padding-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-horizontal">
                                <div class="form-group center">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                        CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hidURL" runat="server" Value="" />
        
        <asp:HiddenField ID="hidQryA" runat="server" Value="" />
        <asp:HiddenField ID="hidQryB" runat="server" Value="" />
        <asp:HiddenField ID="hidQryC" runat="server" Value="" />
        <asp:HiddenField ID="hidQryD" runat="server" Value="" />
        <asp:HiddenField ID="hidQryE" runat="server" Value="" />

        <asp:HiddenField ID="hidDesignationId" runat="server" Value="0" />
        
        <asp:HiddenField ID="hidStaffRequisitionAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
        <asp:HiddenField ID="hid_Addmode" runat="server" Value="Y" />
        <asp:HiddenField ID="hid_Rownumber" runat="server" Value="0" />

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/StaffRequisition.asmx/GetStaffRequisition") %>',
                '#<%=hidStaffRequisitionAutoId.ClientID %>');
        });

    </script>

</asp:Content>
