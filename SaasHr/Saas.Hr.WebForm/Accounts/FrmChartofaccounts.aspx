﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmChartofaccounts.aspx.cs" Inherits="Saas.Hr.WebForm.Accounts.FrmChartofaccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>Chart of Accounts</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row row-custom">
        <div class="col-lg-12">
            <div class="col-md-4">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hidModuleInfoId" Value="0" runat="server" />
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lblModuleName" runat="server" Text="Account Head :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAccountHead" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPriority" runat="server" Text="Account Code :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAccountCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblParentModule" runat="server" Text="Parent Head :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:DropDownList ID="ddParentHead" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblIcon" runat="server" Text="Contact :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtContact" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="E-Mail :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblModuleDescription" runat="server" Text="Account Details :" CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAccountDetails" runat="server" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text=" " CssClass="col-sm-4 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:CheckBox ID="chkIsParent" runat="server" Text="Is Parent Head" />
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px;">
                        <div class="col-sm-offset-4 col-sm-10">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btnSave_Click" Width="80px" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="false"
                                CssClass="btn btn-default btn-sm" OnClick="btnRefresh_Click" Width="80px" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div style="height: 400px; overflow: scroll;">
                    <asp:GridView ID="gdvModule" runat="server" Style="width: 100%; margin-left: 0;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered" OnRowDataBound="gdvModule_RowDataBound" OnSelectedIndexChanged="gdvModule_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <%--ChartOfAccountsInfoId, ParentKey, AccountCode, AccountHead, AccountDetails, Contact, Email, Location, CreditLimit, [Level], SubHead, CompanyId, CreateBy, CreateTime, CreateIp, UpdateBy, UpdateTime, UpdateIp,--%> 
                         

                            <asp:TemplateField HeaderText="Account Head">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridAccountHead" runat="server" Text='<%# Bind("AccountHead") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridChartOfAccountsInfoId" Value='<%# Bind("ChartOfAccountsInfoId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Account Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridAccountCode" runat="server" Text='<%# Bind("AccountCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Parent Head">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridParentHead" runat="server" Text='<%# Eval("ChartOfAccountsInfo2.AccountHead") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                            <asp:TemplateField HeaderText="Account Detials">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridAccountDetails" runat="server" Text='<%# Bind("AccountDetails") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>