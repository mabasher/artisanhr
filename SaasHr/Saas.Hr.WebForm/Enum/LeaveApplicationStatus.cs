﻿namespace Saas.Hr.WebForm.Enum
{
    public enum LeaveApplicationStatus
    {
        Pending,
        Approved,
        Rejected,
        Canceled
    }
}