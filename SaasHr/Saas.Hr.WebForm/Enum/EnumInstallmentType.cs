﻿namespace Saas.Hr.WebForm.Enum
{
    public enum EnumInstallmentType
    {
        Yearly,
        Monthly,
        Weekly,
        Daily
    }
}