﻿namespace Saas.Hr.WebForm.Enum
{
    public enum EnumDurationType
    {
        Day = 1,
        Week = 7,
        Month = 30,
        Year = 365
    }
}