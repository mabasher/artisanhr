﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftSideBar.ascx.cs" Inherits="Saas.Hr.WebForm.TemplateControls.LeftSideBar" %>
<%@ Import Namespace="Saas.Hr.WebForm.Common" %>

<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar');
        } catch (e) {
        }
    </script>
    <ul class="nav nav-list" id="cssmenu" runat="server">
    </ul>
    <%if (Session[GlobalVariables.g_s_userLevel].ToString() == "2")
        {%>
    <ul class="nav nav-list">
        <li class="dropdown-li">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-lock"></i>
                <span class="menu-text">Dev. Menu Setup
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="">
                    <a href="<%= Page.ResolveUrl("~/") %>Security/FrmForms.aspx">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Form Setup
                    </a>
                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="<%= Page.ResolveUrl("~/") %>Security/FrmModules.aspx">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Module Setup
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
    </ul>

    <% } %>
    <!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>