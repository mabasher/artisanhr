﻿using System;
using System.Globalization;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.TemplateControls
{
    public partial class UserNavbar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblUserName.Text = Session[GlobalVariables.g_s_userName].ToString();
            lblUserName.Text = Session[GlobalVariables.g_s_DummyMode].ToString()+" "+lblUserName.Text;
            //            lblCompanyName.Text = Session[GlobalVariables.g_s_companyName].ToString();
        }

        public void Logout()
        {
            Connection connection = new Connection();
            CommonFunctions.Date_Validation date_validation = new CommonFunctions.Date_Validation();
            string s_upadateLogbook = string.Empty;
            s_upadateLogbook = Procedures.Common.Proc_Web_Update_UserLogBook
                               + "'"
                               + Session[GlobalVariables.g_s_userLogBookAutoId].ToString()
                               + "','"
                               + DateTime.Now.ToString(new CultureInfo("en-US"))
                               + "'";
            connection.connection_DB(s_upadateLogbook, 1, false, true, true);
            Session.RemoveAll();
            Response.Redirect(GlobalVariables.g_s_URL_loginPage);
        }

        protected void lnkBtnSignOut_Click(object sender, EventArgs e)
        {
            Logout();
        }
    }
}