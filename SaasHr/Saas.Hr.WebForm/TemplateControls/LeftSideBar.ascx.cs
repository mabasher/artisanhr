﻿using Saas.Hr.WebForm.Common;
using System;
using System.Web.UI;

namespace Saas.Hr.WebForm.TemplateControls
{
    public partial class LeftSideBar : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cssmenu.InnerHtml = Session[GlobalVariables.g_s_userMenu].ToString();
            }
        }
    }
}