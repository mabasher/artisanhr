﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserNavbar.ascx.cs" Inherits="Saas.Hr.WebForm.TemplateControls.UserNavbar" %>
<%@ Import Namespace="Saas.Hr.WebForm.Common" %>
<div id="navbar" class="navbar navbar-default ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="<%= Page.ResolveUrl("~/") %>Default.aspx" class="navbar-brand-logo">
                <small>
                    <img style="background-color:white;" src="<%= Page.ResolveUrl("~/") %>images/avatars/logo.png" alt="<%=GlobalVariables.BaseCompanyName %>" class="img img-responsive" />
                </small>
            </a>
        </div>

        <div class="navbar-header pull-left">
            <a href="<%= Page.ResolveUrl("~/") %>Default.aspx" class="navbar-brand">
            <%--<a href="~/Default.aspx" class="navbar-brand">--%>
                <%=GlobalVariables.BaseCompanyName %> 
            </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">

                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                         <% if (Session[GlobalVariables.g_s_userLevel].ToString()!="4")
                            { %>
                        <img class="nav-user-photo" src='<%= Page.ResolveUrl("~/") %>UserPic/<%= Session[GlobalVariables.g_s_userPIC].ToString() %>' alt="Photo" />
                        
                        <%} else { %>
                        <img class="nav-user-photo" src='<%= Page.ResolveUrl( Session[GlobalVariables.g_s_userPIC].ToString()) %>' alt="Photo" />
                        
                        <%} %>

                        <span class="user-info">
                            <asp:Label ID="lblUserName" runat="server" Text="Label"></asp:Label>
                        </span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-cog"></i>
                                Settings
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-user"></i>
                                Profile
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <asp:LinkButton ID="lnkBtnSignOut" runat="server" OnClick="lnkBtnSignOut_Click"><i class="ace-icon fa fa-power-off"></i>
                                Sign Out</asp:LinkButton>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="navbar-header pull-right">
            <a href="#" class="navbar-brand">
                <asp:Label ID="lblSoftwareName" runat="server" Text="SAAS ERP SYSTEM" CssClass="software-name"></asp:Label>
            </a>
        </div>
        <%-- <div class="navbar-header pull-right"> --%>
        <%--     <a href="#" class="navbar-brand"> --%>
        <%--         <asp:Label ID="lblCompanyName" runat="server" Text="" CssClass="company-name"></asp:Label> --%>
        <%--     </a> --%>
        <%-- </div> --%>
    </div>
    <!-- /.navbar-container -->
</div>