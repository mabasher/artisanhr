//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Saas.Hr.WebForm.EntityFrameworkData.Menu
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScrRoleInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScrRoleInfo()
        {
            this.ScrModuleFormForRoles = new HashSet<ScrModuleFormForRole>();
        }
    
        public int ScrRoleInfoId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateIpAddress { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateIpAddress { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScrModuleFormForRole> ScrModuleFormForRoles { get; set; }
    }
}
