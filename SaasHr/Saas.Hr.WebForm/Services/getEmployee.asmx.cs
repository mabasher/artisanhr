﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Services;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Services
{
    /// <summary>
    /// Summary description for ProductService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class getEmployee : WebService
    {
        [WebMethod(EnableSession = true)]
        public List<string> Getemp(string searchTerm)
        {
            var products = new List<string>();
            var commonFunctions = new CommonFunctions();
            var sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string ssUserLevel = string.Empty;
            string ssUserId = string.Empty;
            ssUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
            ssUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            //if (ssUserLevel == "5")
            //{
            //    qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo p where (p.PIN like '%W%') and (PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%') Order by EmployeeName asc";
            //}
            //else if (ssUserLevel == "4")
            //{
            //    qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo p where p.EmployeePersonalInfoId='" + ssUserId + "'  and (PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%') Order by EmployeeName asc";    //and (p.PIN like '%W%')
            //}
            //else
            //{
                qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo  where PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%' Order by EmployeeName asc";
            //}
            
            //var cmd = new SqlCommand("select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo  where PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%' Order by EmployeeName asc", sqlconnection);
            var cmd = new SqlCommand(qry, sqlconnection);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    products.Add($"{sdr["PartyName"]}@{ sdr["AutoId"]}");
                }
            }
            sqlconnection.Close();
            return products;
        }



        [WebMethod(EnableSession = true)]
        public List<string> GetAllEmp(string searchTerm)
        {
            var products = new List<string>();
            var commonFunctions = new CommonFunctions();
            var sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string ssUserLevel = string.Empty;
            string ssUserId = string.Empty;
            ssUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);
            ssUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            
             qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo  where PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%' Order by EmployeeName asc";
           

            //var cmd = new SqlCommand("select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo  where PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%' Order by EmployeeName asc", sqlconnection);
            var cmd = new SqlCommand(qry, sqlconnection);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    products.Add($"{sdr["PartyName"]}@{ sdr["AutoId"]}");
                }
            }
            sqlconnection.Close();
            return products;
        }
    }




}