﻿using Saas.Hr.WebForm.Common;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Services;

namespace Saas.Hr.WebForm.Services
{
    /// <summary>
    /// Summary description for GetExamName
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GetExamName : System.Web.Services.WebService
    {
        [WebMethod]
        public List<string> GetExam(string searchTerm)
        {
            var products = new List<string>();
            var commonFunctions = new CommonFunctions();
            var sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();
            var cmd = new SqlCommand("select top 20 isnull(ExamName,'') as ExamName,ExamNameId as AutoId from ExamName  where  ExamName like '%" + searchTerm.Replace("'", "''") + "%' Order by ExamName asc", sqlconnection);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    products.Add($"{sdr["ExamName"]}@{ sdr["AutoId"]}");
                }
            }
            sqlconnection.Close();
            return products;
        }
    }
    
}
