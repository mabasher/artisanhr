﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Services;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Services
{
    /// <summary>
    /// Summary description for ProductService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class getEmployeeforsalary : WebService
    {
        [WebMethod(EnableSession =true)]
        public List<string> Getemp(string searchTerm)
        {
            var products = new List<string>();
            var commonFunctions = new CommonFunctions();
            var sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string ssUserLevel = string.Empty;
            ssUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            //if (ssUserLevel == "2" || ssUserLevel == "3")
            //{
            //    qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo  where PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%' Order by EmployeeName asc";
            //}
            //else
            //{
                qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo p left outer join EmployeeOfficialInfo o on p.EmployeePersonalInfoId=o.EmpAutoId where  (PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%') Order by EmployeeName asc";
              //  qry = "select top 20 isnull(PIN,'')+' '+isnull(EmployeeName,'') as PartyName,EmployeePersonalInfoId as AutoId from EmployeePersonalInfo  where PIN like '%" + searchTerm.Replace("'", "''") + "%' OR EmployeeName like '%" + searchTerm.Replace("'", "''") + "%' Order by EmployeeName asc";
            //}

            var cmd = new SqlCommand(qry, sqlconnection);
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    products.Add($"{sdr["PartyName"]}@{ sdr["AutoId"]}");
                }
            }
            sqlconnection.Close();
            return products;
        }
    }




}