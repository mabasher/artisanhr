﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Saas.Hr.WebForm.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
    <link href="Content/ionicons/ionicons.min.css" rel="stylesheet" />
    <link href="AdminLTE/css/AdminLTE.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div style="width: 100%;">
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>


        <div style="width: 50%; float: left;">
            <asp:Literal ID="ltScripts" runat="server"></asp:Literal>
            <div id="chart_div" style="width: 100%; height: 500px;"></div>
        </div>
        <div style="width: 50%; float: left;">
            <asp:Literal ID="ltScripts1" runat="server"></asp:Literal>
            <div id="chart_div1" style="width: 100%; height: 500px;"></div>
        </div>

    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
