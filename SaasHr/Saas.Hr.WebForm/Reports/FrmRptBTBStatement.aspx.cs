﻿using System;
using System.Globalization;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports
{
    public partial class FrmRptBTBStatement : BasePage
    {
        private string sdateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                commonfunctions.g_b_FillDropDownList(ddSupplier,
                    "T_Supplier",
                    "Name",
                    "Supplier_Id", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddPINo,
                    "ExpBTBLCInformation",
                    "LCNo",
                    "autoId", string.Empty);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            sdateUse = "N";
            if (chkDateUse.Checked)
            {
                sdateUse = "Y";
            }

            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "BTB Statement");
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("8FromDate", txtFromDate.Text.ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("9Todate", txtToDate.Text.ToString());

                TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptBTBStatement.rpt";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptBTBStatement"
                                                              + " '"
                                                              + SessionCompanyId
                                                              + "','"
                                                              + hidPIId.Value
                                                              + "','"
                                                              + hidSupplierId.Value
                                                              + "','"
                                                              + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + sdateUse
                                                              + "'";

                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (dd_Category.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Type Can Not Blank!";
            //    return false;
            //}
            hidPIId.Value = "%";
            if (ddPINo.SelectedValue != "0")
            {
                hidPIId.Value = ddPINo.SelectedValue.ToString() + "X%";
            }
            hidSupplierId.Value = "%";
            if (ddSupplier.SelectedValue != "0")
            {
                hidSupplierId.Value = ddSupplier.SelectedValue.ToString() + "X%";
            }
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            ddSupplier.SelectedValue = "0";
        }
    }
}