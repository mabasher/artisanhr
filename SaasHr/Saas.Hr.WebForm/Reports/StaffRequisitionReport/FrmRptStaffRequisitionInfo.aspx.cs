﻿using System;
using System.Globalization;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{
    public partial class FrmRptStaffRequisitionInfo : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                string SessionUserLevel = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtIssuedDateFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtIssuedDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");


                commonfunctions.g_b_FillDropDownList(ddEmployeeName,
                    "StaffRequisitionForRecruitment",
                    "referenceNumber",
                    "autoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddProject,
                //    "ProjectInfo",
                //    "ProjectName",
                //    "ProjectInfoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddLocation,
                //    "LocationInfo",
                //    "LocationName",
                //    "LocationInfoId", string.Empty);
                //commonfunctions.g_b_FillDropDownList(ddLine,
                //    "LineInfo",
                //    "Line",
                //    "LineInfoId", string.Empty);




            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string SessionUserlevel = Session[GlobalVariables.g_s_userLevel].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            sdateUse = "N";
            if (chkIssuedDateUse.Checked)
            {
                sdateUse = "Y";
            }
           
            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "Staff Requisition");
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("8FromDate", txtIssuedDateFrom.Text.ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("9Todate", txtIssuedDateTo.Text.ToString());
                   
                TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptStaffRequisition.rpt";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptStaffRequisition"
                                                              + " '"
                                                              + SessionCompanyId
                                                              + "','"
                                                              + hid_autoId.Value                                                             
                                                              + "','"
                                                              + Convert.ToDateTime(txtIssuedDateFrom.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtIssuedDateTo.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + sdateUse                                                             
                                                              + "'";
                
                                                          
                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

           
            hid_autoId.Value = "%";
            if (ddEmployeeName.SelectedValue != "0")
            {
                hid_autoId.Value = ddEmployeeName.SelectedValue.ToString() + "X%";
            }




            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtIssuedDateFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtIssuedDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //ddSupplier.SelectedValue = "0";
        }
    }
}