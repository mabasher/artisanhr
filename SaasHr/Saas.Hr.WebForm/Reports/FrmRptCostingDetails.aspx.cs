﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.ReportFiles;

namespace Saas.Hr.WebForm.Reports
{
    public partial class FrmRptCostingDetails : BasePage
    {
        private string sdateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                commonfunctions.g_b_FillDropDownList(dd_Category,
                    "ExpOrderCostingInformation",
                    "CostingNo",
                    "autoId", string.Empty);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Session[GlobalVariables.g_s_printopt] = "N";
            if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }

            hidPOId.Value = "%";
            if (dd_Category.SelectedValue != "0")
            { hidPOId.Value = dd_Category.SelectedValue.ToString() + "X%"; }
            hidFormDate.Value = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy");
            hidToDate.Value = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy");
            if (chkPOList.Checked)
            {
                ViewList();
            }
            else
            {
                ViewSheet();
            }
        }

        private void ViewSheet()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyAddress = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);
            var poSheetSql = "ProcRptCostingSheet";
            //var poSheetTermsSql = "ProcRptConfirmOrderSheet";//Form Order Information
            var poSheetTermsSql = "ProcRptCostingWithStyle";

            var dic = new Dictionary<string, string>
            {
                {"CompanyId", SessionCompanyId},
                {"CostingId", hidPOId.Value},
                {"FromDate", hidFormDate.Value},
                {"ToDate", hidToDate.Value},
                {"Chkdate", "N"}
            };

            var dicd = new Dictionary<string, string>
            {
                {"CompanyId", SessionCompanyId},
                {"OrderNoId", hidPOId.Value},
                {"Buyer_Id", hidPOId.Value},
                {"FromDate", hidFormDate.Value},
                {"ToDate", hidToDate.Value},
                {"Chkdate", "N"}
            };

            var poSheetData = fn.GetDataSetFromSp(poSheetSql, dic);
            var poSheetTermsData = fn.GetDataSetFromSp(poSheetTermsSql, dicd);

            var rpt = new RptCostingDetail();
            rpt.Database.Tables["ProcRptCostingSheet"].SetDataSource(poSheetData);
            rpt.Database.Tables["ProcRptConfirmOrderSheet"].SetDataSource(poSheetTermsData);

            //rpt.SetParameterValue("CompanyName", SessionCompanyId);
            //rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            Session["rpt"] = rpt;
            OpenSubReport();
        }

        private void ViewList()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyAddress = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);
            var poSheetSql = "RptProcPurchaseOrderList";

            var dic = new Dictionary<string, string>
            {
                {"trAutoId", hidPOId.Value},
                {"fromDate", hidFormDate.Value},
                {"toDate", hidToDate.Value}
            };

            var poSheetData = fn.GetDataSetFromSp(poSheetSql, dic);
            var rpt = new RptCostingDetail();
            rpt.SetDataSource(poSheetData);
            //rpt.SetParameterValue("CompanyName", SessionCompanyId);
            //rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            Session["rpt"] = rpt;
            OpenGnReport();
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (dd_Category.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Type Can Not Blank!";
            //    return false;
            //}
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            dd_Category.SelectedValue = "0";
        }
    }
}