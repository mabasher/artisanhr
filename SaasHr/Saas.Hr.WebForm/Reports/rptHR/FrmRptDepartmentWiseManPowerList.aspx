﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="~/Reports/rptHR/FrmRptDepartmentWiseManPowerList.aspx.cs" Inherits="Saas.Hr.WebForm.Reports.rptHR.FrmRptDepartmentWiseManPowerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
       Department Wise Man Power List Report
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-sm-5">
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Employee Name :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddEmployeeName" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                  <div class="form-group">
                    <asp:Label ID="Label11" runat="server" Text="Employee Type:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddEmpType" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                 <div class="form-group">
                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Join Date :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                        <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                        <asp:CheckBox ID="chkDateUse" runat="server" Text="" />
                    </div>
                </div>


            </div>
        </div>
       
    </div>
   <%-- <div class="col-sm-10 ">
        <fieldset>
            <div class="form-group">
                <div class="form-group" style="margin-top: 10px;">
                    <div class="col-sm-3" style="font-size: 10px;">
                        <asp:CheckBox ID="chkWorker" runat="server" Text="Worker" />
                    </div>
                    <div class="col-sm-3" style="font-size: 10px;">
                        <asp:CheckBox ID="chkOfficer" runat="server" Text="Officer" />
                    </div>
                   
                </div>
            </div>
        </fieldset>
    </div>--%>


    <div class="col-sm-12 ">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10" style="margin-top: 50px;">
                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btn_save_Click" Width="80px" />
                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
            </div>
        </div>
    </div>


    <div class="row row-custom">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="text-danger">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </div>
        </div>
    </div>



    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hidPIId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSupplierId" runat="server" Value="0" />
    <asp:HiddenField ID="hid_autoId" runat="server" Value="0" />
    <asp:HiddenField ID="hid_DesignationId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidReligionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidBloodGroupId" runat="server" Value="0" />
    <asp:HiddenField ID="hidGenderId" runat="server" Value="0" />
    <asp:HiddenField ID="hidNationalityId" runat="server" Value="0" />
    <asp:HiddenField ID="hidMaritalStatusId" runat="server" Value="0" />
    <asp:HiddenField ID="hidShiftId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmpTypeId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmpGroupId" runat="server" Value="0" />
    <asp:HiddenField ID="hidFloorId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidBranchId" runat="server" Value="0" />
    <asp:HiddenField ID="hidProjectId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLocationId" runat="server" Value="0" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
