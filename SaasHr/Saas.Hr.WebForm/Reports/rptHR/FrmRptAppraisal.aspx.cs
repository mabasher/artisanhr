﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{
    public partial class FrmRptAppraisalList : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                int cy = DateTime.Today.Year - 10;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                ddYear.DataSource = years;
                ddYear.DataBind();
                //txtYear.Text = DateTime.Today.Year.ToString();
                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                  "DepartmentInfo",
                  "Department",
                  "DepartmentInfoId", "Order by Department");

            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforYear] = ddYear.SelectedItem.ToString();

            //Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedIndex == 0 ? "%" : ddMonth.SelectedValue + "X%";

            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedIndex == 0 ? "0" : ddMonth.SelectedValue ;

            Session[GlobalVariables.g_s_Period] = "For Year : " + ddYear.SelectedItem.ToString();
            if (ddMonth.SelectedValue != "0")
            {
                Session[GlobalVariables.g_s_Period] = "For Month : " + ddMonth.SelectedItem.ToString() + "'" + ddYear.SelectedItem.ToString();
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewAppraisal.aspx');", true);
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";


        }
    }
}