﻿using System;
using System.Globalization;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{
    public partial class FrmRptPersonalInfo : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                string SessionUserLevel = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtAppointmentDateFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtAppointmentDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtConfirmationDateFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtConfirmationDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtReleasedFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtReleasedToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                //string Proc = string.Empty;
                //Proc = "[ProCEmployeeLoad] '" + SessionCompanyId + "','" + SessionUserLevel + "'";
                //commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);

                string Proc = string.Empty;
                //if (SessionUserLevel == "5")//HR Only
                //{
                //    Proc = "select  EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                //    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                //}
               if (SessionUserLevel == "4")//Single Employee Only
                {
                    ddEmployeeName.Enabled = false;
                    Proc = "select EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                    ddEmployeeName.SelectedValue = SessionUserId;
                }
                else
                {
                    Proc = "select EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                }

                commonfunctions.g_b_FillDropDownList(ddCardId,
                    "EmployeePersonalInfo",
                    "CardNo",
                    "EmployeePersonalInfoId", "Where CardNo='0'");
                
                commonfunctions.g_b_FillDropDownList(ddDesignation,
                    "DesignationInfo",
                    "Designation",
                    "DesignationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddReligion,
                    "ReligionInfo",
                    "ReligionName",
                    "ReligionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBloodGroup,
                    "BloodGroupInfo",
                    "BloodGroupName",
                    "BloodGroupInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddGender,
                    "GenderInfo",
                    "GenderName",
                    "GenderInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddNationality,
                    "NationalityInfo",
                    "NationalityName",
                    "NationalityInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddMaritalStatus,
                    "MaritalStatusInfo",
                    "MaritalStatusName",
                    "MaritalStatusInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddShift,
                    "ShiftInformation",
                    "ShiftName",
                    "autoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddEmpType,
                    "EmployeeType",
                    "TypeName",
                    "EmployeeTypeId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddEmpGroup,
                    "EmployeeGroup",
                    "GroupName",
                    "EmployeeGroupId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddFloor,
                    "FloorInfo",
                    "FloorName",
                    "FloorInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfoId",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddBranch,
                    "BranchInfo",
                    "BranchName",
                    "BranchInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddProject,
                    "ProjectInfo",
                    "ProjectName",
                    "ProjectInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty); 
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string SessionUserlevel = Session[GlobalVariables.g_s_userLevel].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            sdateUse = "N";
            if (chkDateUse.Checked)
            {
                sdateUse = "Y";
            }
            sAppointmentDateUse = "N";
            if (chkAppointmentDateUse.Checked)
            {
                sAppointmentDateUse = "Y";
            }
            sConfirmationDateUse = "N";
            if (chkConfirmationDateUse.Checked)
            {
                sConfirmationDateUse = "Y";
            }
            sReleasedDateUse = "N";
            if (chkReleasedDateUse.Checked)
            {
                sReleasedDateUse = "Y";
            }
            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "Personal Information");
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("8FromDate", txtFromDate.Text.ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("9Todate", txtToDate.Text.ToString());

                if (chkChildInfo.Checked == true)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptEmployeeChildInfo.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptEmployeeChildInformation"
                                                                  + " '"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + SessionUserlevel
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "'";
                }
                else if(chkNomineeInfo.Checked == true)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptEmployeeNomineeInfo.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptEmployeeNomineeInformation"
                                                                  + " '"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + SessionUserlevel
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "'";
                }
                else if (chkOfficialInfo.Checked == true)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptEmployeeNomineeInfo.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptEmployeeNomineeInformation"
                                                                  + " '"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + SessionUserlevel
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "'";
                }
                else if (chkRelease.Checked == true)
                {                    
                TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptEmployeePersonalInfoRelease.rpt";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptEmployeeInformation_Release"
                                                              + " '"
                                                              + SessionCompanyId
                                                              + "','"
                                                              + SessionUserlevel
                                                              + "','"
                                                              + hid_autoId.Value
                                                              + "','"
                                                              + hid_autoId.Value
                                                              + "','"
                                                              + hid_DesignationId.Value
                                                              + "','"
                                                              + hidDepartmentId.Value
                                                              + "','"
                                                              + hidReligionId.Value
                                                              + "','"
                                                              + hidBloodGroupId.Value
                                                              + "','"
                                                              + hidGenderId.Value
                                                              + "','"
                                                              + hidNationalityId.Value
                                                              + "','"
                                                              + hidMaritalStatusId.Value
                                                              + "','"
                                                              + hidEmpTypeId.Value
                                                              + "','"
                                                              + hidEmpGroupId.Value
                                                              + "','"
                                                              + hidFloorId.Value
                                                              + "','"
                                                              + hidLineId.Value
                                                              + "','"
                                                              + hidBranchId.Value
                                                              + "','"
                                                              + hidProjectId.Value
                                                              + "','"
                                                              + hidShiftId.Value
                                                              + "','"
                                                              + hidLocationId.Value
                                                              + "','"
                                                              + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + sdateUse
                                                              + "','" 
                                                              + Convert.ToDateTime(txtReleasedFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtReleasedToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + sReleasedDateUse
                                                              + "','"
                                                              + Convert.ToDateTime(txtAppointmentDateFrom.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtAppointmentDateTo.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + sAppointmentDateUse
                                                              + "','"
                                                              + Convert.ToDateTime(txtConfirmationDateFrom.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtConfirmationDateTo.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + sConfirmationDateUse
                                                              + "'";
                }
                else
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptEmployeePersonalInfo.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptEmployeeInformation"
                                                                  + " '"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + SessionUserlevel
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hid_DesignationId.Value
                                                                  + "','"
                                                                  + hidDepartmentId.Value
                                                                  + "','"
                                                                  + hidReligionId.Value
                                                                  + "','"
                                                                  + hidBloodGroupId.Value
                                                                  + "','"
                                                                  + hidGenderId.Value
                                                                  + "','"
                                                                  + hidNationalityId.Value
                                                                  + "','"
                                                                  + hidMaritalStatusId.Value
                                                                  + "','"
                                                                  + hidEmpTypeId.Value
                                                                  + "','"
                                                                  + hidEmpGroupId.Value
                                                                  + "','"
                                                                  + hidFloorId.Value
                                                                  + "','"
                                                                  + hidLineId.Value
                                                                  + "','"
                                                                  + hidBranchId.Value
                                                                  + "','"
                                                                  + hidProjectId.Value
                                                                  + "','"
                                                                  + hidShiftId.Value
                                                                  + "','"
                                                                  + hidLocationId.Value
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + sdateUse
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtReleasedFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtReleasedToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + sReleasedDateUse
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtAppointmentDateFrom.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtAppointmentDateTo.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + sAppointmentDateUse
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtConfirmationDateFrom.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtConfirmationDateTo.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + sConfirmationDateUse
                                                                  + "'";
                }

                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            //if (dd_Category.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Type Can Not Blank!";
            //    return false;
            //}

            hid_autoId.Value = "%";
            if (ddEmployeeName.SelectedValue != "0")
            {
                hid_autoId.Value = ddEmployeeName.SelectedValue.ToString() + "X%";
            }

            //hid_autoId.Value = "%";
            //if (ddCardId.SelectedValue != "0")
            //{
            //    hid_autoId.Value = ddCardId.SelectedValue.ToString() + "X%";
            //}

            hid_DesignationId.Value = "%";
            if (ddDesignation.SelectedValue != "0")
            {
                hid_DesignationId.Value = ddDesignation.SelectedValue.ToString() + "X%";
            }
            hidDepartmentId.Value = "%";
            if (ddDepartment.SelectedValue != "0")
            {
                hidDepartmentId.Value = ddDepartment.SelectedValue.ToString() + "X%";
            }
            hidReligionId.Value = "%";
            if (ddReligion.SelectedValue != "0")
            {
                hidReligionId.Value = ddReligion.SelectedValue.ToString() + "X%";
            }
            hidBloodGroupId.Value = "%";
            if (ddBloodGroup.SelectedValue != "0")
            {
                hidBloodGroupId.Value = ddBloodGroup.SelectedValue.ToString() + "X%";
            }
            hidGenderId.Value = "%";
            if (ddGender.SelectedValue != "0")
            {
                hidGenderId.Value = ddGender.SelectedValue.ToString() + "X%";
            }
            hidNationalityId.Value = "%";
            if (ddNationality.SelectedValue != "0")
            {
                hidNationalityId.Value = ddNationality.SelectedValue.ToString() + "X%";
            }
            hidMaritalStatusId.Value = "%";
            if (ddMaritalStatus.SelectedValue != "0")
            {
                hidMaritalStatusId.Value = ddMaritalStatus.SelectedValue.ToString() + "X%";
            }
            hidShiftId.Value = "%";
            if (ddShift.SelectedValue != "0")
            {
                hidShiftId.Value = ddShift.SelectedValue.ToString() + "X%";
            }
            hidEmpTypeId.Value = "%";
            if (ddEmpType.SelectedValue != "0")
            {
                hidEmpTypeId.Value = ddEmpType.SelectedValue.ToString() + "X%";
            }
            hidEmpGroupId.Value = "%";
            if (ddEmpGroup.SelectedValue != "0")
            {
                hidEmpGroupId.Value = ddEmpGroup.SelectedValue.ToString() + "X%";
            }
            hidFloorId.Value = "%";
            if (ddFloor.SelectedValue != "0")
            {
                hidFloorId.Value = ddFloor.SelectedValue.ToString() + "X%";
            }
            hidLineId.Value = "%";
            if (ddLine.SelectedValue != "0")
            {
                hidLineId.Value = ddLine.SelectedValue.ToString() + "X%";
            }
            hidBranchId.Value = "%";
            if (ddBranch.SelectedValue != "0")
            {
                hidBranchId.Value = ddBranch.SelectedValue.ToString() + "X%";
            }
            hidLocationId.Value = "%";
            if (ddLocation.SelectedValue != "0")
            {
                hidLocationId.Value = ddLocation.SelectedValue.ToString() + "X%";
            }
            hidProjectId.Value = "%";
            if (ddProject.SelectedValue != "0")
            {
                hidProjectId.Value = ddProject.SelectedValue.ToString() + "X%";
            }



            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtAppointmentDateFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtAppointmentDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtConfirmationDateFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtConfirmationDateTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtReleasedFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtReleasedToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            //ddSupplier.SelectedValue = "0";
        }
    }
}