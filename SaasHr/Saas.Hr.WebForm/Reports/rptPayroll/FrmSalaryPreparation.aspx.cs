﻿using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmSalaryPreparation : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");
                ddMonth.SelectedValue = DateTime.Now.Month.ToString();
                //dateChange();
                v_loadGridView_CostingHead();

                int cy = DateTime.Today.Year - 5;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                txtYear.DataSource = years;
                txtYear.DataBind();
            }
        }

        protected void v_loadGridView_CostingHead()
        {
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            string qry = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            // qry = "select distinct  top 60 (Select DateName( month , DateAdd( month , SalaryMonth , -1 ) ))+'-'+cast(salaryYear as varchar(4)) as trMonth,isnull(SalaryDate,'') as accessDate,SalaryYear as sYear,SalaryMonth as sMonth from  SalarySheetMaster  order by isnull(SalaryDate,'') desc";

            qry = "select distinct  top 60 (Select DateName( month , DateAdd( month , s.salaryMonth , -1 ) ))+'-'+cast(s.salaryYear as varchar(4)) as trMonth,isnull(s.SalaryDate,'') as accessDate,s.salaryYear as sYear,s.salaryMonth as sMonth,lck=case when isnull(sl.salaryYear,0)=0 then 'UnLock' else 'Locked' end from  SalarySheetMaster  s left join SalarySheetLock sl on s.salaryMonth=sl.salaryMonth and s.salaryYear=sl.salaryYear order by isnull(SalaryDate,'') desc";

            SqlCommand cmd = new SqlCommand(qry, sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gdvList.DataSource = ds;
            //gdvList.AllowPaging = true;
            gdvList.DataBind();
            sqlconnection.Close();
        }
        protected void gdvList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvList, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }
        protected void gdvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged
            try
            {
                ddMonth.SelectedValue = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidGridMonth")).Value;
                txtYear.Text = ((HiddenField)gdvList.Rows[gdvList.SelectedIndex].FindControl("hidGridYear")).Value;
                txtSalaryDate.Text = ((Label)gdvList.Rows[gdvList.SelectedIndex].FindControl("lblGridDate")).Text;
                dateChange();
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            if (b_validationReturn == false)
            {
                return;
            }

            //s_save_ = "Delete SalarySheetMaster Where SalaryMonth= "
            //           + "'"
            //           + ddMonth.SelectedValue
            //           + "' and SalaryYear= '"
            //           + txtYear.Text
            //           + "' Select ''";
            s_save_ = "ProcSalarySheetDelete"
                       + "'"
                       + ddMonth.SelectedValue
                       + "','"
                       + txtYear.SelectedItem.ToString()
                       + "','D','" + SessionUserId + "'";

            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void btnLocked_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            if (b_validationReturn == false)
            {
                return;
            }

            s_save_ = "ProcSalarySheetDelete"
                       + "'"
                       + ddMonth.SelectedValue
                       + "','"
                       + txtYear.SelectedItem.ToString()
                       + "','L','" + SessionUserId + "'";

            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void btnUnLocked_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            if (b_validationReturn == false)
            {
                return;
            }

            //s_save_ = "Delete SalarySheetMaster Where SalaryMonth= "
            //           + "'"
            //           + ddMonth.SelectedValue
            //           + "' and SalaryYear= '"
            //           + txtYear.Text
            //           + "' Select ''";
            s_save_ = "ProcSalarySheetDelete"
                       + "'"
                       + ddMonth.SelectedValue
                       + "','"
                       + txtYear.SelectedItem.ToString()
                       + "','UL','" + SessionUserId + "'";

            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;
            if (b_validationReturn == false)
            {
                return;
            }

            //     m_CDB.oConn.Execute("ProcPrepareSalarySheet '" + m_s_tableName + "','" _
            //+ s_salaryMonth + "','" + s_salaryYear + "','" + s_date + "','" + s_fromDate + "','" + s_toDate + "','" + s_daysInMonth + "','" _
            //+ s_departmentAutoId + "','0','N','" + Format(Date, "yyyy-MM-dd") + "','0','N','0';")

            int Monthdays = DateTime.DaysInMonth(Convert.ToInt16(txtYear.SelectedItem.ToString()), Convert.ToInt16(ddMonth.SelectedValue));
            s_save_ = "[ProcPrepareSalarySheet]"
                        + "'"
                        + "TempTable"
                        + "','"
                        + ddMonth.SelectedValue
                        + "','"
                        + txtYear.SelectedItem.ToString()
                        + "','"
                        + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','"
                        + Monthdays
                        + "','"
                        + "%"//DepartmentId
                        + "','"
                        + "0"//CompanyId
                        + "','"
                        + "N"//With Bonus
                        + "','"
                        + Convert.ToDateTime(DateTime.Now, new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                        + "','0','N','0'";

            s_save_ = s_save_ + " Select ''";
            s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

            if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                }
            }
            else
            {
                lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
            }
        }


        protected void ddMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            dateChange();
        }
        protected void txtYear_TextChanged(object sender, EventArgs e)
        {
            dateChange();
        }

        private void dateChange()
        {
            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            string sMonth = string.Empty;
            string stoday = string.Empty;
            string sdt1 = string.Empty;
            string sdt2 = string.Empty;
            sMonth = ddMonth.SelectedValue.ToString();
            if (sMonth.Length < 2) { sMonth = "0" + sMonth; }
            sdt1 = "01/" + sMonth + "/" + txtYear.SelectedItem.ToString();
            txtFromDate.Text = sdt1.ToString();

            stoday = (DateTime.DaysInMonth(Convert.ToInt32(txtYear.SelectedItem.ToString()), Convert.ToInt32(sMonth))).ToString();
            if (stoday.Length < 2) { stoday = "0" + stoday; }

            sdt2 = stoday + "/" + sMonth + "/" + txtYear.SelectedItem.ToString();
            txtToDate.Text = sdt2.ToString(); 
            DateTime salDate = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Int64 addedDays = 5;
            salDate = salDate.AddDays(addedDays);
            DateTime end = salDate;
            txtSalaryDate.Text = end.ToString("dd/MM/yyyy");

        }



        ///////================
    }
}