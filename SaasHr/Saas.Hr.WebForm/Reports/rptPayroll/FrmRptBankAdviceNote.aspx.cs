﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{
    public partial class FrmRptBankAdviceNote : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");

                int cy = DateTime.Today.Year - 10; 
                List<int> years = Enumerable.Range(cy, 15).ToList();
                txtYear.DataSource = years;
                txtYear.DataBind();

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                "T_Bank",
                "Name",
                "Bank_Id", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddBonusType,
                  "BonusType",
                  "BonusType",
                  "AutoId", "Order by AutoId");

                string Proc = string.Empty;
                Proc = "[ProCEmployeeLoad] '" + SessionCompanyId + "','"+ SessionUserLevel + "'";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Session[GlobalVariables.g_s_printopt] = "N";

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "BankAdvice");

                if (ddBonusType.SelectedValue == "0")
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptSalaryBankAdvice.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptSalaryBankAdvice"
                                                                  + "'"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hidDepartmentId.Value
                                                                  + "','"
                                                                  + ddMonth.SelectedValue
                                                                  + "','"
                                                                  + txtYear.SelectedItem.ToString()
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + SessionUserLevel
                                                                  + "','"
                                                                  + hidRptType.Value
                                                                  + "','"
                                                                  + ddDepartment.SelectedValue
                                                                  + "'";


                }
                if (chkCSV.Checked == true)
                {
                    Session[GlobalVariables.g_s_rptQry] = "ProcRptSalaryBankAdvice"
                                                                  + "'"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hidDepartmentId.Value
                                                                  + "','"
                                                                  + ddMonth.SelectedValue
                                                                  + "','"
                                                                  + txtYear.SelectedItem.ToString()
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + SessionUserLevel
                                                                  + "','"
                                                                  + hidRptType.Value
                                                                  + "','"
                                                                  + ddDepartment.SelectedValue
                                                                  + "'";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/Payroll/ViewMonthlySalaryAdviceSheet.aspx');", true);

                }
                else if (ddBonusType.SelectedValue != "0")
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptSalaryBankAdvice.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcRptBonusBankAdvice"
                                                                  + "'"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hidDepartmentId.Value
                                                                  + "','"
                                                                  + ddMonth.SelectedValue
                                                                  + "','"
                                                                  + txtYear.SelectedItem.ToString()
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + SessionUserLevel
                                                                  + "','"
                                                                  + hidRptType.Value
                                                                  + "','"
                                                                  + ddDepartment.SelectedValue
                                                                  + "','"
                                                                  + ddBonusType.SelectedValue
                                                                  + "'";

                }


                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }
        private void ViewSalaryAdviceSheet()
        {
            Session[GlobalVariables.g_s_rptSalaryDate] = Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptTpe] = hidRptType.Value;
            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedValue.ToString();
            Session[GlobalVariables.g_s_rptforYear] = txtYear.Text.ToString();
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = "%";// ddEmployeeName.Sel ectedIndex == 0 ? "%" : ddEmployeeName.SelectedValue + "X%";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + ddMonth.SelectedItem.ToString() + "-" + txtYear.Text;


        }
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }
            
        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            if (ddMonth.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Month Can Not Blank!";
                return false;
            }
            if (ddDepartment.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Bank Can Not Blank!";
                return false;
            }
            hidRptType.Value = "N";
            hid_autoId.Value = "%";
           

            if (chkStaff.Checked) { hidRptType.Value = "S"; }
            if (chkWorker.Checked) { hidRptType.Value = "W"; }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}