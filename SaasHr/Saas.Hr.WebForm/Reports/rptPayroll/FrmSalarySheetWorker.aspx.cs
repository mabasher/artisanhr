﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Saas.Hr.WebForm.Common;
namespace Saas.Hr.WebForm.Reports.rptHR
{
    public partial class FrmSalarySheetWorker : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");

                int cy = DateTime.Today.Year - 10;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                txtYear.DataSource = years;
                txtYear.DataBind();

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                "DepartmentInfo",
                "Department",
                "DepartmentInfoId", string.Empty);

                string Proc = string.Empty;
                Proc = "[ProCEmployeeLoad] '" + SessionCompanyId + "','"+ SessionUserLevel + "'";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Session[GlobalVariables.g_s_printopt] = "N";

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }


            try
            {
                if (chkExtraSalarySheetforGeneraldayOT.Checked == true)
                {
                    //This come with General Days OT (After 2 Hour) Only
                    ViewSalarySheetAfter2hourOTGeneralDay();
                }
                else if (chkExtraSalarySheetforholidayOT.Checked == true)
                {
                    //This come with Holiday OT Only
                    ViewSalarySheetHolidayOT();
                }
                else if (chkBangla.Checked == true)
                {
                    //This come with 2 hour OT Only in Bangla Format
                    ViewSalarySheet2hourOTBangla();
                }
                else
                {
                    //This come with 2 hour OT Only
                    ViewSalarySheetwith2hourOT();
                }
            }
            catch (Exception)
            {
            }
        }

        private void ViewSalarySheetAfter2hourOTGeneralDay()
        {
            Session[GlobalVariables.g_s_rptSalaryDate] = Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptTpe] = hidRptType.Value;
            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedValue.ToString();
            Session[GlobalVariables.g_s_rptforYear] = txtYear.Text.ToString();
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = "%";// ddEmployeeName.SelectedIndex == 0 ? "%" : ddEmployeeName.SelectedValue + "X%";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + ddMonth.SelectedItem.ToString() + "-" + txtYear.Text;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/Payroll/ViewMonthlySalarySheetathogen.aspx');", true);
        }

        private void ViewSalarySheetHolidayOT()
        {
            Session[GlobalVariables.g_s_rptSalaryDate] = Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptTpe] = hidRptType.Value;
            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedValue.ToString();
            Session[GlobalVariables.g_s_rptforYear] = txtYear.Text.ToString();
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = "%";// ddEmployeeName.SelectedIndex == 0 ? "%" : ddEmployeeName.SelectedValue + "X%";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + ddMonth.SelectedItem.ToString() + "-" + txtYear.Text;

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/Payroll/ViewMonthlySalarySheethdo.aspx');", true);
        }
        private void ViewSalarySheet2hourOTBangla()
        {
            Session[GlobalVariables.g_s_rptSalaryDate] = Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptTpe] = hidRptType.Value;
            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedValue.ToString();
            Session[GlobalVariables.g_s_rptforYear] = txtYear.Text.ToString();
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = "%";// ddEmployeeName.SelectedIndex == 0 ? "%" : ddEmployeeName.SelectedValue + "X%";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + ddMonth.SelectedItem.ToString() + "-" + txtYear.Text;

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/Payroll/ViewMonthlySalarySheetthoBangla.aspx');", true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewMonthlySalarySheetBangla.aspx');", true);
            
        }
        private void ViewSalarySheetwith2hourOT()
        {
            Session[GlobalVariables.g_s_rptSalaryDate] = Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptTpe] = hidRptType.Value;
            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedValue.ToString();
            Session[GlobalVariables.g_s_rptforYear] = txtYear.Text.ToString();
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = "%";// ddEmployeeName.SelectedIndex == 0 ? "%" : ddEmployeeName.SelectedValue + "X%";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + ddMonth.SelectedItem.ToString() + "-" + txtYear.Text;
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewMonthlySalarySheet.aspx');", true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/Payroll/ViewMonthlySalarySheettho.aspx');", true);
            
        }
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            if (ddMonth.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Month Can Not Blank!";
                return false;
            }

            hidRptType.Value = "W";
            hid_autoId.Value = "%";
            //if (ddEmployeeName.SelectedValue != "0")
            //{
            //    hid_autoId.Value = ddEmployeeName.SelectedValue.ToString() + "X%";
            //}
            hidDepartmentId.Value = "%";
            if (ddDepartment.SelectedValue != "0")
            {
                hidDepartmentId.Value = ddDepartment.SelectedValue.ToString() + "X%";
            }
            //if (chkStaff.Checked) { hidRptType.Value = "S"; }
            //if (chkWorker.Checked) { hidRptType.Value = "W"; }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}