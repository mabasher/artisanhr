﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmBonusPreparation.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmBonusPreparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Prepare Bonus Sheet
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-md-5">
            <div class="form-horizontal">

                <div class="form-group">
                    <asp:Label ID="Label5" runat="server" Text="Bonus Type :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-5">
                        <asp:DropDownList ID="ddBonusType"
                            CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label11" runat="server" Text="Bonus Year :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="txtYear" CssClass="form-control input-sm chosen-select" AutoPostBack="true" OnTextChanged="txtYear_TextChanged" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" Text="Month :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddMonth"
                            AutoPostBack="true" OnSelectedIndexChanged="ddMonth_SelectedIndexChanged"
                            CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" Text="From Date :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtFromDate" Enabled="false" CssClass="input-sm date-picker" />
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label12" runat="server" Text="To Date :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtToDate" Enabled="false" CssClass="input-sm date-picker" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Bonus Date :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtSalaryDate" CssClass="input-sm date-picker" />
                    </div>
                </div>


                <div class="form-group hidden">
                    <asp:Label ID="Label4" runat="server" Text="Days in Month :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:CheckBox ID="chkDateUse" runat="server" Text="" />
                        <asp:TextBox ID="txtDaysInMonth" runat="server" placeholder="" CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>

                    </div>
                </div>

                <div class="form-group center" style="margin-top: 20px;">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>

                <div class="form-group center">
                    <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Prepare Bonus" OnClick="btn_save_Click" Width="120px" />
                    <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false" CssClass="btn btn-default btn-sm" Visible="false" OnClick="btn_refresh_Click" Width="80px" />
                    <asp:Button ID="btnRemove" runat="server" Text="Remove Bonus" CausesValidation="false" CssClass="btn btn-danger btn-sm" OnClick="btnRemove_Click" Width="120px" />
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="main-data-grid" style="overflow: scroll; text-align: center;">

                    <asp:GridView ID="gdvList" runat="server" Style="width: 100%; margin-left: 0px;"
                        AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                        CssClass="table table-striped table-bordered"
                        OnRowDataBound="gdvList_RowDataBound" OnSelectedIndexChanged="gdvList_SelectedIndexChanged"
                        EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>
                                <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="Bonus Month" HeaderStyle-CssClass="center">
                                <ItemTemplate>
                                    <asp:Label ID="lblSalaryMonth" runat="server" Text='<%# Bind("trMonth") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bonus Date" HeaderStyle-CssClass="center">
                                <ItemTemplate>
                                    <asp:Label ID="lblGridDate" runat="server" Text='<%# Bind("accessDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                    <asp:HiddenField ID="hidGridMonth" Value='<%# Bind("sMonth") %>' runat="server" />
                                    <asp:HiddenField ID="hidGridYear" Value='<%# Bind("sYear") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Bonus Type" HeaderStyle-CssClass="center">
                                <ItemTemplate>
                                    <asp:Label ID="lblBonusType" runat="server" Text='<%# Bind("BonusType") %>'></asp:Label>
                                    <asp:HiddenField ID="hidBonusTypeId" Value='<%# Bind("BonusTypeId") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <PagerStyle CssClass="pagination-sa" />
                        <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
