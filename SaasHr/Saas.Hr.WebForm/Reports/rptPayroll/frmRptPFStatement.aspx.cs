﻿using System;
using System.Globalization;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{   
    public partial class frmRptPFStatement : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                "DepartmentInfo",
                "Department",
                "DepartmentInfoId", string.Empty);

                commonfunctions.g_b_FillDropDownList(ddDesignation,
                "DesignationInfo",
                "Designation",
                "DesignationInfoId", string.Empty);

                string Proc = string.Empty;              

                if (SessionUserLevel == "5")//HR Only
                {
                    Proc = "select  EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                }
                else if (SessionUserLevel == "4")//Single Employee Only
                {
                    ddEmployeeName.Enabled = false;
                    Proc = "select EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                    ddEmployeeName.SelectedValue = SessionUserId;
                    ddEmployeeName.Enabled = false;
                    ddDepartment.Enabled = false;
                    ddDepartment.Visible = false;
                    lbldept.Visible = false;
                }
                else
                {
                    Proc = "select EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            Session[GlobalVariables.g_s_printopt] = "N";

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "PF Summary Statement");
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("5FromDate", txtFromDate.Text.ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("6Todate", txtToDate.Text.ToString());
                if (chkPFStatement.Checked)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptPFStatementDetails.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcPFStatementDetails"
                                                                  + " '"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hidDepartmentId.Value
                                                                  + "','"
                                                                  + hid_DesignationId.Value
                                                                  + "','"
                                                                  + '%'
                                                                  + "','"
                                                                  + '%'
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + SessionUserLevel
                                                                  + "','"
                                                                  + ddGroupByCollum.SelectedItem.Text.Trim()
                                                                  + "'";
                }
                else
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptPFSummaryStatement.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcPFSummaryStatement"
                                                                  + " '"
                                                                  + SessionCompanyId
                                                                  + "','"
                                                                  + hid_autoId.Value
                                                                  + "','"
                                                                  + hidDepartmentId.Value
                                                                  + "','"
                                                                  + hid_DesignationId.Value
                                                                  + "','"
                                                                  + '%'
                                                                  + "','"
                                                                  + '%'
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                                  + "','"
                                                                  + SessionUserLevel
                                                                  + "','"
                                                                  + ddGroupByCollum.SelectedItem.Text.Trim()
                                                                  + "'";
                }
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            //if (ddMonth.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Month Can Not Blank!";
            //    return false;
            //}

            hidRptType.Value = "N";
            hid_autoId.Value = "%";
            if (ddEmployeeName.SelectedValue != "0")
            {
                hid_autoId.Value = ddEmployeeName.SelectedValue.ToString() + "X%";
            }

            hidDepartmentId.Value = "%";
            if (ddDepartment.SelectedValue != "0")
            {
                hidDepartmentId.Value = ddDepartment.SelectedValue.ToString() + "X%";
            }
            hid_DesignationId.Value = "%";
            if (ddDesignation.SelectedValue != "0")
            {
                hid_DesignationId.Value = ddDesignation.SelectedValue.ToString() + "X%";
            }

            if (chkStaff.Checked) { hidRptType.Value = "S"; }
            if (chkWorker.Checked) { hidRptType.Value = "W"; }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}