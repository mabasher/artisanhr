﻿using System;
using System.Globalization;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{
    public partial class FrmSalarySheetPartial : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddMonth,
                  "T_Month",
                  "MonthName",
                  "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");
                commonfunctions.g_b_FillDropDownList(ddDepartment,
                "DepartmentInfo",
                "Department",
                "DepartmentInfoId", string.Empty);

                string Proc = string.Empty;
                Proc = "[ProCEmployeeLoad] '" + SessionCompanyId + "','"+ SessionUserLevel + "'";
                commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
            }
        }
        

        protected void btnPrepare_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();



            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "Salary Info");
                // TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("8FromDate", txtFromDate.Text.ToString());

                TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptSalaryInfo.rpt";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcPreparePartialSalarySheet"
                                                              + " '"
                                                              + SessionCompanyId
                                                              + "','"
                                                              + hid_autoId.Value
                                                              + "','"
                                                              + hidDepartmentId
                                                              + "','"
                                                              + ddMonth.SelectedValue
                                                              + "','"
                                                              + txtYear.Text.Trim()
                                                              + "','"
                                                              + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                               + "','"
                                                              + SessionUserLevel
                                                              + "'";

                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            
           
            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "Salary Info");
               // TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("8FromDate", txtFromDate.Text.ToString());
                      
                TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptSalaryInfo.rpt";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "ProcSalarySheet"
                                                              + " '"
                                                              + SessionCompanyId
                                                              + "','"
                                                              + hid_autoId.Value
                                                              + "','"
                                                              + hidDepartmentId
                                                              + "','"
                                                              + ddMonth.SelectedValue
                                                              + "','"
                                                              + txtYear.Text.Trim()
                                                              + "','"
                                                              + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                               + "','"
                                                              + SessionUserLevel
                                                              + "'";    

                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            //if (dd_Category.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Type Can Not Blank!";
            //    return false;
            //}

            hid_autoId.Value = "%";
            if (ddEmployeeName.SelectedValue != "0")
            {
                hid_autoId.Value = ddEmployeeName.SelectedValue.ToString() + "X%";
            }
            hidDepartmentId.Value = "%";
            if (ddDepartment.SelectedValue != "0")
            {
                hidDepartmentId.Value = ddDepartment.SelectedValue.ToString() + "X%";
            }


            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}