﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="~/Reports/rptPayroll/FrmSalaryPayHeadSummary.aspx.cs" Inherits="Saas.Hr.WebForm.Reports.rptHR.FrmSalaryPayHeadSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Salary Pay Head Statement Summary
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-sm-5">
            <div class="form-horizontal">
                <div class="form-group ">
                    <asp:Label ID="Label1" runat="server" Text="Employee Name :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddEmployeeName" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Department :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>

                 <div class="form-group">
                    <asp:Label ID="Label12" runat="server" Text="Designation :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddDesignation" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                 <div class="form-group">
                            <asp:Label ID="Label11" runat="server" Text="Report Type :" CssClass="col-sm-4 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList runat="server" ID="ddGroupByCollum">
                                    <asp:ListItem Text="Department" Value="Department" />
                                    <asp:ListItem Text="Designation" Value="Designation" />
                                    <asp:ListItem Text="Section" Value="Section" />
                                    <asp:ListItem Text="Line" Value="Line" />
                                    <asp:ListItem Text="TypeName" Value="TypeName" />
                                    <asp:ListItem Text="ShiftName" Value="ShiftName" />
                                    <asp:ListItem Text="LocationName" Value="LocationName" />
                                </asp:DropDownList>
                            </div>
                        </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label6" runat="server" Text="Month :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddMonth" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtYear" runat="server" CssClass="form-control input-sm" MaxLength="4"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group ">
                    <asp:Label ID="Label3" runat="server" Text="From Date :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                        <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />                        
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Salary Date :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtSalaryDate" CssClass="input-sm date-picker" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label4" runat="server" Text="Days in Month :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:CheckBox ID="chkDateUse" runat="server" Text="" />
                        <asp:TextBox ID="txtDaysInMonth" runat="server" placeholder="" CssClass="form-control input-sm" FilterType="Numbers,Custom"></asp:TextBox>
                        <ajx:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers,Custom"
                            ValidChars="." TargetControlID="txtDaysInMonth" />
                    </div>
                </div>
                <div class="form-group hidden">
            <asp:Label ID="Label5" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
            <div class="col-sm-8">
                <asp:CheckBox ID="chkStaff" runat="server" Text="Staff Salary" />
            </div>
        </div>
        <div class="form-group hidden">
            <asp:Label ID="Label8" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
            <div class="col-sm-8">
                <asp:CheckBox ID="chkWorker" runat="server" Text="Worker Salary" />
            </div>
        </div>
        

        <div class="form-group hidden">
            <asp:Label ID="Label9" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
            <div class="col-sm-8">
                <asp:CheckBox ID="chkBonus" runat="server" Text="Only Bonus" />
            </div>
        </div>

        <div class="form-group hidden ">
            <asp:Label ID="Label10" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
            <div class="col-sm-8">
                <asp:CheckBox ID="chkBangla" runat="server" Text="Format In Bangla Language" />
            </div>
        </div>


            </div>
        </div>

    </div>
    <div class="col-sm-10 hidden">
        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="" CssClass="col-sm-1 control-label"></asp:Label>
            <div class="col-sm-1">
                <div class="radio">
                    <label>
                        <input type="radio" name="optradio" checked>Bank</label>
                </div>
            </div>
            <div class="col-sm-1">
                <div class="radio">
                    <label>
                        <input type="radio" name="optradio">Cash</label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="radio">
                    <label> <input type="radio" name="optradio">On Hold</label>
                </div>
            </div>

        </div>
        
        
    </div>


    <div class="col-sm-12 ">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10" style="margin-top: 50px;">
                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btn_save_Click" Width="80px" />
                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
            </div>
        </div>
    </div>


    <div class="row row-custom">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="text-danger">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </div>
        </div>
    </div>



    <asp:HiddenField ID="hid_autoId" runat="server" Value="0" />
    <asp:HiddenField ID="hid_DesignationId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />

    <asp:HiddenField ID="hidReligionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidBloodGroupId" runat="server" Value="0" />
    <asp:HiddenField ID="hidGenderId" runat="server" Value="0" />
    <asp:HiddenField ID="hidNationalityId" runat="server" Value="0" />
    <asp:HiddenField ID="hidMaritalStatusId" runat="server" Value="0" />
    <asp:HiddenField ID="hidShiftId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmpTypeId" runat="server" Value="0" />
    <asp:HiddenField ID="hidEmpGroupId" runat="server" Value="0" />
    <asp:HiddenField ID="hidFloorId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidBranchId" runat="server" Value="0" />
    <asp:HiddenField ID="hidProjectId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLocationId" runat="server" Value="0" />
    <asp:HiddenField ID="hidRptType" runat="server" Value="N" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
