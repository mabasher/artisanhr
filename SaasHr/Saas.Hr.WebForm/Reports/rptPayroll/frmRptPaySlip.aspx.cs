﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Saas.Hr.WebForm.Common;

namespace Saas.Hr.WebForm.Reports.rptHR
{   
    public partial class frmRptPaySlip : BasePage
    {
        private string sdateUse = string.Empty;
        private string sAppointmentDateUse = string.Empty;
        private string sConfirmationDateUse = string.Empty;
        private string sReleasedDateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddMonth,
                      "T_Month",
                      "MonthName",
                      "MonthNum", "Order by MonthNum");

                txtYear.Text = DateTime.Now.ToString("yyyy");

                int cy = DateTime.Today.Year - 10;
                List<int> years = Enumerable.Range(cy, 15).ToList();
                txtYear.DataSource = years;
                txtYear.DataBind();

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);

                string Proc = string.Empty; 
                //if (SessionUserLevel == "5")//HR Only
                //{
                //    Proc = "select  EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                //    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                //}
                //else if (SessionUserLevel == "4")//Single Employee Only

                if (SessionUserLevel == "4")//Single Employee Only
                {
                    ddEmployeeName.Enabled = false;
                    Proc = "select EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                    ddEmployeeName.SelectedValue = SessionUserId;
                    ddEmployeeName.Enabled = false;
                    ddDepartment.Enabled = false;
                    ddDepartment.Visible = false;
                    lbldept.Visible = false;
                }
                else
                {
                    Proc = "select EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployeeName, Proc);
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserLevel = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;
            string sQry = string.Empty;
            Session[GlobalVariables.g_s_printopt] = "N";

            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            try
            {
                if (chkYearlySalaryCertificate.Checked)
                {
                    ViewYearlySalaryCertificate();
                    return;
                }

                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();

                sQry="ProcSalarySheet"
                                + " '"
                                + SessionCompanyId
                                + "','"
                                + hid_autoId.Value
                                + "','"
                                + hidDepartmentId.Value
                                + "','"
                                + ddMonth.SelectedValue
                                + "','"
                                + txtYear.SelectedItem.ToString()
                                + "','"
                                + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "','"
                                + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "','"
                                + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "','"
                                + SessionUserLevel
                                + "','"
                                + hidRptType.Value
                                + "'";

                if (chkEnglish.Checked)
                {
                    //TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RpLblPaySlipEnglish.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptPaySlipEnglishNewFormat.rpt";
                }
                else if (chkSalaryCertificate.Checked)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptSalaryCertificate.rpt";
                    sQry = "ProcSalaryCertificate"
                                + " '"
                                + SessionCompanyId
                                + "','"
                                + hid_autoId.Value
                                + "','"
                                + hidDepartmentId.Value
                                + "','"
                                + ddMonth.SelectedValue
                                + "','"
                                + txtYear.SelectedItem.ToString()
                                + "','"
                                + Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "','"
                                + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "','"
                                + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                + "','"
                                + SessionUserLevel
                                + "','"
                                + hidRptType.Value
                                + "'";
                }
                else
                {
                    //TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RpLblPaySlip.rpt";
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptPaySlipBanglaNewFormat1.rpt";
                }

                TableColumnName.Rpt_ErpReport.g_s_sql_query = sQry;
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }

        private void ViewYearlySalaryCertificate()
        {
            Session[GlobalVariables.g_s_rptSalaryDate] = Convert.ToDateTime(txtSalaryDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptTpe] = hidRptType.Value;
            Session[GlobalVariables.g_s_rptforMonth] = ddMonth.SelectedValue.ToString();
            Session[GlobalVariables.g_s_rptforYear] = txtYear.Text.ToString();
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = hid_autoId.Value;// ddEmployeeName.SelectedIndex == 0 ? "%" : ddEmployeeName.SelectedValue + "X%";
            Session[GlobalVariables.g_s_Period] = "For the Session of " + Convert.ToString(Convert.ToDouble(txtYear.Text)-1)+" - " + txtYear.Text;

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewYearlySalaryCertificate.aspx');", true);
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            if (ddMonth.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Month Can Not Blank!";
                return false;
            }

            hidRptType.Value = "N";
            hid_autoId.Value = "%";
            if (ddEmployeeName.SelectedValue != "0")
            {
                hid_autoId.Value = ddEmployeeName.SelectedValue.ToString() + "X%";
            }
            hidDepartmentId.Value = "%";
            if (ddDepartment.SelectedValue != "0")
            {
                hidDepartmentId.Value = ddDepartment.SelectedValue.ToString() + "X%";
            }
            if (chkStaff.Checked) { hidRptType.Value = "S"; }
            if (chkWorker.Checked) { hidRptType.Value = "W"; }
            if (chkSalaryCertificate.Checked || chkYearlySalaryCertificate.Checked)
            {
                if (ddEmployeeName.SelectedValue == "0")
                {
                    return false;
                }
            }

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}