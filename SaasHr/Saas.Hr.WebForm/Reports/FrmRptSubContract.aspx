﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmRptSubContract.aspx.cs" Inherits="Saas.Hr.WebForm.Reports.FrmRptSubContract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Sub-Contract Report
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="PO No :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddPINo" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Label ID="Label8" runat="server" Text="Invocie No :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddInvocie" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Label ID="Label9" runat="server" Text="Bill | Collection No :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddCollection" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label1" runat="server" Text="Buyer Name :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="ddSupplier" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="lbl_issue_Date" runat="server" Text="Date :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                                <asp:CheckBox ID="chkDateUse" runat="server" Text="" />
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <asp:Label ID="Label14" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="chkSubContract" runat="server" Text="Sub Contract"/>
                                <asp:CheckBox ID="chkSample" runat="server" Text="Sample"/>
                            </div>
                        </div>
                        
                        <div class="form-group" style="margin-top: 20px;">
                            <asp:Label ID="Label3" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="chkPOFormat" runat="server" Text="PO List" />
                            </div>
                        </div>                        
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="chkExFactory" runat="server" Text="Ex-Factory List" />
                            </div>
                        </div>                                            
                        <div class="form-group">
                            <asp:Label ID="Label6" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="chkInvoice" runat="server" Text="Bill | Collection List" />
                            </div>
                        </div>
                                                                  
                        <div class="form-group">
                            <asp:Label ID="Label7" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:CheckBox ID="chkCoolectionSheet" runat="server" Text="Bill | Collection Sheet" />
                            </div>
                        </div>

                        <div class="row row-custom">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="text-danger">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />

                                <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hidPOId" runat="server" Value="" />
        <asp:HiddenField ID="hidSupplierId" runat="server" Value="" />
        <asp:HiddenField ID="hidInvoiceId" runat="server" Value="" />
        <asp:HiddenField ID="hidBuyerId" runat="server" Value="" />
        <asp:HiddenField ID="hidBillId" runat="server" Value="" />
        <asp:HiddenField ID="hidtrType" runat="server" Value="" />
        <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>