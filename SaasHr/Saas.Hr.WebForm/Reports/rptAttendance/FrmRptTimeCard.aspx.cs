﻿using CrystalDecisions.CrystalReports.Engine;
using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.ReportFiles;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Saas.Hr.WebForm.Reports.rptAttendance
{
    public partial class FrmRptTimeCard : BasePage
    {
        private string sdateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLeaveType,
                    "LeaveType",
                    "LeaveType",
                    "LeaveTypeId", string.Empty);
                //commonfunctions.g_b_FillDropDownListMultiColumn(ddEmployee,
                //    "EmployeePersonalInfo",
                //    "PIN",
                //    "EmployeeName",
                //    "EmployeePersonalInfoId", string.Empty);

                string Proc = string.Empty;
                //if (SessionUserLevel == "5")//HR Only
                //{
                //    Proc = "select  EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                //    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                //}
               // else if (SessionUserLevel == "4")//Single Employee Only
                if (SessionUserLevel == "4")//Single Employee Only
                {
                    ddEmployee.Enabled = false;
                    Proc = "select EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                    ddEmployee.SelectedValue = SessionUserId;
                }
                else
                {
                    Proc = "select EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                }

            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

            //txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            Boolean b_validationReturn = true;
            b_validationReturn = isValid();
            if (b_validationReturn == false)
            {
                return;
            }

            try
            {
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Clear();
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Company_Name1", Session[GlobalVariables.g_s_companyName].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Address2", Session[GlobalVariables.g_s_Address].ToString() + "," + Session[GlobalVariables.g_s_City].ToString() + "," + Session[GlobalVariables.g_s_Country].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Phone_Fax3", Session[GlobalVariables.g_s_Phone].ToString() + "," + Session[GlobalVariables.g_s_Fax].ToString() + "," + Session[GlobalVariables.g_s_Email].ToString());
                TableColumnName.Rpt_ErpReport.dct_reportFormulaField.Add("Report_Name4", "TIME CARD");

                if (chkFormat1.Checked)
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptTimeCard.rpt";//Format-1
                }
                else
                {
                    TableColumnName.Rpt_ErpReport.g_s_rptFilePath = "~\\ReportFiles\\RptTimeCardF2.rpt";//Format-2
                }

                hid_autoId.Value = ddEmployee.SelectedValue + "X%";
                TableColumnName.Rpt_ErpReport.g_s_sql_query = "Proc_Rpt_TimeCard_BGMEA"
                                                              + " '"
                                                              + hid_autoId.Value
                                                              + "','"
                                                              + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")
                                                              + "','"
                                                              + "N"
                                                              + "','"
                                                              + "N"
                                                              + "'";
                Session[GlobalVariables.g_s_printopt] = "N";
                if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }
                OpenReport();
            }
            catch (Exception)
            {
            }
        }
        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            CommonFunctions commonFunctions = new CommonFunctions();

            if (ddEmployee.SelectedValue == "0")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Can Not Blank!";
                return false;
            }
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            ddDepartment.SelectedValue = "0";
        }
    }
}