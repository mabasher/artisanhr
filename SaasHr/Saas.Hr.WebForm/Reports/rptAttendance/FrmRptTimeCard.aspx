﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmRptTimeCard.aspx.cs" Inherits="Saas.Hr.WebForm.Reports.rptAttendance.FrmRptTimeCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Time Card
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-sm-12">
            <div class="form-horizontal">

                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Department :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label1" runat="server" Text="Section :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label3" runat="server" Text="Line :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label5" runat="server" Text="Leave Type :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddLeaveType" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" Text="Employee Name :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddEmployee" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Date :" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                        <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                        <asp:CheckBox ID="chkDateUse" Visible="false" runat="server" Text="" />
                    </div>
                </div>

                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkFormat1" runat="server" Checked="false" Text=" Format-1" CssClass="text-default" />
                    </div>
                </div>

                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkleaveCard" runat="server" Checked="false" Text=" Leave Card" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkleaveStatus" runat="server" Checked="false" Text=" Leave Status" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceSummary" runat="server" Checked="false" Text=" Monthly Attendance Summary" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceRegister" runat="server" Checked="false" Text=" Monthly Attendance Register" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceLog" runat="server" Checked="false" Text=" Attendance Log Book" CssClass="text-default" />
                    </div>
                </div>

                <div class="row row-custom">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="text-danger">
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />

                        <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
                    </div>
                </div>

            </div>
        </div>
    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    <asp:HiddenField ID="hidFormDate" runat="server" Value="" />
    <asp:HiddenField ID="hidToDate" runat="server" Value="" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
