﻿using CrystalDecisions.CrystalReports.Engine;
using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.ReportFiles;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Saas.Hr.WebForm.Reports.rptAttendance
{
    public partial class FrmRptLeave : BasePage
    {
        private string sdateUse = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddJobLocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLeaveType,
                    "LeaveType",
                    "LeaveType",
                    "LeaveTypeId", string.Empty);
                //commonfunctions.g_b_FillDropDownListMultiColumn(ddEmployee,
                //    "EmployeePersonalInfo",
                //    "PIN",
                //    "EmployeeName",
                //    "EmployeePersonalInfoId", string.Empty);

                string Proc = string.Empty;
                //if (SessionUserLevel == "5")//HR Only
                //{
                //    Proc = "select  EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                //    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                //}
                //else if (SessionUserLevel == "4")//Single Employee Only
                 if (SessionUserLevel == "4")//Single Employee Only
                {
                    ddEmployee.Enabled = false;
                    Proc = "select EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                    ddEmployee.SelectedValue = SessionUserId;
                }
                else
                {
                    Proc = "select EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                }

            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Session[GlobalVariables.g_s_printopt] = "N";
            if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }

            hidFormDate.Value = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy");
            hidToDate.Value = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy");
            if (chkleaveCard.Checked)
            {
                if (ddEmployee.SelectedValue != "0")
                {
                    ViewCard();
                }
            }
            else if (chkleaveStatus.Checked)
            {
                ViewLeaveStatus();
            }
            else if (chksummary.Checked)
            {
                ViewLeaveSummary();
            }            
            else
            {
                if (ddEmployee.SelectedValue != "0")
                {
                    ViewCard();
                }
            }
        }

        private void ViewCard()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);
            var poSheetSql = "ProcRptLeaveSummary";
            var poSheetTermsSql = "ProcRptLeaveDetails";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId", SessionCompanyId},
                {"empAutoId", ddEmployee.SelectedValue+"X%"},
                {"year", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("yyyy")}
            };

            //{ "empAutoId", ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%"},

            var leaveSummery = fn.GetDataSetFromSp(poSheetSql, dic);
            var leaveDetails = fn.GetDataSetFromSp(poSheetTermsSql, dic);

            var rpt = new RptLeave();
            rpt.SetDataSource(leaveSummery);
            rpt.Subreports["rptleavedetails"].SetDataSource(leaveDetails);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);

            Session["rpt"] = rpt;
            OpenSubReport();
        }

        private void ViewCard1()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            procSql = "ProcRptLeaveSummary";
            rptURL = "~\\ReportFiles\\RptAbsent1.rpt";
            rptName = "Absent List";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",SessionCompanyId},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"shiftId", "%"},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            Session["rpt"] = rpt;
            OpenGnReport();
        }

        private void ViewLeaveStatus()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            string sMode = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            procSql = "ProcRptDailyAttendance";
            rptURL = "~\\ReportFiles\\RptDailyAttendance.rpt";
            rptName = "Daily Arrival List";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",SessionCompanyId},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"shiftId", "%"},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"status", "%"},
                {"trActual", "Y"}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            rpt.SetParameterValue("Period", "Report Period : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy"));
            Session["rpt"] = rpt;
            OpenGnReport();
        }

        private void ViewLeaveSummary()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJobLocation.SelectedIndex == 0 ? "%" : ddJobLocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";

            Session[GlobalVariables.g_s_Period] = "For the Year of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("yyyy");
            //Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewLeaveSummary.aspx');", true);
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (dd_Category.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Type Can Not Blank!";
            //    return false;
            //}
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            ddDepartment.SelectedValue = "0";
        }
    }
}