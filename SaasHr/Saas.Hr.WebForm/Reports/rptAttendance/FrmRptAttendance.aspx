﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="~/Reports/rptAttendance/FrmRptAttendance.aspx.cs" Inherits="Saas.Hr.WebForm.Reports.rptAttendance.FrmRptAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Daily Attendance Information
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-sm-5">
            <div class="form-horizontal">

                <div class="form-group">
                    <asp:Label ID="Label7" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        &nbsp;
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label6" runat="server" Text="Job Location :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddJoblocation" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Department :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddDepartment" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type:" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddLeaveType" runat="server" CssClass="form-control input-sm chosen-select"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label1" runat="server" Text="Section :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddSection" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label3" runat="server" Text="Line :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddLine" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" Text="Employee Name :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList ID="ddEmployee" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group hidden">
                    <asp:Label ID="Label5" runat="server" Text="Report Type :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:DropDownList runat="server" ID="ddGroupByCollum" CssClass="form-control input-sm chosen-select">
                            <asp:ListItem Text="Department" Value="Department" />
                            <asp:ListItem Text="Designation" Value="Designation" />
                            <asp:ListItem Text="Section" Value="Section" />
                            <asp:ListItem Text="Line" Value="Line" />
                            <asp:ListItem Text="TypeName" Value="TypeName" />
                            <asp:ListItem Text="ShiftName" Value="ShiftName" />
                            <asp:ListItem Text="LocationName" Value="LocationName" />
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="lbl_issue_Date" runat="server" Text="Date Between :" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                        <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                        <asp:CheckBox ID="chkDateUse" Visible="false" runat="server" Text="" />
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label8" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:CheckBox ID="chkday" runat="server" Checked="false" Text="&nbsp; Day" CssClass="text-default" />
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label22" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:CheckBox ID="chkStaff" runat="server" Checked="false" Text="&nbsp; Staff" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label9" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:CheckBox ID="chknight" runat="server" Checked="false" Text="&nbsp; Night" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label10" runat="server" Text="" CssClass="col-sm-4 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <asp:CheckBox ID="chkAll" runat="server" Checked="false" Text="&nbsp; All Shift" CssClass="text-default" />
                    </div>
                </div>

            </div>
        </div>


        <div class="col-sm-4 hidden">
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        &nbsp; 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAbsentList" runat="server" Checked="false" Text="&nbsp; Absent List" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkArrivalList" runat="server" Checked="false" Text="&nbsp; Arrival List" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkDailyAttendance" runat="server" Checked="false" Text="&nbsp; Daily Attendance" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkleavelist" runat="server" Checked="false" Text="&nbsp; Leave Report" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkshortleave" runat="server" Checked="false" Text="&nbsp; Short Leave Report" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkTiffinTime" runat="server" Checked="false" Text="&nbsp; Tiffin Time Attendance" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkTiffinBill" runat="server" Checked="false" Text="&nbsp; Monthly Tiffin Bill" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkMonthlyAbsenteeism" runat="server" Checked="false" Text="&nbsp; Monthly Absenteeism Report" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceSummary" runat="server" Checked="false" Text="&nbsp; Monthly Attendance Summary" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceDetails" runat="server" Checked="false" Text="&nbsp; Monthly Attendance Details" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceRegister" runat="server" Checked="false" Text="&nbsp; Monthly Attendance Register" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkAttendanceLog" runat="server" Checked="false" Text="&nbsp; Attendance Log Book" CssClass="text-default" />
                    </div>
                </div>
                <div class="form-group hidden">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:CheckBox ID="chkEarnedLeave" runat="server" Checked="false" Text="&nbsp; Earned Leave" CssClass="text-default" />
                    </div>
                </div>
            </div>
        </div>


        <div class="col-sm-4">
            <div class="form-horizontal">


                <div class="form-group">
                    <asp:Label ID="Label11" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbAbsentlist" runat="server">
                            <label class="form-check-label">
                                Absent List
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label12" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbArrivallist" runat="server">
                            <label class="form-check-label">
                                Arrival List
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label13" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbDailyAttendance" runat="server">
                            <label class="form-check-label">
                                Daily Attendance
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label14" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbLeave" runat="server">
                            <label class="form-check-label">
                                Leave Report
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label15" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbLeaveShort" runat="server">
                            <label class="form-check-label">
                                Short Leave Report
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label16" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbMonthlyTiffin" runat="server">
                            <label class="form-check-label">
                                Monthly Tiffin Bill
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label19" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbDailyTiffin" runat="server">
                            <label class="form-check-label">
                                Daily Tiffin Bill
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label17" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbMonthlyAbsenteeism" runat="server">
                            <label class="form-check-label">
                                Monthly Absenteeism Report
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label18" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbMonthlyAttendanceSummary" runat="server">
                            <label class="form-check-label">
                                Monthly Attendance Summary
                            </label>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <asp:Label ID="Label20" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                    <div class="col-sm-8">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="attnrdo" id="rdbAttendancelog" runat="server">
                            <label class="form-check-label">
                                Attendance Log Book
                            </label>
                        </div>
                    </div>
                </div>


                <%--<div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
                    <label class="form-check-label" for="exampleRadios3">
                        Disabled radio
                    </label>
                </div>--%>
            </div>
        </div>




        <div class="col-sm-6">
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        &nbsp; 
                    </div>
                </div>
                <div class="row row-custom">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="text-danger">
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />

                        <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
                    </div>
                </div>
            </div>
        </div>


    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
    <asp:HiddenField ID="hidFormDate" runat="server" Value="" />
    <asp:HiddenField ID="hidToDate" runat="server" Value="" />
    <asp:HiddenField ID="hidShift" runat="server" Value="N" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
