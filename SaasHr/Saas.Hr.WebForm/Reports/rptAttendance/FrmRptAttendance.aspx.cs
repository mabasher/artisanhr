﻿using CrystalDecisions.CrystalReports.Engine;
using Saas.Hr.WebForm.Common;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Saas.Hr.WebForm.Reports.rptAttendance
{
    public partial class FrmRptAttendance : BasePage
    {
        private string sdateUse = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
                string SessionUserLevel = Convert.ToString(Session[GlobalVariables.g_s_userLevel]);

                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                commonfunctions.g_b_FillDropDownList(ddDepartment,
                    "DepartmentInfo",
                    "Department",
                    "DepartmentInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddSection,
                    "SectionInfo",
                    "Section",
                    "SectionInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLine,
                    "LineInfo",
                    "Line",
                    "LineInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddJoblocation,
                    "LocationInfo",
                    "LocationName",
                    "LocationInfoId", string.Empty);
                commonfunctions.g_b_FillDropDownList(ddLeaveType,
                    "LeaveType",
                    "leaveType",
                    "LeaveTypeId", string.Empty);

                string Proc = string.Empty;
                //if (SessionUserLevel == "5")//HR Only
                //{
                //    Proc = "select  EmployeePersonalInfoId as ItemId, substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo p where p.CompanyAutoId= '" + SessionCompanyId + "' and (p.PIN like '%W%')  Order by EmployeeName asc";
                //    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                //}
                //else if (SessionUserLevel == "4")//Single Employee Only
                if (SessionUserLevel == "4")//Single Employee Only
                {
                    ddEmployee.Enabled = false;
                    Proc = "select EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                    ddEmployee.SelectedValue = SessionUserId;
                }
                else
                {
                    Proc = "select EmployeePersonalInfoId as ItemId,  substring(PIN,5,len(PIN)-4) + ' ' + isnull(EmployeeName, '')+' ('+PIN+')' as ItemName from EmployeePersonalInfo  where CompanyAutoId='" + SessionCompanyId + "' Order by EmployeeName asc";
                    commonfunctions.g_b_FillDropDownListByQurey(ddEmployee, Proc);
                }

            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Session[GlobalVariables.g_s_printopt] = "N";
            if (chkpdf.Checked == true) { Session[GlobalVariables.g_s_printopt] = "P"; }

            hidFormDate.Value = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy");
            hidToDate.Value = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy");

            DateTime d1 = Convert.ToDateTime(txtFromDate.Text);
            DateTime d2 = Convert.ToDateTime(txtToDate.Text);

            TimeSpan t = d2 - d1;
            double NrOfDays = t.TotalDays;

            //if (chkAbsentList.Checked)
            //{
            //    ViewList();
            //}
            if (rdbAbsentlist.Checked)
            {
                ViewList();
            }

            //else if (chkArrivalList.Checked)
            //{
            //    if (ddEmployee.SelectedValue == "0" || ddDepartment.SelectedValue == "0")
            //    {
            //        if (ddJoblocation.SelectedValue == "0")
            //        {
            //            return;
            //        }
            //    }
            //    if (NrOfDays > 31)
            //    {
            //        return;
            //    }
            //    ViewArrivalListReport();
            //}

            else if (rdbArrivallist.Checked)
            {
                if (ddEmployee.SelectedValue == "0" || ddDepartment.SelectedValue == "0")
                {
                    if (ddJoblocation.SelectedValue == "0")
                    {
                        return;
                    }
                }
                if (NrOfDays > 31)
                {
                    return;
                }
                ViewArrivalListReport();
            }
            //else if (chkDailyAttendance.Checked)
            //{
            //    ViewDailyAttendance();
            //}
            else if (rdbDailyAttendance.Checked)
            {
                ViewDailyAttendance();
            }
            //else if (chkTiffinBill.Checked)
            //{
            //    ViewMonthlyTiffinBill();
            //}
            else if (rdbMonthlyTiffin.Checked)
            {
                ViewMonthlyTiffinBill();
            }
            else if (rdbDailyTiffin.Checked)
            {
                ViewDailyTiffinBill();
            }
            else if (rdbLeave.Checked)
            {
                ViewLeavelist();
            }
            //else if (chkshortleave.Checked)
            //{
            //    ViewShortLeavelist();
            //}
            else if (rdbLeaveShort.Checked)
            {
                ViewShortLeavelist();
            }
            //else if (chkMonthlyAbsenteeism.Checked)
            //{
            //    if (ddJoblocation.SelectedValue == "0")
            //    {
            //        return;
            //    }
            //    ViewMonthlyAbsenteeism();
            //}
            else if (rdbMonthlyAbsenteeism.Checked)
            {
                if (ddJoblocation.SelectedValue == "0")
                {
                    return;
                }
                ViewMonthlyAbsenteeism();
            }
            else if (chkTiffinTime.Checked)
            {
                ViewTiffinTimeAttendance();
            }
            //else if (chkAttendanceSummary.Checked)
            //{
            //    ViewAttendanceSummaryWebReport();
            //}
            else if (rdbMonthlyAttendanceSummary.Checked)
            {
                ViewAttendanceSummaryWebReport();
            }
            //else if (chkAttendanceLog.Checked)
            //{
            //    ViewAttendanceLogList();
            //}
            else if (rdbAttendancelog.Checked)
            {
                ViewAttendanceLogList();
            }
            //else if (chkEarnedLeave.Checked)
            //{
            //    ViewEarnedLeave();
            //}
            else
            {
                ViewList();
            }
        }

        private void ViewEarnedLeave()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            procSql = "ProcRptEarnedLeave";
            rptURL = "~\\ReportFiles\\RptAbsent1.rpt";
            rptName = "Earned Leave";

            var dic = new Dictionary<string, string>
            {
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            Session["rpt"] = rpt;
            OpenGnReport();
        }

        private void ViewList()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            hidShift.Value = "N";//Night Shift
            if (chkday.Checked) { hidShift.Value = "D"; }//Day Shift
            if (chkAll.Checked) { hidShift.Value = "A"; }//All Shift 

            procSql = "ProcRptAbsentlist";
            rptURL = "~\\ReportFiles\\RptAbsent1.rpt";
            rptName = "Absent List";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",ddJoblocation.SelectedIndex == 0? "%": ddJoblocation.SelectedValue + "X%"},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"shiftId", hidShift.Value},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            Session["rpt"] = rpt;
            OpenGnReport();
        }

        private void ViewTiffinTimeAttendance()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            string sMode = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            procSql = "ProcRptDailyAttendanceForTiffinTime";
            rptURL = "~\\ReportFiles\\RptDailyAttendanceForTiffinTime.rpt";
            rptName = "Tiffin Time Attendance List";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",SessionCompanyId},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"shiftId", "%"},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"status", "%"},
                {"trActual", "Y"},
                {"GroupByCollum", ddGroupByCollum.Text.Trim()}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            rpt.SetParameterValue("Period", "For Date : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy"));
            Session["rpt"] = rpt;
            OpenGnReport();
        }

        private void ViewArrivalList()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            string sMode = string.Empty;
            string s_trMode = Convert.ToString(Session[GlobalVariables.g_s_WinMode]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            procSql = "ProcRptDailyAttendance";
            rptURL = "~\\ReportFiles\\RptDailyAttendance.rpt";
            rptName = "Daily Arrival List";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",SessionCompanyId},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"shiftId", "%"},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"status", "%"},
                {"trActual", s_trMode}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            rpt.SetParameterValue("Period", "Report Period : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy"));
            Session["rpt"] = rpt;
            OpenGnReport();
        }
        private void ViewArrivalListReport()
        {
            hidShift.Value = "N";//Night
            if (chkday.Checked) { hidShift.Value = "D"; }//Day  
            if (chkAll.Checked) { hidShift.Value = "A"; }//All Shift  

            Session[GlobalVariables.g_s_rptShift] = hidShift.Value;
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "%";

            Session[GlobalVariables.g_s_Period] = "( Night Shift ) For Period : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy") + " to " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");

            hidShift.Value = "N";//Night
            if (chkday.Checked)
            {
                hidShift.Value = "D";
                Session[GlobalVariables.g_s_Period] = "( Day/General/Staff Shift ) For Period : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy") + " to " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            }//Day  

            if (chkAll.Checked)
            {
                hidShift.Value = "A";
                Session[GlobalVariables.g_s_Period] = "( All Shift ) For Period : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy") + " to " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            }//Day  

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewArrivalList.aspx');", true);
        }
        private void ViewDailyAttendance()
        {
            hidShift.Value = "N";//Night Shift
            if (chkday.Checked) { hidShift.Value = "D"; }//Day Shift
            if (chkAll.Checked) { hidShift.Value = "A"; }//All Shift 

            Session[GlobalVariables.g_s_rptShift] = hidShift.Value;
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "ANA";
            //Session[GlobalVariables.g_s_Period] = "For the Period of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy") + " -- " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            //Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();

            Session[GlobalVariables.g_s_Period] = "( ALL Shift ) For Date : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            if (hidShift.Value == "N")
            {
                Session[GlobalVariables.g_s_Period] = "( Night Shift ) For Date : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            }
            if (hidShift.Value == "D")
            {
                Session[GlobalVariables.g_s_Period] = "( Day/General/Staff Shift ) For Date : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            }


            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewDailyAttendance.aspx');", true);
        }
        private void ViewMonthlyAbsenteeism()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "ANA";
            Session[GlobalVariables.g_s_Period] = "For the Period of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy") + " -- " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, dd yyyy");
            //Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewMonthlyAbsenteeism.aspx');", true);
        }
        private void ViewMonthlyTiffinBill()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "ANA";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, yyyy");
            //Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewMonthlyTiffinBill.aspx');", true);
        }
        private void ViewDailyTiffinBill()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "ANA";
            Session[GlobalVariables.g_s_Period] = "For the Month of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, yyyy");
            //Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewDailyTiffinBill.aspx');", true);
        }
        private void ViewLeavelist()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforLeaveTypeId] = ddLeaveType.SelectedIndex == 0 ? "%" : ddLeaveType.SelectedValue + "X%";

            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "All";
            Session[GlobalVariables.g_s_rptName] = "All Type Leave Report";
            //Session[GlobalVariables.g_s_Period] = "For the Month of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, yyyy");
            Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewDailyLeaveList.aspx');", true);
        }
        private void ViewShortLeavelist()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforLeaveTypeId] = ddLeaveType.SelectedIndex == 0 ? "%" : ddLeaveType.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "Short";
            Session[GlobalVariables.g_s_rptName] = "Short Leave Report";

            //Session[GlobalVariables.g_s_Period] = "For the Month of " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMM, yyyy");
            Session[GlobalVariables.g_s_Period] = "For the Period of " + Session[GlobalVariables.g_s_rptfromDate].ToString() + " to " + Session[GlobalVariables.g_s_rpttoDate].ToString();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewDailyLeaveList.aspx');", true);
        }

        private void ViewAttendanceSummaryWebReport()
        {
            Session[GlobalVariables.g_s_rptfromDate] = Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rpttoDate] = Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy");
            Session[GlobalVariables.g_s_rptforDeparmentId] = ddDepartment.SelectedIndex == 0 ? "%" : ddDepartment.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforJobLocationId] = ddJoblocation.SelectedIndex == 0 ? "%" : ddJoblocation.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptforEmpId] = ddEmployee.SelectedIndex == 0 ? "%" : ddEmployee.SelectedValue + "X%";
            Session[GlobalVariables.g_s_rptStatus] = "%";
            Session[GlobalVariables.g_s_Period] = "For Month : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMMM-yyyy");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "window.open('../../RptViewer/ViewMonthlyAttendanceSummary.aspx');", true);
        }
        private void ViewAttendanceSummary()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            string sMode = string.Empty;
            int sdaysofMonth = 0;
            int iMonth = 0;
            int iYear = 0;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);
            string SessionUserlevel = Session[GlobalVariables.g_s_userLevel].ToString();
            iYear = Convert.ToInt32(Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("yyyy"));
            iMonth = Convert.ToInt32(Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM"));
            sdaysofMonth = System.DateTime.DaysInMonth(iYear, iMonth);

            procSql = "ProcRptAttendanceSummary";
            if (sdaysofMonth == 28)
            {
                rptURL = "~\\ReportFiles\\RptAttendanceSummary28.rpt";
            }
            else if (sdaysofMonth == 29)
            {
                rptURL = "~\\ReportFiles\\RptAttendanceSummary29.rpt";
            }
            else if (sdaysofMonth == 30)
            {
                rptURL = "~\\ReportFiles\\RptAttendanceSummary30.rpt";
            }
            else if (sdaysofMonth == 31)
            {
                rptURL = "~\\ReportFiles\\RptAttendanceSummary31.rpt";
            }
            rptName = "Monthly Attendance Summary";
            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",SessionCompanyId},
                {"UserLevel",SessionUserlevel},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"month", iMonth.ToString()},
                {"year", iYear.ToString()},
                {"daysInMonth", sdaysofMonth.ToString()}
            };
            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            rpt.SetParameterValue("Period", "For Month : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MMMM-yyyy"));
            Session["rpt"] = rpt;
            OpenGnReport();
        }



        private void ViewAttendanceLogList()
        {
            CommonFunctions fn = new CommonFunctions();
            string SessionCompanyId = string.Empty;
            string SessionCompanyName = string.Empty;
            string SessionCompanyAddress = string.Empty;
            string strQry = string.Empty;
            string rptURL = string.Empty;
            string rptName = string.Empty;
            string procSql = string.Empty;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionCompanyName = Convert.ToString(Session[GlobalVariables.g_s_companyName]);
            SessionCompanyAddress = Convert.ToString(Session[GlobalVariables.g_s_Address]);

            procSql = "ProcRptAttendanceLog";
            if (ddEmployee.SelectedValue == "0")
            {
                rptURL = "~\\ReportFiles\\RptAttendanceLogDateWise.rpt";
            }
            else
            {
                rptURL = "~\\ReportFiles\\RptAttendanceLogEmployeeWise.rpt";
            }
            rptName = "Attendance Log List";

            var dic = new Dictionary<string, string>
            {
                {"companyAutoId",SessionCompanyId},
                {"employeeId", ddEmployee.SelectedIndex == 0? "%": ddEmployee.SelectedValue + "X%"},
                {"sectionId", ddSection.SelectedIndex == 0? "%": ddSection.SelectedValue + "X%"},
                {"LineId", ddLine.SelectedIndex == 0? "%": ddLine.SelectedValue + "X%"},
                {"departmentId", ddDepartment.SelectedIndex == 0? "%": ddDepartment.SelectedValue + "X%"},
                {"fromDate", Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")},
                {"toDate", Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("MM/dd/yyyy")}
            };

            var rptData = fn.GetDataSetFromSp(procSql, dic);
            var rpt = new ReportDocument();
            rpt.Load(Server.MapPath(rptURL));
            rpt.SetDataSource(rptData);

            rpt.SetParameterValue("CompanyName", SessionCompanyName);
            rpt.SetParameterValue("AddressInfo", SessionCompanyAddress);
            rpt.SetParameterValue("Period", "Report Period : " + Convert.ToDateTime(txtFromDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(txtToDate.Text.Trim(), new CultureInfo("fr-FR")).ToString("dd/MM/yyyy"));

            Session["rpt"] = rpt;
            OpenGnReport();
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            //if (dd_Category.SelectedValue == "0")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Item Type Can Not Blank!";
            //    return false;
            //}
            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            ddDepartment.SelectedValue = "0";
        }
    }
}