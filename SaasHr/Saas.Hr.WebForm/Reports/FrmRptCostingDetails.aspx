﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true" CodeBehind="FrmRptCostingDetails.aspx.cs" Inherits="Saas.Hr.WebForm.Reports.FrmRptCostingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Costing Sheet
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>

        <div>
            <div class="row row-custom">
                <div class="col-sm-12">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <asp:Label ID="Label2" runat="server" Text="Costing NO :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:DropDownList ID="dd_Category" CssClass="form-control input-sm chosen-select" runat="server"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="lbl_issue_Date" runat="server" Text="Date :" CssClass="col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-4">
                                <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                                <asp:TextBox runat="server" ID="txtToDate" CssClass="input-sm date-picker" />
                                <asp:CheckBox ID="chkDateUse" runat="server" Text="" />
                            </div>
                        </div>

                        <div class="form-group hidden" style="margin-bottom: 50px;">
                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:CheckBox ID="chkPOList" runat="server" Checked="false" Text=" Purchase Order List" CssClass="text-default" />
                            </div>
                        </div>

                        <div class="row row-custom">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="text-danger">
                                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="PreView" OnClick="btn_save_Click" Width="80px" />
                                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />

                                <asp:CheckBox ID="chkpdf" runat="server" Checked="true" Text=" View to PDF" CssClass="text-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
        <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
        <asp:HiddenField ID="hid_autoId" runat="server" Value="True" />
        <asp:HiddenField ID="hidPOId" runat="server" Value="" />
        <asp:HiddenField ID="hidFormDate" runat="server" Value="" />
        <asp:HiddenField ID="hidToDate" runat="server" Value="" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>