﻿using Saas.Hr.WebForm.EntityFrameworkData.Menu;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Saas.Hr.WebForm.Common
{
    public class MenuGenerator
    {
        private readonly IList<ScrModuleFormForRole> _scrModuleFormForRoles;
        private readonly IList<SpGetModuleByUserRole_Result> _scrModuleInfos;

        public MenuGenerator(int userRoleId)
        {
            MenuEntities context;
            using (context = new MenuEntities())
            {
                _scrModuleInfos = context.SpGetModuleByUserRole(userRoleId).ToList();
                _scrModuleFormForRoles = context
                    .ScrModuleFormForRoles
                    .Include(m => m.ScrFormInfo)
                    .Where(m => m.ScrRoleInfoId == userRoleId)
                    .OrderBy(f => f.ScrFormInfo.Priority)
                    .ToList();
            }
        }

        public string GetMenu(string basePath)
        {
            var stringBuilder = new StringBuilder();

            //For navigation Dashboard
            stringBuilder.Append("<li class=\"\">");
            stringBuilder.Append($"<a href=\"{basePath + "Default.aspx"}\">");
            stringBuilder.Append("<i class=\"menu-icon fa fa-tachometer\"></i> ");
            stringBuilder.Append("Dashboard");
            stringBuilder.Append("</a>");
            stringBuilder.Append("<b class=\"arrow\"></b>");
            stringBuilder.Append("</li>");

            LoadModule(stringBuilder, basePath);
            return stringBuilder.ToString();
        }

        private void LoadModule(StringBuilder stringBuilder, string basePath, int? parentModuleId = null)
        {
            var modules = _scrModuleInfos
                .Where(m => m.ParentModuleInfoId == parentModuleId)
                .Distinct().ToList();

            foreach (var module in modules)
            {
                stringBuilder.Append("<li class=\"dropdown-li\">");
                stringBuilder.Append("<a href=\"#\" class=\"dropdown-toggle\">");
                stringBuilder.Append($"<i class=\"{module.ModuleIcon}\"></i>");
                stringBuilder.Append($"<span class=\"menu-text\">{module.ModuleName}</span>");
                stringBuilder.Append("<b class=\"arrow fa fa-angle-down\"></b>");
                stringBuilder.Append("</a>");
                stringBuilder.Append("<ul class=\"submenu\">");
                //sub module call
                if (_scrModuleInfos.Any(m => m.ParentModuleInfoId == module.ScrModuleInfoId))
                {
                    LoadModule(stringBuilder, basePath, module.ScrModuleInfoId);
                }
                //menu call
                LoadMenuItem(stringBuilder, basePath, module.ScrModuleInfoId);
                stringBuilder.Append("</ul>");
                stringBuilder.Append("</li>");
            }
        }

        private void LoadMenuItem(StringBuilder stringBuilder, string basePath, int moduleId)
        {
            var formInfos = _scrModuleFormForRoles.Where(m => m.ScrFormInfo.ModuleId == moduleId && m.ScrFormInfoId != null)
                .Select(m => m.ScrFormInfo).ToList();
            foreach (var formInfo in formInfos)
            {
                stringBuilder.Append("<li class=\"menu-item\">");
                stringBuilder.Append($"<a href=\"{basePath + formInfo.FormPath + "/" + formInfo.FormName}\">");
                stringBuilder.Append($"<i class=\"{formInfo.FormIcon}\"></i> ");
                stringBuilder.Append(formInfo.FormDisplayName);
                stringBuilder.Append("</a>");
                stringBuilder.Append("<b class=\"arrow\"></b>");
                stringBuilder.Append("</li>");
            }
        }
    }
}