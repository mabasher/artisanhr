﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace Saas.Hr.WebForm.Common
{
    public static class MailSender
    {
        public static void SendMail(MailSendingOption mailSendingOption)
        {
            var senderEmail = ConfigurationManager.AppSettings.Get("EmailAddress");

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SmtpClient"))
            {
                Port = int.Parse(ConfigurationManager.AppSettings.Get("SmtpPortNo")),
                Credentials = new NetworkCredential(senderEmail,
                    ConfigurationManager.AppSettings.Get("EmailPassword")),
                EnableSsl = bool.Parse(ConfigurationManager.AppSettings.Get("EnableEmailSSL"))
            };
            smtpClient.Send(GetMailMessage(mailSendingOption, senderEmail));
        }

        public static void SendMultipleMail(List<MailSendingOption> mailSendingOptions)
        {
            var senderEmail = ConfigurationManager.AppSettings.Get("EmailAddress");

            var smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SmtpClient"))
            {
                Port = int.Parse(ConfigurationManager.AppSettings.Get("SmtpPortNo")),
                Credentials = new NetworkCredential(senderEmail,
                    ConfigurationManager.AppSettings.Get("EmailPassword")),
                EnableSsl = bool.Parse(ConfigurationManager.AppSettings.Get("EnableEmailSSL"))
            };
            foreach (var mailSendingOption in mailSendingOptions)
            {
                smtpClient.Send(GetMailMessage(mailSendingOption, senderEmail));
            }
        }

        private static MailMessage GetMailMessage(MailSendingOption mailSendingOption, string senderEmail)
        {
            var mailMsg = new MailMessage();
            foreach (var receiver in mailSendingOption.Receivers)
            {
                mailMsg.To.Add(receiver);
            }

            if (mailSendingOption.CcReceivers != null)
            {
                foreach (var ccReceiver in mailSendingOption.CcReceivers)
                {
                    mailMsg.CC.Add(ccReceiver);
                }
            }

            mailMsg.From = new MailAddress(senderEmail, mailSendingOption.SenderDisplayName);

            mailMsg.Subject = mailSendingOption.Subject;
            mailMsg.IsBodyHtml = mailSendingOption.IsbodyHtml;
            mailMsg.Body = mailSendingOption.MessageBody;

            if (mailSendingOption.AttachmentLinks == null) return mailMsg;
            foreach (var attachmentLink in mailSendingOption.AttachmentLinks)
            {
                mailMsg.Attachments.Add(new Attachment(attachmentLink));
            }

            return mailMsg;
        }
    }
}