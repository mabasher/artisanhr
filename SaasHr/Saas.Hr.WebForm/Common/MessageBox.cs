﻿using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Common
{
    /// <summary>
    /// Summary description for MessagebOX
    /// </summary>
    public class MessageBox
    {
        public MessageBox()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void Show(string s_message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language='javascript'>");
            sb.Append(@"alert( """ + s_message + @""" );");
            sb.Append(@"</script>");
            HttpContext.Current.Response.Write(sb);
        }

        public static void ShowMessageInLabel(string message, Label label, MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.ErrorMessage:
                    label.ForeColor = Color.Red;
                    break;

                case MessageType.WarningMessage:
                    label.ForeColor = Color.DarkGoldenrod;
                    break;

                default:
                    label.ForeColor = Color.Green;
                    break;
            }

            label.Text = message;
        }
    }
}