﻿using System.Collections.Generic;

namespace Saas.Hr.WebForm.Common
{
    public class GlobalVariables
    {
        public const string BaseCompanyName = "Artisan Business Network Bangladesh";
        public const string BaseCompanyWebAddress = "https://artisan-biz.com/";
        //public const string BaseCompanyName = "STM Software Ltd";
        //public const string BaseCompanyWebAddress = "http://www.stmsoftwareltd.com/";

        //Global Variables for Validation Type
        public const string g_s_MustFieldValdation = "_MFVTxt";

        //public const string g_s_MustFieldValdation = "_MFVDd";
        public const string g_s_DecimalValdation = "_DV";

        public const string g_s_DecimalValdation_AllowMinus = "_DV_AM";
        public const string g_s_IntegerValdation = "_IV";
        public const string g_s_IntegerValdation_AllowMinus = "_IV_AM";
        public const string g_s_DateValidation = "_DateV";
        //public const string g_s_validationErrorMessage = "Error in Control ";

        //browser name declaration
        public const string g_s_Browser_InternetExplorer = "IE";

        public const string g_s_Browser_MozillaFirefox = "Firefox";
        public const string g_s_Browser_AppleMac_Safari = "AppleMac-Safari";
        public const string g_s_Browser_Opera = "Opera";
        public const string g_s_ConnectionStringName = "SaasHrConnection";

        public const string g_s_procedureDuplicateReturnValue = "D";
        //public const string g_s_procedureReturnType = "D";

        public const string numericValidation = @"^(-)?\d+(\.\d\d)?$";
        public const string g_s_stringToReplace = "'";
        public const string g_s_stringByReplace = "''''";
        public const string g_s_stringByReplaceFrontEnd = "''";

        public const int g_s_referenceId = 0;

        public const string g_s_serverValidationErrorControlId = "g_s_serverValidationErrorControlId";
        //This will Be permanent
        //public const string g_s_companyAutoId = string.Empty;

        //For Test Purpose
        //public const string g_s_CompanyAutoId = "6";

        public const string g_s_No = "N";
        public const string g_s_Yes = "Y";
        public const string g_s_exported = "N";
        //public const string g_s_companyName = "STM Software Ltd.";

        //public const string g_s_costCenterAutoId = "0";
        //public const string g_s_branchAutoId = "0";

        //Voucher Entry Form

        public const string g_s_updateAlertKey = "onclick";
        public const string g_s_updateAlertValue = "return showConfirmDialog(this.id);";

        public const string g_s_insertOperationSuccessfull = "Successfully Inserted";
        public const string g_s_insertSendSuccessfull = "Success!!";
        public const string g_s_insertOperationFailed = "Insertion Failed";
        public const string g_s_updateOperationFailed = "Update Failed";
        public const string g_s_updateOperationSuccessfull = "Successfully Updated";
        public const string g_s_deleteOperationFailed = "Data Already Exists in ";
        public const string g_s_deleteOperationSuccessfull = "Successfully Deleted";
        public const string g_s_duplicateCheckWarningMessage = "Data Already Exist";

        //public const string g_s_duplicateCheckWarningMessage_U = "D";
        //public const string g_s_duplicate = "data already exists";
        public const string g_s_validationErrorMessage = "Error in Control ";

        public const string g_s_encryptionKey = "~@#$^%@!";

        //public const string g_s_encryptionKey = "~@#$^%@!";
        //public const string g_s_encryptionKey = "~@#$^%@!";
        public const string g_s_performanceTypeSupervisor = "S";

        public const string g_s_performanceTypeEmployee = "E";

        public const string g_s_hrEamilAddress = "testhr99@yahoo.com";
        public const string g_s_fromEamilAddress = "hr.bd@mondialorg.com";

        //connection Class Return Value
        //public const string g_s_connectionErrorReturnValue = "Error";
        // grid style
        //public const string g_s_onmouseover = "onmouseover";
        //public const string g_s_style_onmouseover = "this.style.cursor='hand';this.style.color='Fuchsia';";
        //public const string g_s_onmouseout = "this.style.textdecoration='none'";
        public const string g_s_style_onmouseout = "this.style.textdecoration='none';this.style.color='Black';";

        public const string g_s_style_onmouseover = "this.style.cursor='hand';this.style.cursor='pointer';this.style.color='DarkGreen';";

        //for security implementation
        public const string g_s_insertSecurityMessage = "You are not permitted to save";

        public const string g_s_updateSecurityMessage = "You are not permitted to update";
        public const string g_s_deleteSecurityMessage = "You are not permitted to delete";

        //catch sql error
        public const string g_s_connectionErrorReturnValue = "Error";

        //report footer signature formula field
        public const string g_s_signature1 = "Receivers Signature:";

        public const string g_s_signature2 = "Authorised Signatory:";
        public const string g_s_signature3 = "Chairman Signature";
        public const string g_s_signature4 = "Director Signature";
        public const string g_s_signature5 = "Accountant Signature";

        //attribute ratings check
        public const string g_s_attributeRatingCheck = "No need to insert attribute";

        //validation expression
        public const string g_vldExp_positiveIntrgerNotZero = @"^[1-9]([0-9]+)?";

        public const string g_vldExp_decimalValue = @"^(-)?\d+(\.\d\d)?$";

        //login pageURL
        //public const string g_s_URL_loginPage = "~/HumanResource/Common/frm_login.aspx";
        public const string g_s_URL_loginPage = "~/Security/FrmSignIn.aspx";

        public const string g_s_URL_homePage = "~/Default.aspx";

        //public const string g_s_URL_homePage = "~/HumanResource/LeaveManagement/frm_empLeaveApplication.aspx";
        public const string g_s_URL_permissionDenied = "~/Common/frm_permissionDenied.aspx";

        //public const string g_s_URL_permissionDenied = "~/HumanResource/Common/frm_permissionDenied.aspx";
        public const string g_s_URL_reportViewer = "~/Common/ReportViewer/frm_reportViewer_gp.aspx";

        //public const string g_s_URL_reportViewer = "~/HumanResource/ReportViewer/frm_reportViewer_gp.aspx";
        public const string g_s_URL_reportViewerWithSubreport = "~/Common/ReportViewer/frm_reportViewerWithSubReport_gp.aspx";

        //public const string g_s_URL_reportViewer = "~/HumanResource/Report/HumanResource/frm_reportViewer_gp.aspx";
        //public const string g_s_URL_reportViewerWithSubreport = "~/HumanResource/Report/ReportViewer/frm_reportViewerWithSubReport_gp.aspx";

        //To Replace for Session
        //common Variables for ISUD string format
        public const string g_s_TableName = "g_s_TableName";

        public const string g_s_Parameter = "g_s_Parameter";
        public const string g_s_ColumnName = "g_s_ColumnName";
        public const string g_s_DuplicateChecking = "g_s_DuplicateChecking";
        public const string g_s_deletedColumnName = "g_s_deletedColumnName";
        public const string g_s_deleteValue = "g_s_deleteValue";
        public const string g_s_procedureReturnType = "g_s_procedureReturnType";

        public const string g_s_ModuleAutoId = "g_s_ModuleAutoId";
        public const string g_s_financialYearFrom = "financialYearFrom";

        public const string g_s_insertMode = "g_s_insertMode";//check for insert or update mode
        public const string g_s_autoId = "g_s_autoId";//station 1 database identification
        public const string Addmode = "Addmode";//station 1 database identification

        //trace user
        public const string g_s_userSupervisorAutoId = "g_s_userSupervisorAutoId";
        public const string g_s_userAutoId = "g_s_userAutoId";
        public const string g_s_userMode = "g_s_userMode";// Employee User=Gn,Admin User=A
        public const string g_s_WinMode = "g_s_WinMode";
        public const string g_s_DummyMode = "g_s_DummyMode";

        public const string g_s_userPassword = "g_s_userPassword";
        public const string g_s_userName = "g_s_userName";
        public const string g_s_userPresentEmail = "g_s_userPresentEmail";
        public const string g_s_userEmergencyEmail = "g_s_userEmergencyEmail";
        public const string g_s_userStatus = "g_s_userStatus";
        public const string g_s_userEmail = "g_s_userEmail";
        public const string g_s_userLevel = "g_s_userLevel";
        public const string g_s_userMenu = "g_s_userMenu";
        public const string g_s_userPIC = "g_s_userPIC";
        public const string g_s_userIP = "g_s_userIP";
        public const string g_s_printopt = "g_s_printopt";
        public const string g_s_rptQry = "g_s_rptQry";

        public const string g_s_userLogBookAutoId = "g_s_userLogBookAutoId";


        //For HTML Rpt Parameter
        public const string g_s_rptforSLNo = "g_s_rptforSLNo";
        public const string g_s_rptforAutoId = "g_s_rptforAutoId";
        public const string g_s_rptforDepotId = "g_s_rptforDepotId";
        public const string g_s_rptforDeparmentId = "g_s_rptforDeparmentId";
        public const string g_s_rptforSectionId = "g_s_rptforSectionId";
        public const string g_s_rptforJobLocationId = "g_s_rptforJobLocationId";
        public const string g_s_rptforLeaveTypeId = "g_s_rptforLeaveTypeId";
        public const string g_s_rptforEmpId = "g_s_rptforEmpId";
        public const string g_s_rptforMonth = "g_s_rptforMonth";
        public const string g_s_rptforYear = "g_s_rptforYear";
        public const string g_s_rptfromDate = "g_s_rptfromDate";
        public const string g_s_rptSalaryDate = "g_s_rptSalaryDate";
        public const string g_s_rptShift = "g_s_rptShift";
        public const string g_s_rpttoDate = "g_s_rpttoDate";
        public const string g_s_rptName = "g_s_rptName";
        public const string g_s_Period = "g_s_Period";
        public const string g_s_rptStatus = "g_s_rptStatus";
        public const string g_s_rptTpe = "g_s_rptTpe";
        //For HTML Rpt Parameter


        //Company Information
        public const string g_s_companyDefaultCurrencyAutoId = "g_s_companyDefaultCurrencyAutoId";

        public const string g_s_companyDefaultCurrencySymbol = "g_s_companyDefaultCurrencySymbol";

        //new update
        public const string g_s_CompanyAutoId = "g_s_CompanyAutoId";

        public const string g_s_GroupName = "g_s_GroupName";
        public const string g_s_companyName = "g_s_companyName";
        public const string g_s_Address = "g_s_Address";
        public const string g_s_City = "g_s_City";
        public const string g_s_Zip = "g_s_Zip";
        public const string g_s_Country = "g_s_Country";
        public const string g_s_Phone = "g_s_Phone";
        public const string g_s_Fax = "g_s_Fax";
        public const string g_s_Email = "g_s_Email";
        public const string g_s_Code = "g_s_Code";
        public const string g_s_Web = "g_s_Web";
        //end company New update

        //Month Year
        public static Dictionary<int, string> Months = new Dictionary<int, string>()
                                     {
                                        {
                                            1, "January"
                                        },
                                        {
                                            2, "February"
                                        },
                                        {
                                            3, "March"
                                        },
                                        {
                                            4, "April"
                                        },
                                        {
                                            5, "May"
                                        },
                                        {
                                            6, "June"
                                        },
                                        {
                                            7, "July"
                                        },
                                        {
                                            8, "August"
                                        },
                                        {
                                            9, "September"
                                        },
                                        {
                                            10, "October"
                                        },
                                        {
                                            11, "November"
                                        },
                                        {
                                            12, "Decemeber"
                                        }
                                    };
    }
}