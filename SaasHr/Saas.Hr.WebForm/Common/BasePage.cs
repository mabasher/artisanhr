﻿using System.Web.UI;

namespace Saas.Hr.WebForm.Common
{
    /// <summary>
    /// Summary description for BasePage
    /// </summary>
    public class BasePage : Page
    {
        public BasePage()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected string GetReportPath()
        {
            return Page.ResolveUrl("~/RptViewer/FrmRptViewer.aspx");
        }

        protected string GetGnReportPath()
        {
            return Page.ResolveUrl("~/RptViewer/FrmRptViewerGn.aspx");
        }

        protected string GetSubReportPath()
        {
            return Page.ResolveUrl("~/RptViewer/FrmRptViewerGn.aspx");
        }

        protected void RegisterNewWindowOpen(string functionName, string pagePath)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), functionName, "window.open('" + pagePath + "');", true);
        }

        protected void OpenReport()
        {
            RegisterNewWindowOpen("MyFunction", GetReportPath());
        }

        protected void OpenSubReport()
        {
            RegisterNewWindowOpen("MyFunction", GetSubReportPath());
        }

        protected void OpenGnReport()
        {
            RegisterNewWindowOpen("MyFunction", GetSubReportPath());
        }
    }
}