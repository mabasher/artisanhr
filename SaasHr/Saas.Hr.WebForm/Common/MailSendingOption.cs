﻿using System.Collections.Generic;

namespace Saas.Hr.WebForm.Common
{
    public class MailSendingOption
    {
        public MailSendingOption(List<string> receivers, List<string> ccReceivers, string senderDisplayName, string subject, string messageBody, List<string> attachmentLinks, bool isbodyHtml = true)
        {
            Receivers = receivers;
            CcReceivers = ccReceivers;
            SenderDisplayName = senderDisplayName;
            Subject = subject;
            MessageBody = messageBody;
            AttachmentLinks = attachmentLinks;
            IsbodyHtml = isbodyHtml;
        }

        public List<string> Receivers { get; private set; }
        public List<string> CcReceivers { get; private set; }
        public string SenderDisplayName { get; private set; }
        public string Subject { get; private set; }
        public string MessageBody { get; private set; }
        public List<string> AttachmentLinks { get; private set; }
        public bool IsbodyHtml { get; private set; }
    }
}