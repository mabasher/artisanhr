﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace Saas.Hr.WebForm.Common
{
    /// <summary>
    /// Summary description for DML
    /// </summary>
    public class DML
    {
        public DML()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public Boolean g_b_fillDropDownListByControlar(DropDownList dd_dropDownListName, string query)
        {
            try
            {
                Connection connection = new Connection()
                {
                    ResultsDataSet = null,
                    connection = null
                };
                string returnValue = string.Empty;
                returnValue = connection.connection_DB(query, 0, false, false, false);
                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    DataTable dataTable = connection.ResultsDataSet.Tables[0];
                    dd_dropDownListName.Items.Clear();
                    ListItem listItemBlank = new ListItem();
                    listItemBlank.Text = "# Select #";
                    listItemBlank.Value = "0";
                    dd_dropDownListName.Items.Add(listItemBlank);
                    foreach (DataRow drow in dataTable.Rows)
                    {
                        ListItem li = new ListItem();
                        li.Text = drow["ItemNO"].ToString();
                        li.Value = drow["ItemId"].ToString();
                        dd_dropDownListName.Items.Add(li);
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
            return false;
        }

        public Boolean g_b_funcDMLGeneralOneToManyInsert(string queryA, string queryB, string queryD, string queryC, string statusA, string statusB, string statusC)
        {
            try
            {
                Connection connection = new Connection()
                {
                    ResultsDataSet = null,
                    connection = null
                };
                string returnValue = string.Empty;
                string s_save_ = string.Empty;

                if (statusA == "A")
                {
                    returnValue = connection.connection_DB(queryA, 1, true, true, false);
                    if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                    {
                        if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                        {
                            if (statusB == "B")
                            {
                                returnValue = connection.connection_DB(queryB, 1, true, false, false);
                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                {
                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                    {
                                        returnValue = connection.connection_DB(queryD, 1, true, false, false);
                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                        {
                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                            {
                                                returnValue = connection.connection_DB(queryC, 1, true, false, true);
                                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                {
                                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                    {
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //ex.Message;
            }

            return false;
        }
        public Boolean g_b_funcDMLRealizationInsert(string queryA, string queryB, string queryD, string queryE, string queryF, string queryC, string statusA, string statusB, string statusC)
        {
            try
            {
                Connection connection = new Connection()
                {
                    ResultsDataSet = null,
                    connection = null
                };
                string returnValue = string.Empty;
                string s_save_ = string.Empty;

                if (statusA == "A")
                {
                    returnValue = connection.connection_DB(queryA, 1, true, true, false);
                    if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                    {
                        if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                        {
                            if (statusB == "B")
                            {
                                returnValue = connection.connection_DB(queryB, 1, true, false, false);
                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                {
                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                    {
                                        returnValue = connection.connection_DB(queryD, 1, true, false, false);
                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                        {
                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                            {
                                                returnValue = connection.connection_DB(queryE, 1, true, false, false);
                                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                {
                                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                    {
                                                        returnValue = connection.connection_DB(queryF, 1, true, false, false);
                                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                        {
                                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                            {
                                                                returnValue = connection.connection_DB(queryC, 1, true, false, true);
                                                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                                {
                                                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                                    {
                                                                        return true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //ex.Message;
            }

            return false;
        }

        public Boolean g_b_funcDMLCostingInsert(string queryA, string queryB, string queryD, string queryE, string queryC, string statusA, string statusB, string statusC)
        {
            try
            {
                Connection connection = new Connection()
                {
                    ResultsDataSet = null,
                    connection = null
                };
                string returnValue = string.Empty;
                string s_save_ = string.Empty;

                if (statusA == "A")
                {
                    returnValue = connection.connection_DB(queryA, 1, true, true, false);
                    if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                    {
                        if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                        {
                            if (statusB == "B")
                            {
                                returnValue = connection.connection_DB(queryB, 1, true, false, false);
                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                {
                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                    {
                                        returnValue = connection.connection_DB(queryD, 1, true, false, false);
                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                        {
                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                            {
                                                returnValue = connection.connection_DB(queryE, 1, true, false, false);
                                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                {
                                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                    {
                                                        returnValue = connection.connection_DB(queryC, 1, true, false, true);
                                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                        {
                                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                            {
                                                                return true;
                                                            }
                                                        }                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //ex.Message;
            }

            return false;
        }

        public Boolean g_b_funcDMLPOInsert(string queryA, string queryB, string queryD, string queryE, string queryF, string queryC, string statusA, string statusB, string statusC)
        {
            try
            {
                Connection connection = new Connection()
                {
                    ResultsDataSet = null,
                    connection = null
                };
                string returnValue = string.Empty;
                string s_save_ = string.Empty;

                if (statusA == "A")
                {
                    returnValue = connection.connection_DB(queryA, 1, true, true, false);
                    if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                    {
                        if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                        {
                            if (statusB == "B")
                            {
                                returnValue = connection.connection_DB(queryB, 1, true, false, false);
                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                {
                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                    {
                                        returnValue = connection.connection_DB(queryD, 1, true, false, false);
                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                        {
                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                            {
                                                returnValue = connection.connection_DB(queryE, 1, true, false, false);
                                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                {
                                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                    {
                                                        returnValue = connection.connection_DB(queryF, 1, true, false, false);
                                                        if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                        {
                                                            if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                            {
                                                                returnValue = connection.connection_DB(queryC, 1, true, false, true);
                                                                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                                                                {
                                                                    if (returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                                                                    {
                                                                        return true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //ex.Message;
            }

            return false;
        }

        public DataTable getDealerData(string dealer_Id)
        {
            string returnValue = string.Empty;
            string strValue = string.Empty;
            string query = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                Connection connection = new Connection()
                {
                    ResultsDataSet = null,
                    connection = null
                };
                query = "Proc_getDealerLedger '" + dealer_Id + "'";
                returnValue = connection.connection_DB(query, 0, false, false, false);
                if (returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    dt = connection.ResultsDataSet.Tables[0];
                    //foreach (DataRow drow in dataTable.Rows)
                    //{
                    //    strValue = drow["ItemNO"].ToString();
                    //}
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
            return dt;
        }
    }
}