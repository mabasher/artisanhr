﻿namespace Saas.Hr.WebForm.Common
{
    public enum MessageType
    {
        SuccessMessage,
        ErrorMessage,
        WarningMessage
    }
}