﻿using System;

namespace Saas.Hr.WebForm.Common
{
    public static class StringGenerator
    {
        public static string TimespanToAmPmFormat(TimeSpan time)
        {
            return (DateTime.Today + time).ToString("hh:mm tt");
        }
    }
}