﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmAccountsOrInvestmentInfo.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmAccountsOrInvestmentInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        Account / Investment Info
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">
        <div class="col-lg-12">



            <div class="form-group" hidden>
                <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                <div class="col-sm-8">
                    <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
            <div class="form-group" hidden>
                <asp:Label ID="lbl_GroupName" runat="server" Text="Employee Id :" CssClass="col-sm-3 control-label"></asp:Label>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
                <div class="col-sm-3 hidden">
                    <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>
            <div class="form-group" hidden>
                <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="col-sm-3 control-label"></asp:Label>
                <div class="col-sm-8">
                    <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                </div>
            </div>


            <fieldset>
                <legend>Loan Info</legend>
                <div class="form-horizontal">


                    <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Account No :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtAccountNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label6" runat="server" Text="Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox runat="server" ID="txtDate" CssClass="input-sm date-picker" />
                        </div>
                        <asp:Label ID="Label11" runat="server" Text="Account Type :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3 ">
                            <asp:DropDownList ID="ddACType" CssClass="form-control input-sm" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label19" runat="server" Text="On A/C Of :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="ddOnAcOf" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" Text="Profit A/C Of :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="ddProfitAcOf" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label9" runat="server" Text="Account Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:TextBox runat="server" ID="txtAccountName" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <asp:Label ID="Label10" runat="server" Text="Description :" CssClass="col-sm-3 "></asp:Label>
                        <div class="col-sm-3 ">
                            <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="row row-custom">
            <div class="col-md-6">
                <fieldset>
                    <legend>Account Details</legend>
                    <div class="form-horizontal">

                        <div class="form-group">
                            <asp:Label ID="Label8" runat="server" Text="Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label3" runat="server" Text="Duration :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-2">
                                <asp:TextBox runat="server" ID="txtDuration" CCssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 ">
                                <asp:DropDownList ID="ddPaymentSystem" CssClass="form-control input-sm" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label5" runat="server" Text="Interest Rate :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtInterestRate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 ">
                                <asp:Label ID="Label12" runat="server" Text="%" CssClass="col-sm-3 control-label"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label13" runat="server" Text="Installment :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="Installment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label14" runat="server" Text="In Every :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtInEvery" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 ">
                                <asp:DropDownList ID="ddEvery" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label15" runat="server" Text="Tax Rate :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtTaxRate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 ">
                                <asp:Label ID="Label16" runat="server" Text="%" CssClass="col-sm-3 control-label"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label17" runat="server" Text="Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtTaxAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label18" runat="server" Text="VAT Rate :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtVATRate" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-sm-3 ">
                                <asp:Label ID="Label20" runat="server" Text="%" CssClass="col-sm-3 control-label"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label21" runat="server" Text="Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtVatAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label22" runat="server" Text="Ins. Start Date :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox runat="server" ID="txtInsStartDate" CssClass="input-sm date-picker" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label23" runat="server" Text="Total Receiveable:" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtTotalReceiveable" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label24" runat="server" Text="Profit Amount :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txtProfitAmount" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label25" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="col-md-5">
                <fieldset>
                    <legend>Loan Schedule</legend>
                    <div class="form-horizontal">

                        <div style="height: 373px; overflow: scroll;">
                            <asp:GridView ID="gdv_costingHead" runat="server" Style="margin-left: 0;"
                                AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                                CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                        <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                        <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="SL">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nominee Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridName" runat="server" Text='<%# Bind("NomineeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridFather" runat="server" Text='<%# Bind("FatherName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMother" runat="server" Text='<%# Bind("MotherName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridContact" runat="server" Text='<%# Bind("PersonalContact") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vat">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMail" runat="server" Text='<%# Bind("PersonalMail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Profit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridShare" runat="server" Text='<%# Bind("Share") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_GridItemAutoId" Value='<%# Bind("AutoId") %>' runat="server" />
                                            <asp:HiddenField ID="hidRelationId" Value='<%# Bind("RelationId") %>' runat="server" />
                                            <asp:HiddenField ID="hidGridAddress" Value='<%# Bind("PermanentAddress") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-sa" />
                                <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                                <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="row row-custom" style="padding-top: 20px;">
            <div class="col-sm-12">
                <div class="form-horizontal">
                    <div class="form-group center">
                        <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-horizontal">
                    <div class="form-group center">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                    </div>
                </div>
            </div>
        </div>

    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidNomineeId" runat="server" Value="0" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">

    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>

</asp:Content>
