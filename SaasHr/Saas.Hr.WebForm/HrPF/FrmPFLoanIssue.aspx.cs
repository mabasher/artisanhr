﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Saas.Hr.WebForm.Common;
using Saas.Hr.WebForm.Enum;
using Saas.Hr.WebForm.Models;

namespace Saas.Hr.WebForm.Hr
{
    public partial class FrmPFLoanIssue : Page
    {
        private string URL = string.Empty;
        private string filename = string.Empty;

        //ttS
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CommonFunctions commonfunctions = new CommonFunctions();
                string SessionUserId = string.Empty;
                string SessionCompanyId = string.Empty;
                SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
                SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

                //commonfunctions.g_b_FillDropDownList(ddGender,
                //    "T_Relation",
                //    "Relation",
                //    "AutoId", string.Empty);

                v_loadGridView_CostingHead();
                DisplayDurationType();
                DisplayInstallmentPeriodType();
            }
        }

        private void v_loadGridView_CostingHead()
        {
            if (hidEmpAutoId.Value == "")
            { hidEmpAutoId.Value = "0"; }
            CommonFunctions commonFunctions = new CommonFunctions();
            SqlConnection sqlconnection = new SqlConnection(commonFunctions.connection_String(ConfigurationManager.ConnectionStrings[GlobalVariables.g_s_ConnectionStringName].ToString()));
            sqlconnection.Open();

            SqlCommand cmd = new SqlCommand("SELECT EmployeeNomineeInfoId as AutoId,NomineeName as NomineeName,ni.FatherName as FatherName,ni.MotherName as MotherName,ni.DOB as DOB,ni.PermanentAddress as PermanentAddress,ni.PersonalContact as PersonalContact,ni.PersonalMail as PersonalMail,ni.NID as NID,r.Relation as Relation,r.AutoId as RelationId ,ni.Share as Share FROM EmployeeNomineeInfo ni left outer join EmployeePersonalInfo ei on ni.EmployeePersonalInfoId=ei.EmployeePersonalInfoId left outer join T_Relation r on ni.RelationId=r.AutoId Where ni.EmployeePersonalInfoId='" + hidEmpAutoId.Value + "'", sqlconnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            // gdvLoanSchedule.DataSource = ds; gdvLoanSchedule.AllowPaging = true;
            // gdvLoanSchedule.PageSize = 8; gdvLoanSchedule.DataBind();
            sqlconnection.Close();

            var loanSchedules = new List<LoanScheduleViewModel>
            {
                new LoanScheduleViewModel
                {
                    LoanId = 1,
                    LoanScheduleId = 1,
                    InterestAmount = 500,
                    PrincipleAmount = 5000,
                    ScheduledPaymentDate = DateTime.Now
                }
            };
            DisplayLoanSchedule(loanSchedules);
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            Connection connection = new Connection();

            string SessionUserId = string.Empty;
            string SessionCompanyId = string.Empty;
            string SessionUserIP = string.Empty;

            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);
            SessionUserIP = Session[GlobalVariables.g_s_userIP].ToString();

            string HR_AutoId = Session[GlobalVariables.g_s_userAutoId].ToString();
            string s_returnValue = string.Empty;
            string s_save_ = string.Empty;
            string s_Update_returnValue = string.Empty;
            string s_Update = string.Empty;

            Boolean b_validationReturn = true;

            b_validationReturn = isValid();

            if (b_validationReturn == false)
            {
                return;
            }

            if (btn_save.Text == "Update")
            {
                var filePath = Server.MapPath("~/UserPic/" + hidURL.Value);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //filename = Path.GetFileName(imgUpload.PostedFile.FileName);
                //filename = txtPIN.Text + "_" + filename;
                //URL = "~/UserPic/" + filename;
                //filename = "";

                s_Update = "[ProcNomineeInformationINSERT]"
                        + "'"
                        + hidNomineeId.Value
                        + "','"
                        + hidEmpAutoId.Value
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtNomineeName.Text.Trim())
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtFather.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtMother.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtDOB.Text.Trim())
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtNationalId.Text.Trim())
                        //+ "','"
                        //+ ddGender.SelectedValue
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtTIN.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','0','','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','0','U'";

                s_Update_returnValue = connection.connection_DB(s_Update, 1, true, true, true);

                if (s_Update_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_Update_returnValue == GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_updateOperationSuccessfull;
                    }
                }
            }
            else
            {
                s_save_ = "[ProcNomineeInformationINSERT]"
                        + "'0','"
                        + hidEmpAutoId.Value
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtNomineeName.Text.Trim())
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtFather.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtMother.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtDOB.Text.Trim())
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtPermanentAddress.Text.Trim())
                        + "','"
                        //+ HttpUtility.HtmlDecode(txtPersonalContact.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtPersonalEmail.Text.Trim())
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtNationalId.Text.Trim())
                        //+ "','"
                        //+ ddGender.SelectedValue
                        //+ "','"
                        //+ HttpUtility.HtmlDecode(txtTIN.Text.Trim())
                        + "','"
                        + SessionCompanyId
                        + "','"
                        + SessionUserId
                        + "','"
                        + SessionUserIP
                        + "','0','','0','0','I'";
                s_returnValue = connection.connection_DB(s_save_, 1, true, true, true);

                if (s_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
                {
                    if (s_returnValue != GlobalVariables.g_s_procedureDuplicateReturnValue)
                    {
                        lblMsg.Text = GlobalVariables.g_s_insertOperationSuccessfull;
                    }
                    else
                    {
                        lblMsg.Text = GlobalVariables.g_s_duplicateCheckWarningMessage;
                    }
                }
                else
                {
                    lblMsg.Text = GlobalVariables.g_s_insertOperationFailed;
                }
            }
        }

        protected void gdv_costingHead_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region gdv_ChildrenInfo_RowDataBound

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = GlobalVariables.g_s_style_onmouseover;
                    e.Row.Attributes["onmouseout"] = GlobalVariables.g_s_style_onmouseout;
                    e.Row.Attributes["onclick"] = ClientScript.GetPostBackEventReference(this.gdvLoanSchedule, "Select$" + e.Row.RowIndex);
                }
            }
            catch (Exception exception)
            {
                //lbl_msg_StaffRequisitionDetails.ForeColor = GlobalVariables.g_clr_errorColor;
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_RowDataBound
        }

        protected void gdv_costingHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region gdv_ChildrenInfo_SelectedIndexChanged

            try
            {
                //hidAutoIdForUpdate.Value = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hid_GridItemAutoId")).Value;
                //txtPIN.Text = ((Label)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("lblGridCategoryName")).Text;
                //txtEmpName.Text = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGridAddress")).Value;
                //ddGender.SelectedValue = ((HiddenField)gdv_costingHead.Rows[gdv_costingHead.SelectedIndex].FindControl("hidGriduserlevel")).Value;

                //btn_save.Text = "Update";
            }
            catch (Exception exception)
            {
                lblMsg.Text = exception.Message;
            }

            #endregion gdv_ChildrenInfo_SelectedIndexChanged
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            InsertMode();
            InsertMode_Msg();
        }

        private void InsertMode_Msg()
        {
            btn_save.Text = "Save";
            lblMsg.Text = string.Empty;
        }

        private Boolean isValid()
        {
            //CommonFunctions commonFunctions = new CommonFunctions();

            if (txtPIN.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "PIN Can Not Blank!";
                return false;
            }

            if (txtEmpName.Text == "")
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Employee Name Can Not Blank!";
                return false;
            }

            //if (txtNomineeName.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Nominee Name Can Not Blank!";
            //    return false;
            //}

            //if (txtPermanentAddress.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Address Can Not Blank!";
            //    return false;
            //}

            //if (txtTIN.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Share Can Not Blank!";
            //    return false;
            //}

            //if (txtPersonalContact.Text == "")
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Contact No Can Not Blank!";
            //    return false;
            //}

            return true;
        }

        private void InsertMode()
        {
            btn_save.Text = "Save";
            txtPIN.Text = string.Empty;
            //txtNomineeName.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtCard.Text = string.Empty;
            //txtPresentAddress.Text = string.Empty;
            //txtPermanentAddress.Text = string.Empty;
            //txtFather.Text = string.Empty;
            //txtMother.Text = string.Empty;
            //txtNationalId.Text = string.Empty;
            //txtPassport.Text = string.Empty;
            //txtBirthCertificate.Text = string.Empty;
            //txtPlaceofBirth.Text = string.Empty;
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (hidEmpAutoId.Value != "" && hidEmpAutoId.Value != "0")
            {
                getEmpData(hidEmpAutoId.Value);
            }
        }

        private void getEmpData(string sEmpId)
        {
            Connection connection = new Connection();
            string s_select = string.Empty;
            string s_Select_returnValue = string.Empty;

            string SessionUserId = string.Empty;
            SessionUserId = Convert.ToString(Session[GlobalVariables.g_s_userAutoId]);
            string SessionCompanyId = string.Empty; ;
            SessionCompanyId = Convert.ToString(Session[GlobalVariables.g_s_CompanyAutoId]);

            s_select = "SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='" + sEmpId + "'";
            s_Select_returnValue = connection.connection_DB(s_select, 0, false, false, false);
            if (s_Select_returnValue != GlobalVariables.g_s_connectionErrorReturnValue)
            {
                if (connection.ResultsDataSet.Tables != null)
                {
                    foreach (DataRow drow in connection.ResultsDataSet.Tables[0].Rows)
                    {
                        txtPIN.Text = drow["PIN"].ToString();
                        txtEmpName.Text = drow["EmployeeName"].ToString();
                    }
                }
            }
            v_loadGridView_CostingHead();
        }

        private void DisplayDurationType()
        {
            ddLoanDurationType.DataSource = System.Enum.GetNames(typeof(EnumDurationType));
            ddLoanDurationType.DataBind();
        }

        private void DisplayInstallmentPeriodType()
        {
            ddInstallmentPeriodType.DataSource = System.Enum.GetNames(typeof(EnumDurationType));
            ddInstallmentPeriodType.DataBind();
        }

        private void DisplayLoanSchedule(List<LoanScheduleViewModel> loanSchedules)
        {
            gdvLoanSchedule.DataSource = loanSchedules;
            gdvLoanSchedule.DataBind();
        }

        private List<LoanScheduleViewModel> GetLoanSchedules(LoanScheduleParamViewModel loanInfo)
        {
            var loanSchedules = new List<LoanScheduleViewModel>();

            for (var i = 0; i < loanInfo.InstallmentPeriod; i++)
            {
                var loanSchedule = new LoanScheduleViewModel();
                loanSchedule.ScheduledPaymentDate =
                    GetLoanScheduleDate(loanInfo.InstallmentStartDate, loanInfo.InstallmentPeriodType, i);
                // loanSchedule.
            }

            return loanSchedules;
        }

        private DateTime GetLoanScheduleDate(DateTime firstInstallmentDate, EnumDurationType installmentDurationType,
            int installmentIndex)
        {
            var installmentDate = firstInstallmentDate;
            switch (installmentDurationType)
            {
                case EnumDurationType.Day:
                    installmentDate = installmentDate.AddDays(installmentIndex);
                    break;

                case EnumDurationType.Week:
                    installmentDate = installmentDate.AddDays(7 * installmentIndex);
                    break;

                case EnumDurationType.Month:
                    installmentDate = installmentDate.AddMonths(installmentIndex);
                    break;

                case EnumDurationType.Year:
                    installmentDate = installmentDate.AddYears(installmentIndex);
                    break;

                default:
                    installmentDate = installmentDate.AddMonths(installmentIndex);
                    break;
            }

            return installmentDate;
        }

        //===========End===============
    }
}