﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmFinalSettlement.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmFinalSettlement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        PF Final Settlement
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">

        <div class="col-md-6">
            <fieldset>
                <legend>Employee Info</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                      
                    </div>
                     <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Employee Name :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label31" runat="server" Text="Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtFromDate" CssClass="form-control input-sm date-picker" />
                        </div>
                    </div>
                  


                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>..</legend>
                <div class="form-horizontal">
                    <div class="form-group">
                          <div class="form-group">
                        <asp:Label ID="Label6" runat="server" Text="Employee Head :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtEmployeeHead" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                        <asp:Label ID="Label5" runat="server" Text="Employer Head :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtEmployerHead" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <asp:Label ID="Label3" runat="server" Text="PF Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtPFDate" CssClass="form-control input-sm date-picker" />
                        </div>
                    </div>
                   
                    <div class="form-group">
                    <asp:Label ID="Label26" runat="server" Text="Payable Ratio :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-3">
                       <asp:TextBox ID="txtPayableRatio" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <asp:Label ID="Label27" runat="server" Text="Job Length :" CssClass="col-sm-2 control-label"></asp:Label>

                    <div class="col-sm-3">
                        <asp:TextBox ID="txtJobLength" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>

                  

                </div>

                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Employee's Contribution</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" Text="Contribution :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployeeContribution" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Profit Rcv. :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployeeProfitRcv" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label9" runat="server" Text="Other's :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployeeOthers" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label10" runat="server" Text="Balance :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployeeBalance" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Employer's Contribution</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label11" runat="server" Text="Contribution :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersContribution" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label12" runat="server" Text="Profit Rcv. :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersProfitRcv" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label13" runat="server" Text="Other's :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersOthers" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label14" runat="server" Text="Balance :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersBalance" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Payment</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label15" runat="server" Text="Contribution :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtPaymentContribution" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label16" runat="server" Text="Profit Rcv. :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtPaymentProfitRcv" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label18" runat="server" Text="Balance :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtPaymentBalance" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label17" runat="server" Text="PF Profit :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtPaymentPFProfit" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset>
                <legend>Payment</legend>

                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="Label19" runat="server" Text="Contribution :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersPaymentContribution" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label20" runat="server" Text="Profit Rcv. :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersPaymentProfitRcv" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label22" runat="server" Text="Balance :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersPaymentBalance" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label21" runat="server" Text="PF Profit :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmployersPaymentPFProfit" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-6" style="margin-top: 5px;">
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="Label23" runat="server" Text="Total Payment :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtTotalEmployeesPayment" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="margin-top: 5px;">
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="Label24" runat="server" Text="Total PF Profit :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtEmployersTotalPFProfit" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-horizontal">
                <div class="form-group">
                    <asp:Label ID="Label25" runat="server" Text="On A/C Of :" CssClass="col-sm-3 control-label"></asp:Label>
                    <div class="col-sm-6">
                        <asp:DropDownList ID="ddOnACOf" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Pay Head Name" runat="server"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-sm-12">
            <div class="form-horizontal">
                <div class="form-group center">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group center">
                <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                    CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btn_save_Click" Width="80px" />
            </div>
        </div>

        <div class="col-md-12 hidden">
            <div style="height: 250px; overflow: scroll;">
                <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                    AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                    CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                            <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                            <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                        </asp:CommandField>
                        <asp:TemplateField HeaderText="Card No">
                            <ItemTemplate>
                                <asp:Label ID="lblGridCardNo" runat="server" Text='<%# Bind("CardNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Employee Name">
                            <ItemTemplate>
                                <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblGridAmount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="EffictiveDate">
                            <ItemTemplate>
                                <asp:Label ID="lblGridEffictiveDate" runat="server" Text='<%# Bind("EffictiveDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblGridRemarks" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                                <asp:HiddenField ID="hidempAutoId" Value='<%# Bind("empAutoId") %>' runat="server" />
                                <asp:HiddenField ID="hidTaxCreditId" Value='<%# Bind("TaxCreditId") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <PagerStyle CssClass="pagination-sa" />
                    <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                    <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                </asp:GridView>
            </div>
        </div>


    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>
</asp:Content>
