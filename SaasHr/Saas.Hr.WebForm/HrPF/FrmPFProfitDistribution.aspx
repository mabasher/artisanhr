﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HrUser.Master" AutoEventWireup="true"
    CodeBehind="FrmPFProfitDistribution.aspx.cs" Inherits="Saas.Hr.WebForm.Hr.FrmPFProfitDistribution" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleContent" runat="server">
</asp:Content>

<asp:Content ID="ContentHeading" ContentPlaceHolderID="PageHeading" runat="server">
    <div>
        PF Profit Distribution
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row row-custom">

        <div class="col-md-7">
            <fieldset>
                <legend>PF Profit Distribution</legend>

                <div class="form-horizontal">
                    <div class="form-group hidden">
                        <asp:Label ID="Label4" runat="server" Text="" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-5">
                            <asp:TextBox ID="txtSearch" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" placeholder="Search Employee..." runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <asp:Label ID="lbl_GroupName" runat="server" Text="Employee :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtPIN" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-2 ">
                            <asp:TextBox ID="txtCard" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txtEmpName" ReadOnly="true" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <asp:Label ID="Label5" runat="server" Text="On A/C Of :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-4">
                            <asp:DropDownList ID="ddOnACOf" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Pay Head Name" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <asp:Label ID="Label6" runat="server" Text="Month :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-3">
                            <asp:DropDownList ID="ddMonth" CssClass="form-control input-sm chosen-select" data-placeholder="Choose a Level" runat="server"></asp:DropDownList>
                        </div>
                        <div class="col-sm-1">
                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label7" runat="server" Text="From Date :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-2">
                            <asp:TextBox runat="server" ID="txtFromDate" CssClass="input-sm date-picker" />
                        </div>
                        <asp:Label ID="Label9" runat="server" Text="To Date :" CssClass="col-sm-2 control-label"></asp:Label>

                        <div class="col-sm-2">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="input-sm date-picker"></asp:TextBox>
                        </div>
                    </div>

                       
                    <div class="form-group">
                        <asp:Label ID="Label8" runat="server" Text="Remarks :" CssClass="col-sm-3 control-label"></asp:Label>
                        <div class="col-sm-7">
                            <asp:TextBox ID="txtAdditionalNote" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div>
                        <asp:Button ID="btnCalculateProfit" runat="server" CssClass="btn btn-primary btn-sm" Text="Calculate Profit" Width="150px" />
                    </div>
                    &nbsp;
                    <div style="height: 300px; overflow: scroll;">
                        <asp:GridView ID="gdv_costingHead" runat="server" Style="width: 100%; margin-left: 0;"
                            AutoGenerateColumns="False" HeaderStyle-CssClass="gridViewHeaderStyle" AlternatingRowStyle-CssClass="gridViewAlternateRowStyle"
                            CssClass="table table-striped table-bordered" OnRowDataBound="gdv_costingHead_RowDataBound" OnSelectedIndexChanged="gdv_costingHead_SelectedIndexChanged" EmptyDataText="No Data Found!!!" ShowHeaderWhenEmpty="True">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" HeaderStyle-CssClass="hidGridColumn" ItemStyle-CssClass="hidGridColumn">
                                    <HeaderStyle CssClass="hidGridColumn"></HeaderStyle>

                                    <ItemStyle CssClass="hidGridColumn"></ItemStyle>
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="Employee Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridEmpName" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pay Head ">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridPayHead" runat="server" Text='<%# Bind("PayHeadName") %>'></asp:Label>
                                        <asp:HiddenField ID="hidpayHeadNameAutoId" Value='<%# Bind("payHeadNameAutoId") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridAmount" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="TotalAmount">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridTotalAmount" runat="server" Text='<%# Bind("TotalAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                            <PagerStyle CssClass="pagination-sa" />
                            <SelectedRowStyle CssClass="gridViewSelectedRowStyle"></SelectedRowStyle>
                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                            <AlternatingRowStyle CssClass="gridViewAlternateRowStyle"></AlternatingRowStyle>
                        </asp:GridView>
                    </div>
                    <div class="form-group center">
                        <asp:Button ID="btn_save" runat="server" CssClass="btn btn-primary btn-sm" Text="Save" OnClick="btn_save_Click" Width="80px" />
                        <asp:Button ID="btn_refresh" runat="server" Text="Refresh" CausesValidation="false"
                            CssClass="btn btn-default btn-sm" OnClick="btn_refresh_Click" Width="80px" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" OnClick="btn_save_Click" Width="80px" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-5" style="margin-top: 10px;">
            <fieldset>
                <legend>..</legend>
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="15px" ForeColor="Red" Text="============= Before Profit Calculation ============="></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Font-Size="15px" Text="1. Check All FDR (Issue/ReIssue/Collection/Closing)"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Size="15px" Text="2. Check All PF Loan (Issue/Collection/Closing)"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label10" runat="server" ForeColor="Red" Font-Size="15px" Text="3. Check PF Closing of Terminated Staff"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label11" runat="server" ForeColor="Red" Text="4. Receive Others Income From PF"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label12" runat="server" ForeColor="Red" Font-Size="15px" Text="5. Pay Other Expense From PF"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label13" runat="server" Font-Bold="true" Font-Size="15px" ForeColor="Red" Text=" ========== After Profit Calculation ========== "></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label14" runat="server" ForeColor="Red" Font-Size="15px" Text="1. Check Staff PF Balance "></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label15" runat="server" ForeColor="Red" Font-Size="15px" Text="2. Check Bank Statement "></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label16" runat="server" ForeColor="Red" Font-Size="15px" Text="3. If any discrepency occured then need Adjustment Journal"></asp:Label>
                            </div>
                            <div class="col-sm-12">
                                <asp:Label ID="Label17" runat="server" ForeColor="Red" Font-Size="15px" Text="4. Check Accounting Statement "></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <div class="col-sm-12">
            <div class="form-horizontal">
                <div class="form-group center">
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                </div>
            </div>
        </div>




    </div>


    <asp:HiddenField ID="hidIsInsertMode" runat="server" Value="True" />
    <asp:HiddenField ID="hidAutoIdForUpdate" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_formName" runat="server" Value="True" />
    <asp:HiddenField ID="hid_s_tableName" runat="server" Value="True" />
    <asp:HiddenField ID="hidURL" runat="server" Value="" />
    <asp:HiddenField ID="hidEmpAutoId" runat="server" Value="0" />
    <asp:HiddenField ID="hidSectionId" runat="server" Value="0" />
    <asp:HiddenField ID="hidDepartmentId" runat="server" Value="0" />
    <asp:HiddenField ID="hidLineId" runat="server" Value="0" />
    <asp:HiddenField ID="hidCardid" runat="server" Value="0" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
        $(function () {
            makeAutoComplete('#<%=txtSearch.ClientID %>',
                '<%=ResolveUrl("~/Services/getEmployee.asmx/Getemp") %>',
                '#<%=hidEmpAutoId.ClientID %>');
        });
    </script>
</asp:Content>
