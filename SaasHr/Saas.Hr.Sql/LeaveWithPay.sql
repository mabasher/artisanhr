﻿

ALTER TABLE [dbo].[LeaveWithPay] DROP CONSTRAINT [DF_LeaveWithPay_IsDeleted]
GO

ALTER TABLE [dbo].[LeaveWithPay] DROP CONSTRAINT [DF_LeaveWithPay_IsInactive]
GO

/****** Object:  Table [dbo].[LeaveWithPay]    Script Date: 08/03/2019 4:10:07 PM ******/
DROP TABLE [dbo].[LeaveWithPay]
GO

/****** Object:  Table [dbo].[LeaveWithPay]    Script Date: 08/03/2019 4:10:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LeaveWithPay](
	[LeaveWithPayId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[leaveTypeAutoId] [numeric](18, 0) NOT NULL,
	[calType] [varchar](10) NOT NULL,
	[fromDate] [datetime] NOT NULL,
	[fromTime] [time](7) NOT NULL,
	[hrsDays] [numeric](18, 2) NOT NULL,
	[toDate] [datetime] NOT NULL,
	[toTime] [time](7) NOT NULL,
	[Remarks] [varchar](250) NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LeaveWithPay] ADD  CONSTRAINT [DF_LeaveWithPay_IsInactive]  DEFAULT ((0)) FOR [IsInactive]
GO

ALTER TABLE [dbo].[LeaveWithPay] ADD  CONSTRAINT [DF_LeaveWithPay_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

-------------------------------------





/****** Object:  StoredProcedure [dbo].[ProcEmpAcademicInformation]    Script Date: 08/03/2019 4:41:46 PM ******/
DROP PROCEDURE [dbo].[ProcLeaveWithPaySelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcEmpAcademicInformation]    Script Date: 08/03/2019 4:41:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







---[procPIForLC] '6'

CREATE   proc [dbo].[ProcLeaveWithPaySelect]

    @EmpAutoId numeric(18,0)
    
as
set nocount on

SELECT  
lwp.LeaveWithPayId,
lwp.EmpAutoId,
lt.leaveType, 
lwp.LeaveWithPayId,
lwp.EmpAutoId,
lwp.leaveTypeAutoId,
lwp.calType,
lwp.fromDate,
lwp.fromTime,
lwp.hrsDays,
lwp.toDate,
lwp.toTime,
lwp.Remarks,
lwp.CompanyAutoId
 
FROM LeaveWithPay lwp
left outer join EmployeePersonalInfo emp
ON lwp.EmpAutoId=emp.EmployeePersonalInfoId
left outer join [dbo].[LeaveType] lt
ON lwp.leaveTypeAutoId=lt.LeaveTypeId


WHERE lwp.EmpAutoId=@EmpAutoId
--group by 
--p.autoId,
--p.OrderNo,
--p.OrderDate





GO

-----------------------------

/****** Object:  StoredProcedure [dbo].[ProcAcademicInfoINSERT]    Script Date: 08/03/2019 10:57:51 AM ******/
DROP PROCEDURE [dbo].[ProcLeaveWithPayINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcAcademicInfoINSERT]    Script Date: 08/03/2019 10:57:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




--[ProcNomineeInformationINSERT]'0','1','Sayem Ahmed','','Basher','naa','12/02/2019','ax','01730358095','','324234','3','100','1','3','::1','0','','0','0','I'

CREATE   proc [dbo].[ProcLeaveWithPayINSERT]

	@trAutoId int,
	@EmpAutoId numeric(18, 0),	
	@leaveTypeAutoId numeric (18, 0),
	@calType varchar (10) ,
	@fromDate datetime,
	@fromTime time(7) ,
	@hrsDays numeric(18, 2) ,
	@toDate datetime ,
	@toTime time(7) ,
	@Remarks	varchar(250)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO LeaveWithPay  
		VALUES((Select isnull(max(LeaveWithPayId),0)+1 From LeaveWithPay),
				@EmpAutoId,@leaveTypeAutoId ,@calType ,	@fromDate ,	@fromTime ,	@hrsDays ,@toDate ,	@toTime,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	dbo.EmpAcademicInformation WHERE	ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update LeaveWithPay  
SET EmpAutoId=@EmpAutoId, leaveTypeAutoId=@leaveTypeAutoId,	calType =@calType ,	fromDate=@fromDate ,fromTime =@fromTime ,
	@hrsDays =@hrsDays ,toDate =@toDate ,@toTime=@toTime, Remarks=@Remarks,
	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where LeaveWithPayId=@trAutoId		  

End



SELECT @Isduplicate




GO


