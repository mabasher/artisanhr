﻿

/****** Object:  Table [dbo].[StaffRequisitionForRecruitment]    Script Date: 09/03/2019 8:20:34 PM ******/
DROP TABLE [dbo].[StaffRequisitionForRecruitment]
GO

/****** Object:  Table [dbo].[StaffRequisitionForRecruitment]    Script Date: 09/03/2019 8:20:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StaffRequisitionForRecruitment](
	[autoId] [numeric](18, 0) NOT NULL,
	[inDate] [datetime] NOT NULL,
	[referenceNumber] [varchar](100) NOT NULL,
	[DepartmentAutoId] [numeric](18, 0) NOT NULL,
	[ExistingManPower] [int] NOT NULL,
	[NecessityRecruitment] [varchar](1000) NOT NULL,
	[FactoryInchargeAutoId] [numeric](18, 0) NOT NULL,
	[FactoryInchargeNote] [varchar](1000) NOT NULL,
	[RecruitJustification] [varchar](500) NOT NULL,
	[RecruitPublications] [varchar](500) NOT NULL,
	[RecruitTimeFrame] [varchar](500) NOT NULL,
	[RecruitExperience] [varchar](500) NOT NULL,
	[RecomendationNote] [varchar](1000) NOT NULL,
	[ApprovalNote] [varchar](500) NOT NULL,
	[Remarks] [varchar](550) NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL CONSTRAINT [DF_StaffRequisitionForRecruitment_IsInactive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_StaffRequisitionForRecruitment_IsDeleted]  DEFAULT ((0))
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
----------------------------------


/****** Object:  Table [dbo].[StaffRequisitionForRecruitmentDetails]    Script Date: 09/03/2019 8:21:04 PM ******/
DROP TABLE [dbo].[StaffRequisitionForRecruitmentDetails]
GO

/****** Object:  Table [dbo].[StaffRequisitionForRecruitmentDetails]    Script Date: 09/03/2019 8:21:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StaffRequisitionForRecruitmentDetails](
	[autoId] [numeric](18, 0) NOT NULL,
	[RecruitmentAutoId] [numeric](18, 0) NOT NULL,
	[DesignationAutoId] [numeric](18, 0) NOT NULL,
	[ManPowerMale] [int] NOT NULL,
	[ManPowerFeMale] [int] NOT NULL,
	[LocationAutoId] [numeric](18, 0) NOT NULL,
	[Responsibility] [varchar](100) NOT NULL,
	[Education] [varchar](100) NOT NULL,
	[Experience] [varchar](100) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


---------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 09/03/2019 3:23:58 PM ******/
DROP PROCEDURE [dbo].[ProcRequiredManpowerwithDesignationSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 09/03/2019 3:23:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO








--- [ProcRequiredManpowerwithDesignationSelect] '1'

CREATE   proc [dbo].[ProcRequiredManpowerwithDesignationSelect]

    @RecruitmentAutoId numeric(18,0)
    
as
set nocount on

SELECT  
isnull(srd.RecruitmentAutoId,0) as RecruitmentAutoId,
isnull(desig.Designation,'') as Designation,
isnull(srd.ManPowerMale,'') as ManPowerMale,
isnull(srd.ManPowerFeMale,'') as ManPowerFeMale,
isnull(srd.DesignationAutoId,'0') as DesignationAutoId

FROM [dbo].[StaffRequisitionForRecruitmentDetails] srd

left outer join [dbo].[DesignationInfo] desig
ON srd.DesignationAutoId=desig.DesignationInfoId

WHERE isnull(srd.RecruitmentAutoId,'0')=RecruitmentAutoId
--group by 
--p.autoId,
--p.OrderNo,
--p.OrderDate






GO


--------------------------------

/****** Object:  StoredProcedure [dbo].[ProcExpProformaInvoiceInsert]    Script Date: 09/03/2019 4:32:03 PM ******/
DROP PROCEDURE [dbo].[ProcStaffRequisitionForRecruitmentInsert]
GO

/****** Object:  StoredProcedure [dbo].[ProcExpProformaInvoiceInsert]    Script Date: 09/03/2019 4:32:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- [ProcStaffRequisitionForRecruitmentInsert]'03/09/2019','1111','1','1','Brief The Necessity of the Recruitment :','1','qwerqwer','Manpower Recruiment Justification','Media to be Used for Publication','Time Frame of Recruitment','Brief In Experience Details','Recommendation of Executive Director','Approval of Chairman/ Managing Director :','Approval of Chairman/ Managing Director :','1','0','','2','::1','0','0'



CREATE PROCEDURE [dbo].[ProcStaffRequisitionForRecruitmentInsert] 	

	@inDate	datetime	,
	@referenceNumber	varchar(100)	,
	@DepartmentAutoId	numeric(18, 0)	,
	@ExistingManPower	int	,
	@NecessityRecruitment	varchar(1000)	,
	@FactoryInchargeAutoId	numeric(18, 0)	,
	@FactoryInchargeNote	varchar(1000)	,
	@RecruitJustification	varchar(500)	,
	@RecruitPublications	varchar(500)	,
	@RecruitTimeFrame	varchar(500)	,
	@RecruitExperience	varchar(500)	,
	@RecomendationNote	varchar(1000)	,
	@ApprovalNote	varchar(500)	,
	@Remarks	varchar(550)	,	
	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50)	
	
AS
SET NOCOUNT ON

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''


if exists (	SELECT referenceNumber FROM dbo.StaffRequisitionForRecruitment WHERE referenceNumber = @referenceNumber)
Begin
set @Isduplicate = 'D'
END

if @Isduplicate != 'D'
Begin
	INSERT INTO
		dbo.StaffRequisitionForRecruitment
	VALUES
		(
		(Select isnull(max(autoId),0)+1 From dbo.StaffRequisitionForRecruitment),	
		@inDate		,
	@referenceNumber,
	@DepartmentAutoId	,
	@ExistingManPower		,
	@NecessityRecruitment		,
	@FactoryInchargeAutoId	,
	@FactoryInchargeNote		,
	@RecruitJustification		,
	@RecruitPublications		,
	@RecruitTimeFrame	,
	@RecruitExperience,
	@RecomendationNote,
	@ApprovalNote		,
	@Remarks		,	
	
	@CompanyAutoId ,
	@CreateBy ,
	getdate() ,
	@CreateIp ,
	null,
	null,
	null,
	'0',--@IsInactive,
	'0'--@IsDeleted

		)
End

select @Isduplicate







GO


-------------------------------

/****** Object:  StoredProcedure [dbo].[ProcExpProformaInvoiceDetailsInsert]    Script Date: 09/03/2019 5:16:18 PM ******/
DROP PROCEDURE [dbo].[ProcStaffRequisitionForRecruitmentDetailsInsert]
GO

/****** Object:  StoredProcedure [dbo].[ProcExpProformaInvoiceDetailsInsert]    Script Date: 09/03/2019 5:16:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--Execute ProcExpProformaInvoiceDetailsInsert'expPi-430499','4','H34','Black','45','1','1000000','0.00058','580','fg' Select ''

CREATE PROCEDURE [dbo].[ProcStaffRequisitionForRecruitmentDetailsInsert] 	

	@referenceNumber varchar (50),
	@DesignationAutoId	numeric(18, 0)	,
	@ManPowerMale	int	,
	@ManPowerFeMale	int	,
	@LocationAutoId	numeric(18, 0)	,
	@Responsibility	varchar(100)	,
	@Education	varchar(100)	,
	@Experience	varchar(100)	


AS
SET NOCOUNT ON

Declare @Isduplicate nvarchar(1)
declare @trAutoId numeric(18,0)
set @trAutoId=0
set @Isduplicate = ''
SELECT @trAutoId=isnull(AutoId,0) FROM dbo.StaffRequisitionForRecruitment WHERE referenceNumber = @referenceNumber

if @Isduplicate != 'D'
Begin
	INSERT INTO
		dbo.[StaffRequisitionForRecruitmentDetails]
	VALUES
		(
		(Select isnull(max(AutoId),0)+1 From dbo.[StaffRequisitionForRecruitmentDetails]),	
		@trAutoId ,
		@DesignationAutoId		,
		@ManPowerMale		,
		@ManPowerFeMale,
		'0',
		'',
		'',
		''
		)

End

--select @Isduplicate












