﻿

/****** Object:  StoredProcedure [dbo].[proc_CommonForm_DML]    Script Date: 08/02/2019 3:53:56 PM ******/
DROP PROCEDURE [dbo].[ProcCommonFormHR]
GO


-- [proc_CommonForm_DML]'0','Consumption Type','CT','14','T_Inv_ConsumptionType','I'


--[proc_CommonForm_DML]'DSM','01','1231234','asdd@rftth','5','Tbl_DSM','I'

CREATE proc [dbo].[ProcCommonFormHR]
    @trAutoId varchar(50),
    @Code varchar(50),
    @Name varchar(50),
    @BanglaName nvarchar(100),

	@CreateBy int ,
	@CreateIp nvarchar(50),
	@UpdateBy  int ,
	@UpdateIp nvarchar (50),
	@IsInactive bit ,
	@IsDeleted bit ,

    @trTable varchar(50),
    @trMode varchar(50)
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

IF (@trTable='DepartmentInfo')
BEGIN
    IF(@trMode='I')
    BEGIN
        IF exists (select Department From DepartmentInfo where Department =  @Name) Begin set @Isduplicate = 'D' END
        IF exists (select DepartmentBangla From DepartmentInfo where DepartmentBangla = @BanglaName) Begin set @Isduplicate = 'D' END
        IF exists (select DepartmentCode from DepartmentInfo where DepartmentCode =@Code) Begin set @Isduplicate = 'D' END
        IF @Isduplicate != 'D' 
        Begin 
        INSERT INTO DepartmentInfo
		 ([DepartmentInfoId],[DepartmentCode],[Department],[DepartmentBangla],[CreateBy],[CreateTime],[CreateIp])
		 VALUES((Select isnull(max(DepartmentInfoId),0)+1 From DepartmentInfo),
				@Code,@Name,@BanglaName,@CreateBy,getdate(),@CreateIp)
        END		
    END    
    IF(@trMode='U')
    BEGIN
        IF exists (select Department From DepartmentInfo where Department = @Name and DepartmentInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF exists (select DepartmentBangla From DepartmentInfo where DepartmentBangla =@BanglaName and DepartmentInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF exists (select DepartmentCode from DepartmentInfo where DepartmentCode =  @Code and DepartmentInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF @Isduplicate != 'D' 
        Begin 
        UPDATE DepartmentInfo  set Department=@Name,DepartmentCode=@Code,DepartmentBangla=@BanglaName,UpdateBy=@UpdateBy,UpdateTime=getdate() ,UpdateIp=@UpdateIp,IsInactive=@IsInactive,IsDeleted=@IsDeleted
		Where DepartmentInfoId=@trAutoId
        END
    END
End

IF (@trTable='T_Currency')
BEGIN
    IF(@trMode='I')
    BEGIN
        IF exists (select Currency From T_Currency where Currency =  @Name) Begin set @Isduplicate = 'D' END
        IF exists (select Code from T_Currency where Code =  @Code) Begin set @Isduplicate = 'D' END
        IF @Isduplicate != 'D' 
        Begin 
        INSERT INTO T_Currency VALUES((Select isnull(max(AutoId),0)+1 From T_Currency),@Name,@Code,'0')
        END
    END    
    IF(@trMode='U')
    BEGIN
        IF exists (select Currency From T_Currency where Currency =  @Name and AutoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF exists (select Code from T_Currency where Code =  @Code and AutoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF @Isduplicate != 'D' 
        Begin 
        UPDATE T_Currency  set Currency=@Name,Code=@Code Where AutoId=@trAutoId
        END
    END
End

IF (@trTable='T_UOM')
BEGIN
    IF(@trMode='I')
    BEGIN
        IF exists (select UOM From T_UOM where UOM =  @Name) Begin set @Isduplicate = 'D' END
        IF exists (select Code from T_UOM where Code =  @Code) Begin set @Isduplicate = 'D' END
        IF @Isduplicate != 'D' 
        Begin 
        INSERT INTO T_UOM VALUES((Select isnull(max(AutoId),0)+1 From T_UOM),@Name,@Code,'0')
        END
    END    
    IF(@trMode='U')
    BEGIN
        IF exists (select UOM From T_UOM where UOM =  @Name and AutoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF exists (select Code from T_UOM where Code =  @Code and AutoId<>@trAutoId) Begin set @Isduplicate = 'D' END
        IF @Isduplicate != 'D' 
        Begin 
        UPDATE T_UOM  set UOM=@Name,Code=@Code Where AutoId=@trAutoId
        END
    END
End


select @Isduplicate










GO


