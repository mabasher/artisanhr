﻿

/****** Object:  Table [dbo].[SalaryInfo]    Script Date: 21/07/2019 7:49:16 AM ******/
DROP TABLE [dbo].[SalaryInfo]
GO

/****** Object:  Table [dbo].[SalaryInfo]    Script Date: 21/07/2019 7:49:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SalaryInfo](
	[SalaryInfoId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[budgetCodeAutoId] [numeric](18, 0) NOT NULL,
	[SalaryGroupId] [numeric](18, 0) NOT NULL,
	[sGross] [numeric](18, 6) NOT NULL,
	[sBasic] [numeric](18, 6) NOT NULL,
	[sHR] [numeric](18, 6) NOT NULL,
	[sConveyance] [numeric](18, 6) NOT NULL,
	[sMedical] [numeric](18, 6) NOT NULL,
	[sPF] [numeric](18, 6) NOT NULL,
	[sFood] [numeric](18, 6) NOT NULL,
	[sTransportation] [numeric](18, 6) NOT NULL,
	[sOtherAllowance] [numeric](18, 6) NOT NULL,
	[sEffectiveDate] [datetime] NOT NULL,
	[sIncrDate] [datetime] NOT NULL,
	[sAccountNumber] [varchar](50) NOT NULL,
	[sPaymentMode] [varchar](1) NOT NULL,
	[sIsTaxxable] [varchar](1) NOT NULL,
	[sIsOTPayable] [varchar](1) NOT NULL,
	[sIsApprove] [varchar](1) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
 CONSTRAINT [PK_SalaryInfo] PRIMARY KEY CLUSTERED 
(
	[SalaryInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------




/****** Object:  StoredProcedure [dbo].[proc_frm_UserCreation_INSERT_T_UserCreation]    Script Date: 16/02/2019 5:44:17 PM ******/
DROP PROCEDURE [dbo].[ProcSalaryInfoINSERT]
GO

/****** Object:  StoredProcedure [dbo].[proc_frm_UserCreation_INSERT_T_UserCreation]    Script Date: 16/02/2019 5:44:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

--[ProcSalaryInfoINSERT]'0','4','0','0','8300','4300','2150','0','600','344','900','350','23/03/2019','23/03/2019','','C','N','Y','Y','1','3','::1','I'

CREATE   proc [dbo].[ProcSalaryInfoINSERT]
	@trAutoId numeric (18, 0) ,
	@EmpAutoId int  ,
	@budgetCodeAutoId numeric (18, 0) ,
	@SalaryGroupId numeric (18, 0) ,
	@sGross numeric (18, 6) ,
	@sBasic numeric (18, 6) ,
	@sHR numeric (18, 6) ,
	@sConveyance numeric (18, 6) ,
	@sMedical numeric (18, 6) ,
	@sPF numeric (18, 6) ,
	@sFood numeric (18, 6) ,
	@sTransportation numeric (18, 6) ,
	@sOtherAllowance numeric (18, 6) ,
	@sEffectiveDate datetime  ,
	@sIncrDate datetime  ,
	@sAccountNumber varchar (50) ,
	@sPaymentMode varchar (1) ,
	@sIsTaxxable varchar (1) ,
	@sIsOTPayable varchar (1) ,
	@sIsApprove varchar (1) ,	
	@CompanyId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@trType nvarchar(50)

with encryption
as
set nocount on
Declare @Isduplicate nvarchar(1)
Declare @sGroup nvarchar(50)
Declare @sPayHeadId nvarchar(50)
Declare	@sPayHaedValue numeric (18, 6)
set @Isduplicate = ''
set @sPayHeadId=''
set @sPayHaedValue=0
set @sGroup=cast(@EmpAutoId as varchar(20))+'-SG-'+cast(@sGross as varchar(20))
if (@trType='I')
Begin
if exists (SELECT sGross FROM	dbo.SalaryInfo WHERE EmpAutoId=@EmpAutoId) Begin set @Isduplicate = 'D' END

INSERT INTO dbo.SalaryGroupInfo  
VALUES((Select isnull(max(SalaryGroupInfoId),0)+1 From dbo.SalaryGroupInfo),@sGroup,'',@sEffectiveDate)
Select @SalaryGroupId=isnull(SalaryGroupInfoId,0) From dbo.SalaryGroupInfo Where GroupName=@sGroup 	

if (@sGross>0)	
Begin
set @sPayHeadId='1' set @sPayHaedValue=@sGross
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sBasic>0)	
Begin
set @sPayHeadId='2' set @sPayHaedValue=@sBasic
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sHR>0)	
Begin
set @sPayHeadId='3' set @sPayHaedValue=@sHR
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sMedical>0)	
Begin
set @sPayHeadId='4' set @sPayHaedValue=@sMedical
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sConveyance>0)	
Begin
set @sPayHeadId='5' set @sPayHaedValue=@sConveyance
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sFood>0)	
Begin
set @sPayHeadId='6' set @sPayHaedValue=@sFood
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sTransportation>0)	
Begin
set @sPayHeadId='7' set @sPayHaedValue=@sTransportation
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sPF>0)	
Begin
set @sPayHeadId='8' set @sPayHaedValue=@sPF
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
INSERT INTO dbo.SalaryInfo  
	VALUES((Select isnull(max(SalaryInfoId),0)+1 From dbo.SalaryInfo),
	@EmpAutoId,	@budgetCodeAutoId,@SalaryGroupId,@sGross,@sBasic,@sHR,@sConveyance,@sMedical,
	@sPF,@sFood,@sTransportation,@sOtherAllowance,@sEffectiveDate,@sIncrDate,@sAccountNumber,@sPaymentMode,
	@sIsTaxxable,@sIsOTPayable,@sIsApprove,@CompanyId,@CreateBy ,getdate(),@CreateIp ,null,null,null)

End--//End of Insert


if (@trType='U')
Begin
if exists (SELECT sGross FROM	dbo.SalaryInfo WHERE EmpAutoId=@EmpAutoId and SalaryInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END

Select @SalaryGroupId=isnull(SalaryGroupInfoId,0) From dbo.SalaryGroupInfo Where GroupName=@sGroup 	
delete SalaryGroupDetailsInfo where SalaryGroupInfoId=@SalaryGroupId
delete SalaryGroupInfo where SalaryGroupInfoId=@SalaryGroupId
INSERT INTO dbo.SalaryGroupInfo  
VALUES((Select isnull(max(SalaryGroupInfoId),0)+1 From dbo.SalaryGroupInfo),@sGroup,'',@sEffectiveDate)
Select @SalaryGroupId=isnull(SalaryGroupInfoId,0) From dbo.SalaryGroupInfo Where GroupName=@sGroup 	

if (@sGross>0)	
Begin
set @sPayHeadId='1' set @sPayHaedValue=@sGross
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sBasic>0)	
Begin
set @sPayHeadId='2' set @sPayHaedValue=@sBasic
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sHR>0)	
Begin
set @sPayHeadId='3' set @sPayHaedValue=@sHR
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sMedical>0)	
Begin
set @sPayHeadId='4' set @sPayHaedValue=@sMedical
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sConveyance>0)	
Begin
set @sPayHeadId='5' set @sPayHaedValue=@sConveyance
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sFood>0)	
Begin
set @sPayHeadId='6' set @sPayHaedValue=@sFood
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sTransportation>0)	
Begin
set @sPayHeadId='7' set @sPayHaedValue=@sTransportation
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End
if (@sPF>0)	
Begin
set @sPayHeadId='8' set @sPayHaedValue=@sPF
INSERT INTO dbo.SalaryGroupDetailsInfo VALUES((Select isnull(max(SalaryGroupDetailsInfoId),0)+1 From dbo.SalaryGroupDetailsInfo),@SalaryGroupId,@sPayHeadId,@sPayHaedValue,@sPayHaedValue)
End

Update dbo.SalaryInfo  
SET SalaryGroupId=@SalaryGroupId,sGross=@sGross,sBasic=@sBasic,sHR=@sHR,sConveyance=@sConveyance,
	sMedical=@sMedical,sPF=@sPF,sFood=@sFood,sTransportation=@sTransportation,sOtherAllowance=@sOtherAllowance,
	sEffectiveDate=@sEffectiveDate,sIncrDate=@sIncrDate,sAccountNumber=@sAccountNumber,
	sPaymentMode=@sPaymentMode,sIsTaxxable=@sIsTaxxable,sIsOTPayable=@sIsOTPayable,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where SalaryInfoId=@trAutoId and EmpAutoId=@EmpAutoId	  

End



SELECT @Isduplicate


GO

----------------------



DROP TABLE [dbo].[OTAdjustment]
GO


CREATE TABLE [dbo].[OTAdjustment](
	[AutoId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[eEffectiveDate] [datetime] NOT NULL,	
	[AddHour] [decimal](6,2) NOT NULL,
	[DeductHour] [decimal](6,2) NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


---------------------



/****** Object:  StoredProcedure [dbo].[ProcPFCloseDateINSERT]    Script Date: 25/07/2019 4:27:29 PM ******/
DROP PROCEDURE [dbo].[ProcOTAdjustmentINSERT]
GO



CREATE   proc [dbo].[ProcOTAdjustmentINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@eEffectiveDate datetime ,
	@AddHour decimal(6, 2),	
	@DeductHour decimal(6, 2),	
	@Remarks	varchar(max),
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO OTAdjustment  
		VALUES((Select isnull(max(AutoId),0)+1 From OTAdjustment),
				@EmpAutoId,@eEffectiveDate,@AddHour,@DeductHour,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update OTAdjustment  
SET EmpAutoId=@EmpAutoId,eEffectiveDate=@eEffectiveDate,   Remarks=@Remarks,
	AddHour=@AddHour,DeductHour=@DeductHour,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where AutoId=@trAutoId		  

End



SELECT @Isduplicate





GO


------------------------




DROP PROCEDURE [dbo].[ProcOTAdjustmentSelect]
GO


-- [ProcOTAdjustmentSelect]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcOTAdjustmentSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.PIN as PIN,
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
sa.eEffectiveDate as EffictiveDate,
sa.AddHour as AddHour,
sa.DeductHour as DeductHour,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.AutoId


FROM OTAdjustment sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
	and sa.eEffectiveDate between @Fromdate and @Todate






GO


--------------


/****** Object:  StoredProcedure [dbo].[proc_frm_UserCreation_INSERT_T_UserCreation]    Script Date: 16/02/2019 5:44:17 PM ******/
DROP PROCEDURE [dbo].[ProcPersonalProfileINSERT]
GO

/****** Object:  StoredProcedure [dbo].[proc_frm_UserCreation_INSERT_T_UserCreation]    Script Date: 16/02/2019 5:44:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


--[ProcPersonalProfileINSERT]'27','MPS-0113','00002113','A K M Faizul Haque','','asd','asd','asd','01730358095','111','232','234','mabasherstm@gmail.com','mabasherstm@gmail.com',',,,sadasd',',,,asdasd','','01/01/1900','asd','asd','','','','','','','','','2','1','2','1','1','1','0','','3','::1','0','0','U'

CREATE   proc [dbo].[ProcPersonalProfileINSERT]

	@trAutoId int,
	@PIN varchar (20),
	@CardNo varchar (20),
	@EmployeeName varchar (100),
	@EmployeeNeekName varchar (50) ,
	@FatherName varchar (100) ,
	@MotherName varchar (100),
	@SpouseName varchar (100),
	@PersonalContact varchar (100),
	@OfficialContact varchar (100),
	@EmergencyContact varchar (100),
	@ContactPerson varchar (100),
	@PersonalMail varchar (100),
	@OfficialMail varchar (100) ,
	@PresentAddress varchar (500) ,
	@PermanentAddress varchar (500) ,
	@EmergencyAddress varchar (500),
	@DOB datetime  ,
	@PlaceOfBirth varchar(100) ,
	@BirthCertificate varchar(100),
	@NID varchar(100) ,
	@TIN varchar(100) ,
	@PassportNo varchar(100) ,
	@DrivingLicense varchar(100),
	@ExtraCurricularActivities varchar(500),
	@Remark varchar(100),
	@PicUrl varchar(100),
	@SigUrl varchar (100),
	@GenderAutoId int,
	@NationalityAutoId int,
	@ReligionAutoId int,
	@BloodGroupAutoId int,
	@MaritalStatusAutoId int,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

with encryption
as
set nocount on
Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''
if (@PicUrl='') begin set @PicUrl=null end
if (@SigUrl='') begin set @SigUrl=null end
	

if (@trType='I')
Begin
if exists (SELECT PIN FROM	dbo.EmployeePersonalInfo WHERE	PIN=@PIN) Begin set @Isduplicate = 'D' END
INSERT INTO EmployeePersonalInfo  
		VALUES((Select isnull(max(EmployeePersonalInfoId),0)+1 From EmployeePersonalInfo),
				@PIN,@CardNo,@EmployeeName,@EmployeeNeekName,@FatherName ,@MotherName ,@SpouseName ,
				@PersonalContact,@OfficialContact,@EmergencyContact,@ContactPerson,@PersonalMail,
				@OfficialMail,@PresentAddress,@PermanentAddress,@EmergencyAddress,@DOB   ,
				@PlaceOfBirth,@BirthCertificate ,@NID ,@TIN ,@PassportNo ,@DrivingLicense ,
				@ExtraCurricularActivities ,@Remark ,ISNULL(@PicUrl, '') ,ISNULL(@SigUrl, '') ,
				@GenderAutoId  ,@NationalityAutoId ,@ReligionAutoId ,@BloodGroupAutoId ,
				@MaritalStatusAutoId ,@CompanyAutoId ,@CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
if exists (SELECT PIN FROM	dbo.EmployeePersonalInfo WHERE	PIN=@PIN and EmployeePersonalInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update EmployeePersonalInfo  
SET PIN=@PIN,CardNo=@CardNo,EmployeeName=@EmployeeName,
	EmployeeNeekName=@EmployeeNeekName,FatherName=@FatherName ,MotherName=@MotherName ,SpouseName=@SpouseName ,
	PersonalContact=@PersonalContact,OfficialContact=@OfficialContact,EmergencyContact=@EmergencyContact,
	ContactPerson=@ContactPerson,PersonalMail=@PersonalMail,OfficialMail=@OfficialMail,
	PresentAddress=@PresentAddress,PermanentAddress=@PermanentAddress,
	--@EmergencyAddress,
	DOB=@DOB,PlaceOfBirth=@PlaceOfBirth,BirthCertificate=@BirthCertificate ,
	NID=@NID ,TIN=@TIN ,PassportNo=@PassportNo ,DrivingLicense=@DrivingLicense ,
	--@ExtraCurricularActivities ,
	--@Remark ,
	PicUrl=ISNULL(@PicUrl, PicUrl) ,SigUrl=ISNULL(@SigUrl, SigUrl) ,
	GenderAutoId=@GenderAutoId  ,NationalityAutoId=@NationalityAutoId ,
	ReligionAutoId =@ReligionAutoId ,BloodGroupAutoId=@BloodGroupAutoId ,
	MaritalStatusAutoId=@MaritalStatusAutoId ,
	--@CompanyAutoId ,
	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where EmployeePersonalInfoId=@trAutoId		  

End


SELECT @Isduplicate


GO


--------------------

