﻿
/****** Object:  Table [dbo].[ExpProformaInvoice]    Script Date: 29/11/2018 1:51:29 PM ******/
DROP TABLE [dbo].[ExpProformaInvoiceApproval]
GO

/****** Object:  Table [dbo].[ExpProformaInvoice]    Script Date: 29/11/2018 1:51:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ExpProformaInvoiceApproval](
	[AutoId] [numeric](18, 0) NOT NULL,
	[PIID] [numeric](18, 0) NOT NULL,
	[AppDate] [date] NOT NULL,
	[Remarks] [varchar](300) NOT NULL,
	[ApproveById] [smallint] NOT NULL,
	[EntryDate] [date] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


