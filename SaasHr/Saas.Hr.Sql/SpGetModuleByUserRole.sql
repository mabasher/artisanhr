﻿-- =============================================
-- Author:		Sabbir Ahmed Osmani
-- Create date: April 6, 2019
-- SpGetModuleByUserRole 2
-- =============================================
ALTER PROCEDURE SpGetModuleByUserRole @RoleId INT
AS
BEGIN
    SELECT DISTINCT mi.ScrModuleInfoId,
           mi.ModuleName,
           mi.Priority,
           mi.ModuleIcon,
           mi.ParentModuleInfoId
    FROM dbo.ScrModuleInfo mi
        LEFT JOIN dbo.ScrModuleInfo cMi
            ON cMi.ParentModuleInfoId = mi.ScrModuleInfoId
        INNER JOIN dbo.ScrModuleFormForRole mffr
            ON mffr.ScrModuleInfoId = mi.ScrModuleInfoId
               OR mffr.ScrModuleInfoId = cMi.ScrModuleInfoId
    WHERE mffr.ScrRoleInfoId = @RoleId
    ORDER BY mi.Priority;
END;
GO
