﻿

/****** Object:  Table [dbo].[EmpWeekEnd]    Script Date: 08/03/2019 5:20:13 PM ******/
DROP TABLE [dbo].[EmpWeekEnd]
GO

/****** Object:  Table [dbo].[EmpWeekEnd]    Script Date: 08/03/2019 5:20:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmpWeekEnd](
	[EmpWeekEndId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[weekend] [varchar](25) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[Remarks] [varchar](250) NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL CONSTRAINT [DF_EmpWeekEnd_IsInactive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_EmpWeekEnd_IsDeleted]  DEFAULT ((0))
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-------------------------------------




/****** Object:  StoredProcedure [dbo].[ProcEmpWeekEndINSERT]    Script Date: 08/03/2019 5:19:23 PM ******/
DROP PROCEDURE [dbo].[ProcEmpWeekEndINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcEmpWeekEndINSERT]    Script Date: 08/03/2019 5:19:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





--[ProcNomineeInformationINSERT]'0','1','Sayem Ahmed','','Basher','naa','12/02/2019','ax','01730358095','','324234','3','100','1','3','::1','0','','0','0','I'

CREATE   proc [dbo].[ProcEmpWeekEndINSERT]

	@trAutoId int,
	@EmpAutoId numeric(18, 0),	
	@weekend	varchar(50)	,
	@effectiveDate datetime,
	@Remarks	varchar(250)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO EmpWeekEnd  
		VALUES((Select isnull(max(EmpWeekEndId),0)+1 From EmpWeekEnd),
				@EmpAutoId,@weekend,@effectiveDate,@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	dbo.EmpAcademicInformation WHERE	ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update EmpWeekEnd  
SET EmpAutoId=@EmpAutoId, weekend=weekend,effectiveDate=effectiveDate,Remarks=@Remarks,

	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where EmpWeekEndId=@trAutoId		  

End



SELECT @Isduplicate





GO


