﻿
/****** Object:  StoredProcedure [dbo].[ProcSearchInfoForfrmLateApprove]    Script Date: 05/04/2019 10:59:26 AM ******/
DROP PROCEDURE [dbo].[ProcSearchInfoForfrmLateApprove]
GO

/****** Object:  StoredProcedure [dbo].[ProcSearchInfoForfrmLateApprove]    Script Date: 05/04/2019 10:59:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





--- ProcSearchInfoForfrmLateApprove'345','1','2019-04-01','N'




CREATE  proc [dbo].[ProcSearchInfoForfrmLateApprove]
@empId varchar(26),
@companyAutoId numeric(18),
@accessDate datetime,
@status as char(1)

as

		select 
		isnull(fd.autoId,0) as autoId,
		isnull(e.CardNo,'') as empId,
		isnull(e.EmployeeName,'') as empName,
		isnull(d.designation,'') as designation,
		fd.accessDate as accessDate,
		fd.inTime as inTime,
		fd.standardAccessTime as standardAccessTime,
		fd.lateStatus as lateStatus
		
		from  AttendaceFilteredData fd
		Left Outer Join EmployeePersonalInfo e
		on fd.cardId = e.CardNo
		Left Outer Join EmployeeOfficialInfo eo
		on e.EmployeePersonalInfoId = eo.EmpAutoId		
		Left Outer Join DesignationInfo d
		on eo.DesignationAutoId = d.DesignationInfoId
		
		where e.CompanyAutoId = @companyAutoId
		and e.CardNo = @empId 
		and fd.accessDate=@accessDate
		and fd.inTime > fd.standardAccessTime
	
GO


