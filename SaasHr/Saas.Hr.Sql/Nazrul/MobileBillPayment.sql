﻿
/****** Object:  Table [dbo].[TaxCredit]    Script Date: 21/06/2019 12:24:35 PM ******/
DROP TABLE [dbo].[MobileBillPayment]
GO

/****** Object:  Table [dbo].[TaxCredit]    Script Date: 21/06/2019 12:24:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MobileBillPayment](
	[MobileBillPaymentId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[Month] [varchar](20) NOT NULL,
	[Year] [varchar](4) NOT NULL,
	[LocalBillAmount] [numeric](18, 2) NOT NULL,
	[ISDBillAmount] [numeric](18, 2) NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MobileBillPayment] PRIMARY KEY CLUSTERED 
(
	[MobileBillPaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-------------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxCreditINSERT]    Script Date: 21/06/2019 12:28:51 PM ******/
DROP PROCEDURE [dbo].[ProcMobileBillPaymentINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditINSERT]    Script Date: 21/06/2019 12:28:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcMobileBillPaymentINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@PaymentDate datetime ,
	@Month	varchar(20)	,
	@Year	varchar(4)	,
	@LocalBillAmount numeric(18, 2),	
	@ISDBillAmount numeric(18, 2) ,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO MobileBillPayment  
		VALUES((Select isnull(max(MobileBillPaymentId),0)+1 From MobileBillPayment),
				@EmpAutoId,@PaymentDate  ,@Month,@Year	,@LocalBillAmount ,	@ISDBillAmount ,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update MobileBillPayment  
SET EmpAutoId=@EmpAutoId, PaymentDate=@PaymentDate, Month=@Month ,Year=@Year,
	LocalBillAmount=@LocalBillAmount , ISDBillAmount=@ISDBillAmount ,Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where MobileBillPaymentId=@trAutoId		  

End



SELECT @Isduplicate




GO


--------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxCreditSelect]    Script Date: 21/06/2019 12:32:55 PM ******/
DROP PROCEDURE [dbo].[ProcMobileBillPaymentSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditSelect]    Script Date: 21/06/2019 12:32:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcMobileBillPaymentSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
sa.PaymentDate as PaymentDate,
m.MonthName as MonthOfBill,
sa.Year as Year,
sa.LocalBillAmount,
sa.ISDBillAmount,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.MobileBillPaymentId


FROM MobileBillPayment sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
left outer join T_Month m 
on sa.Month=m.MonthName
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
	and sa.PaymentDate between @Fromdate and @Todate





GO


















