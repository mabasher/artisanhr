﻿
/****** Object:  StoredProcedure [dbo].[P_Contact_Load_Search_for_Costing]    Script Date: 12/05/2019 1:02:25 PM ******/
DROP PROCEDURE [dbo].[ProCEmployeeLoad]
GO

/****** Object:  StoredProcedure [dbo].[P_Contact_Load_Search_for_Costing]    Script Date: 12/05/2019 1:02:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--   [P_Contact_Load_Search_for_Costing]'8'


CREATE    PROCEDURE [dbo].[ProCEmployeeLoad]   

	@Company_Id numeric(9)
	--@Order_Id varchar(50)

AS

SELECT  e.EmployeePersonalInfoId as ItemId,
     	e.EmployeeName +' '+ e.PIN as ItemName

FROM  dbo.EmployeePersonalInfo e 

WHERE e.CompanyAutoId=@Company_Id 
    
GO


