﻿

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 26/07/2019 4:13:31 PM ******/
DROP PROCEDURE [dbo].[ProcLeaveWithPaySelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 26/07/2019 4:13:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO









---[procPIForLC] '6'

CREATE   proc [dbo].[ProcLeaveWithPaySelect]

    @EmpAutoId numeric(18,0)
    
as
set nocount on

SELECT  
lwp.LeaveWithPayId,
lt.leaveType, 
lwp.leaveTypeAutoId,
lwp.calType,
lwp.fromDate,
lwp.fromTime,
lwp.hrsDays,
lwp.toDate,
lwp.toTime,
lwp.Remarks,
emp.EmployeePersonalInfoId as EmpAutoId


FROM LeaveWithPay lwp
left outer join [dbo].[LeaveType] lt
ON lwp.leaveTypeAutoId=lt.LeaveTypeId
left outer join EmployeePersonalInfo emp
ON lwp.EmpAutoId=emp.EmployeePersonalInfoId

WHERE lwp.EmpAutoId=@EmpAutoId
--group by 
--p.autoId,
--p.OrderNo,
--p.OrderDate







GO
-------------------------------

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 26/07/2019 4:45:07 PM ******/
DROP PROCEDURE [dbo].[ProcLeaveWithOutPaySelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 26/07/2019 4:45:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO










---[procPIForLC] '6'

CREATE   proc [dbo].[ProcLeaveWithOutPaySelect]

    @EmpAutoId numeric(18,0)
    
as
set nocount on

SELECT  
lwp.LeaveWithOutPayId,
lt.leaveType, 
lwp.leaveTypeAutoId,
lwp.calType,
lwp.fromDate,
lwp.fromTime,
lwp.hrsDays,
lwp.toDate,
lwp.toTime,
lwp.Remarks,
emp.EmployeePersonalInfoId as EmpAutoId


FROM LeaveWithOutPay lwp
left outer join [dbo].[LeaveType] lt
ON lwp.leaveTypeAutoId=lt.LeaveTypeId
left outer join EmployeePersonalInfo emp
ON lwp.EmpAutoId=emp.EmployeePersonalInfoId

WHERE lwp.EmpAutoId=@EmpAutoId
--group by 
--p.autoId,
--p.OrderNo,
--p.OrderDate



GO


------------------------------


/****** Object:  Table [dbo].[LeaveWithOutPay]    Script Date: 26/07/2019 6:31:52 PM ******/
DROP TABLE [dbo].[LeaveWithOutPay]
GO

/****** Object:  Table [dbo].[LeaveWithOutPay]    Script Date: 26/07/2019 6:31:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LeaveWithOutPay](
	[LeaveWithOutPayId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[leaveTypeAutoId] [numeric](18, 0) NOT NULL,
	[calType] [varchar](10) NOT NULL,
	[fromDate] [datetime] NOT NULL,
	[fromTime] [time](7) NOT NULL,
	[hrsDays] [numeric](18, 2) NOT NULL,
	[toDate] [datetime] NOT NULL,
	[toTime] [time](7) NOT NULL,
	[Remarks] [varchar](250) NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-----------------------------------------


/****** Object:  Table [dbo].[LeaveWithOutPayDays]    Script Date: 26/07/2019 6:32:18 PM ******/
DROP TABLE [dbo].[LeaveWithOutPayDays]
GO

/****** Object:  Table [dbo].[LeaveWithOutPayDays]    Script Date: 26/07/2019 6:32:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LeaveWithOutPayDays](
	[LeaveWithOutPayDaysId] [bigint] NOT NULL,
	[LeaveWithOutPayId] [bigint] NOT NULL,
	[LeaveDate] [datetime] NOT NULL,
 CONSTRAINT [PK_LeaveWithOutPayDays] PRIMARY KEY CLUSTERED 
(
	[LeaveWithOutPayDaysId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


----------------------------------


/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPayINSERT]    Script Date: 26/07/2019 4:52:24 PM ******/
DROP PROCEDURE [dbo].[ProcLeaveWithOutPayINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPayINSERT]    Script Date: 26/07/2019 4:52:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







--[ProcLeaveWithPayINSERT]'0','1','Sayem Ahmed','','Basher','naa','12/02/2019','ax','01730358095','','324234','3','100','1','3','::1','0','','0','0','I'

CREATE PROC [dbo].[ProcLeaveWithOutPayINSERT]
    @trAutoId NUMERIC(18, 0),
    @EmpAutoId NUMERIC(18, 0),
    @leaveTypeAutoId NUMERIC(18, 0),
    @calType VARCHAR(10),
    @fromDate DATETIME,
    @fromTime TIME(7),
    @hrsDays NUMERIC(18, 2),
    @toDate DATETIME,
    @toTime TIME(7),
    @Remarks VARCHAR(250),
    @CompanyAutoId INT,
    @CreateBy INT,
    @CreateIp NVARCHAR(50),
    @trType NVARCHAR(50)

--with encryption   LeaveWithPayDays
AS
SET NOCOUNT ON;

DECLARE @Isduplicate NVARCHAR(1);

DECLARE @date DATE;

SET @date = @fromDate;
SET @Isduplicate = N'';

IF (@trType = 'I')
BEGIN
    --if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
    DECLARE @leaveID AS BIGINT;
    SET @leaveID =
    (
        SELECT ISNULL(MAX(LeaveWithOutPayId), 0) + 1 FROM LeaveWithOutPay
    );
    INSERT INTO LeaveWithOutPay
    VALUES
    (   @leaveID, @EmpAutoId, @leaveTypeAutoId, @calType, @fromDate, @fromTime, @hrsDays, @toDate, @toTime, @Remarks,
        @CompanyAutoId, @CreateBy, GETDATE(), @CreateIp, NULL, NULL, NULL, '0', --@IsInactive,
        '0'                                                                     --@IsDeleted
        );

    WHILE (@date <= @toDate)
    BEGIN

        INSERT INTO LeaveWithOutPayDays
        VALUES
        ((SELECT ISNULL(MAX(LeaveWithOutPayDaysId), 0) + 1 FROM LeaveWithOutPayDays), @leaveID, @date);

        SET @date = DATEADD(DAY,1,@date);
    END;
END;

IF (@trType = 'U')
BEGIN
    --if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
    UPDATE LeaveWithOutPay
    SET EmpAutoId = @EmpAutoId,
        leaveTypeAutoId = @leaveTypeAutoId,
        calType = @calType,
        fromDate = @fromDate,
        fromTime = @fromTime,
        hrsDays = @hrsDays,
        toDate = @toDate,
        @toTime = @toTime,
        Remarks = @Remarks,
        UpdateBy = @CreateBy,
        UpdateTime = GETDATE(),
        UpdateIp = @CreateIp
    WHERE LeaveWithOutPayId = @trAutoId;

    DELETE FROM dbo.LeaveWithOutPayDays
    WHERE LeaveWithOutPayId = @trAutoId;

    WHILE (@date <= @toDate)
    BEGIN

        INSERT INTO LeaveWithOutPayDays
        VALUES
        (
            (
                SELECT ISNULL(MAX(LeaveWithOutPayDaysId), 0) + 1 FROM LeaveWithOutPayDays
            ), @trAutoId, @date);
        SET @date = DATEADD(DAY,1,@date);
    END;
END;



SELECT @Isduplicate;






GO


-------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 26/07/2019 4:45:07 PM ******/
DROP PROCEDURE [dbo].[ProcLeaveWithOutPaySelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcLeaveWithPaySelect]    Script Date: 26/07/2019 4:45:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





--- [ProcLeaveWithOutPaySelect] '92'


--- SELECT * FROM EmployeePersonalInfo Where EmployeePersonalInfoId='26'



CREATE   proc [dbo].[ProcLeaveWithOutPaySelect]

    @EmpAutoId numeric(18,0)
    
as
set nocount on

SELECT  
lwp.LeaveWithOutPayId,
lt.leaveType, 
lwp.leaveTypeAutoId,
lwp.calType,
lwp.fromDate,
lwp.fromTime,
lwp.hrsDays,
lwp.toDate,
lwp.toTime,
lwp.Remarks,
emp.EmployeePersonalInfoId as EmpAutoId


FROM LeaveWithOutPay lwp
left outer join [dbo].[LeaveType] lt
ON lwp.leaveTypeAutoId=lt.LeaveTypeId
left outer join EmployeePersonalInfo emp
ON lwp.EmpAutoId=emp.EmployeePersonalInfoId

WHERE lwp.EmpAutoId=@EmpAutoId
--group by 
--p.autoId,
--p.OrderNo,
--p.OrderDate








GO










































