﻿
/****** Object:  Table [dbo].[TaxableHead]    Script Date: 29/05/2019 4:16:25 PM ******/
DROP TABLE [dbo].[TaxableHead]
GO

/****** Object:  Table [dbo].[TaxableHead]    Script Date: 29/05/2019 4:16:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaxableHead](
	[TaxableHeadId] [numeric](18, 0) NOT NULL,
	[payHeadNameAutoId] [numeric](18, 0) NOT NULL,
	[percentageValue] [decimal](18, 2) NOT NULL,
	[fixedAmount] [decimal](18, 2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[insertDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TaxableHead] PRIMARY KEY CLUSTERED 
(
	[TaxableHeadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-----------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
DROP PROCEDURE [dbo].ProcTaxableHeadINSERT
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO








-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].ProcTaxableHeadINSERT

	@trAutoId numeric (18, 0),
	@payHeadNameAutoId numeric(18, 0) ,
	@percentageValue decimal(18, 2) ,
	@fixedAmount decimal(18, 2) ,
	@effectiveDate datetime ,
	@Remarks varchar(max) ,

	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO TaxableHead  
		VALUES((Select isnull(max(TaxableHeadId),0)+1 From TaxableHead),
				 @payHeadNameAutoId,@fixedAmount ,@percentageValue, @effectiveDate, getdate() ,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update TaxableHead  
SET payHeadNameAutoId=@payHeadNameAutoId,
	percentageValue=@percentageValue,
	fixedAmount=@fixedAmount,
	effectiveDate=@effectiveDate,
	Remarks=@Remarks,

	UpdateBy=@CreateBy ,
	UpdateTime=getdate() ,
	UpdateIp=@CreateIp 



Where TaxableHeadId=@trAutoId		  

End



SELECT @Isduplicate


GO

------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxableHeadSelect]    Script Date: 29/05/2019 4:35:03 PM ******/
DROP PROCEDURE [dbo].[ProcTaxableHeadSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxableHeadSelect]    Script Date: 29/05/2019 4:35:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





-- [ProcTaxableHeadSelect]'03/29/2019','05/29/2019'


CREATE   proc [dbo].[ProcTaxableHeadSelect]
	

	--@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 

phi.PayHeadName,
sa.payHeadNameAutoId,
sa.percentageValue as percentageValue,
sa.fixedAmount as Amount,
sa.effectiveDate as EffictiveDate,
sa.Remarks,
sa.TaxableHeadId

FROM TaxableHead sa 
left outer join PayHeadInfo phi
on sa.payHeadNameAutoId=phi.PayHeadInfoId
 

Where sa.effectiveDate between @Fromdate and @Todate



SET ANSI_NULLS ON


GO



