﻿

ALTER TABLE [dbo].[AttendanceBonusSetup] DROP CONSTRAINT [DF_AttendanceBonusSetup_IsDeleted]
GO

ALTER TABLE [dbo].[AttendanceBonusSetup] DROP CONSTRAINT [DF_AttendanceBonusSetup_IsInactive]
GO

/****** Object:  Table [dbo].[AttendanceBonusSetup]    Script Date: 15/03/2019 1:15:06 PM ******/
DROP TABLE [dbo].[AttendanceBonusSetup]
GO

/****** Object:  Table [dbo].[AttendanceBonusSetup]    Script Date: 15/03/2019 1:15:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AttendanceBonusSetup](
	[autoId] [numeric](18, 0) NOT NULL,
	[EffictiveDate] [datetime] NOT NULL,
	[EmployeeId] [numeric](18, 0) NOT NULL,
	[DepartmentAutoId] [numeric](18, 0) NOT NULL,	
	[DesignationId] [numeric](18, 0) NOT NULL,
	[EmployeeTypeId] [numeric](18, 0) NOT NULL,
	[BonusAmount] [numeric](18, 0) NOT NULL,
	[PresentRatio] [numeric](18, 0) NOT NULL,	
	[Remarks] [varchar](550) NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AttendanceBonusSetup] ADD  CONSTRAINT [DF_AttendanceBonusSetup_IsInactive]  DEFAULT ((0)) FOR [IsInactive]
GO

ALTER TABLE [dbo].[AttendanceBonusSetup] ADD  CONSTRAINT [DF_AttendanceBonusSetup_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO


-------------------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcStaffRequisitionForRecruitmentInsert]    Script Date: 15/03/2019 3:05:33 PM ******/
DROP PROCEDURE [dbo].[ProcAttendanceBonusSetupInsert]
GO

/****** Object:  StoredProcedure [dbo].[ProcStaffRequisitionForRecruitmentInsert]    Script Date: 15/03/2019 3:05:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- [ProcAttendanceBonusSetupInsert]'03/15/2019','0','1','1','2','1','1','Remarks','1','2','::1'


CREATE PROCEDURE [dbo].[ProcAttendanceBonusSetupInsert] 	

	--@autoId numeric(18, 0)	,
	@EffictiveDate as datetime,
	@EmployeeId numeric(18, 0)	,
	@DepartmentAutoId numeric(18, 0)	,
	@DesignationId numeric(18, 0)	,
	@EmployeeTypeId numeric(18, 0)	,
	@BonusAmount numeric(18, 0)	,
	@PresentRatio numeric(18, 0)	,
	@Remarks	varchar(550)	,	
	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50)	
	
AS
SET NOCOUNT ON

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''


--if exists (	SELECT referenceNumber FROM dbo.StaffRequisitionForRecruitment WHERE referenceNumber = @referenceNumber)
--Begin
--set @Isduplicate = 'D'
--END

if @Isduplicate != 'D'
Begin
	INSERT INTO
		dbo.AttendanceBonusSetup
	VALUES
		(
		(Select isnull(max(autoId),0)+1 From dbo.AttendanceBonusSetup),	
		
	@EffictiveDate ,
	@EmployeeId ,
	@DepartmentAutoId	,
	@DesignationId,
	@EmployeeTypeId 	,
	@BonusAmount ,
	@PresentRatio,
	@Remarks,
	
	@CompanyAutoId ,
	@CreateBy ,
	getdate() ,
	@CreateIp ,
	null,
	null,
	null,
	'0',--@IsInactive,
	'0'--@IsDeleted

		)
End

select @Isduplicate








GO


-------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcAttendanceBonusSetupUpdate]    Script Date: 15/03/2019 4:49:25 PM ******/
DROP PROCEDURE [dbo].[ProcAttendanceBonusSetupUpdate]
GO

/****** Object:  StoredProcedure [dbo].[ProcAttendanceBonusSetupUpdate]    Script Date: 15/03/2019 4:49:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








---[ProcExpProformaInvoiceInsert]'expPi-4304','11/21/2018','1','1','1','1','3','60 days','Additional Note','1','3'

CREATE PROCEDURE [dbo].[ProcAttendanceBonusSetupUpdate] 	

	@autoId numeric(18, 0)	,
	@EffictiveDate as datetime,
	@EmployeeId numeric(18, 0)	,
	@DepartmentAutoId numeric(18, 0)	,
	@DesignationId numeric(18, 0)	,
	@EmployeeTypeId numeric(18, 0)	,
	@BonusAmount numeric(18, 0)	,
	@PresentRatio numeric(18, 0)	,
	@Remarks	varchar(550)	,	
	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50)	
	
AS
SET NOCOUNT ON

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''


--if exists (	SELECT PINo FROM dbo.ExpProformaInvoice WHERE PINo = @PINo and SupplierId=@SupplierId and AutoId<>@AutoId)
--Begin
--set @Isduplicate = 'D'
--END

--if exists (	SELECT PINo FROM dbo.ExpProformaInvoice WHERE PINo = @PINo and SupplierId=@SupplierId and IsApprove='Y')
--Begin
--set @Isduplicate = 'D'
--END

if @Isduplicate != 'D'
Begin
	Update
		dbo.AttendanceBonusSetup

	Set 
	EffictiveDate =@EffictiveDate,
	EmployeeId =@EmployeeId	,
	DepartmentAutoId =@DepartmentAutoId	,
	DesignationId =@DesignationId,
	EmployeeTypeId =@EmployeeTypeId	,
	BonusAmount =@BonusAmount,
	PresentRatio =	@PresentRatio,
	Remarks=@Remarks

		Where AutoId=@AutoId
End

select @Isduplicate


GO

----------------------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcRequiredManpowerwithDesignationSelect]    Script Date: 15/03/2019 2:32:08 PM ******/
DROP PROCEDURE [dbo].[ProcAttendanceBonusSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcRequiredManpowerwithDesignationSelect]    Script Date: 15/03/2019 2:32:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





--- [ProcRequiredManpowerwithDesignationSelect] '1'

CREATE   proc [dbo].[ProcAttendanceBonusSelect]

   -- @RecruitmentAutoId numeric(18,0)
    
as
set nocount on

SELECT  
	   attb.AutoId as AutoId,
	   attb.EffictiveDate as EffictiveDate,
isnull(emp.EmployeeName,'') as EmployeeName,
isnull(attb.EmployeeId,0) as EmployeeId,
isnull(dep.Department,'') as Department, 
isnull(attb.DepartmentAutoId,0) as DepartmentAutoId,
isnull(desig.Designation,'') as Designation, 
isnull(attb.DesignationId,0) as DesignationId,
isnull(eType.TypeName ,'') as EmpTypeName,
isnull(attb.EmployeeTypeId,0) as EmployeeTypeId,
isnull(attb.BonusAmount,0) as BonusAmount,
isnull(attb.PresentRatio,0) as PresentRatio,
isnull(attb.Remarks,'') as Remarks 


FROM [dbo].AttendanceBonusSetup attb

left outer join [dbo].EmployeePersonalInfo emp
ON attb.EmployeeId=emp.EmployeePersonalInfoId

left outer join [dbo].[DesignationInfo] desig
ON attb.DesignationId=desig.DesignationInfoId

left outer join [dbo].DepartmentInfo dep
ON attb.DepartmentAutoId=dep.DepartmentInfoId

left outer join [dbo].EmployeeType eType
ON attb.EmployeeTypeId=eType.EmployeeTypeId

--WHERE isnull(srd.RecruitmentAutoId,'0')=RecruitmentAutoId
--group by 
--p.autoId,
--p.OrderNo,
--p.OrderDate







GO

---------------------------------------------------


/****** Object:  Table [dbo].[CompanyWeekEnd]    Script Date: 15/03/2019 5:51:21 PM ******/
DROP TABLE [dbo].[ShiftWorkingAllowance]
GO

/****** Object:  Table [dbo].[CompanyWeekEnd]    Script Date: 15/03/2019 5:51:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShiftWorkingAllowance](
	[ShiftWorkingId] [numeric](18, 0) NOT NULL,
	[ShiftId] [varchar](25) NOT NULL,
	[Amount] [numeric] (9,2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[Remarks] [nvarchar](150) NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcCompanyWeekEndINSERT]    Script Date: 15/03/2019 6:01:25 PM ******/
DROP PROCEDURE [dbo].[ProcShiftWorkingAllowanceINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcCompanyWeekEndINSERT]    Script Date: 15/03/2019 6:01:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





--[ProcNomineeInformationINSERT]'0','1','Sayem Ahmed','','Basher','naa','12/02/2019','ax','01730358095','','324234','3','100','1','3','::1','0','','0','0','I'

CREATE   proc [dbo].[ProcShiftWorkingAllowanceINSERT]

	@trAutoId int,
	@ShiftId	numeric(9)	,
	@Amount as numeric(9,2),
	@effectiveDate datetime,
	@CompanyAutoId int,
	@Remarks	varchar(250)	,
	
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO ShiftWorkingAllowance  
		VALUES((Select isnull(max(ShiftWorkingId),0)+1 From ShiftWorkingAllowance),
				@ShiftId,@Amount,@effectiveDate,@CompanyAutoId,@Remarks, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	dbo.EmpAcademicInformation WHERE	ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update ShiftWorkingAllowance  
SET ShiftId=@ShiftId, Amount=@Amount, effectiveDate=@effectiveDate,Remarks=@Remarks,

	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where ShiftWorkingId=@trAutoId		  

End



SELECT @Isduplicate





GO

-------------------------------------------------------------------


ALTER TABLE [dbo].[OTFixedAllowanceSetup] DROP CONSTRAINT [DF_OTFixedAllowanceSetup_IsDeleted]
GO

ALTER TABLE [dbo].[OTFixedAllowanceSetup] DROP CONSTRAINT [DF_OTFixedAllowanceSetup_IsInactive]
GO

/****** Object:  Table [dbo].[OTFixedAllowanceSetup]    Script Date: 16/03/2019 11:43:43 AM ******/
DROP TABLE [dbo].[OTFixedAllowanceSetup]
GO

/****** Object:  Table [dbo].[OTFixedAllowanceSetup]    Script Date: 16/03/2019 11:43:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OTFixedAllowanceSetup](
	[OTFixedAllowanceSetupId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[Remarks] [varchar](250) NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OTFixedAllowanceSetup] ADD  CONSTRAINT [DF_OTFixedAllowanceSetup_IsInactive]  DEFAULT ((0)) FOR [IsInactive]
GO

ALTER TABLE [dbo].[OTFixedAllowanceSetup] ADD  CONSTRAINT [DF_OTFixedAllowanceSetup_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO


-------------------------------------------------


ALTER TABLE [dbo].[OTFixedAllowanceSetup] DROP CONSTRAINT [DF_OTFixedAllowanceSetup_IsDeleted]
GO

ALTER TABLE [dbo].[OTFixedAllowanceSetup] DROP CONSTRAINT [DF_OTFixedAllowanceSetup_IsInactive]
GO

/****** Object:  Table [dbo].[OTFixedAllowanceSetup]    Script Date: 16/03/2019 11:43:43 AM ******/
DROP TABLE [dbo].[OTFixedAllowanceSetup]
GO

/****** Object:  Table [dbo].[OTFixedAllowanceSetup]    Script Date: 16/03/2019 11:43:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OTFixedAllowanceSetup](
	[OTFixedAllowanceSetupId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[Remarks] [varchar](250) NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OTFixedAllowanceSetup] ADD  CONSTRAINT [DF_OTFixedAllowanceSetup_IsInactive]  DEFAULT ((0)) FOR [IsInactive]
GO

ALTER TABLE [dbo].[OTFixedAllowanceSetup] ADD  CONSTRAINT [DF_OTFixedAllowanceSetup_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO


--------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcEmpWeekEndINSERT]    Script Date: 16/03/2019 11:43:15 AM ******/
DROP PROCEDURE [dbo].[ProcOTFixedAllowanceINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcEmpWeekEndINSERT]    Script Date: 16/03/2019 11:43:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO






-- [ProcOTFixedAllowanceINSERT]'0','1','100','16/03/2019','Remarks','1','2','::1','0','','0','0','I'


CREATE   proc [dbo].[ProcOTFixedAllowanceINSERT]

	@trAutoId int,
	@EmpAutoId numeric(18, 0),	
	@Amount	numeric(18, 2),
	@effectiveDate datetime,
	@Remarks	varchar(250)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO OTFixedAllowanceSetup  
		VALUES((Select isnull(max(OTFixedAllowanceSetupId),0)+1 From OTFixedAllowanceSetup),
				@EmpAutoId,@Amount,@effectiveDate,@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	dbo.EmpAcademicInformation WHERE	ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update OTFixedAllowanceSetup  
SET EmpAutoId=@EmpAutoId, Amount=@Amount,effectiveDate=effectiveDate,Remarks=@Remarks,
	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where OTFixedAllowanceSetupId=@trAutoId		  

End



SELECT @Isduplicate






GO


---------------------------------



















