﻿

/****** Object:  Table [dbo].[T_RawData]    Script Date: 29/03/2019 4:41:57 PM ******/
DROP TABLE [dbo].[T_RawData]
GO

/****** Object:  Table [dbo].[T_RawData]    Script Date: 29/03/2019 4:41:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[T_RawData](
	[autoId] [numeric](18, 0)  NOT NULL,
	[cardId] [varchar](50) NOT NULL,
	[accessDate] [datetime] NOT NULL,
	[accessTime] [datetime] NOT NULL,
	[filtered] [char](1) NOT NULL,
	[earlyArrival] [char](1) NOT NULL,
	[isManual] [char](1) NOT NULL,
	[node] [varchar](50) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcAcademicInfoINSERT]    Script Date: 29/03/2019 3:39:51 PM ******/
DROP PROCEDURE [dbo].[ProcDailyAttandenceINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcAcademicInfoINSERT]    Script Date: 29/03/2019 3:39:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcDailyAttandenceINSERT]

	
	@cardId varchar(50),
	@date datetime,
	@time datetime,
	@OutTime datetime


--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''
declare @date1 datetime
SET @time=@date+' '+@time
SET @OutTime=@date+' '+@OutTime

set @date1=@date
IF (@time > @OutTime)
	Begin
		set @date1=dateadd(DAY,1,@date)
		set @OutTime=dateadd(DAY,1,@OutTime)
	END

Insert into T_RawData Values((Select isnull(max(autoId),0)+1 From T_RawData),@cardId,@date,@time,'N','N','Y','0')
Insert into T_RawData Values((Select isnull(max(autoId),0)+1 From T_RawData),@cardId,@date1,@OutTime,'N','N','Y','0')



SELECT @Isduplicate




GO


---------------------------------








/****** Object:  StoredProcedure [dbo].[proc_searchInfoForfrmLateApprove]    Script Date: 30/03/2019 12:03:11 PM ******/
DROP PROCEDURE [dbo].[ProcSearchInfoForfrmLateApprove]
GO

/****** Object:  StoredProcedure [dbo].[proc_searchInfoForfrmLateApprove]    Script Date: 30/03/2019 12:03:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




---proc_searchInfoForfrmLateApprove'222','1','04/19/2010','N'

CREATE  proc [dbo].[ProcSearchInfoForfrmLateApprove]
@empId varchar(26),
@companyAutoId numeric(18),
@accessDate datetime,
@status as char(1)

as
if @empId = ''
begin
	if @status = 'N'
	begin
		select 
		isnull(fd.autoId,0) as autoId,
		isnull(e.CardNo,'') as empId,
		isnull(e.EmployeeName,'') as empName,
		isnull(d.designation,'') as designation,
		fd.accessDate as accessDate,
		fd.inTime as inTime,
		fd.standardAccessTime as standardAccessTime,
		fd.lateStatus as lateStatus
		
		from  AttendaceFilteredData fd
		Left Outer Join EmployeePersonalInfo e
		on fd.cardId = e.CardNo
		Left Outer Join EmployeeOfficialInfo eo
		on e.EmployeePersonalInfoId = eo.EmpAutoId
		
		Left Outer Join DesignationInfo d
		on eo.DesignationAutoId = d.DesignationInfoId
		
		where e.CompanyAutoId = @companyAutoId
		and fd.inTime > fd.standardAccessTime

	end
	else if @status = 'Y'
	begin 
		select 
		isnull(fd.autoId,0) as autoId,
		isnull(e.CardNo,'') as empId,
		isnull(e.EmployeeName,'') as empName,
		isnull(d.designation,'') as designation,
		fd.accessDate as accessDate,
		fd.inTime as inTime,
		fd.standardAccessTime as standardAccessTime,
		fd.lateStatus as lateStatus
	    
		
		from  AttendaceFilteredData fd
		Left Outer Join EmployeePersonalInfo e
		on fd.cardId = e.CardNo
		Left Outer Join EmployeeOfficialInfo eo
		on e.EmployeePersonalInfoId = eo.EmpAutoId
		
		Left Outer Join DesignationInfo d
		on eo.DesignationAutoId = d.DesignationInfoId
		
		where e.CompanyAutoId = @companyAutoId
		and fd.accessDate = @accessDate
		and fd.inTime > fd.standardAccessTime
	end
end
else
begin
	if @status = 'N'
	begin
		select 
		isnull(fd.autoId,0) as autoId,
		isnull(e.CardNo,'') as empId,
		isnull(e.EmployeeName,'') as empName,
		isnull(d.designation,'') as designation,
		fd.accessDate as accessDate,
		fd.inTime as inTime,
		fd.standardAccessTime as standardAccessTime,
		fd.lateStatus as lateStatus
		
		from  AttendaceFilteredData fd
		Left Outer Join EmployeePersonalInfo e
		on fd.cardId = e.CardNo
		Left Outer Join EmployeeOfficialInfo eo
		on e.EmployeePersonalInfoId = eo.EmpAutoId
		
		Left Outer Join DesignationInfo d
		on eo.DesignationAutoId = d.DesignationInfoId
		
		where e.CompanyAutoId = @companyAutoId
		and e.CardNo = @empId 
		and fd.inTime > fd.standardAccessTime
	end
	else if @status = 'Y'
	begin 
		select 
		isnull(fd.autoId,0) as autoId,
		isnull(e.CardNo,'') as empId,
		isnull(e.EmployeeName,'') as empName,
		isnull(d.designation,'') as designation,
		fd.accessDate as accessDate,
		fd.inTime as inTime,
		fd.standardAccessTime as standardAccessTime,
		fd.lateStatus as lateStatus

		from  AttendaceFilteredData fd
		Left Outer Join EmployeePersonalInfo e
		on fd.cardId = e.CardNo
		Left Outer Join EmployeeOfficialInfo eo
		on e.EmployeePersonalInfoId = eo.EmpAutoId
		
		Left Outer Join DesignationInfo d
		on eo.DesignationAutoId = d.DesignationInfoId
		
		where e.CompanyAutoId = @companyAutoId		
		and e.CardNo = @empId 
		and fd.accessDate = @accessDate
		and fd.inTime > fd.standardAccessTime
	end
end





















GO











