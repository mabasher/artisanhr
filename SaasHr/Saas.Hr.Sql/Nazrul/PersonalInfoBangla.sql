﻿.
/****** Object:  Table [dbo].[EmployeePersonalBanglaInfo]    Script Date: 25/05/2019 2:14:54 PM ******/
DROP TABLE [dbo].[EmployeePersonalBanglaInfo]
GO

/****** Object:  Table [dbo].[EmployeePersonalBanglaInfo]    Script Date: 25/05/2019 2:14:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmployeePersonalBanglaInfo](
	[EmployeePersonalBanglaInfoId] [int] NOT NULL,
	[EmployeeAutoId] [int] NOT NULL,
	[PIN] [nvarchar](20) NOT NULL,
	[EmployeeName] [nvarchar](100) NOT NULL,
	[FatherName] [nvarchar](100) NOT NULL,
	[MotherName] [nvarchar](100) NOT NULL,
	[PresentAddress] [nvarchar](500) NOT NULL,
	[PermanentAddress] [nvarchar](500) NOT NULL,
	[CompanyAutoId] [numeric](18, 0) NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL CONSTRAINT [DF_EmployeePersonalBanglaInfo_IsInactive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_EmployeePersonalBanglaInfo_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_EmployeePersonalBanglaInfo] PRIMARY KEY CLUSTERED 
(
	[EmployeePersonalBanglaInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_EmployeePersonalBanglaInfo_PIN] UNIQUE NONCLUSTERED 
(
	[PIN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


---------------------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcChildInformationINSERT]    Script Date: 25/05/2019 10:25:01 AM ******/
DROP PROCEDURE [dbo].[ProcEmployeePersonalBanglaInfoINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcChildInformationINSERT]    Script Date: 25/05/2019 10:25:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcChildInformationINSERT]'0','7','Child Name','Father's Name','Mother's Name :','11/05/2019','Address','Contact No','E-mail Address','National Id/ Birth Cert. :','1','1','2','::1','0','','0','0','I'



CREATE   proc [dbo].[ProcEmployeePersonalBanglaInfoINSERT]

	@trAutoId int,
	@EmployeeAutoId numeric(9),
	@PIN nvarchar (100)  ,
	@EmployeeName nvarchar (100)  ,
	@FatherName nvarchar (100)  ,
	@MotherName nvarchar (100)  ,
	@PresentAddress nvarchar (max)  ,
	@PermanentAddress nvarchar (max)  ,
				
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on
Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''
if (@trType='I')
Begin
--if exists (SELECT  EmployeeAutoId=@EmployeeAutoId) Begin set @Isduplicate = 'D' END
INSERT INTO EmployeePersonalBanglaInfo  
		VALUES((Select isnull(max(EmployeePersonalBanglaInfoId),0)+1 From EmployeePersonalBanglaInfo),
				@EmployeeAutoId,	@PIN,@EmployeeName, @FatherName, @MotherName ,
				@PresentAddress, @PermanentAddress,
				@CompanyAutoId ,@CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT EmployeeAutoId=@EmployeeAutoId and EmployeePersonalBanglaInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update EmployeePersonalBanglaInfo  
SET PIN=@PIN, EmployeeName=@EmployeeName,FatherName=@FatherName,MotherName=@MotherName ,
	PresentAddress=@PresentAddress, PermanentAddress=@PermanentAddress,
	UpdateBy=@UpdateBy ,
	UpdateTime=getdate() ,
	UpdateIp=@UpdateIp 
Where EmployeeAutoId=@EmployeeAutoId		  

End



SELECT @Isduplicate



GO





