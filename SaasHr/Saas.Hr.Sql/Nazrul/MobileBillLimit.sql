﻿
/****** Object:  Table [dbo].[TaxCredit]    Script Date: 21/06/2019 12:24:35 PM ******/
DROP TABLE [dbo].[MobileBillLimit]
GO

/****** Object:  Table [dbo].[TaxCredit]    Script Date: 21/06/2019 12:24:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MobileBillLimit](
	[MobileBillLimitId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[DesignationId] [int] NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[eEffectiveDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MobileBillLimit] PRIMARY KEY CLUSTERED 
(
	[MobileBillLimitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


---------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditINSERT]    Script Date: 21/06/2019 12:28:51 PM ******/
DROP PROCEDURE [dbo].[ProcMobileBillLimitINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditINSERT]    Script Date: 21/06/2019 12:28:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcMobileBillLimitINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@DesignationId numeric(18, 0),	
	@Amount numeric(18, 2) ,
	@eEffectiveDate datetime ,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO MobileBillLimit  
		VALUES((Select isnull(max(MobileBillLimitId),0)+1 From MobileBillLimit),
				@EmpAutoId,@DesignationId,@Amount ,@eEffectiveDate,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update MobileBillLimit  
SET EmpAutoId=@EmpAutoId,DesignationId=@DesignationId,Amount =@Amount , eEffectiveDate=@eEffectiveDate,   Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where MobileBillLimitId=@trAutoId		  

End



SELECT @Isduplicate




GO


-------------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditSelect]    Script Date: 21/06/2019 12:32:55 PM ******/
DROP PROCEDURE [dbo].[ProcMobileBillLimitSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditSelect]    Script Date: 21/06/2019 12:32:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcMobileBillLimitSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
de.Designation as Designation,
sa.DesignationId,
sa.Amount,
sa.eEffectiveDate as EffictiveDate,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.MobileBillLimitId


FROM MobileBillLimit sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
left outer join DesignationInfo de 
on sa.DesignationId=de.DesignationInfoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
--	and sa.sEffectiveDate between @Fromdate and @Todate

GO
------------------------------









