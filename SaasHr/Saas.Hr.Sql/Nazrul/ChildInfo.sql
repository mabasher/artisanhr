﻿

/****** Object:  Table [dbo].[EmployeeNomineeInfo]    Script Date: 11/05/2019 12:29:32 PM ******/
DROP TABLE [dbo].[EmployeeChildInfo]
GO

/****** Object:  Table [dbo].[EmployeeNomineeInfo]    Script Date: 11/05/2019 12:29:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EmployeeChildInfo](
	[EmployeeChildInfoId] [int] NOT NULL,
	[EmployeePersonalInfoId] [int] NOT NULL,
	[ChildName] [varchar](100) NOT NULL,
	[FatherName] [varchar](100) NOT NULL,
	[MotherName] [varchar](100) NOT NULL,
	[DOB] [datetime] NULL,
	[PermanentAddress] [varchar](500) NOT NULL,
	[PersonalContact] [varchar](100) NOT NULL,
	[PersonalMail] [varchar](100) NOT NULL,
	[NID] [varchar](100) NOT NULL,
	[RelationId] [int] NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL CONSTRAINT [DF_EmployeeChildInfo_IsInactive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_EmployeeChildInfo_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_EmployeeChildInfo] PRIMARY KEY CLUSTERED 
(
	[EmployeeChildInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-----------------------------------


/****** Object:  StoredProcedure [dbo].[proc_frm_UserCreation_INSERT_T_UserCreation]    Script Date: 16/02/2019 5:44:17 PM ******/
DROP PROCEDURE [dbo].[ProcChildInformationINSERT]
GO

/****** Object:  StoredProcedure [dbo].[proc_frm_UserCreation_INSERT_T_UserCreation]    Script Date: 16/02/2019 5:44:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


-- [ProcChildInformationINSERT]'0','7','Child Name','Father's Name','Mother's Name :','11/05/2019','Address','Contact No','E-mail Address','National Id/ Birth Cert. :','1','1','2','::1','0','','0','0','I'



CREATE   proc [dbo].[ProcChildInformationINSERT]

	@trAutoId int,
	@EmpAutoId int,
	@ChildName varchar (100)  ,
	@FatherName varchar (100)  ,
	@MotherName varchar (100)  ,
	@DOB datetime ,
	@PermanentAddress varchar (500)  ,
	@PersonalContact varchar (100)  ,
	@PersonalMail varchar (100)  ,
	@NID varchar (100)  ,
	@RelationId int   ,				
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on
Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''
if (@trType='I')
Begin
if exists (SELECT ChildName FROM	dbo.EmployeeChildInfo WHERE	ChildName=@ChildName and EmployeePersonalInfoId=@EmpAutoId) Begin set @Isduplicate = 'D' END
INSERT INTO EmployeeChildInfo  
		VALUES((Select isnull(max(EmployeeChildInfoId),0)+1 From EmployeeChildInfo),
				@EmpAutoId,	@ChildName,@FatherName,@MotherName ,@DOB  ,
				@PermanentAddress,@PersonalContact,@PersonalMail,@NID ,
				@RelationId ,@CompanyAutoId ,@CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
if exists (SELECT ChildName FROM	dbo.EmployeeChildInfo WHERE	ChildName=@ChildName and EmployeePersonalInfoId=@EmpAutoId and EmployeeChildInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update EmployeeChildInfo  
SET ChildName=@ChildName,FatherName=@FatherName,MotherName=@MotherName ,DOB=@DOB  ,
	PermanentAddress=@PermanentAddress,PersonalContact=@PersonalContact,PersonalMail=@PersonalMail,NID=@NID ,
	RelationId=@RelationId ,
	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where EmployeeChildInfoId=@trAutoId		  

End



SELECT @Isduplicate


GO


-----------------------














