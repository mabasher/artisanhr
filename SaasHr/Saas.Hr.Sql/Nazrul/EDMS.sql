﻿USE [DB_SaasHr]
GO

/****** Object:  Table [dbo].[EDMS]    Script Date: 29/05/2019 12:17:30 PM ******/
DROP TABLE [dbo].[EDMS]
GO

/****** Object:  Table [dbo].[EDMS]    Script Date: 29/05/2019 12:17:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EDMS](
	[EDMSId] [int] NOT NULL,
	[EmployeePersonalInfoId] [int] NOT NULL,
	[Date] [datetime] NULL,
	[URL]  [nvarchar](500) NULL,
	[Remarks]  [nvarchar](500) NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL CONSTRAINT [DF_EDMS_IsInactive]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_EDMS_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_EDMS] PRIMARY KEY CLUSTERED 
(
	[EDMSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcWorkExperienceInfoINSERT]    Script Date: 29/05/2019 1:51:45 PM ******/
DROP PROCEDURE [dbo].[ProcEDMSAttachmentINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcWorkExperienceInfoINSERT]    Script Date: 29/05/2019 1:51:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcChildInformationINSERT]'0','7','Child Name','Father's Name','Mother's Name :','11/05/2019','Address','Contact No','E-mail Address','National Id/ Birth Cert. :','1','1','2','::1','0','','0','0','I'



CREATE   proc [dbo].[ProcEDMSAttachmentINSERT]

	@trAutoId int,
	@EmployeePersonalInfoId numeric(18, 0),
	@Date datetime,
	@URL nvarchar(500) ,
	@Remarks nvarchar(500) ,
	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on
Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''
if (@trType='I')
Begin
--if exists (SELECT ChildName FROM	dbo.WorkExperienceInfo WHERE	ChildName=@ChildName and EmployeePersonalInfoId=@EmpAutoId) Begin set @Isduplicate = 'D' END
INSERT INTO EDMS  
		VALUES((Select isnull(max(EDMSId),0)+1 From EDMS),
				@EmployeePersonalInfoId,@Date  ,	@URL , @Remarks,@CompanyAutoId,
				@CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ChildName FROM	dbo.WorkExperienceInfo WHERE	ChildName=@ChildName and EmployeePersonalInfoId=@EmpAutoId and EmployeeChildInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update EDMS  
SET EmployeePersonalInfoId=@EmployeePersonalInfoId  ,	Date=@Date ,	URL=@URL  ,
	Remarks=@Remarks,
	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where EDMSId=@trAutoId		  

End



SELECT @Isduplicate




GO





















