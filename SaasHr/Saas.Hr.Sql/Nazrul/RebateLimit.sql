﻿

/****** Object:  Table [dbo].[T_RebateLimit]    Script Date: 31/05/2019 9:53:06 AM ******/
DROP TABLE [dbo].[RebateLimit]
GO

/****** Object:  Table [dbo].[T_RebateLimit]    Script Date: 31/05/2019 9:53:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RebateLimit](
	[RebateLimitId] [numeric](18, 0)  NOT NULL,
	[effectivedate] [datetime] NOT NULL,
	[Rate] [numeric](18, 0) NOT NULL,
	[Amount] [numeric](18, 0) NOT NULL,
	[rStep] [int] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL

) ON [PRIMARY]

GO


----------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
DROP PROCEDURE [dbo].ProcTaxRebateLimitINSERT
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcTaxRebateLimitINSERT]'0','05/31/2019','15','155500','1','','1','2','::1','I'



CREATE   proc [dbo].ProcTaxRebateLimitINSERT

	@trAutoId numeric (18, 0),

	@effectivedate datetime ,
	@Rate numeric(18, 0) ,
	@Amount numeric(18, 0) ,
	@rStep int ,
	@Remarks varchar(max) ,
	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO RebateLimit  
		VALUES((Select isnull(max(RebateLimitId),0)+1 From RebateLimit),
				@effectivedate ,@Rate,@Amount ,	@rStep ,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update RebateLimit  
SET
	effectivedate=@effectivedate,
	Rate=@Rate,
	Amount=@Amount,
	rStep=@rStep,
	Remarks=@Remarks,

	UpdateBy=@CreateBy ,
	UpdateTime=getdate() ,
	UpdateIp=@CreateIp 



Where RebateLimitId=@trAutoId		  

End



SELECT @Isduplicate









GO









