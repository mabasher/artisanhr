﻿
/****** Object:  Table [dbo].[SalaryInfo]    Script Date: 19/04/2019 4:10:04 PM ******/
DROP TABLE [dbo].[SalaryAddition]
GO

/****** Object:  Table [dbo].[SalaryInfo]    Script Date: 19/04/2019 4:10:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SalaryAddition](
	[SalaryAdditionId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[payHeadNameAutoId] [numeric](18, 0) NOT NULL,
	[Amount] [numeric](18, 6) NOT NULL,
	[sEffectiveDate] [datetime] NOT NULL,	
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL

 CONSTRAINT [PK_SalaryAddition] PRIMARY KEY CLUSTERED 
(
	[SalaryAdditionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-------------------------------------

/****** Object:  Table [dbo].[SalaryYearlyBonus]    Script Date: 20/04/2019 11:09:49 AM ******/
DROP TABLE [dbo].[SalaryYearlyBonus]
GO

/****** Object:  Table [dbo].[SalaryYearlyBonus]    Script Date: 20/04/2019 11:09:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SalaryYearlyBonus](
	[YearlyBonusId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[payHeadNameAutoId] [numeric](18, 0) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[NoOfBonus] [numeric](18, 0) NOT NULL,
	[TotalAmount] [numeric](18, 2) NOT NULL,
	[sEffectiveDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SalaryYearlyBonus] PRIMARY KEY CLUSTERED 
(
	[YearlyBonusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryYearlyBonusINSERT]    Script Date: 20/04/2019 10:23:43 AM ******/
DROP PROCEDURE [dbo].[ProcSalaryYearlyBonusINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryYearlyBonusINSERT]    Script Date: 20/04/2019 10:23:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO






-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcSalaryYearlyBonusINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@payHeadNameAutoId numeric (18, 0),
	@Amount numeric(18, 2) ,
	@NoOfBonus numeric (18, 0),
	@TotalAmount numeric(18, 2) ,
	@sEffectiveDate datetime ,	
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO SalaryYearlyBonus  
		VALUES((Select isnull(max(YearlyBonusId),0)+1 From SalaryYearlyBonus),
				@EmpAutoId,@payHeadNameAutoId ,@Amount ,@NoOfBonus,@TotalAmount,	@sEffectiveDate,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update SalaryYearlyBonus  
SET EmpAutoId=@EmpAutoId, payHeadNameAutoId=@payHeadNameAutoId,	Amount =@Amount , NoOfBonus=@NoOfBonus, TotalAmount=@TotalAmount, sEffectiveDate=@sEffectiveDate , Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where YearlyBonusId=@trAutoId		  

End



SELECT @Isduplicate


GO
-----------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 10:52:51 AM ******/
DROP PROCEDURE [dbo].[ProcSalaryYearlyBonusSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 10:52:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcSalaryYearlyBonusSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
phi.PayHeadName,
sa.payHeadNameAutoId,
sa.Amount,
sa.NoOfBonus,
sa.TotalAmount,
sa.sEffectiveDate as EffictiveDate,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.YearlyBonusId

FROM SalaryYearlyBonus sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
left outer join PayHeadInfo phi
on sa.payHeadNameAutoId=phi.PayHeadInfoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
	and sa.sEffectiveDate between @Fromdate and @Todate



GO
---------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 11:15:43 AM ******/
DROP PROCEDURE [dbo].[ProcSalaryAdditionSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 11:15:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcSalaryAdditionSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
phi.PayHeadName,
sa.payHeadNameAutoId,
sa.Amount,
sa.sEffectiveDate as EffictiveDate,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.SalaryAdditionId

FROM SalaryAddition sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
left outer join PayHeadInfo phi
on sa.payHeadNameAutoId=phi.PayHeadInfoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
	and sa.sEffectiveDate between @Fromdate and @Todate


-----------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionINSERT]    Script Date: 20/04/2019 11:16:41 AM ******/
DROP PROCEDURE [dbo].[ProcSalaryAdditionINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionINSERT]    Script Date: 20/04/2019 11:16:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO






-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcSalaryAdditionINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@payHeadNameAutoId numeric (18, 0),
	@Amount numeric(18, 2) ,
	@sEffectiveDate datetime ,	
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO SalaryAddition  
		VALUES((Select isnull(max(SalaryAdditionId),0)+1 From SalaryAddition),
				@EmpAutoId,@payHeadNameAutoId ,@Amount ,	@sEffectiveDate,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update SalaryAddition  
SET EmpAutoId=@EmpAutoId, payHeadNameAutoId=@payHeadNameAutoId,	Amount =@Amount , sEffectiveDate=@sEffectiveDate , Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where SalaryAdditionId=@trAutoId		  

End



SELECT @Isduplicate







GO
------------------

/****** Object:  Table [dbo].[SalaryYearlyBonus]    Script Date: 20/04/2019 11:09:49 AM ******/
DROP TABLE [dbo].[TaxAdjustment]
GO

/****** Object:  Table [dbo].[SalaryYearlyBonus]    Script Date: 20/04/2019 11:09:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaxAdjustment](
	[TaxAdjustmentId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[salaryMonth] [int] NOT NULL,
	[salaryYear] [int] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TaxAdjustment] PRIMARY KEY CLUSTERED 
(
	[TaxAdjustmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


---------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryYearlyBonusINSERT]    Script Date: 20/04/2019 10:23:43 AM ******/
DROP PROCEDURE [dbo].[ProcTaxAdjustmentINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryYearlyBonusINSERT]    Script Date: 20/04/2019 10:23:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO






-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcTaxAdjustmentINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@Amount numeric(18, 2) ,
	@salaryMonth int ,
	@salaryYear int ,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO TaxAdjustment  
		VALUES((Select isnull(max(TaxAdjustmentId),0)+1 From TaxAdjustment),
				@EmpAutoId,@Amount ,@salaryMonth,@salaryYear,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update TaxAdjustment  
SET EmpAutoId=@EmpAutoId,Amount =@Amount , salaryMonth=@salaryMonth, salaryYear=@salaryYear,  Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where TaxAdjustmentId=@trAutoId		  

End



SELECT @Isduplicate







GO


-------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 10:52:51 AM ******/
DROP PROCEDURE [dbo].[ProcTaxAdjustmentSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 10:52:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcTaxAdjustmentSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,

m.MonthName as MonthName,
sa.salaryMonth,
sa.Amount,
sa.salaryYear,

sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.TaxAdjustmentId

FROM TaxAdjustment sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
left outer join T_Month m
on sa.salaryMonth=m.autoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
--	and sa.sEffectiveDate between @Fromdate and @Todate


GO


------------------------------

/****** Object:  Table [dbo].[SalaryYearlyBonus]    Script Date: 20/04/2019 11:09:49 AM ******/
DROP TABLE [dbo].[TaxCredit]
GO

/****** Object:  Table [dbo].[SalaryYearlyBonus]    Script Date: 20/04/2019 11:09:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaxCredit](
	[TaxCreditId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[eEffectiveDate] [datetime]  NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TaxCredit] PRIMARY KEY CLUSTERED 
(
	[TaxCreditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryYearlyBonusINSERT]    Script Date: 20/04/2019 10:23:43 AM ******/
DROP PROCEDURE [dbo].[ProcTaxCreditINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryYearlyBonusINSERT]    Script Date: 20/04/2019 10:23:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO






-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcTaxCreditINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@Amount numeric(18, 2) ,
	@eEffectiveDate datetime ,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO TaxCredit  
		VALUES((Select isnull(max(TaxCreditId),0)+1 From TaxCredit),
				@EmpAutoId,@Amount ,@eEffectiveDate,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update TaxCredit  
SET EmpAutoId=@EmpAutoId,Amount =@Amount , eEffectiveDate=@eEffectiveDate,   Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where TaxCreditId=@trAutoId		  

End



SELECT @Isduplicate



GO


--------------------------------------------



/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 10:52:51 AM ******/
DROP PROCEDURE [dbo].[ProcTaxCreditSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 20/04/2019 10:52:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcTaxCreditSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
sa.Amount,
sa.eEffectiveDate as EffictiveDate,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.TaxCreditId


FROM TaxCredit sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
--	and sa.sEffectiveDate between @Fromdate and @Todate




GO
----------------------------------



/****** Object:  Table [dbo].[TaxAdjustment]    Script Date: 20/04/2019 1:56:28 PM ******/
DROP TABLE [dbo].[SalaryOnHold]
GO

/****** Object:  Table [dbo].[TaxAdjustment]    Script Date: 20/04/2019 1:56:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SalaryOnHold](
	[SalaryOnHoldId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [int] NOT NULL,
	[salaryMonth] [int] NOT NULL,
	[salaryYear] [int] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SalaryOnHold] PRIMARY KEY CLUSTERED 
(
	[SalaryOnHoldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxAdjustmentINSERT]    Script Date: 20/04/2019 1:59:11 PM ******/
DROP PROCEDURE [dbo].[ProcSalaryOnHoldINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxAdjustmentINSERT]    Script Date: 20/04/2019 1:59:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcSalaryOnHoldINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@salaryMonth int ,
	@salaryYear int ,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO SalaryOnHold  
		VALUES((Select isnull(max(SalaryOnHoldId),0)+1 From SalaryOnHold),
				@EmpAutoId ,@salaryMonth,@salaryYear,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update SalaryOnHold  
SET EmpAutoId=@EmpAutoId, salaryMonth=@salaryMonth, salaryYear=@salaryYear,  Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where SalaryOnHoldId=@trAutoId		  

End



SELECT @Isduplicate



GO

---------------------------------

/****** Object:  StoredProcedure [dbo].[ProcTaxAdjustmentSelect]    Script Date: 20/04/2019 2:05:01 PM ******/
DROP PROCEDURE [dbo].[ProcSalaryOnHoldSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxAdjustmentSelect]    Script Date: 20/04/2019 2:05:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcSalaryOnHoldSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,

m.MonthName as MonthName,
sa.salaryMonth,
sa.salaryYear,

sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.SalaryOnHoldId

FROM SalaryOnHold sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
left outer join T_Month m
on sa.salaryMonth=m.autoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
--	and sa.sEffectiveDate between @Fromdate and @Todate


     








GO












































