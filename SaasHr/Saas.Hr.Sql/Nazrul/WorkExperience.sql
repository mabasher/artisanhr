﻿

/****** Object:  Table [dbo].[T_WorkExperienceInfo]    Script Date: 25/05/2019 2:22:52 PM ******/
DROP TABLE [dbo].[WorkExperienceInfo]
GO

/****** Object:  Table [dbo].[T_WorkExperienceInfo]    Script Date: 25/05/2019 2:22:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WorkExperienceInfo](
	[WorkExperienceInfoId] [numeric](18, 0)  NOT NULL,
	[EmployeePersonalInfoId] [numeric](18, 0) NOT NULL,
	[companyName] [varchar](250) NOT NULL,
	[companyBusiness] [varchar](250) NOT NULL,
	[companyAddress] [varchar](350) NOT NULL,
	[designation] [varchar](150) NOT NULL,
	[department] [varchar](150) NOT NULL,
	[jobNature] [varchar](100) NOT NULL,
	[jobResponsibilities] [varchar](max) NOT NULL,
	[lastSalary] [decimal](18, 2) NOT NULL,
	[fromDate] [datetime] NOT NULL,
	[toDate] [datetime] NOT NULL,
	[continuing] [char](2) NOT NULL,
	[remark] [varchar](350) NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-------------------------------

/****** Object:  StoredProcedure [dbo].[ProcChildInformationINSERT]    Script Date: 25/05/2019 3:30:58 PM ******/
DROP PROCEDURE [dbo].[ProcWorkExperienceInfoINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcChildInformationINSERT]    Script Date: 25/05/2019 3:30:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



--  [ProcWorkExperienceInfoINSERT]'0','1','STM SOFTWARE LIMITED','Company Business','CompanyAddress','Designation','Department','Job Nature','Job Responsibilities','10000','25/05/2019','25/05/2019','10','Remarks','1','2','::1','0','','0','0','I'



CREATE   proc [dbo].[ProcWorkExperienceInfoINSERT]

	@trAutoId int,
	@EmployeePersonalInfoId numeric(18, 0),
	@companyName varchar(250) ,
	@companyBusiness varchar(250) ,
	@companyAddress varchar(350) ,
	@designation varchar(150) ,
	@department varchar(150) ,
	@jobNature varchar(100) ,
	@jobResponsibilities varchar(max) ,
	@lastSalary decimal(18, 2),
	@fromDate datetime,
	@toDate datetime,
	@continuing char(2),
	@remark varchar(350) ,		
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on
Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''
if (@trType='I')
Begin
--if exists (SELECT ChildName FROM	dbo.WorkExperienceInfo WHERE	ChildName=@ChildName and EmployeePersonalInfoId=@EmpAutoId) Begin set @Isduplicate = 'D' END
INSERT INTO WorkExperienceInfo  
		VALUES((Select isnull(max(WorkExperienceInfoId),0)+1 From WorkExperienceInfo),
				@EmployeePersonalInfoId,@companyName  ,	@companyBusiness ,	@companyAddress  ,
				@designation  ,	@department ,@jobNature  ,	@jobResponsibilities ,	@lastSalary ,	
				@fromDate ,@toDate ,	@continuing ,@remark,@CompanyAutoId,
				@CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ChildName FROM	dbo.WorkExperienceInfo WHERE	ChildName=@ChildName and EmployeePersonalInfoId=@EmpAutoId and EmployeeChildInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update WorkExperienceInfo  
SET companyName=@companyName  ,	companyBusiness=@companyBusiness ,	companyAddress=@companyAddress  ,
	designation=@designation  ,	department=@department ,jobNature=@jobNature  ,jobResponsibilities=	@jobResponsibilities ,lastSalary=@lastSalary ,	
	fromDate=@fromDate ,toDate=@toDate ,	continuing=@continuing ,remark=@remark,
	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where WorkExperienceInfoId=@trAutoId		  

End



SELECT @Isduplicate



GO










