﻿

/****** Object:  Table [dbo].[TaxAdjustment]    Script Date: 29/05/2019 3:39:21 PM ******/
DROP TABLE [dbo].[TaxReduction]
GO

/****** Object:  Table [dbo].[TaxAdjustment]    Script Date: 29/05/2019 3:39:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaxReduction](
	[TaxReductionId] [numeric](18, 0) NOT NULL,
	[GenderAutoId] [int] NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[eEffectiveDate] datetime,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TaxReduction] PRIMARY KEY CLUSTERED 
(
	TaxReductionId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------------------------



/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
DROP PROCEDURE [dbo].ProcTaxReductionINSERT
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO








-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].ProcTaxReductionINSERT

	@trAutoId numeric (18, 0),

	@GenderAutoId int,
	@Amount decimal(18, 2) ,
	@effectiveDate datetime ,
	@Remarks varchar(max) ,

	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO TaxReduction  
		VALUES((Select isnull(max(TaxReductionId),0)+1 From TaxReduction),
				 @GenderAutoId,@Amount , @effectiveDate, 
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update TaxReduction  
SET GenderAutoId=@GenderAutoId,
	Amount=@Amount,
	eEffectiveDate=@effectiveDate,
	Remarks=@Remarks,

	UpdateBy=@CreateBy ,
	UpdateTime=getdate() ,
	UpdateIp=@CreateIp 



Where TaxReductionId=@trAutoId		  

End



SELECT @Isduplicate









GO
----------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxableHeadSelect]    Script Date: 29/05/2019 4:35:03 PM ******/
DROP PROCEDURE [dbo].[ProcTaxReductionSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxableHeadSelect]    Script Date: 29/05/2019 4:35:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO





-- [ProcTaxableHeadSelect]'03/29/2019','05/29/2019'


CREATE   proc [dbo].[ProcTaxReductionSelect]
	

	--@Fromdate datetime,
	--@Todate datetime

	
--with encryption
as
set nocount on


SELECT 

g.GenderName as Gender,
sa.GenderAutoId,

sa.Amount as Amount,
sa.eEffectiveDate as EffictiveDate,
sa.Remarks,
sa.TaxReductionId

FROM TaxReduction sa 
left outer join GenderInfo g
on sa.GenderAutoId=g.GenderInfoId
 

--Where sa.effectiveDate between @Fromdate and @Todate



SET ANSI_NULLS ON


GO





