﻿

/****** Object:  Table [dbo].[T_EmployeeWithPF]    Script Date: 15/06/2019 4:02:41 PM ******/
DROP TABLE [dbo].[EmployeeWithPF]
GO

/****** Object:  Table [dbo].[T_EmployeeWithPF]    Script Date: 15/06/2019 4:02:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmployeeWithPF](
	[EmployeeWithPFId] [numeric](18, 0) NOT NULL,
	[EmpAutoId] [numeric](18, 0) NOT NULL,
	[eEffectiveDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
) ON [PRIMARY]

GO


------------------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxCreditINSERT]    Script Date: 15/06/2019 4:10:50 PM ******/
DROP PROCEDURE [dbo].[ProcEmployeeWithPFINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditINSERT]    Script Date: 15/06/2019 4:10:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcEmployeeWithPFINSERT]

	@trAutoId numeric (18, 0),
	@EmpAutoId numeric(18, 0),	
	@eEffectiveDate datetime ,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO EmployeeWithPF  
		VALUES((Select isnull(max(EmployeeWithPFId),0)+1 From EmployeeWithPF),
				@EmpAutoId,@eEffectiveDate,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )
End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update EmployeeWithPF  
SET EmpAutoId=@EmpAutoId,eEffectiveDate=@eEffectiveDate,   Remarks=@Remarks,
	UpdateBy=@CreateBy ,UpdateTime=getdate() ,UpdateIp=@CreateIp 
Where EmployeeWithPFId=@trAutoId		  

End



SELECT @Isduplicate




GO


----------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxCreditSelect]    Script Date: 15/06/2019 4:24:53 PM ******/
DROP PROCEDURE [dbo].[ProcEmployeeWithPFSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxCreditSelect]    Script Date: 15/06/2019 4:24:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcEmployeeWithPFSelect]
	

	@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 
ei.CardNo as CardNo,
ei.EmployeeName as EmployeeName,
sa.eEffectiveDate as EffictiveDate,
sa.Remarks,
ei.EmployeePersonalInfoId as empAutoId,
sa.EmployeeWithPFId


FROM EmployeeWithPF sa 
left outer join EmployeePersonalInfo ei 
on sa.EmpAutoId=ei.EmployeePersonalInfoId
 

Where cast(sa.EmpAutoId as varchar(20))+'X' like @EmpAutoId
--	and sa.sEffectiveDate between @Fromdate and @Todate





GO






