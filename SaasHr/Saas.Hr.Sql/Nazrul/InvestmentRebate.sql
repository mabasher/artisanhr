﻿

/****** Object:  Table [dbo].[T_RebateLimit]    Script Date: 31/05/2019 9:53:06 AM ******/
DROP TABLE [dbo].InvestmentRebate
GO

/****** Object:  Table [dbo].[T_RebateLimit]    Script Date: 31/05/2019 9:53:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InvestmentRebate](
	[InvestmentRebateId] [numeric](18, 0)  NOT NULL,
	[percentageOfTotal] [decimal](18, 2) NOT NULL,
	[percentageRebate] [decimal](18, 2) NOT NULL,
	[fixedAmount] [decimal](18, 2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[calculateCondition] [varchar](2) NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL

) ON [PRIMARY]

GO


------------------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
DROP PROCEDURE [dbo].ProcTaxInvestmentRebateINSERT
GO

/****** Object:  StoredProcedure [dbo].[ProcTaxLimitINSERT]    Script Date: 29/05/2019 4:14:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



-- [ProcTaxRebateLimitINSERT]'0','05/31/2019','15','155500','1','','1','2','::1','I'



CREATE   proc [dbo].ProcTaxInvestmentRebateINSERT

	@trAutoId numeric (18, 0),

	--InvestmentRebateId,
	@percentageOfTotal decimal(18, 2) ,
	@percentageRebate decimal(18, 2) ,
	@fixedAmount decimal(18, 2) ,
	@effectiveDate datetime,
	@calculateCondition varchar(2) ,

	@Remarks varchar(max) ,
	
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO InvestmentRebate  
		VALUES((Select isnull(max(InvestmentRebateId),0)+1 From InvestmentRebate),
				@percentageOfTotal ,@percentageRebate ,	@fixedAmount ,@effectiveDate ,@calculateCondition,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update InvestmentRebate  
SET
	percentageOfTotal=@percentageOfTotal ,
	percentageRebate=@percentageRebate ,	
	fixedAmount=@fixedAmount ,
	effectiveDate=@effectiveDate ,
	calculateCondition=@calculateCondition,
	Remarks=@Remarks,

	UpdateBy=@CreateBy ,
	UpdateTime=getdate() ,
	UpdateIp=@CreateIp 



Where InvestmentRebateId=@trAutoId		  

End



SELECT @Isduplicate









GO















