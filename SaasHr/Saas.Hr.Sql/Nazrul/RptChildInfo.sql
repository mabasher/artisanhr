﻿
/****** Object:  StoredProcedure [dbo].[ProcRptEmployeeInformation]    Script Date: 11/05/2019 3:27:32 PM ******/
DROP PROCEDURE [dbo].[ProcRptEmployeeChildInformation]
GO

/****** Object:  StoredProcedure [dbo].[ProcRptEmployeeInformation]    Script Date: 11/05/2019 3:27:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








--proc_RptCommonForHR '%','%','%','%','%','01/01/2000','01/01/2010'

CREATE    PROC [dbo].[ProcRptEmployeeChildInformation]

	@companyAutoId as varchar(50),
	@empAutoID varchar(20)

	--@CardIDAutoID varchar(20),
	--@DesignationId varchar(20),
	--@DepartmentId varchar(20),

	--@ReligionAutoID varchar(20),
	--@BloodGroupAutoID varchar(20),
	--@GenderAutoID varchar(20),
	--@NationalityAutoID varchar(20),
	--@MaritalStatusAutoID varchar(50),
	--@EmpTypeId  varchar(20),
	
	--@EmpGroupId  varchar(20),
	--@FloorId  varchar(20),
	--@LineId  varchar(20),
	--@BranchId  varchar(20),
	--@ProjectId  varchar(20),
	--@ShiftId  varchar(20),
	--@LocationId  varchar(20),

	--@JoinFromDate datetime,
	--@JoinToDate datetime,
	--@JoinDateUse varchar(1),

	--@releasedFromDate datetime,
	--@releasedToDate datetime,
	--@releasedDateUse varchar(1),

	--@AppointmentFromDate datetime,
	--@AppointmentToDate datetime,
	--@AppointmentDateUse varchar(1),

	--@ConfirmationFromDate datetime,
	--@ConfirmationToDate datetime,
	--@ConfirmationDateUse varchar(1)
	
	
as
Select 	
	isnull(emp.PIN,'') as PIN ,
	isnull(emp.CardNo,'') as CardNo,
	isnull(emp.EmployeeName,'') as EmployeeName,
	isnull(emp.EmployeeNeekName,'') as EmployeeNeekName ,
	isnull(emp.FatherName,'') as FatherName,
	isnull(emp.MotherName,'') as MotherName,
	isnull(emp.SpouseName,'') as SpouseName,
	isnull(emp.PersonalContact,'') as PersonalContact,
	isnull(emp.OfficialContact,'') as OfficialContact,
	isnull(emp.EmergencyContact,'') as EmergencyContact,
	isnull(emp.ContactPerson,'') as ContactPerson,
	isnull(emp.PersonalMail,'') as PersonalMail ,
	isnull(emp.OfficialMail,'') as OfficialMail ,
	isnull(emp.PresentAddress,'') as PresentAddress,
	isnull(emp.PermanentAddress,'') as PermanentAddress,
	isnull(emp.EmergencyAddress,'') as EmergencyAddress,
	isnull(emp.DOB,'') as DOB,
	isnull(emp.PlaceOfBirth,'') as PlaceOfBirth,
	isnull(emp.BirthCertificate,'') as BirthCertificate,
	isnull(emp.NID,'') as NID,
	isnull(emp.TIN,'') as TIN ,
	isnull(emp.PassportNo,'') as PassportNo,
	isnull(emp.DrivingLicense,'') as DrivingLicense,
	isnull(emp.ExtraCurricularActivities,'') as ExtraCurricularActivities,
	isnull(emp.Remark ,'') as Remark,

	isnull(deg.Designation  ,'') as Designation,
	isnull(deg.DesignationBangla  ,'') as DesignationBangla,

	--isnull(dep.Department  ,'') as Department,
	--isnull(dep.DepartmentBangla  ,'') as DepartmentBangla,

	--isnull(g.GenderName ,'') as GenderName,
	--isnull(g.GenderNameBangla ,'') as GenderNameBangla,

	--isnull(n.NationalityName ,'') as NationalityName,
 --   isnull(s.ShiftName ,'') as ShiftName,
	--isnull(s.inTime ,'') as inTime,
	--isnull(s.outTime ,'') as outTime,

	--isnull(sec.Section ,'') as Section,
	--isnull(sec.SectionBangla ,'') as SectionBangla,

	--isnull(b.BloodGroupName  ,'') as BloodGroupName,

	--isnull(emptype.TypeName, '' ) as EmpTypeName,

	--isnull(l.LocationName , '' ) as LocationName,

	--isnull(line.Line, '' ) as LineNum,
	--isnull(line.LineBangla, '' ) as LineBangla,

	--isnull(pro.ProjectName , '' ) as ProjectName,

	--isnull(re.ReligionName , '' ) as ReligionName,
	--isnull(re.ReligionNameBangla , '' ) as ReligionNameBangla,

	--isnull(ma.MaritalStatusName  , '' ) as MaritalStatusName,
	--isnull(empGrp.GroupName , '' ) as EmpGroupName,
	--isnull(Fl.FloorName, '' ) as FloorName,
	--isnull(Fl.FloorBangla, '' ) as FloorBangla,
	--isnull(brn.BranchName, '' ) as BranchName,
	--isnull(brn.BranchNameBangla , '' ) as BranchNameBangla,


	--oc.JoinDate,
	--oc.AppointmentDate,
	--oc.ConfirmationDate,
	--oc.ProbationPeriod,
	--oc.PFEnable,
	--oc.PFDate,

	isnull(nom.ChildName,'' ) as asNomineeName,
	isnull(nom.FatherName,'' ) as NomFatherName,
	isnull(nom.MotherName,'' ) as NomMotherName,
	nom.DOB as NomineeDOB,
	isnull(nom.PermanentAddress,'' ) as NomPermanentAddress,
	isnull(nom.PersonalContact,'' ) as NomPersonalContact,
	isnull(nom.PersonalMail,'' ) as NomPersonalMail,
	isnull(nom.NID,'' ) as NomineeNID,
	
	rel.Relation as strOption1,
	'' as strOption2,
	'' as strOption3,
	'' as strOption4,
	'' as strOption5,
	'' as strOption6,
	'' as strOption7,
	'' as strOption8,

	'01/01/2018' as dateOption1,
	'01/01/2018' as dateOption2,
	'01/01/2018' as dateOption3,
	'01/01/2018' as dateOption4,
	'01/01/2018' as dateOption5,

	'0' as numericOption1,
	'0' as numericOption2,
	'0' as numericOption3,
	'0' as numericOption4,
	'0' as numericOption5
		
From EmployeePersonalInfo emp

LEFT OUTER JOIN [dbo].[EmployeeOfficialInfo] oc
ON emp.EmployeePersonalInfoId= oc.EmpAutoId

LEFT OUTER JOIN [dbo].EmployeeChildInfo nom
ON emp.EmployeePersonalInfoId= nom.EmployeePersonalInfoId


LEFT OUTER JOIN [dbo].[DesignationInfo] deg
on deg.DesignationInfoId=oc.DesignationAutoId

LEFT OUTER JOIN [dbo].[T_Relation] rel
on nom.RelationId=rel.AutoId



--left outer join DepartmentInfo dep
--on dep.DepartmentInfoId=oc.DepartmentAutoId

--left outer join GenderInfo g
--on g.GenderInfoId=emp.GenderAutoId

--left outer join NationalityInfo n
--on n.NationalityInfoId=emp.NationalityAutoId

--left outer join ShiftInformation s
--on s.autoId=oc.ShiftAutoId

--left outer join SectionInfo sec
--on sec.SectionInfoId=oc.SectionAutoId

--left outer join BloodGroupInfo b
--on b.BloodGroupInfoId=emp.BloodGroupAutoId

--left outer join EmployeeType emptype
--on emptype.EmployeeTypeId=oc.EmployeeTypeAutoId

--left outer join LocationInfo l
--on l.LocationInfoId=oc.LocationAutoId

--left outer join LineInfo line
--on line.LineInfoId=oc.LocationAutoId

--left outer join ProjectInfo pro
--on pro.ProjectInfoId=oc.ProjectAutoId

--left outer join ReligionInfo re
--on re.ReligionInfoId=emp.ReligionAutoId

--left outer join MaritalStatusInfo ma
--on ma.MaritalStatusInfoId=emp.ReligionAutoId

--left outer join [dbo].[EmployeeGroup] empGrp
--on empgrp.EmployeeGroupId=oc.EmployeeGroupAutoId

--left outer join [dbo].[FloorInfo] Fl
--on fl.FloorInfoId=oc.FloorAutoId

--left outer join BranchInfo brn
--on brn.BranchInfoId=oc.BranchAutoId


where emp.companyAutoId = @companyAutoId
	and isnull(Cast(emp.EmployeePersonalInfoId as varchar(30)),'0') + 'X'like @empAutoID 
	--and isnull(emp.CardNo,'') + 'X'like @CardIDAutoID
	--and isnull(Cast(re.ReligionInfoId as varchar(30)),'0') + 'X'like @ReligionAutoID
	--and isnull(Cast(b.BloodGroupInfoId as varchar(30)),'0') + 'X'like @BloodGroupAutoID
	--and isnull(Cast(g.GenderInfoId as varchar(30)),'0') + 'X'like @GenderAutoID
	--and isnull(Cast(n.NationalityInfoId as varchar(30)),'0') + 'X'like @NationalityAutoID
	--and isnull(Cast(ma.MaritalStatusInfoId as varchar(30)),'0') + 'X'like @MaritalStatusAutoID
	
	--and isnull(Cast(dep.DepartmentInfoId as varchar(30)),'0') + 'X'like @DepartmentId 
	--and isnull(Cast(deg.DesignationInfoId as varchar(30)),'0') + 'X'like @DesignationId

	--and isnull(Cast(emptype.EmployeeTypeId as varchar(30)),'0') + 'X'like @EmpTypeId
	--and isnull(Cast(empgrp.EmployeeGroupId as varchar(30)),'0') + 'X'like @EmpGroupId
	--and isnull(Cast(line.LineInfoId as varchar(30)),'0') + 'X'like @LineId
	--and isnull(Cast(pro.ProjectInfoId as varchar(30)),'0') + 'X'like @ProjectId
	--and isnull(Cast(s.autoId as varchar(30)),'0') + 'X'like @ShiftId
	--and isnull(Cast(l.LocationInfoId as varchar(30)),'0') + 'X'like @LocationId
	
	--and isnull(Cast(fl.FloorInfoId as varchar(30)),'0') + 'X'like @FloorId
	--and isnull(Cast(brn.BranchInfoId as varchar(30)),'0') + 'X'like @BranchId

 --	and ( @JoinDateUse = 'N' OR (@JoinDateUse = 'Y' AND oc.JoinDate between @JoinFromDate and @JoinToDate))
 --	and ( @releasedDateUse = 'N' OR (@releasedDateUse = 'Y' AND oc.JoinDate between @releasedFromDate and @releasedToDate))
 --	and ( @AppointmentDateUse = 'N' OR (@AppointmentDateUse = 'Y' AND oc.AppointmentDate between @AppointmentFromDate and @AppointmentToDate))
 --	and ( @ConfirmationDateUse = 'N' OR (@ConfirmationDateUse = 'Y' AND oc.ConfirmationDate between @ConfirmationFromDate and @ConfirmationToDate))

 

		








GO


