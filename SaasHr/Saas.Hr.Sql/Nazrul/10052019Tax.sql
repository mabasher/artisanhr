﻿
/****** Object:  Table [dbo].[TaxCredit]    Script Date: 10/05/2019 12:47:04 PM ******/
DROP TABLE [dbo].[TaxableHead]
GO

/****** Object:  Table [dbo].[TaxCredit]    Script Date: 10/05/2019 12:47:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaxableHead](
	[TaxableHeadId] [numeric](18, 0) NOT NULL,
	[payHeadNameAutoId] [numeric](18, 0) NOT NULL,
	[fixedAmount] [decimal](18, 2) NOT NULL,
	[percentageValue] [decimal](18, 2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[insertDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TaxableHead] PRIMARY KEY CLUSTERED 
(
	[TaxableHeadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


------------------------------------


/****** Object:  Table [dbo].[TaxCredit]    Script Date: 10/05/2019 12:47:04 PM ******/
DROP TABLE [dbo].[TaxLimit]
GO

/****** Object:  Table [dbo].[TaxCredit]    Script Date: 10/05/2019 12:47:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaxLimit](
	[TaxLimitId] [numeric](18, 0) NOT NULL,
	[startLimit] [decimal](18, 2) NOT NULL,
	[limitRange] [decimal](18, 2) NOT NULL,
	[endLimit] [decimal](18, 2) NOT NULL,
	[taxAmount] [decimal](18, 2) NOT NULL,
	[fixedTaxAmount] [decimal](18, 2) NOT NULL,
	[calculateCondition] [varchar](2) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[insertDate] [datetime] NOT NULL,
	[Remarks] [varchar](max) NOT NULL,
	[CompanyId] [int] NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_TaxLimit] PRIMARY KEY CLUSTERED 
(
	[TaxLimitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO




---------------------------------


/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 10/05/2019 12:57:17 PM ******/
DROP PROCEDURE [dbo].[ProcTaxableHeadSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 10/05/2019 12:57:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcTaxableHeadSelect]
	

	--@EmpAutoId varchar(50),
	@Fromdate datetime,
	@Todate datetime

	
--with encryption
as
set nocount on


SELECT 

phi.PayHeadName,
sa.payHeadNameAutoId,
sa.fixedAmount as Amount,
sa.percentageValue as percentageValue,
sa.effectiveDate as EffictiveDate,
sa.Remarks,
sa.TaxableHeadId

FROM TaxableHead sa 
left outer join PayHeadInfo phi
on sa.payHeadNameAutoId=phi.PayHeadInfoId
 

Where sa.effectiveDate between @Fromdate and @Todate



SET ANSI_NULLS ON

GO

-----------------------------------



/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 10/05/2019 12:57:17 PM ******/
DROP PROCEDURE [dbo].[ProcTaxLimitSelect]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionSelect]    Script Date: 10/05/2019 12:57:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




-- [ProcDailyAttandenceINSERT]'345','03/29/2019','06:11:10 PM','06:11:10 PM'


CREATE   proc [dbo].[ProcTaxLimitSelect]
	

	--@EmpAutoId varchar(50),
	--@Fromdate datetime,
	--@Todate datetime

	
--with encryption
as
set nocount on


SELECT 

sa.startLimit,
sa.limitRange,
sa.endLimit as endLimit,
sa.taxAmount as taxAmount,
sa.fixedTaxAmount as fixedTaxAmount,
sa.calculateCondition as calculateCondition,

sa.effectiveDate as EffictiveDate,
sa.Remarks,
sa.TaxLimitId

FROM TaxLimit sa 
 

--Where sa.effectiveDate between @Fromdate and @Todate


-----------------------------------

SET ANSI_NULLS ON

GO


------------------------------------



/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionINSERT]    Script Date: 10/05/2019 2:00:49 PM ******/
DROP PROCEDURE [dbo].[ProcTaxLimitINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcSalaryAdditionINSERT]    Script Date: 10/05/2019 2:00:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







-- [ProcSalaryAdditionINSERT]'0','1','2','100','04/19/2019','','1','2','::1'




CREATE   proc [dbo].[ProcTaxLimitINSERT]

	@trAutoId numeric (18, 0),
	
	@startLimit decimal(18, 2) ,
	@limitRange decimal(18, 2) ,
	@endLimit decimal(18, 2) ,
	@taxAmount decimal(18, 2) ,
	@fixedTaxAmount decimal(18, 2) ,
	@calculateCondition varchar(2) ,
	
	@effectiveDate datetime,
	@Remarks	varchar(max)	,
	@CompanyAutoId int,
	@CreateBy int,
	@CreateIp nvarchar(50),
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO TaxLimit  
		VALUES((Select isnull(max(TaxLimitId),0)+1 From TaxLimit),
				@startLimit , @limitRange,@endLimit ,@taxAmount, @fixedTaxAmount, @calculateCondition,	@effectiveDate,getdate() ,
				@Remarks,@CompanyAutoId, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	EmpAcademicInformation WHERE ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update TaxLimit  
SET startLimit=@startLimit,
	limitRange=@limitRange,
	endLimit=@endLimit,
	taxAmount=@taxAmount,
	fixedTaxAmount=@fixedTaxAmount,
	calculateCondition=@calculateCondition,

	effectiveDate=@effectiveDate , 
	Remarks=@Remarks,
	UpdateBy=@CreateBy ,
	UpdateTime=getdate() ,
	UpdateIp=@CreateIp 



Where TaxLimitId=@trAutoId		  

End



SELECT @Isduplicate








GO
















