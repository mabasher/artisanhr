﻿SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[func_jobduration]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[func_jobduration]
GO





--SELECT dbo.func_jobduration('567',getDate())
--SELECT dbo.func_jobduration('567','2011-04-21')
--SELECT dbo.func_jobduration('567','2011-04-20')
--SELECT dbo.func_jobduration('567','2010-11-20')
--SELECT dbo.func_jobduration('567','2013-04-21')
--SELECT dbo.func_jobduration('567','2013-11-25')

--SELECT dbo.func_jobduration('2011-04-21','2011-04-21')
--SELECT dbo.func_jobduration('2013-02-20','2011-04-21')
--SELECT dbo.func_jobduration('2010-04-20','2011-04-21')
--SELECT dbo.func_jobduration('2005-05-22','2011-04-21')
--SELECT dbo.func_jobduration('2010-04-21','2011-04-21')




CREATE   FUNCTION func_jobduration (@joinDate as datetime,@currentdate as datetime)
RETURNS varchar(50)

--WITH ENCRYPTION
AS 
BEGIN
declare @Jobduration as varchar(50)
declare @Day as int
declare @Month as int
declare @Year as int
declare @TotalDay as int
declare @TotalRemainDay as int

SET @Jobduration = ''
set @Day =0
set @Month =0
set @Year =0
set @TotalDay =0
set @TotalRemainDay =0

--Select 	@TotalDay = datediff(day,o.joinDate,@currentdate)

--From T_OfficialInfo o
--Where o.empAutoId = @EmpAutoId --'565'
IF @joinDate <= @currentdate
BEGIN
	--SET @Year = datediff(Year,@joinDate,@currentdate)
	
	--IF dateadd(YEAR,@Year,@joinDate) > @currentdate
		--SET @Year = @Year-1
	--SET @joinDate = dateadd(YEAR,@Year,@joinDate)
	
	SET @Month = datediff(MONTH,@joinDate,@currentdate)
	IF dateadd(MONTH,@Month,@joinDate) > @currentdate + 1
		SET @Month = @Month-1	
	SET @joinDate = dateadd(MONTH,@Month,@joinDate)

	SET @Year = @Month / 12
	SET @Month = @Month % 12
	
	SET @Day = datediff(DAY,@joinDate,@currentdate) + 1
END

--SET @joinDate = dateadd(MONTH,@Month,@joinDate)

/*
if (@TotalDay > 365)
	BEGIN
		set @TotalRemainDay = @TotalDay % 365 
		set @Year = (@TotalDay - @TotalRemainDay)/365		
	END
else
	BEGIN
		set @TotalRemainDay = @TotalDay 	
		set @Year = 0 	
	END

if (@TotalRemainDay > 30)
	BEGIN
		set @Day = @TotalRemainDay % 30 
		set @Month = (@TotalRemainDay - @Day )/ 30 
	END
else
	BEGIN
		set @Day = @TotalRemainDay 
		set @Month = 0
	END
*/
	
--select @Jobduration = LTRIM(STR(@Year)) +' Year' +' ' + LTRIM(STR(@Month)) +' Month' +' ' + LTRIM(STR(@Day)) + ' Days'
IF @Year > 0
	BEGIN
		SET @Jobduration = @Jobduration + LTRIM(STR(@Year)) 
		IF @Year > 1
			SET @Jobduration = @Jobduration +' Years'
		ELSE
			SET @Jobduration = @Jobduration +' Year'
	END 
IF @Month > 0
	BEGIN
		IF LEN(@Jobduration) > 0 			
			SET @Jobduration = @Jobduration + ', '
		SET @Jobduration = @Jobduration + LTRIM(STR(@Month)) 
		IF @Month > 1
			SET @Jobduration = @Jobduration +' Months'
		ELSE
			SET @Jobduration = @Jobduration +' Month'
	END 
IF @Day > 0
	BEGIN
		IF LEN(@Jobduration) > 0 			
			SET @Jobduration = @Jobduration + ' and '
		SET @Jobduration = @Jobduration + LTRIM(STR(@Day)) 
		IF @Day > 1
			SET @Jobduration = @Jobduration +' Days'
		ELSE
			SET @Jobduration = @Jobduration +' Day'
	END 

	RETURN(@Jobduration)
END










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

--------------------------------------------------------


/****** Object:  StoredProcedure [dbo].[ProcRptEmployeeInformation]    Script Date: 20/06/2019 2:24:45 PM ******/
DROP PROCEDURE [dbo].[ProcRptDepartmentWiseManPowerList]
GO

/****** Object:  StoredProcedure [dbo].[ProcRptEmployeeInformation]    Script Date: 20/06/2019 2:24:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- ProcRptDepartmentWiseManPowerList '1','%','01/01/2000','01/01/2010','N','%'





CREATE    PROC [dbo].[ProcRptDepartmentWiseManPowerList]

	@companyAutoId as varchar(50),
	@empAutoID varchar(20),	

	@JoinFromDate datetime,
	@JoinToDate datetime,
	@JoinDateUse varchar(1),
	@Emptype varchar(20)
	
	
as
Select 	
	isnull(emp.PIN,'') as PIN ,
	isnull(emp.CardNo,'') as CardNo,
	isnull(emp.EmployeeName,'') as EmployeeName,
	isnull(emp.EmployeeNeekName,'') as EmployeeNeekName ,
	isnull(emp.FatherName,'') as FatherName,
	isnull(emp.MotherName,'') as MotherName,
	isnull(emp.SpouseName,'') as SpouseName,
	isnull(emp.PersonalContact,'') as PersonalContact,
	isnull(emp.OfficialContact,'') as OfficialContact,
	isnull(emp.EmergencyContact,'') as EmergencyContact,
	isnull(emp.ContactPerson,'') as ContactPerson,
	isnull(emp.PersonalMail,'') as PersonalMail ,
	isnull(emp.OfficialMail,'') as OfficialMail ,
	isnull(emp.PresentAddress,'') as PresentAddress,
	isnull(emp.PermanentAddress,'') as PermanentAddress,
	isnull(emp.EmergencyAddress,'') as EmergencyAddress,
	isnull(emp.DOB,'') as DOB,
	isnull(emp.PlaceOfBirth,'') as PlaceOfBirth,
	isnull(emp.BirthCertificate,'') as BirthCertificate,
	isnull(emp.NID,'') as NID,
	isnull(emp.TIN,'') as TIN ,
	isnull(emp.PassportNo,'') as PassportNo,
	isnull(emp.DrivingLicense,'') as DrivingLicense,
	isnull(emp.ExtraCurricularActivities,'') as ExtraCurricularActivities,
	isnull(emp.Remark ,'') as Remark,

	isnull(deg.Designation  ,'') as Designation,
	isnull(deg.DesignationBangla  ,'') as DesignationBangla,

	isnull(dep.Department  ,'') as Department,
	isnull(dep.DepartmentBangla  ,'') as DepartmentBangla,

	isnull(g.GenderName ,'') as GenderName,
	isnull(g.GenderNameBangla ,'') as GenderNameBangla,

	isnull(n.NationalityName ,'') as NationalityName,
    isnull(s.ShiftName ,'') as ShiftName,
	isnull(s.inTime ,'') as inTime,
	isnull(s.outTime ,'') as outTime,

	isnull(sec.Section ,'') as Section,
	isnull(sec.SectionBangla ,'') as SectionBangla,

	isnull(b.BloodGroupName  ,'') as BloodGroupName,

	isnull(emptype.TypeName, '' ) as EmpTypeName,

	isnull(l.LocationName , '' ) as LocationName,

	isnull(line.Line, '' ) as LineNum,
	isnull(line.LineBangla, '' ) as LineBangla,

	isnull(pro.ProjectName , '' ) as ProjectName,

	isnull(re.ReligionName , '' ) as ReligionName,
	isnull(re.ReligionNameBangla , '' ) as ReligionNameBangla,

	isnull(ma.MaritalStatusName  , '' ) as MaritalStatusName,
	isnull(empGrp.GroupName , '' ) as EmpGroupName,
	isnull(Fl.FloorName, '' ) as FloorName,
	isnull(Fl.FloorBangla, '' ) as FloorBangla,
	isnull(brn.BranchName, '' ) as BranchName,
	isnull(brn.BranchNameBangla , '' ) as BranchNameBangla,


	oc.JoinDate,
	oc.AppointmentDate,
	oc.ConfirmationDate,
	oc.ProbationPeriod,
	oc.PFEnable,
	oc.PFDate,

	(select dbo.func_jobduration(oc.JoinDate,getdate())) as strOption1,
	'' as strOption2,
	'' as strOption3,
	'' as strOption4,
	'' as strOption5,
	'' as strOption6,
	'' as strOption7,
	'' as strOption8,

	'01/01/2018' as dateOption1,
	'01/01/2018' as dateOption2,
	'01/01/2018' as dateOption3,
	'01/01/2018' as dateOption4,
	'01/01/2018' as dateOption5,

	'0' as numericOption1,
	'0' as numericOption2,
	'0' as numericOption3,
	'0' as numericOption4,
	'0' as numericOption5
		
From EmployeePersonalInfo emp

LEFT OUTER JOIN [dbo].[EmployeeOfficialInfo] oc
ON emp.EmployeePersonalInfoId= oc.EmpAutoId

LEFT OUTER JOIN [dbo].[DesignationInfo] deg
on deg.DesignationInfoId=oc.DesignationAutoId

left outer join DepartmentInfo dep
on dep.DepartmentInfoId=oc.DepartmentAutoId

left outer join GenderInfo g
on g.GenderInfoId=emp.GenderAutoId

left outer join NationalityInfo n
on n.NationalityInfoId=emp.NationalityAutoId

left outer join ShiftInformation s
on s.autoId=oc.ShiftAutoId

left outer join SectionInfo sec
on sec.SectionInfoId=oc.SectionAutoId

left outer join BloodGroupInfo b
on b.BloodGroupInfoId=emp.BloodGroupAutoId

left outer join EmployeeType emptype
on emptype.EmployeeTypeId=oc.EmployeeTypeAutoId

left outer join LocationInfo l
on l.LocationInfoId=oc.LocationAutoId

left outer join LineInfo line
on line.LineInfoId=oc.LocationAutoId

left outer join ProjectInfo pro
on pro.ProjectInfoId=oc.ProjectAutoId

left outer join ReligionInfo re
on re.ReligionInfoId=emp.ReligionAutoId

left outer join MaritalStatusInfo ma
on ma.MaritalStatusInfoId=emp.ReligionAutoId

left outer join [dbo].[EmployeeGroup] empGrp
on empgrp.EmployeeGroupId=oc.EmployeeGroupAutoId

left outer join [dbo].[FloorInfo] Fl
on fl.FloorInfoId=oc.FloorAutoId

left outer join BranchInfo brn
on brn.BranchInfoId=oc.BranchAutoId


where emp.companyAutoId = @companyAutoId
	and isnull(Cast(emp.EmployeePersonalInfoId as varchar(30)),'0') + 'X'like @empAutoID 
	and isnull(Cast(oc.EmployeeTypeAutoId as varchar(30)),'0') + 'X'like @Emptype 

 	and ( @JoinDateUse = 'N' OR (@JoinDateUse = 'Y' AND oc.JoinDate between @JoinFromDate and @JoinToDate))
 	 



GO

----------------------------------------------------




