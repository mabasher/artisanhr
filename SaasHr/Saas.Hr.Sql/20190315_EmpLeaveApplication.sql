﻿ALTER TABLE [dbo].[EmpLeaveApplicationDay] DROP CONSTRAINT [FK_EmpLeaveApplicationDay_EmpLeaveApplication]
GO
ALTER TABLE [dbo].[EmpLeaveApplication] DROP CONSTRAINT [FK_EmpLeaveApplication_LeaveType]
GO
/****** Object:  Table [dbo].[EmpLeaveApplicationDay]    Script Date: 15/03/2019 4:55:16 PM ******/
DROP TABLE [dbo].[EmpLeaveApplicationDay]
GO
/****** Object:  Table [dbo].[EmpLeaveApplication]    Script Date: 15/03/2019 4:55:16 PM ******/
DROP TABLE [dbo].[EmpLeaveApplication]
GO
/****** Object:  Table [dbo].[LeaveSession]   Script Date: 15/03/2019 4:55:16 PM ******/
DROP TABLE [dbo].[LeaveSession]
GO
/****** Object:  Table [dbo].[EmpLeaveApplication]    Script Date: 15/03/2019 4:55:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpLeaveApplication](
	[EmpLeaveApplicationId] [bigint] IDENTITY(1,1) NOT NULL,
	[LeaveTypeId] [int] NOT NULL,
	[ReasonOfLeave] [nvarchar](500) NOT NULL,
	[SupervisorId] [int] NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[IsPartialLeave] [BIT] NULL,
	[FromTime] [TIME](7) NOT NULL,
	[ToTime] [TIME](7) NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_EmpLeaveApplication] PRIMARY KEY CLUSTERED 
(
	[EmpLeaveApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmpLeaveApplicationDay]    Script Date: 15/03/2019 4:55:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpLeaveApplicationDay](
	[EmpLeaveApplicationDayId] [bigint] IDENTITY(1,1) NOT NULL,
	[EmpLeaveApplicationId] [bigint] NOT NULL,
	[LeaveDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EmpLeaveApplicationDay] PRIMARY KEY CLUSTERED 
(
	[EmpLeaveApplicationDayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmpLeaveApplication]  WITH CHECK ADD  CONSTRAINT [FK_EmpLeaveApplication_LeaveType] FOREIGN KEY([LeaveTypeId])
REFERENCES [dbo].[LeaveType] ([LeaveTypeId])
GO
ALTER TABLE [dbo].[EmpLeaveApplication] CHECK CONSTRAINT [FK_EmpLeaveApplication_LeaveType]
GO
ALTER TABLE [dbo].[EmpLeaveApplicationDay]  WITH CHECK ADD  CONSTRAINT [FK_EmpLeaveApplicationDay_EmpLeaveApplication] FOREIGN KEY([EmpLeaveApplicationId])
REFERENCES [dbo].[EmpLeaveApplication] ([EmpLeaveApplicationId])
GO
ALTER TABLE [dbo].[EmpLeaveApplicationDay] CHECK CONSTRAINT [FK_EmpLeaveApplicationDay_EmpLeaveApplication]
GO
