﻿



/****** Object:  Table [dbo].[CompanyWeekEnd]    Script Date: 08/03/2019 11:24:49 AM ******/
DROP TABLE [dbo].[CompanyWeekEnd]
GO

/****** Object:  Table [dbo].[CompanyWeekEnd]    Script Date: 08/03/2019 11:24:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CompanyWeekEnd](
	[CompanyWeekEndId] [numeric](18, 0) NOT NULL,
	[weekend] [varchar](25) NOT NULL,
	[effectiveDate] [datetime] NOT NULL,
	[CompanyAutoId] [int] NOT NULL,
	[Remarks] [nvarchar](150) NOT NULL,
	[CreateBy] [int] NULL,
	[CreateTime] [datetime] NULL,
	[CreateIp] [nvarchar](50) NULL,
	[UpdateBy] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateIp] [nvarchar](50) NULL,
	[IsInactive] [bit] NULL,
	[IsDeleted] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

---------------------------------------

/****** Object:  StoredProcedure [dbo].[ProcAcademicInfoINSERT]    Script Date: 08/03/2019 10:57:51 AM ******/
DROP PROCEDURE [dbo].[ProcCompanyWeekEndINSERT]
GO

/****** Object:  StoredProcedure [dbo].[ProcAcademicInfoINSERT]    Script Date: 08/03/2019 10:57:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




--[ProcNomineeInformationINSERT]'0','1','Sayem Ahmed','','Basher','naa','12/02/2019','ax','01730358095','','324234','3','100','1','3','::1','0','','0','0','I'

CREATE   proc [dbo].[ProcCompanyWeekEndINSERT]

	@trAutoId int,
	@weekend	varchar(50)	,
	@effectiveDate datetime,
	@CompanyAutoId int,
	@Remarks	varchar(250)	,
	
	@CreateBy int,
	@CreateIp nvarchar(50) ,
	@UpdateBy int ,
	@UpdateIp nvarchar(50),
	@IsInactive bit,
	@IsDeleted bit,
	@trType nvarchar(50)

--with encryption
as
set nocount on

Declare @Isduplicate nvarchar(1)
set @Isduplicate = ''

if (@trType='I')
Begin
--if exists (SELECT CompanyWeekEndId FROM	dbo.CompanyWeekEnd WHERE	CompanyWeekEndId=@ExamNameId ) Begin set @Isduplicate = 'D' END
INSERT INTO CompanyWeekEnd  
		VALUES((Select isnull(max(CompanyWeekEndId),0)+1 From CompanyWeekEnd),
				@weekend,@effectiveDate,@CompanyAutoId,@Remarks, @CreateBy ,
				getdate() ,@CreateIp ,null,null,null,'0',--@IsInactive,
				'0'--@IsDeleted
		  )

End

if (@trType='U')
Begin
--if exists (SELECT ExamNameId FROM	dbo.EmpAcademicInformation WHERE	ExamNameId=@ExamNameId and EmpAutoId=@EmpAutoId and EmpAcademicInfoId<>@trAutoId) Begin set @Isduplicate = 'D' END
Update CompanyWeekEnd  
SET weekend=weekend,effectiveDate=effectiveDate,Remarks=@Remarks,

	UpdateBy=@UpdateBy ,UpdateTime=getdate() ,UpdateIp=@UpdateIp 
Where CompanyWeekEndId=@trAutoId		  

End



SELECT @Isduplicate




GO


